
TYPE
	gtLinearRobotInput : 	STRUCT  (*Instance of Linear Robot Input object *)
		bWaferSize200mm : BOOL; (*TRUE if 200mm wafers are being used*)
		bWaferSize300mm : BOOL; (*TRUE if 300mm wafers are being used*)
		bBatchCount1 : BOOL; (*TRUE if 1 batch is being loaded into/unloaded out of the GC*)
		bBatchCount2 : BOOL; (*TRUE if bWaferSize 200mm is TRUE and 2 batches are being loaded into/unloaded out of the GC*)
		siBatch1FPort : SINT(0..8) ;
		siBatch2FPort : SINT(0..8) ;
		bGC1 : BOOL; (*If TRUE, GC 1 will be loaded into/unloaded*)
		bGC2 : BOOL; (*If TRUE, GC 2 will be loaded into/unloaded*)
		bLoadGC : BOOL; (*If TRUE, specified GC will be loaded with wafers from specified FPs*)
		bUnloadGC : BOOL; (*If TRUE, specified GC will be unloaded, with wafers returned to specified FPs*)
		bBatch1GCTopLevel : BOOL; (*If TRUE,  the first batch will be loaded into/unloaded out of the top level of the specified GC*)
		bBatch1GCBottomLevel : BOOL; (*If TRUE, the first batch will be loaded into/unloaded out of the bottom level of the specified GC*)
		bBatch2GCTopLevel : BOOL; (*If TRUE, the second batch will be loaded into/unloaded out of the top level of the specified GC*)
		bBatch2GCBottomLevel : BOOL; (*If TRUE, the second batch will be loaded into/unloaded out of the bottom level of the specified GC*)
		bEmergencyShutoff : BOOL; (*Emergency Shutoff option*)
		bRUN : BOOL; (*Command to begin process*)
		siInputCount : USINT; (*Variable used to verify that there is a proper amount of variables*)
		bStaubliSafePos : BOOL; (*If TRUE, Staubli Robot is in Safe Position for Linear Robot Movement*)
		bStaubliReady : BOOL; (*If TRUE, Staubli Robot is Ready to Run*)
		obStaubliSafePosRE : BOOL; (*Rising Edge Det, Staubli Robot is in Safe Position for Linear Robot Movement*)
		siFOUPPortNumberB1 : USINT; (*FOUP Port number to load/Unload for Batch 1*)
		siFOUPPortNumberB2 : USINT; (*FOUP Port number to load/Unload for Batch 2*)
		bUnload300Batch1 : BOOL; (*Run Config - Unload 300mm Wafer*)
		bLoad300Batch1 : BOOL; (*Run Config - Load 300mm Wafer*)
		bUnload200Batch2TB : BOOL; (*Run Config - Unload 200mm Wafer, Batch 2, Top then Bottom*)
		bUnload200Batch2BT : BOOL; (*Run Config - Unload 200mm Wafer, Batch 2, Bottom then Top*)
		bLoad200Batch2TB : BOOL; (*Run Config - Load 200mm Wafer, Batch 2, Top then Bottom*)
		bLoad200Batch2BT : BOOL; (*Run Config - Load 200mm Wafer, Batch 2, Bottom then Top*)
		bUnload200Batch1 : BOOL; (*Run Config - Unload 200mm Wafer, Batch 1*)
		bUnloadWaferType : BOOL; (*Unloading Wafer from Tool *)
		bLoadWaferType : BOOL; (*Loading Wafer into Tool*)
		bLoad200Batch1 : BOOL; (*Run Config - Load 200mm Wafer, Batch 1*)
		siSelectJogAxis : USINT; (*Selected Axis to Jog - 0=Off, 10=X1, 20=Y1, 30=Y2, 40=Z1, 50=Z2, 60=R, 70=HomeAllAxis*)
		siSetAxisX1JogType : USINT; (*X1 Axis Jog Types -  0 = Off, 1 = Coarse, 2 = Fine*)
		siSelectAxisX1Loc : USINT; (*Axis Location States - 1=FOUP1, 2=FOUP2, 3=FOUP3, 4=FOUP4, 10=Carrier1*)
		siSelectAxisTargetLevel : USINT; (*Z Axis Target Level to set - 1=Upper(Top), 2=Lower(Bottom)*)
		bJogNegAxisX1PB : BOOL; (*Jog Negitive direction pushbuttom*)
		bJogNegAxisX1PBRE : BOOL;
		bJogPosAxisX1PB : BOOL; (*Jog Positive direction pushbuttom*)
		bJogPosAxisX1PBRE : BOOL;
		bHomeAllAxisPB : BOOL;
		bHomeAllAxisPBRE : BOOL;
		bRotateAxisRPB : BOOL;
		bRotateAxisRPBRE : BOOL;
		uiRotateAxisPos : UINT;
		bSetTargetPosPB : BOOL;
		bSetTargetPosPBRE : BOOL;
		bAutoCycleMode : BOOL;
		bStartAutoCyclePB : BOOL;
		bStartAutoCyclePBRE : BOOL;
		bAxisErrorResetPB : BOOL;
		bAxisErrorResetPBRE : BOOL;
		uiAutoCycleCnts : UINT;
		bReadStaubliRunCMDAck : BOOL; (*Staubli Robot Run CMD Once Ack Flag*)
		bReadStaubliTargetPosAck : BOOL; (*Staubli Robot Target Move Block Send Once Ack Flag*)
		bReadStaubliRobotOnceAck : BOOL; (*Staubli Robot Start Move Block Send Once Ack Flag*)
		bWriteStaubliHeartBeatAck : BOOL; (*Write Heartbeat to Staubli Ack Flag*)
		bReadStaubliHeartbeatReturn : BOOL; (*Read Staubli Heartbeat returned from Staubli Controller*)
		bStaubliArmPowerON : BOOL; (*Read Staubli Arm Power is Turned ON*)
		bStaubliPrgRunning : BOOL; (*Read Staubli Program is Running Task*)
		uiStaubliErrorNumber : UINT; (*Read Staubli Error Number from Staubli Controller*)
		bStaubliErrorResetCMDAck : BOOL; (*Staubli Error Reset CMD Ack*)
		bStaubliArmPowerOnCMDAck : BOOL; (*Command to Turn Arm Power ON  Ack "TBD"*)
		bStaubliArmPowerOffCMDAck : BOOL; (*Command to Turn Arm Power OFF  Ack "TBD"*)
	END_STRUCT;
	gtLinearRobotOutpt : 	STRUCT  (*Instance of the Linear Robot Output object*)
		bComplete : BOOL; (*Completion flag set true when process is complete*)
		bGuardianCarrierPresent : BOOL; (*Flag to indicate the presence of the guardian carrier*)
		gstrError : STRING[80]; (*Code for any errors that have occurred during process*)
		usiSRMoveStartPosition : UINT; (*Staubli Robot Move Profile Selection to Start Position*)
		usiSRMoveTargetPosition : UINT; (*Staubli Robot Move Profile Selection to Target Position*)
		uiWriteStaubliRobot : UINT; (*Write Integer to Staubli for Defined Start Move Position to Run *)
		bWriteStaubliRobotOnce : BOOL; (*Staubli Robot Start Move Block Send Once Command*)
		uiWriteStaubliTargetPosition : UINT; (*Write Integer to Staubli for Defined Target Move Position to Run *)
		bWriteStaubliTargetOnce : BOOL; (*Staubli Robot Target Move Block Send Once Command*)
		bStaubliRunCMD : BOOL; (*Write Staubli Run Command*)
		bStaubliRunCMDOnce : BOOL; (*Staubli Robot Run CMD Once Command*)
		bWriteStaubliHeartBeat : BOOL; (*Write Heartbeat to Staubli Controller*)
		bWriteStaubliHeartBeatOnce : BOOL; (*Write Heartbeat to Staubli Once Command*)
		bStaubliErrorResetCMD : BOOL; (*Write Error Reset Command to Staubli*)
		bStaubliErrorResetCMDOnce : BOOL; (*Write Error Reset CMD Once Command*)
		bStaubliArmPowerOnCMD : BOOL; (*Write Command to Turn Arm Power ON "TBD"*)
		bStaubliArmPowerOnCMDOnce : BOOL; (*Write Command to Turn Arm Power ON Once CMD "TBD"*)
		bStaubliArmPowerOffCMD : BOOL; (*Write Command to Turn Arm Power OFF "TBD"*)
		bStaubliArmPowerOffCMDOnce : BOOL; (*Write Command to Turn Arm Power OFF Once CMD "TBD"*)
	END_STRUCT;
END_TYPE
