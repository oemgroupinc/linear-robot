PROGRAM _EXIT

	// X1
	
	AxisX1.Enable := FALSE;
	AxisX1.Power := FALSE;
	AxisX1.Home := FALSE;
	AxisX1.Update := FALSE;
	AxisX1.MoveAbsolute := FALSE;
	AxisX1.MoveAdditive := FALSE;
	AxisX1.MoveVelocity := FALSE;
	AxisX1.Stop := FALSE;
	AxisX1.ErrorReset := FALSE;
	AxisX1();
	
	// Z1
	
	AxisZ1.Enable := FALSE;
	AxisZ1.Power := FALSE;
	AxisZ1.Home := FALSE;
	AxisZ1.Update := FALSE;
	AxisZ1.MoveAbsolute := FALSE;
	AxisZ1.MoveAdditive := FALSE;
	AxisZ1.MoveVelocity := FALSE;
	AxisZ1.Stop := FALSE;
	AxisZ1.ErrorReset := FALSE;
	AxisZ1();
	
	// Z2
	
	AxisZ2.Enable := FALSE;
	AxisZ2.Power := FALSE;
	AxisZ2.Home := FALSE;
	AxisZ2.Update := FALSE;
	AxisZ2.MoveAbsolute := FALSE;
	AxisZ2.MoveAdditive := FALSE;
	AxisZ2.MoveVelocity := FALSE;
	AxisZ2.Stop := FALSE;
	AxisZ2.ErrorReset := FALSE;
	AxisZ2();
	
	// Y1
	
	AxisY1.Enable := FALSE;
	AxisY1.Power := FALSE;
	AxisY1.Home := FALSE;
	AxisY1.Update := FALSE;
	AxisY1.MoveAbsolute := FALSE;
	AxisY1.MoveAdditive := FALSE;
	AxisY1.MoveVelocity := FALSE;
	AxisY1.Stop := FALSE;
	AxisY1.ErrorReset := FALSE;
	AxisY1();
	
	// Y2
	
	AxisY2.Enable := FALSE;
	AxisY2.Power := FALSE;
	AxisY2.Home := FALSE;
	AxisY2.Update := FALSE;
	AxisY2.MoveAbsolute := FALSE;
	AxisY2.MoveAdditive := FALSE;
	AxisY2.MoveVelocity := FALSE;
	AxisY2.Stop := FALSE;
	AxisY2.ErrorReset := FALSE;
	AxisY2();
	
	// Rotary Stage
	
	RotaryOff;
	
END_PROGRAM