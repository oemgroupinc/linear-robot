
ACTION GenerateLocalVar_act :
	//-------------------- Translating input parameters to local variables -----------------------//
            
	// Target FOUP Ports
	CASE gLRInput.siBatch1FPort OF
		1 : tTargetFP1 := tFOUPPort1;
		2 : tTargetFP1 := tFOUPPort2;
		3 : tTargetFP1 := tFOUPPort3;
		4 : tTargetFP1 := tFOUPPort4;
		5 : tTargetFP1 := tFOUPPort5;
		6 : tTargetFP1 := tFOUPPort6;
		7 : tTargetFP1 := tFOUPPort7;
		8 : tTargetFP1 := tFOUPPort8;
	END_CASE;
	
	IF (gLRInput.bBatchCount2 = TRUE) THEN
		CASE gLRInput.siBatch2FPort OF
			1 : tTargetFP2 := tFOUPPort1;
			2 : tTargetFP2 := tFOUPPort2;
			3 : tTargetFP2 := tFOUPPort3;
			4 : tTargetFP2 := tFOUPPort4;
			5 : tTargetFP2 := tFOUPPort5;
			6 : tTargetFP2 := tFOUPPort6;
			7 : tTargetFP2 := tFOUPPort7;
			8 : tTargetFP2 := tFOUPPort8;
		END_CASE;
	END_IF;
	    
	// Guardian carrier
	IF (gLRInput.bGC1 = TRUE) THEN
		tTargetGC := tGuardianCarrier1;
	ELSIF (gLRInput.bGC2 = TRUE) THEN  
		tTargetGC := tGuardianCarrier2;
	END_IF;


	// Guardian Carrier levels
	IF (gLRInput.bWaferSize300mm = TRUE) THEN 
		tGCLevelBatch1.bNA := TRUE;
		tGCLevelBatch1.bTop := FALSE;
		tGCLevelBatch1.bBottom := TRUE;

		tGCLevelBatch2.bNA := FALSE;
		tGCLevelBatch2.bTop := FALSE;
		tGCLevelBatch2.bBottom := TRUE;                
	ELSIF (gLRInput.bWaferSize200mm = TRUE) THEN
		IF (gLRInput.bBatch1GCBottomLevel = TRUE) THEN // batch 1
			tGCLevelBatch1.bNA := FALSE;
			tGCLevelBatch1.bTop := FALSE;
			tGCLevelBatch1.bBottom := TRUE;
		ELSIF (gLRInput.bBatch1GCTopLevel) THEN
			tGCLevelBatch1.bNA := FALSE;
			tGCLevelBatch1.bTop := TRUE;
			tGCLevelBatch1.bBottom := FALSE;
		END_IF;

		IF (gLRInput.bBatchCount2 = TRUE) THEN
			IF (gLRInput.bBatch2GCTopLevel = TRUE) THEN // batch 2
				tGCLevelBatch2.bNA := FALSE;
				tGCLevelBatch2.bTop := TRUE;
				tGCLevelBatch2.bBottom := FALSE;
			ELSIF (gLRInput.bBatch2GCBottomLevel = TRUE) THEN
				tGCLevelBatch2.bNA := FALSE;
				tGCLevelBatch2.bTop := FALSE;
				tGCLevelBatch2.bBottom := TRUE;
			END_IF;
		END_IF;
	END_IF;
END_ACTION
