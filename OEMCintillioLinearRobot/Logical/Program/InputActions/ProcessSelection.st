
ACTION ProcessSelection_act:
	IF (gLRInput.bWaferSize200mm = TRUE) THEN
		// check for valid input
		IF (gLRInput.bWaferSize300mm = TRUE) THEN // If both are attempted to be TRUE
			gLRInput.bWaferSize200mm := FALSE; // reset both parts of the conflict
			gLRInput.bWaferSize300mm := FALSE;
			gLROutput.gstrError:= 'Invalid input. Only one Wafer Size may be selected at a time.'; // show error string
			gLRInput.bRUN := FALSE; // turn RUN off until error is fixed
			RETURN;
		ELSIF (gLRInput.bBatchCount1 = TRUE) THEN	
			// check for valid input
			IF (gLRInput.bBatchCount2 = TRUE) THEN
				gLRInput.bBatchCount1 := FALSE;
				gLRInput.bBatchCount2 := FALSE;
				gLROutput.gstrError:= 'Invalid input. Only one Batch Count may be selected at a time.'; 
				gLRInput.bRUN := FALSE; 
				RETURN;
			ELSIF (gLRInput.bLoadGC = TRUE) THEN
				// check for valid input
				IF (gLRInput.bUnloadGC = TRUE) THEN 
					gLRInput.bLoadGC := FALSE;
					gLRInput.bUnloadGC := FALSE;
					gLROutput.gstrError:= 'Invalid input. Only one Linear Robot Operation may be selected at a time.'; 
					gLRInput.bRUN := FALSE; 
					RETURN;
				ELSE // Only execute if there is no error
					
					// SET PROCESS TO LOAD
					tLR.Process.bLOAD := TRUE; // select process
					uiCurrentSuperState := eRunning; // set Running to TRUE so that selection chain is EXITed and bypassed in future cycles
					gLROutput.gstrError:= ''; // clear error code when a process has been selected
				END_IF;

			ELSIF (gLRInput.bUnloadGC = TRUE) THEN
				// check for valid input
				IF (gLRInput.bLoadGC = TRUE) THEN 
					gLRInput.bUnloadGC := FALSE;
					gLRInput.bLoadGC := FALSE;
					gLRInput.bRUN := FALSE; 
					gLROutput.gstrError:= 'Invalid input. Only one Linear Robot Operation may be selected at a time.'; 
					RETURN;
				ELSE
					// SET PROCESS TO UNLOAD
					tLR.Process.bUNLOAD := TRUE;
					uiCurrentSuperState := eRunning;
					gLROutput.gstrError:= ''; // clear error code
				END_IF;
			END_IF;

		ELSIF (gLRInput.bBatchCount2 = TRUE) THEN
			// check for valid input
			IF (gLRInput.bBatchCount1 = TRUE) THEN
				gLRInput.bBatchCount1 := FALSE;
				gLRInput.bBatchCount2 := FALSE;
				gLRInput.bRUN := FALSE; 
				gLROutput.gstrError:= 'Invalid input. Only one Batch Count may be selected at a time.'; 
				RETURN;
			ELSIF (gLRInput.bLoadGC = TRUE) THEN
				// check for valid input
				IF (gLRInput.bUnloadGC = TRUE) THEN
					gLRInput.bUnloadGC := FALSE;
					gLRInput.bLoadGC := FALSE;
					gLRInput.bRUN := FALSE; 
					gLROutput.gstrError:= 'Invalid input. Only one Linear Robot Operation may be selected at a time.'; 
					RETURN;
				ELSE
					// SET PROCESS TO DUAL LOAD
					tLR.Process.bDUALLOAD := TRUE;
					uiCurrentSuperState := eRunning;
					gLROutput.gstrError:= ''; // clear error code
				END_IF;

			ELSIF (gLRInput.bUnloadGC = TRUE) THEN
				// check for valid input
				IF (gLRInput.bLoadGC = TRUE) THEN 
					gLRInput.bUnloadGC := FALSE;
					gLRInput.bLoadGC := FALSE;
					gLRInput.bRUN := FALSE; 
					gLROutput.gstrError:= 'Invalid input. Only one Linear Robot Operation may be selected at a time.'; 
					RETURN;
				ELSE 
					// SET PROCESS TO DUAL UNLOAD
					tLR.Process.bDUALUNLOAD := TRUE;
					uiCurrentSuperState := eRunning;
					gLROutput.gstrError:= ''; // clear error code
				END_IF;

			END_IF;
		END_IF;
               
                   
	ELSIF (gLRInput.bWaferSize300mm = TRUE) THEN
		// check for valid input
		IF (gLRInput.bWaferSize200mm = TRUE) THEN
			gLRInput.bWaferSize200mm := FALSE;
			gLRInput.bWaferSize300mm := FALSE;
			gLRInput.bRUN := FALSE; 
			gLROutput.gstrError:= 'Invalid input. Only one Wafer Size may be selected at a time.';
			RETURN;
		ELSIF (gLRInput.bBatchCount2 = TRUE) THEN
			// check for valid input (number of batches =/= 2)
			gLRInput.bBatchCount2 := FALSE; 
			gLRInput.bWaferSize300mm := FALSE;
			gLRInput.bRUN := FALSE; 
			gLROutput.gstrError:= 'Invalid input. 300mm wafers do not support more than 1 Batch Count.'; 
			RETURN;
		ELSIF (gLRInput.bLoadGC = TRUE) THEN
			// check for valid input
			IF (gLRInput.bUnloadGC = TRUE) THEN 
				gLRInput.bLoadGC := FALSE;
				gLRInput.bUnloadGC := FALSE;
				gLRInput.bRUN := FALSE; 
				gLROutput.gstrError:= 'Invalid input. Only one Linear Robot Operation may be selected at a time.';
				RETURN;
			ELSE

				// SET PROCESS TO LOAD
				tLR.Process.bLOAD := TRUE;
				uiCurrentSuperState := eRunning;
				gLROutput.gstrError:= ''; // clear error code
			END_IF;

		ELSIF (gLRInput.bUnloadGC = TRUE) THEN
			// check for valid input
			IF (gLRInput.bLoadGC = TRUE) THEN 
				gLRInput.bLoadGC := FALSE;
				gLRInput.bUnloadGC := FALSE;
				gLRInput.bRUN := FALSE; 
				gLROutput.gstrError := 'Invalid input. Only one Linear Robot Operation may be selected at a time'; 
				RETURN;
			ELSE

				// SET PROCESS TO UNLOAD
				tLR.Process.bUNLOAD := TRUE;
				uiCurrentSuperState := eRunning;
				gLROutput.gstrError:= ''; // clear error code
			END_IF;

		END_IF;
	END_IF;
END_ACTION

