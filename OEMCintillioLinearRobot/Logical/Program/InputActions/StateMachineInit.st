
ACTION StateMachineInit_act : 
	
	IF (tLR.Process.bLOAD = TRUE OR tLR.Process.bDUALLOAD = TRUE) THEN // Make sure only one process is selected - LOAD

		uiCurrentSuperState := eRunning; // Turn on the State Machine superstate
		StartFPAlign_act; // Enable the first low-level state of the state machine
		tLR.bWaferPossession := FALSE;
		tLR.Process.uiBatchTracker := 1; // all process start with batch tracker = 1, for single processes the value never changes
		tCurrentTargetFP := tTargetFP1; // set FOUP 1 as current target
			
	ELSIF (tLR.Process.bUNLOAD = TRUE OR tLR.Process.bDUALUNLOAD = TRUE) THEN // UNLOAD

		uiCurrentSuperState := eRunning;
		StartGCAlign_act;
		tLR.bWaferPossession := FALSE;
		tLR.Process.uiBatchTracker := 1;
		tCurrentTargetFP := tTargetFP1;
				
	END_IF
END_ACTION
