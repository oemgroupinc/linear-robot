
ACTION InvalidInputCheck_act : 
	//------------------------- Incomplete and invalid input check ----------------------------//
	IF (gLRInput.bWaferSize200mm = FALSE AND gLRInput.bWaferSize300mm = FALSE) THEN // need to select wafer size
		gLROutput.gstrError := 'Incomplete input. Please select a Wafer Size.';
		gLRInput.bRUN := FALSE;
		RETURN;
	END_IF;
            
	IF (gLRInput.bLoadGC = FALSE AND gLRInput.bUnloadGC = FALSE) THEN // need to select operation
		gLROutput.gstrError := 'Incomplete input. Please select a Linear Robot operation.';
		gLRInput.bRUN := FALSE;
		RETURN;
	END_IF;
            
	IF (gLRInput.bWaferSize200mm = TRUE AND gLRInput.bBatchCount1 = FALSE AND gLRInput.bBatchCount2 = FALSE) THEN // need to select batch count
		gLROutput.gstrError := 'Incomplete input. Please select a Batch Count.';
		gLRInput.bRUN := FALSE;
		RETURN;
	END_IF;
    
	IF  ( gLRInput.siBatch1FPort <= 0 OR gLRInput.siBatch1FPort > 8 )  THEN
		gLROutput.gstrError := 'Incomplete input. Please select a valid Target FOUP Port for batch 1.';
		gLRInput.bRUN := FALSE;
		RETURN;
	END_IF;
	
	IF  ( gLRInput.bBatchCount2 = TRUE AND ( gLRInput.siBatch2FPort <= 0 OR gLRInput.siBatch2FPort > 8 ) ) THEN
		gLROutput.gstrError := 'Incomplete input. Please select a valid Target FOUP Port for batch 2.';
		gLRInput.bRUN := FALSE;
		RETURN;
	END_IF;
	
	IF  ( gLRInput.bBatchCount2 = TRUE AND ( gLRInput.siBatch1FPort = gLRInput.siBatch2FPort ) ) THEN
		gLROutput.gstrError := 'Invalid input. FOUP port for batch 1 and batch 2 must be unique.';
		gLRInput.bRUN := FALSE;
		RETURN;
	END_IF;
	
	
	// Guardian Carrier
	IF (gLRInput.bGC1 = TRUE AND gLRInput.bGC2 = TRUE) THEN
		gLROutput.gstrError := 'Invalid input. Only one Guardian Carrier may be selected.';
		gLRInput.bGC1 := FALSE;
		gLRInput.bGC2 := FALSE;
		gLRInput.bRUN := FALSE;
		RETURN;
	ELSIF (gLRInput.bGC1 = FALSE AND gLRInput.bGC2 = FALSE) THEN
		gLROutput.gstrError := 'Incomplete input. Please select a Guardian Carrier.';
		gLRInput.bRUN := FALSE;
		RETURN;
	END_IF;
            
	// Guardian Carrier Level
	IF (gLRInput.bBatch1GCTopLevel = TRUE AND gLRInput.bBatch1GCBottomLevel = TRUE AND gLRInput.bWaferSize200mm = TRUE) THEN // both levels selected for batch 1
		gLROutput.gstrError := 'Invalid input. Only one Guardian Carrier level may be selected for batch 1.';
		gLRInput.bBatch1GCTopLevel := FALSE;
		gLRInput.bBatch1GCBottomLevel := FALSE;
		gLRInput.bRUN := FALSE;
		RETURN;
	ELSIF (gLRInput.bBatch1GCTopLevel = FALSE AND gLRInput.bBatch1GCBottomLevel = FALSE AND gLRInput.bWaferSize200mm = TRUE) THEN // neither level selected for batch 1
		gLROutput.gstrError := 'Incomplete input. Please select a Guardian Carrier level for batch 1.';
		gLRInput.bRUN := FALSE;
		RETURN;
	ELSIF (gLRInput.bBatch1GCTopLevel = TRUE AND gLRInput.bBatch2GCTopLevel = TRUE AND gLRInput.bWaferSize200mm = TRUE) THEN // both batches top level
		gLROutput.gstrError := 'Invalid input. Guardian Carrier level must be different for each batch.';
		gLRInput.bBatch1GCTopLevel := FALSE;
		gLRInput.bBatch2GCTopLevel := FALSE;
		gLRInput.bRUN := FALSE;
		RETURN;
	ELSIF (gLRInput.bBatch1GCBottomLevel = TRUE AND gLRInput.bBatch2GCBottomLevel = TRUE AND gLRInput.bWaferSize200mm = TRUE) THEN // both batches bottom level
		gLROutput.gstrError := 'Invalid input. Guardian Carrier level must be different for each batch.';
		gLRInput.bBatch1GCBottomLevel := FALSE;
		gLRInput.bBatch2GCBottomLevel := FALSE;
		gLRInput.bRUN := FALSE;
		RETURN;
	END_IF;

	IF (gLRInput.bBatch2GCTopLevel = TRUE AND gLRInput.bBatch2GCBottomLevel = TRUE AND gLRInput.bWaferSize200mm = TRUE) THEN // both levels selected for batch 2
		gLROutput.gstrError := 'Invalid input. Only one Guardian Carrier level may be selected for batch 2.';
		gLRInput.bBatch2GCTopLevel := FALSE;
		gLRInput.bBatch2GCBottomLevel := FALSE;
		gLRInput.bRUN := FALSE;
		RETURN;
	ELSIF (gLRInput.bBatch2GCTopLevel = FALSE AND gLRInput.bBatch2GCBottomLevel = FALSE AND gLRInput.bWaferSize200mm = TRUE AND gLRInput.bBatchCount2 = TRUE) THEN // neither level selected for batch 2
		gLROutput.gstrError := 'Incomplete input. Please select a Guardian Carrier level for batch 2.';
		gLRInput.bRUN := FALSE;
		RETURN;
	END_IF;
            
	IF (gLRInput.bWaferSize300mm = TRUE) THEN // anything selected for batch 2 in a 300mm machine
		IF (gLRInput.bBatch2GCBottomLevel = TRUE) THEN 
			gLROutput.gstrError := 'Invalid input. 300mm wafers do not support 2 batch processes.';
			gLRInput.bBatch2GCBottomLevel := FALSE;
			gLRInput.bRUN := FALSE;
			RETURN;
		ELSIF (gLRInput.bBatch2GCTopLevel = TRUE) THEN
			gLROutput.gstrError := 'Invalid input. 300mm wafers do not support 2 batch processes.';
			gLRInput.bBatch2GCTopLevel := FALSE;
			gLRInput.bRUN := FALSE;
			RETURN;
		END_IF;
	END_IF;
	
	// Verify that the Guardian Carrier is present
	(* IF (tGuardianCarrier1.bAbsentInd = TRUE) THEN
		gLROutput.gstrError := 'Guardian carrier is not present.';
		gLRInput.bRUN := FALSE;
		RETURN;
	END_IF; *)
END_ACTION

	