PROGRAM _INIT
	
	// software revision number
	SW_Revision := "v20190916a";

	// X1
	// Motion Units = 5:1 GearHead x 115mm linear Act motion/rev = 23.0mm/motor rev = 230units/rev [0.1mm units]	
	AxisX1.MpLink := ADR(gX1MpAxis);
	AxisX1.Axis := ADR(X1);
	AxisX1.Parameters := ADR(AxisX1Parameters);
	AxisX1.Enable := FALSE;
	AxisX1Parameters.Position := 0.0;			// Units = [ 0.1mm ]
	AxisX1Parameters.Velocity := 2300.0;		// Units = [ 0.1mm/sec ]
	AxisX1Parameters.Acceleration := 2300.0;	// Units = [ 0.1mm/sec^2 ]
	AxisX1Parameters.Deceleration := 2300.0;	// Units = [ 0.1mm/sec^2 ]
	AxisX1Parameters.Home.Mode := mpAXIS_HOME_MODE_SWITCH_GATE;
	AxisX1Parameters.Home.StartVelocity := 1150.0;	// Units = [ 0.1mm/sec ]
	AxisX1Parameters.Home.HomingVelocity := 115.0;	// Units = [ 0.1mm/sec ]
	AxisX1Parameters.Home.Acceleration := 6900.0;	// Units = [ 0.1mm/sec^2 ]
	AxisX1HardwareInputs.Axis := ADR(X1);
	AxisX1Jog.Axis := ADR(X1);
	
	// Y1
	// Motion Units = 10:1 GearHead x 115mm linear Act motion/rev = 11.5mm/motor rev = 115units/rev [0.1mm units]
	AxisY1.MpLink := ADR(gY1MpAxis);
	AxisY1.Axis := ADR(Y1);
	AxisY1.Parameters := ADR(AxisY1Parameters);
	AxisY1.Enable := FALSE;
	AxisY1Parameters.Position := 0.0;		// Units = [ 0.1mm ]	
	AxisY1Parameters.Velocity := 500.0;	// Units = [ 0.1mm/sec ]
	AxisY1Parameters.Acceleration := 2300.0;	// Units = [ 0.1mm/sec^2 ]
	AxisY1Parameters.Deceleration := 2300.0;	// Units = [ 0.1mm/sec^2 ]
	AxisY1Parameters.Home.Mode := mpAXIS_HOME_MODE_SWITCH_GATE;
	AxisY1Parameters.Home.StartVelocity := 575.0;	// Units = [ 0.1mm/sec ]
	AxisY1Parameters.Home.HomingVelocity := 58.0;	// Units = [ 0.1mm/sec ]
	AxisY1Parameters.Home.Acceleration := 3450.0;	// Units = [ 0.1mm/sec^2 ]
	AxisY1HardwareInputs.Axis := ADR(Y1);
	bAxisY1ArrivedFlag := FALSE;

	// Y2
	// Motion Units = 10:1 GearHead x 115mm linear Act motion/rev = 11.5mm/motor rev = 115units/rev [0.1mm units]
	AxisY2.MpLink := ADR(gY2MpAxis);
	AxisY2.Axis := ADR(Y2);
	AxisY2.Parameters := ADR(AxisY2Parameters);
	AxisY2.Enable := FALSE;
	AxisY2Parameters.Position := 0.0;		// Units = [ 0.1mm ]
	AxisY2Parameters.Velocity := 500.0;	// Units = [ 0.1mm/sec ]
	AxisY2Parameters.Acceleration := 2300.0;	// Units = [ 0.1mm/sec^2 ]
	AxisY2Parameters.Deceleration := 2300.0;	// Units = [ 0.1mm/sec^2 ]
	AxisY2Parameters.Home.Mode := mpAXIS_HOME_MODE_SWITCH_GATE;
	AxisY2Parameters.Home.StartVelocity := 575.0;	// Units = [ 0.1mm/sec ]
	AxisY2Parameters.Home.HomingVelocity := 58.0;	// Units = [ 0.1mm/sec ]
	AxisY2Parameters.Home.Acceleration := 3450.0;	// Units = [ 0.1mm/sec^2 ]
	AxisY2HardwareInputs.Axis := ADR(Y2);
	bAxisY2ArrivedFlag := FALSE;
	
	// Z1
	// Motion Units = 25:1 GearHead x 115mm linear Act motion/rev = 4.6mm/motor rev = 46units/rev [0.1mm units]
	AxisZ1.MpLink := ADR(gZ1MpAxis);
	AxisZ1.Axis := ADR(Z1);
	AxisZ1.Parameters := ADR(AxisZ1Parameters);
	AxisZ1.Enable := FALSE;
	AxisZ1Parameters.Position := 0.0;		// Units = [ 0.1mm]
	AxisZ1Parameters.Velocity := 65.0;		// Units = [ 0.1mm/sec]
	AxisZ1Parameters.Acceleration := 1000.0;	// Units = [ 0.1mm/sec^2 ]
	AxisZ1Parameters.Deceleration := 1000.0;	// Units = [ 0.1mm/sec^2 ]
	AxisZ1Parameters.Home.Mode := mpAXIS_HOME_MODE_SWITCH_GATE;
	AxisZ1Parameters.Home.StartVelocity := 230.0;	// Units = [ 0.1mm/sec]
	AxisZ1Parameters.Home.HomingVelocity := 23.0;	// Units = [ 0.1mm/sec]
	AxisZ1Parameters.Home.Acceleration := 1000.0;	// Units = [ 0.1mm/sec^2 ]
	AxisZ1HardwareInputs.Axis := ADR(Z1);
	
	// Z2
	// Motion Units = 25:1 GearHead x 115mm linear Act motion/rev = 4.6mm/motor rev = 46units/rev [0.1mm units]
	AxisZ2.MpLink := ADR(gZ2MpAxis);
	AxisZ2.Axis := ADR(Z2);
	AxisZ2.Parameters := ADR(AxisZ2Parameters);
	AxisZ2.Enable := FALSE;
	AxisZ2Parameters.Position := 0.0;		// Units = [ 0.1mm]
	AxisZ2Parameters.Velocity := 460.0;		// Units = [ 0.1mm/sec ]
	AxisZ2Parameters.Acceleration := 230.0;	// Units = [ 0.1mm/sec^2 ]
	AxisZ2Parameters.Deceleration := 230.0;	// Units = [ 0.1mm/sec^2 ]
	AxisZ2Parameters.Home.Mode := mpAXIS_HOME_MODE_SWITCH_GATE;
	AxisZ2Parameters.Home.StartVelocity := 230.0;	// Units = [ 0.1mm/sec ]
	AxisZ2Parameters.Home.HomingVelocity := 23.0;	// Units = [ 0.1mm/sec ]
	AxisZ2Parameters.Home.Acceleration := 1380.0;	// Units = [ 0.1mm/sec^2 ]
	AxisZ2HardwareInputs.Axis := ADR(Z2);
	
	// Rotary Stage	
	RotaryPause;
	bAxisRArrivedFlag := FALSE;
	bAxisRStartFlag := FALSE;
	
	//------------------------------------ FOUP Port set point declarations ----------------------------------//
	// Saving Target position in Perm Mem - RWR 7-23-19
	
	tFOUPPort1.siX1Pos := rFP1AxisX1TargetPos; 			// Units = [ 0.1mm ]
	tFOUPPort1.siY1AccessPos := rFP1AxisY1TargetPos; 	// [units = 0.1mm]
	tFOUPPort1.siY2AccessPos := rFP1AxisY2TargetPos;	// [units = 0.1mm]
	tFOUPPort1.siZ1FPPossession := rFP1AxisZ1LowTargetPos;	// [units = 0.1mm]
	tFOUPPort1.siZ1LRPossession := rFP1AxisZ1UpTargetPos;	// [units = 0.1mm]

	tFOUPPort2.siX1Pos := rFP2AxisX1TargetPos; 			// Units = [ 0.1mm ]
	tFOUPPort2.siY1AccessPos := rFP2AxisY1TargetPos; 	// [units = 0.1mm]
	tFOUPPort2.siY2AccessPos := rFP2AxisY2TargetPos; 	// [units = 0.1mm]
	tFOUPPort2.siZ1FPPossession := rFP2AxisZ1LowTargetPos;	// [units = 0.1mm]
	tFOUPPort2.siZ1LRPossession := rFP2AxisZ1UpTargetPos; 	// [units = 0.1mm]

	tFOUPPort3.siX1Pos := rFP3AxisX1TargetPos; 			// Units = [ 0.1mm ]
	tFOUPPort3.siY1AccessPos := rFP3AxisY1TargetPos; 	// [units = 0.1mm]
	tFOUPPort3.siY2AccessPos := rFP3AxisY2TargetPos; 	// [units = 0.1mm]
	tFOUPPort3.siZ1FPPossession := rFP3AxisZ1LowTargetPos;	// [units = 0.1mm]
	tFOUPPort3.siZ1LRPossession := rFP3AxisZ1UpTargetPos; 	// [units = 0.1mm]

	tFOUPPort4.siX1Pos := rFP4AxisX1TargetPos; 			// Units = [ 0.1mm ]
	tFOUPPort4.siY1AccessPos := rFP4AxisY1TargetPos; 	// [units = 0.1mm]
	tFOUPPort4.siY2AccessPos := rFP4AxisY2TargetPos; 	// [units = 0.1mm]
	tFOUPPort4.siZ1FPPossession := rFP4AxisZ1LowTargetPos;	// [units = 0.1mm]
	tFOUPPort4.siZ1LRPossession := rFP4AxisZ1UpTargetPos; 	// [units = 0.1mm]

	//-------------------------------- Guardian Carrier set point declarations -------------------------------//
	// Saving Target position in Perm Mem - RWR 7-23-19

	tGuardianCarrier1.siX1Pos := rGC1AxisX1TargetPos; 				// Units = [ 0.1mm ]
	tGuardianCarrier1.siY1AccessPos := rGC1AxisY1TargetPos; 		// [units = 0.1mm]
	tGuardianCarrier1.siY2AccessPos := rGC1AxisY2TargetPos; 		// [units = 0.1mm]
	tGuardianCarrier1.siZ1GCPossession := rGC1AxisZ1LowTargetPos; 	// Lower Position [units = 0.1mm]
	tGuardianCarrier1.siZ1LRPossession := rGC1AxisZ1UpTargetPos;	// Upper Position [units = 0.1mm]
	tGuardianCarrier1.siZ2TopPos := rGC1AxisZ2UpCarrierPos;			// [units = 0.1mm]
	tGuardianCarrier1.siZ2BottomPos := rGC1AxisZ2LowCarrierPos;		// [units = 0.1mm]
	tGuardianCarrier1.siZ2300Pos := rGC1AxisZ2LowCarrierPos;		// [units = 0.1mm]

	tGuardianCarrier2.siX1Pos := -17135; 			// TBD [Units = 0.1mm]
	tGuardianCarrier2.siY1AccessPos := -1495;		// TBD [units = 0.1mm]
	tGuardianCarrier2.siY2AccessPos := -1495;		// TBD [units = 0.1mm]
	tGuardianCarrier2.siZ1GCPossession := -46;		// TBD [units = 0.1mm]
	tGuardianCarrier2.siZ1LRPossession := 92;		// TBD [units = 0.1mm]
	tGuardianCarrier2.siZ2TopPos := 46.0;			// TBD [units = 0.1mm]
	tGuardianCarrier2.siZ2BottomPos := 51.0;		// TBD [units = 0.1mm]
	tGuardianCarrier2.siZ2300Pos := 51.0;			// TBD [units = 0.1mm]

	//-------------------------------------- Linear Robot declarations --------------------------------------//

	tLR.siX1SafePos := 0.0;		// Units = [ 0.1mm ]
	tLR.siY1SafePos := -414.0;	// Units = [ 0.1mm ]
	tLR.siY2SafePos := 345.0;	// Units = [ 0.1mm ]
	tLR.siZ1SafePos := 0.0;		// Units = [ 0.1mm ]
	tLR.siZ2SafePos := 0.0;		// Units = [ 0.1mm ]
	tLR.siRSafePos := -180;		// Units = ACTION RotaryMove_act [degree]

	tLR.siFastAccelX1 := 4600.0;	// Units = [ 0.1mm/sec^2 ]
	tLR.siSlowAccelX1 := 2300.0;	// Units = [ 0.1mm/sec^2 ]
	tLR.siFastVelX1 := 7500.0;		// Units = [ 0.1mm/sec ]
	tLR.siSlowVelX1 := 2000.0;		// Units = [ 0.1mm/sec ]
	tLR.siFastAccelY1 := 2300.0;	// Units = [ 0.1mm/sec^2 ]	
	tLR.siSlowAccelY1 := 2300.0;	// Units = [ 0.1mm/sec^2 ]
	tLR.siFastVelY1 := 3000.0;		// Units = [ 0.1mm/sec ]
	tLR.siSlowVelY1 := 500.0;		// Units = [ 0.1mm/sec ]
	tLR.siFastAccelY2 := 2300.0;	// Units = [ 0.1mm/sec^2 ]				
	tLR.siSlowAccelY2 := 2300.0;	// Units = [ 0.1mm/sec^2 ]	
	tLR.siFastVelY2 := 3000.0;		// Units = [ 0.1mm/sec ]
	tLR.siSlowVelY2 := 500.0;		// Units = [ 0.1mm/sec ]
	tLR.siFastAccelZ1 := 1000;		// Units = [ 0.1mm/sec^2 ]
	tLR.siSlowAccelZ1 := 1000;		// Units = [ 0.1mm/sec^2 ]
	tLR.siFastVelZ1 := 2000.0;		// Units = [ 0.1mm/sec ]
	tLR.siSlowVelZ1 := 65.0;		// Units = [ 0.1mm/sec ]
	tLR.siFastAccelZ2 := 920;		// Units = [ 0.1mm/sec^2 ]
	tLR.siSlowAccelZ2 := 230;		// Units = [ 0.1mm/sec^2 ]
	tLR.siFastVelZ2 := 2000.0;		// Units = [ 0.1mm/sec ]
	tLR.siSlowVelZ2 := 460.0;		// Units = [ 0.1mm/sec ]
	tLR.siFastAccelR := 30;
	tLR.siSlowAccelR := 1;
	tLR.siFastSpeedR := 40;
	tLR.siSlowSpeedR := 20;
	
	tLR.usiSWStateIndTxt := 0; // HMI Text Indication = ERROR
	tLR.usiSWStateIndColor := usiRedIndColor; // HMI Indicator Color
	
	//-------------------------------- Axis Jog Parameters for setting Target Positions ---------------//
	
	tLR.rCoarseJogVelX1 := 230.0;					// All Acopos Axis Course Jog Vel Rate		[ units = 0.1mm/sec ]
	tLR.rCoarseJogAccelX1 := 1725.0;				// All Acopos Axis Course Jog Accel Rate	[ units = 0.1mm/sec^2 ]
	tLR.rCoarseJogDelX1 := 5750.0;					// All Acopos Axis Course Jog Decel Rate	[ units = 0.1mm/sec^2 ]
	tLR.rFineJogIncX1 := 10.0;						// All Acopos Axis Fine Jog Step Length		[ units = 0.1mm ]
	tLR.rX1JogUpperTravelLimit := 0;				// X1 Axis Jog Upper Travel Limit [ Units = 0.1mm ]
	tLR.rX1JogLowerTravelLimit := -17227;			// X1 Axis Jog Lower Travel Limit [ Units = 0.1mm ]
	tLR.rYJogUpperTravelLimit := 216;				// Y1 Axis Jog Upper Travel Limit [ units = 0.1mm ]
	tLR.rYJogLowerTravelLimit := -2040;				// Y1 Axis Jog Lower Travel Limit [ units = 0.1mm ]
	tLR.rY2JogUpperTravelLimit := 355;				// Y2 Axis Jog Upper Travel Limit [ units = 0.1mm ]
	tLR.rY2JogLowerTravelLimit := -1914;			// Y2 Axis Jog Lower Travel Limit [ units = 0.1mm ]
	tLR.rZ1JogUpperTravelLimit := 288;				// Z1 Axis Jog Upper Travel Limit [ units = 0.1mm ]
	tLR.rZ1JogLowerTravelLimit := -120;				// Z1 Axis Jog Lower Travel Limit [ units = 0.1mm ]
	tLR.rZ2JogUpperTravelLimit := 2415;				// Z2 Axis Jog Upper Travel Limit [ units = 0.1mm ]
	tLR.rZ2JogLowerTravelLimit := -33;				// Z2 Axis Jog Lower Travel Limit [ units = 0.1mm ]	
	uiAxisJogState := eAxisJogOff;					// Axis Selection States - 0=NA, 10=X1, 20=Y1, 30=Y2, 40=Z1, 50=Z2, 60=R, 70=Home
	uiAxisX1JogState := eWaitJogCmdState;
	uiAxisHomeState := eY1HomeState;				// Set Homing State to beginning
	uiAxisRRotateState := eWaitRotateCmdState;
	gLRInput.siSelectJogAxis := 0;					// Selected Axis to Jog - 0=Off, 1=X1, 2=Y1, 3=Y2, 4=Z1, 5=Z2, 6=R
	gLRInput.siSetAxisX1JogType := siJogModeOff;	// Axis Jog Types - 0=Off, 1=Coarse, 2=Fine
	gLRInput.siSelectAxisTargetLevel := 0;			// Z Axis Target Level to set - 1=Upper(Top), 2=Lower(Bottom)
	AxisX1Jog.Enable := FALSE;
	AxisY1Jog.Enable := FALSE;
	gLRInput.bJogNegAxisX1PB := FALSE;
	gLRInput.bJogPosAxisX1PB := FALSE;
	gLRInput.bHomeAllAxisPB := FALSE;
	tLR.bHomeAllAxis := FALSE;
	gLRInput.bSetTargetPosPB := FALSE;
	
	// Perm Mem Test
		// Axis X1
	tFOUPPort1.rNewAxisX1TargetPos := rFP1AxisX1TargetPos;
	tFOUPPort2.rNewAxisX1TargetPos := rFP2AxisX1TargetPos;
	tFOUPPort3.rNewAxisX1TargetPos := rFP3AxisX1TargetPos;
	tFOUPPort4.rNewAxisX1TargetPos := rFP4AxisX1TargetPos;
	tGuardianCarrier1.rNewAxisX1TargetPos := rGC1AxisX1TargetPos;
	
	tFOUPPort1.rPrevAxisX1TargetPos := rFP1AxisX1TargetPos;
	tFOUPPort2.rPrevAxisX1TargetPos := rFP2AxisX1TargetPos;
	tFOUPPort3.rPrevAxisX1TargetPos := rFP3AxisX1TargetPos;
	tFOUPPort4.rPrevAxisX1TargetPos := rFP4AxisX1TargetPos;
	tGuardianCarrier1.rPrevAxisX1TargetPos := rGC1AxisX1TargetPos;
	
		// Axis Y1
	tFOUPPort1.rNewAxisY1TargetPos := rFP1AxisY1TargetPos;
	tFOUPPort2.rNewAxisY1TargetPos := rFP2AxisY1TargetPos;
	tFOUPPort3.rNewAxisY1TargetPos := rFP3AxisY1TargetPos;
	tFOUPPort4.rNewAxisY1TargetPos := rFP4AxisY1TargetPos;
	tGuardianCarrier1.rNewAxisY1TargetPos := rGC1AxisY1TargetPos;
	
	tFOUPPort1.rPrevAxisY1TargetPos := rFP1AxisY1TargetPos;
	tFOUPPort2.rPrevAxisY1TargetPos := rFP2AxisY1TargetPos;
	tFOUPPort3.rPrevAxisY1TargetPos := rFP3AxisY1TargetPos;
	tFOUPPort4.rPrevAxisY1TargetPos := rFP4AxisY1TargetPos;
	tGuardianCarrier1.rPrevAxisY1TargetPos := rGC1AxisY1TargetPos;
	
		// Axis Y2
	tFOUPPort1.rNewAxisY2TargetPos := rFP1AxisY2TargetPos;
	tFOUPPort2.rNewAxisY2TargetPos := rFP2AxisY2TargetPos;
	tFOUPPort3.rNewAxisY2TargetPos := rFP3AxisY2TargetPos;
	tFOUPPort4.rNewAxisY2TargetPos := rFP4AxisY2TargetPos;
	tGuardianCarrier1.rNewAxisY2TargetPos := rGC1AxisY2TargetPos;
	
	tFOUPPort1.rPrevAxisY2TargetPos := rFP1AxisY2TargetPos;
	tFOUPPort2.rPrevAxisY2TargetPos := rFP2AxisY2TargetPos;
	tFOUPPort3.rPrevAxisY2TargetPos := rFP3AxisY2TargetPos;
	tFOUPPort4.rPrevAxisY2TargetPos := rFP4AxisY2TargetPos;
	tGuardianCarrier1.rPrevAxisY2TargetPos := rGC1AxisY2TargetPos;
	
	// Axis Z1 
		// Upper End Effector Position
	tFOUPPort1.rNewAxisZ1UpTargetPos := rFP1AxisZ1UpTargetPos;
	tFOUPPort2.rNewAxisZ1UpTargetPos := rFP2AxisZ1UpTargetPos;
	tFOUPPort3.rNewAxisZ1UpTargetPos := rFP3AxisZ1UpTargetPos;
	tFOUPPort4.rNewAxisZ1UpTargetPos := rFP4AxisZ1UpTargetPos;
	tGuardianCarrier1.rNewAxisZ1UpTargetPos := rGC1AxisZ1UpTargetPos;
	
	tFOUPPort1.rPrevAxisZ1UpTargetPos := rFP1AxisZ1UpTargetPos;
	tFOUPPort2.rPrevAxisZ1UpTargetPos := rFP2AxisZ1UpTargetPos;
	tFOUPPort3.rPrevAxisZ1UpTargetPos := rFP3AxisZ1UpTargetPos;
	tFOUPPort4.rPrevAxisZ1UpTargetPos := rFP4AxisZ1UpTargetPos;
	tGuardianCarrier1.rPrevAxisZ1UpTargetPos := rGC1AxisZ1UpTargetPos;	
		// Lower End Effector Position
	tFOUPPort1.rNewAxisZ1LowTargetPos := rFP1AxisZ1LowTargetPos;
	tFOUPPort2.rNewAxisZ1LowTargetPos := rFP2AxisZ1LowTargetPos;
	tFOUPPort3.rNewAxisZ1LowTargetPos := rFP3AxisZ1LowTargetPos;
	tFOUPPort4.rNewAxisZ1LowTargetPos := rFP4AxisZ1LowTargetPos;
	tGuardianCarrier1.rNewAxisZ1LowTargetPos := rGC1AxisZ1LowTargetPos;
	
	tFOUPPort1.rPrevAxisZ1LowTargetPos := rFP1AxisZ1LowTargetPos;
	tFOUPPort2.rPrevAxisZ1LowTargetPos := rFP2AxisZ1LowTargetPos;
	tFOUPPort3.rPrevAxisZ1LowTargetPos := rFP3AxisZ1LowTargetPos;
	tFOUPPort4.rPrevAxisZ1LowTargetPos := rFP4AxisZ1LowTargetPos;
	tGuardianCarrier1.rPrevAxisZ1LowTargetPos := rGC1AxisZ1LowTargetPos;
	
	// Axis Z2 Carrier Position
	tGuardianCarrier1.rPrevAxisZ2UpCarrierPos := rGC1AxisZ2UpCarrierPos;
	tGuardianCarrier1.rPrevAxisZ2LowCarrierPos := rGC1AxisZ2LowCarrierPos;
	
	// start hardware initialization in cyclic	
	uiCurrentSuperState := eHardwareInitialization;
	
	// Staubli Robot Var Init
	bStaubliRobotReady := 0;
	bStaubliRobotSafePos := 0;
	bStaubliRobotRunCMD := FALSE;
	uiSRState := eIdleState;
	uiSRMoveState := eIdleMoveState;
	usiLoadStartPosition := 7;		// set as default location Carrier
	usiLoadTargetPosition := 4;		// set as default location 1st CPC
	usiUnloadStartPosition := 5;	// set as default location SRD
	usiUnloadTargetPosition := 7;	// set as default location Carrier
	gLRInput.siFOUPPortNumberB1 := 2;	// Set to installed Loadports on Demo Rack 8/14/19
	gLRInput.siFOUPPortNumberB2 := 3;	// Set to installed Loadports on Demo Rack 8/14/19
	gLRInput.bLoad200Batch1 := FALSE;
	gLRInput.bUnload200Batch1 := FALSE;
	gLRInput.bLoad200Batch2TB := FALSE;
	gLRInput.bLoad200Batch2BT := FALSE;
	gLRInput.bUnload200Batch2TB := FALSE;
	gLRInput.bUnload200Batch2BT := FALSE;
	gLRInput.bLoad300Batch1 := FALSE;
	gLRInput.bUnload300Batch1 := FALSE;
	gLRInput.bRUN := FALSE;
	gLRInput.bWaferSize300mm := FALSE;
	gLRInput.bWaferSize200mm := FALSE;
	gLRInput.bLoadGC := FALSE;
	gLRInput.bUnloadGC := FALSE;
	gLRInput.bBatchCount1 := FALSE;
	gLRInput.bBatchCount2 := FALSE;
	gLRInput.bGC1 := FALSE;
	gLRInput.bGC2 := FALSE;
	gLRInput.bBatch1GCBottomLevel := FALSE;
	gLRInput.bBatch1GCTopLevel := FALSE;
	gLRInput.bBatch2GCBottomLevel := FALSE;
	gLRInput.bBatch2GCTopLevel := FALSE;	
	gLRInput.obStaubliSafePosRE := FALSE;
	gLROutput.bComplete := FALSE;
	gLROutput.usiSRMoveStartPosition := 0;
	gLROutput.usiSRMoveTargetPosition := 0;
	gLRInput.bUnloadWaferType := FALSE;
	gLRInput.bLoadWaferType := FALSE;
	gLROutput.bWriteStaubliRobotOnce := FALSE;
	gLROutput.bWriteStaubliTargetOnce := FALSE;
	gLROutput.bStaubliRunCMD := FALSE;
	gLROutput.bStaubliRunCMDOnce := FALSE;
	
	// AutoCycle Mode
	uiSRAutoCycleState := 0; // Idle State
	gLRInput.bAutoCycleMode := FALSE;
	gLRInput.bStartAutoCyclePB := FALSE;
	uiAutoCycleCounts := 0;
	bCycleTimeStart := FALSE;
	dtCycleTime := 0;
	dtPrevCycleTime := 0;
	
	// Drive Fault Recovery
	AxisX1Acknowlege := FALSE;
	AxisY1Acknowlege := FALSE;
	AxisY2Acknowlege := FALSE;
	AxisZ1Acknowlege := FALSE;
	AxisZ2Acknowlege := FALSE;
	AxisRAcknowlege := FALSE;
END_PROGRAM