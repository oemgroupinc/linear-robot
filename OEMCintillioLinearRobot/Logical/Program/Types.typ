
TYPE
	LinearRobot_typ : 	STRUCT  (*Object for parameters specific to the Linear Robot*)
		siX1SafePos : REAL; (*Location along the X axis where the LR is out of the way of the 6-axis robot*)
		siY1SafePos : REAL; (*Location where the Y1 axis slide is out of the way of the 6-axis robot*)
		siY2SafePos : REAL; (*Location where the Y2 axis slide is out of the way of the 6-axis robot*)
		siZ1SafePos : REAL; (*Location at which the LR is at the proper height to be out of the way of the 6-axis robot*)
		siZ2SafePos : REAL; (*Location at which the Guardian Carrier is at the correct height to be manipulated by the 6-axis robot*)
		siRSafePos : REAL; (*Location at which the LR is rotated to an orientation at which it is out of the way of the 6-axis robot*)
		siXHomePos : REAL; (*Location at which the X axis is calibrated*)
		siY1HomePos : REAL; (*Location at which the Y1 axis is calibrated*)
		siY2HomePos : REAL; (*Location at which the Y2 axis is calibrated*)
		siZ1HomePos : REAL; (*Location at which the Z1 axis is calibrated*)
		siZ2HomePos : REAL; (*Location at which the Z2 axis is calibrated*)
		siRHomePos : REAL; (*Location at which the R axis is calibrated*)
		siFastAccelX1 : REAL; (*Acceleration value for the X axis when the LR doesn't posses any wafers*)
		siSlowAccelX1 : REAL; (*Acceleration value for the X axis when the LR possesses wafers*)
		siFastVelX1 : REAL; (*Velocity value for the X1 axis when the LR doesn't possess any wafers*)
		siSlowVelX1 : REAL; (*Velocity value for the X1 axis when the LR possesses wafers*)
		siFastAccelY1 : REAL; (*Acceleration value for the Y1 axis when the LR doesn't possess any wafers*)
		siSlowAccelY1 : REAL; (*Acceleration value for the Y1 axis when the LR possesses wafers*)
		siFastVelY1 : REAL; (*Velocity value for the Y1 axis when the LR doesn't possess any wafers*)
		siSlowVelY1 : REAL; (*Velocity value for the Y1 axis when the LR possesses wafers*)
		siFastAccelY2 : REAL; (*Acceleration value for the Y2 axis when the LR doesn't possess any wafers*)
		siSlowAccelY2 : REAL; (*Acceleration value for the Y2 axis when the LR possesses wafers*)
		siFastVelY2 : REAL; (*Velocity value for the Y2 axis when the LR doesn't possess any wafers*)
		siSlowVelY2 : REAL; (*Velocity value for the Y2 axis when the LR possesses wafers*)
		siFastAccelZ1 : REAL; (*Acceleration value for the Z1 axis when the LR doesn't possess any wafers*)
		siSlowAccelZ1 : REAL; (*Acceleration value for the Z1 axis when the LR possesses wafers*)
		siFastVelZ1 : REAL; (*Velocity value for the Z1 axis when the LR doesn't possess any wafers*)
		siSlowVelZ1 : REAL; (*Velocity value for the Z1 axis when the LR possesses wafers*)
		siFastAccelZ2 : REAL; (*Acceleration value for the Z2 axis when the LR doesn't possess any wafers*)
		siSlowAccelZ2 : REAL; (*Acceleration value for the Z2 axis when the LR possesses wafers*)
		siFastVelZ2 : REAL; (*Velocity value for the Z2 axis when the LR doesn't possess any wafers*)
		siSlowVelZ2 : REAL; (*Velocity value for the Z2 axis when the LR possesses wafers*)
		siFastAccelR : REAL; (*Acceleration value for the R axis when the LR doesn't posses any wafers*)
		siSlowAccelR : REAL; (*Acceleration value for the R axis when the LR possesses wafers*)
		siFastSpeedR : REAL; (*Speed value for the R axis when the LR doesn't posses any wafers*)
		siSlowSpeedR : REAL; (*Speed value for the R axis when the LR posses any wafers*)
		Process : Process_typ; (*Variable holding the value of the process being run and determining the state flow*)
		bWaferPossession : BOOL; (*TRUE if Linear Robot is holding wafers*)
		rCoarseJogDelX1 : REAL;
		rCoarseJogAccelX1 : REAL;
		rFineJogIncX1 : REAL;
		rCoarseJogVelX1 : REAL;
		rX1JogUpperTravelLimit : LREAL;
		rX1JogLowerTravelLimit : LREAL;
		rYJogUpperTravelLimit : LREAL;
		rYJogLowerTravelLimit : LREAL;
		rY2JogUpperTravelLimit : LREAL;
		rY2JogLowerTravelLimit : LREAL;
		rZ1JogUpperTravelLimit : LREAL;
		rZ1JogLowerTravelLimit : LREAL;
		rZ2JogUpperTravelLimit : LREAL;
		rZ2JogLowerTravelLimit : LREAL;
		bHomeAllAxis : BOOL;
		usiSWStateIndTxt : USINT;
		usiSWStateIndColor : USINT;
		usiSRSafePosInd : USINT;
		usiSRReadyInd : USINT;
	END_STRUCT;
	GuardianCarrierLevel_typ : 	STRUCT  (*Object for which level of the GC will be loaded or unloaded*)
		bTop : BOOL; (*LR will load wafers into/unload wafers out of the top slot of the GC*)
		bBottom : BOOL; (*LR will load wafers into/unload wafers out of the bottom slot of the GC*)
		bNA : BOOL; (*LR is loading 300mm wafers and the GC only has one level*)
	END_STRUCT;
	GuardianCarrierOperation_typ : 	STRUCT  (*Object for what the linear robot will be doing*)
		bLoad : BOOL; (*LR will load wafers from FOUP Port into Guardian Carrier*)
		bUnload : BOOL; (*LR will unload wafers from Guardian Carrier and move them to FOUP Port*)
	END_STRUCT;
	FOUPPort_typ : 	STRUCT  (*LR FOUP Port class*)
		siX1Pos : LREAL; (*Position of LR FOUP Port on the X axis*)
		siY1AccessPos : LREAL; (*Position of the Y1 axis when accessing LR FOUP Port*)
		siY2AccessPos : LREAL; (*Position of the Y2 axis when accessing LR FOUP Port*)
		siZ1LRPossession : LREAL; (*Position of the Z1 axis at all times when the wafers are supported by the LR before loading this FP or after unloading this FP*)
		siZ1FPPossession : LREAL; (*Position of the Z1 axis at which the wafers are supported by the FOUP Port, with the wafers having just been loaded or about to be unloaded*)
		rNewAxisX1TargetPos : LREAL;
		rPrevAxisX1TargetPos : LREAL;
		rNewAxisY1TargetPos : LREAL;
		rPrevAxisY1TargetPos : LREAL;
		rNewAxisY2TargetPos : LREAL;
		rPrevAxisY2TargetPos : LREAL;
		rNewAxisZ1UpTargetPos : LREAL;
		rPrevAxisZ1UpTargetPos : LREAL;
		rNewAxisZ1LowTargetPos : LREAL;
		rPrevAxisZ1LowTargetPos : LREAL;
	END_STRUCT;
	GuardianCarrier_typ : 	STRUCT  (*Guardian Carrier classs*)
		eLocationID : INT; (**ID of Guardian Carrier*)
		bAbsentInd : BOOL; (*Sensor output indicating the presence of wafers*)
		siX1Pos : LREAL; (*Position of the Guardian Carrier along the X axis*)
		siY1AccessPos : LREAL; (*Position of the Y1 axis when accessing the Guardian Carrier*)
		siY2AccessPos : LREAL; (*Position of the Y2 axis when accessing the Guardian Carrier*)
		siZ1LRPossession : LREAL; (*Position of the Z1 axis at all times when the wafers are supported by the LR before loading this GC or after unloading this GC*)
		siZ1GCPossession : LREAL; (*Position of the Z1 axis when the wafers are supported by th guardian carrier, with the wafers having just been loaded or about to be unloaded*)
		siZ2TopPos : LREAL; (*Position of the Z2 axis such that that top slot of the Guardian Carrier is aligned with Z1 Access Pos*)
		siZ2BottomPos : LREAL; (*Position of the Z2 axis such that that bottom slot of the Guardian Carrier is aligned with Z1 Access Pos*)
		siZ2300Pos : LREAL; (*Position of the Z2 axis when loading or unloading 300 mm wafers*)
		rNewAxisX1TargetPos : LREAL;
		rPrevAxisX1TargetPos : LREAL;
		rNewAxisY1TargetPos : LREAL;
		rPrevAxisY1TargetPos : LREAL;
		rNewAxisY2TargetPos : LREAL;
		rPrevAxisY2TargetPos : LREAL;
		rNewAxisZ1LowTargetPos : LREAL;
		rPrevAxisZ1LowTargetPos : LREAL;
		rNewAxisZ1UpTargetPos : LREAL;
		rPrevAxisZ1UpTargetPos : LREAL;
		rPrevAxisZ2UpCarrierPos : LREAL;
		rPrevAxisZ2LowCarrierPos : LREAL;
	END_STRUCT;
	Process_typ : 	STRUCT  (*Class holding the names which classes are called in order based on input*)
		bLOAD : BOOL; (*Unload FP Align, Unload FP, Load GC Align, Load GC, Safe Align, Wait*)
		bUNLOAD : BOOL; (*Unload GC Align, Unload GC, Load FP Align, Load FP, Safe Align, Wait*)
		bDUALLOAD : BOOL; (*Unload FP Align, Unload FP, Load GC Align, Load GC, Unload FP Align, Unload FP, Load GC Align, Load GC, Safe Align, Wait*)
		bDUALUNLOAD : BOOL; (*Unload GC Align, Unload GC, Load FP Align, Load FP, Unload GC Align, Unload GC, Load FP Align, Load FP, Safe Align, Wait*)
		uiBatchTracker : USINT; (*Variable tracking which batch is active so that states can distinguish between necessary values*)
	END_STRUCT;
	AxisMove_typ : 	STRUCT  (*Instance of the Linear Robot Axis Movement handler object*)
		AXIS : REFERENCE TO MpAxisBasic; (*Pointer to Axis being worked with *)
		state : AxisState_enum; (*Motion state of the axis *)
		bMove : BOOL; (*If TRUE, axis will move to designated position with designated acceleration*)
		siTargetPos : LREAL; (*Variable that represents target position*)
		rAccel : REAL; (*Variable that represents axis acceleration*)
		rSpeed : REAL; (*Variable that represents axis speed*)
	END_STRUCT;
	AxisState_enum : 
		( (*Enum designating whether an axis is initiated, in motion, or arrived*)
		eInitiate := 10,
		ePending := 11,
		eInMotion := 15,
		eArrived := 20
		);
	LRSuperState_enum : 
		( (*Enumerated class containing all the state machine states the LR can be in*)
		eWait := 0, (*If TRUE, LR is on and awaiting instructions and START command*)
		eHardwareInitialization := 100, (*LR is on and awaiting instructions and RUN command*)
		eRunning := 200 (*LR is on, has received a valid input, and has selected a process*)
		);
	LRState_enum : 
		( (*Enumerated class containing all the high level states the LR can be in*)
		eSafeAlign := 0, (*LR does not posses wafers and quicky moves to the safe location while the six-axis robot manipulates the GC*)
		eFPAlign := 100, (*LR does not posses wafers and quickly moves to the FOUP Port called for by the input to transfer possesion to itself*)
		eUnloadFP := 200, (*LR does not posses wafers and slowly removes them from the FOUP Port called for by the input*)
		eGCAlign := 300, (*LR possesses wafers and slowly moves to the Guardian Carrier called for by the input to place them in the GC*)
		eLoadGC := 400, (*LR possesses wafers and slowly places them in the GC*)
		eUnloadGC := 600, (*LR does not posses wafers and slowly removes them from the GC called for by the input*)
		eLoadFP := 800 (*LR possesses wafers and slowly places them back in the FOUP Port called for by the input*)
		);
	LRSubState_enum : 
		( (*Enumerated class containing all the low-level states the LR can be in*)
		eX1SubState := 10,
		eX1SubStateWait := 11,
		eY1SubState := 20,
		eY1SubStateWait := 21,
		eY2SubState := 30,
		eY2SubStateWait := 31,
		eZ1SubState := 40,
		eZ1SubStateWait := 41,
		eZ2SubState := 50,
		eZ2SubStateWait := 51,
		eRSubState := 60,
		eRSubStateWait := 61,
		eSubStateFinished := 100
		);
	AxisX1Jog_enum : 
		(
		eWaitTargetReached := 30,
		eSetJogDirection := 20,
		eWaitJogCmdState := 10
		);
	SRState_enum : 
		( (*Enumerated class containing all the states the SR (Staubli Robot)*)
		eIdleState := 10,
		eCheckError := 15,
		eWaferHandlingType := 16,
		eRunLinearRobot := 20,
		eLinearRobotMoveComplete := 30,
		eRunStaubliRobot := 40,
		eResetStaubliRobotCmd := 45,
		eWaitStaubliRobotOnceAck := 47,
		eStaubliRobotMoveComplete := 50
		);
	LRAxisHome_enum : 
		( (*Enumerated class containing all the states for Homing Axis in LR Jog Routine*)
		eY1HomeState := 10,
		eY2HomeState := 20,
		eRHomeState := 30,
		eX1HomeState := 40,
		eZ1HomeState := 50,
		eZ2HomeState := 60,
		eHomeStateFinished := 100
		);
	LRAxisJog_enum : 
		( (*Enumerated class containing the Selected Axis to Jog*)
		eAxisJogOff := 0,
		eAxisX1Jog := 10,
		eAxisY1Jog := 20,
		eAxisY2Jog := 30,
		eAxisZ1Jog := 40,
		eAxisZ2Jog := 50,
		eAxisRRotate := 60,
		eAxisReturnHome := 70
		);
	LRAxisRRotate_enum : 
		( (*Enumerated class containing the states to Rotate R Axis*)
		eWaitRotateCmdState := 10,
		eAxisRRotate180 := 20,
		eAxisRRotate90 := 30,
		eAxisRRotate270 := 40
		);
	LRAxisX1TargetPos_enum : 
		( (*Enumerated class containing the states for which FOUP target position is being set*)
		eFOUP1Position := 1,
		eFOUP2Position := 2,
		eFOUP3Position := 3,
		eFOUP4Position := 4,
		eCarrier1Position := 10
		);
	LRAxisTargetLevel_enum : 
		( (*Enumerated class containing Axis Z1 End Effector Position Target Level*)
		eUpperPosition := 1, (*Axis Z1 End Effector Upper Position Target*)
		eLowerPosition := 2 (*Axis Z1 End Effector Lower Position Target*)
		);
	SRAutoCycleState_enum : 
		(
		eIdle := 0,
		eAutoCycleStart := 10,
		eResetCounts := 20,
		eCycleCountCheck := 30,
		eLoadBatchBT := 40,
		eWaitLoadComplete := 50,
		eUnLoadBatchTB := 60,
		eWaitUnLoadComplete := 70,
		eCycleCounts := 80
		);
	SRMoveState_enum : 
		( (*Enumerated class containing Staubli Robot Move commands Start->Target Position*)
		eIdleMoveState := 0,
		eWriteMovePositions := 10,
		eWriteStartPosition := 20,
		eWriteTargetPosition := 30,
		eWaitStaubliOnceAck := 40,
		eSetRunCMDStaubliRobot := 50,
		eResetOnceSignal := 60,
		eResetRunCMD := 70,
		eResetOnceRunCMD := 80
		);
END_TYPE
