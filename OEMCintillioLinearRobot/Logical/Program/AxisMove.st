ACTION RMove_act: 
	// Error handling
	IF (RotaryPWR = FALSE) THEN
		gLROutput.gstrError := 'R Axis not powered. ';
		bHardwareInitError := TRUE;
		RETURN;
	ELSIF (RotarySV = FALSE) THEN
		gLROutput.gstrError := 'R Axis Servo not powered. ';
		bHardwareInitError := TRUE;
		RETURN;
	ELSIF (RotaryHEND = FALSE) THEN
		gLROutput.gstrError := 'R Axis not Homed. ';
		bHardwareInitError := TRUE;
		RETURN;
	END_IF

	// set axis parameters
	CASE tAxisRMove.state OF
		eInitiate: 
			// Start movement
			RotaryMove_act; 
			IF (RotaryMOVE = TRUE) THEN
				tAxisRMove.state := ePending;
			END_IF;
		ePending:
			IF (RotaryMOVE = TRUE OR RotaryPosition = tAxisRMove.siTargetPos) THEN
				tAxisRMove.state := eInMotion;
			END_IF
		eInMotion:
			IF (RotaryMOVE = FALSE) THEN 
				bAxisRArrivedFlag := TRUE;
				tAxisRMove.state := eArrived;
			END_IF
		eArrived:
			RotaryMoveDone_act;
			IF (bAxisRStartFlag = TRUE AND RotaryPEND = TRUE) THEN	
				bAxisRArrivedFlag := FALSE;
				tAxisRMove.state := eInitiate;
			END_IF;
	END_CASE		
END_ACTION

ACTION X1Move_act: 
	// Error handling
	IF (AxisX1.Active = FALSE) THEN
		gLROutput.gstrError := 'X1 Axis disabled. ';
		bHardwareInitError := TRUE;
		RETURN;
	ELSIF (AxisX1.PowerOn = FALSE) THEN
		gLROutput.gstrError := 'X1 Axis not powered. ';
		bHardwareInitError := TRUE;
		RETURN;
	ELSIF (AxisX1.IsHomed = FALSE) THEN
		gLROutput.gstrError := 'X1 Axis not Homed. ';
		bHardwareInitError := TRUE;
		RETURN;
	END_IF

	// set axis parameters
	CASE tAxisX1Move.state OF
		eInitiate: 
			AxisX1Parameters.Acceleration := tAxisX1Move.rAccel; // set parameters for next movement
			AxisX1Parameters.Deceleration := tAxisX1Move.rAccel;
			AxisX1Parameters.Velocity := tAxisX1Move.rSpeed;
			AxisX1Parameters.Position := tAxisX1Move.siTargetPos;
			// Start movement
			AxisX1.MoveAbsolute := TRUE; 
			tAxisX1Move.state := eInMotion; 
		eInMotion:
			IF (AxisX1.InPosition = TRUE OR AxisX1.Position = tAxisX1Move.siTargetPos) THEN
				AxisX1.MoveAbsolute := FALSE;
				tAxisX1Move.state := eArrived;
			END_IF
		eArrived:
			tAxisX1Move.state := eInitiate;
	END_CASE
END_ACTION


ACTION Y1Move_act: 
	// Error handling
	IF (AxisY1.Active = FALSE) THEN
		gLROutput.gstrError := 'Y1 Axis disabled. ';
		bHardwareInitError := TRUE;
		RETURN;
	ELSIF (AxisY1.PowerOn = FALSE) THEN
		gLROutput.gstrError := 'Y1 Axis not powered. ';
		bHardwareInitError := TRUE;
		RETURN;
	ELSIF (AxisY1.IsHomed = FALSE) THEN
		gLROutput.gstrError := 'Y1 Axis not Homed. ';
		bHardwareInitError := TRUE;
		RETURN;
	END_IF

	// set axis parameters
	CASE tAxisY1Move.state OF
		eInitiate: 
			AxisY1Parameters.Acceleration := tAxisY1Move.rAccel; // set parameters for next movement
			AxisY1Parameters.Deceleration := tAxisY1Move.rAccel;
			AxisY1Parameters.Velocity := tAxisY1Move.rSpeed;
			AxisY1Parameters.Position := tAxisY1Move.siTargetPos;
			// Start movement
			AxisY1.MoveAbsolute := TRUE; 
			tAxisY1Move.state := eInMotion; 
		eInMotion:
			IF (AxisY1.InPosition = TRUE OR AxisY1.Position = tAxisY1Move.siTargetPos) THEN
				AxisY1.MoveAbsolute := FALSE;
				tAxisY1Move.state := eArrived;
			END_IF
		eArrived:
			IF (bAxisY1ArrivedFlag = TRUE) THEN
				bAxisY1ArrivedFlag := FALSE;
				tAxisY1Move.state := eInitiate;
				END_IF;
	END_CASE
END_ACTION

ACTION Y2Move_act: 
	// Error handling
	IF (AxisY2.Active = FALSE) THEN
		gLROutput.gstrError := 'Y2 Axis disabled. ';
		bHardwareInitError := TRUE;
		RETURN;
	ELSIF (AxisY2.PowerOn = FALSE) THEN
		gLROutput.gstrError := 'Y2 Axis not powered. ';
		bHardwareInitError := TRUE;
		RETURN;
	ELSIF (AxisY2.IsHomed = FALSE) THEN
		gLROutput.gstrError := 'Y2 Axis not Homed. ';
		bHardwareInitError := TRUE;
		RETURN;
	END_IF

	// set axis parameters
	CASE tAxisY2Move.state OF
		eInitiate: 
			AxisY2Parameters.Acceleration := tAxisY2Move.rAccel; // set parameters for next movement
			AxisY2Parameters.Deceleration := tAxisY2Move.rAccel;
			AxisY2Parameters.Velocity := tAxisY2Move.rSpeed;
			AxisY2Parameters.Position := tAxisY2Move.siTargetPos;
			// Start movement
			AxisY2.MoveAbsolute := TRUE; 
			tAxisY2Move.state := eInMotion; 
		eInMotion:
			IF (AxisY2.InPosition = TRUE OR AxisY2.Position = tAxisY2Move.siTargetPos) THEN
				AxisY2.MoveAbsolute := FALSE;
				tAxisY2Move.state := eArrived;
			END_IF
		eArrived:
			IF (bAxisY2ArrivedFlag = TRUE) THEN
				bAxisY2ArrivedFlag := FALSE;
				tAxisY2Move.state := eInitiate;
			END_IF;
	END_CASE
END_ACTION

ACTION Z1Move_act:
	// Error handling
	IF (AxisZ1.Active = FALSE) THEN
		gLROutput.gstrError := 'Z1 Axis disabled. ';
		bHardwareInitError := TRUE;
		RETURN;
	ELSIF (AxisZ1.PowerOn = FALSE) THEN
		gLROutput.gstrError := 'Z1 Axis not powered. ';
		bHardwareInitError := TRUE;
		RETURN;
	ELSIF (AxisZ1.IsHomed = FALSE) THEN
		gLROutput.gstrError := 'Z1 Axis not Homed. ';
		bHardwareInitError := TRUE;
		RETURN;
	END_IF

	// set axis parameters
	CASE tAxisZ1Move.state OF
		eInitiate: 
			AxisZ1Parameters.Acceleration := tAxisZ1Move.rAccel; // set parameters for next movement
			AxisZ1Parameters.Deceleration := tAxisZ1Move.rAccel;
			AxisZ1Parameters.Velocity := tAxisZ1Move.rSpeed;
			AxisZ1Parameters.Position := tAxisZ1Move.siTargetPos;
			// Start movement
			AxisZ1.MoveAbsolute := TRUE; 
			tAxisZ1Move.state := eInMotion; 
		eInMotion:
			IF (AxisZ1.InPosition = TRUE OR AxisZ1.Position = tAxisZ1Move.siTargetPos) THEN
				AxisZ1.MoveAbsolute := FALSE;
				tAxisZ1Move.state := eArrived;
			END_IF
		eArrived:
			tAxisZ1Move.state := eInitiate;
	END_CASE
END_ACTION

ACTION Z2Move_act: 
	// Error handling
	IF (AxisZ2.Active = FALSE) THEN
		gLROutput.gstrError := 'Z2 Axis disabled. ';
		bHardwareInitError := TRUE;
		RETURN;
	ELSIF (AxisZ2.PowerOn = FALSE) THEN
		gLROutput.gstrError := 'Z2 Axis not powered. ';
		bHardwareInitError := TRUE;
		RETURN;
	ELSIF (AxisZ2.IsHomed = FALSE) THEN
		gLROutput.gstrError := 'Z2 Axis not Homed. ';
		bHardwareInitError := TRUE;
		RETURN;
	END_IF

	// set axis parameters
	CASE tAxisZ2Move.state OF
		eInitiate: 
			AxisZ2Parameters.Acceleration := tAxisZ2Move.rAccel; // set parameters for next movement
			AxisZ2Parameters.Deceleration := tAxisZ2Move.rAccel;
			AxisZ2Parameters.Velocity := tAxisZ2Move.rSpeed;
			AxisZ2Parameters.Position := tAxisZ2Move.siTargetPos;
			// Start movement
			AxisZ2.MoveAbsolute := TRUE;
			tAxisZ2Move.state := eInMotion; 
		eInMotion:
			IF (AxisZ2.InPosition = TRUE OR AxisZ2.Position = tAxisZ2Move.siTargetPos) THEN
				AxisZ2.MoveAbsolute := FALSE;
				tAxisZ2Move.state := eArrived;
			END_IF
		eArrived:
			tAxisZ2Move.state := eInitiate;
	END_CASE
END_ACTION