
ACTION EmergencyCheck_act: 
	// Emergency check
	IF ((gLRInput.bEmergencyShutoff = TRUE) OR
		(X1.message.count.error <> 0) OR (X1.message.count.warning <> 0) OR
		(Y1.message.count.error <> 0) OR (Y1.message.count.warning <> 0) OR
		(Y2.message.count.error <> 0) OR (Y2.message.count.warning <> 0) OR
		(Z1.message.count.error <> 0) OR (Z1.message.count.warning <> 0) OR
		(Z2.message.count.error <> 0) OR (Z2.message.count.warning <> 0) OR
		//(AxisX1.Error = TRUE) OR
		//(AxisY1.Error = TRUE) OR (AxisY2.Error = TRUE) OR
		//(AxisZ1.Error = TRUE) OR (AxisZ2.Error = TRUE) OR
		(RotaryEMGS = TRUE) OR 
		(RotaryALM = TRUE)) THEN
		AxisX1.Stop := TRUE; // stop all axes
		AxisY1.Stop := TRUE;
		AxisY2.Stop := TRUE;
		AxisZ1.Stop := TRUE;
		AxisZ2.Stop := TRUE;
		RotaryPause;
		bErrorCheckError := TRUE;
		bAxisY1ArrivedFlag := FALSE;
		bAxisY2ArrivedFlag := FALSE;
		bAxisRArrivedFlag := FALSE;
		bAxisRStartFlag := FALSE;
		//bHardwareInitError := TRUE;
		bEmergencyCheckError := TRUE;
		// RETURN; // RETURN until emergency shutoff is FALSE
	ELSE	
		bEmergencyCheckError := FALSE;
		AxisX1.ErrorReset := FALSE;
		AxisY1.ErrorReset := FALSE;
		AxisY2.ErrorReset := FALSE;
		AxisZ1.ErrorReset := FALSE;
		AxisZ2.ErrorReset := FALSE;
		AxisX1.Stop := FALSE; // stop all axes
		AxisY1.Stop := FALSE;
		AxisY2.Stop := FALSE;
		AxisZ1.Stop := FALSE;
		AxisZ2.Stop := FALSE;
	END_IF;
	
	IF (gLRInput.bAxisErrorResetPBRE = TRUE) THEN
		AxisX1Acknowlege := TRUE;
		AxisY1Acknowlege := TRUE;
		AxisY2Acknowlege := TRUE;
		AxisZ1Acknowlege := TRUE;
		AxisZ2Acknowlege := TRUE;
		AxisRAcknowlege := TRUE;
	END_IF;
		
	IF (bEmergencyCheckError = TRUE) THEN		
		IF (AxisX1Acknowlege = TRUE) THEN
			action_statusX1 := ncaction(AxisX1.Axis,ncMESSAGE,ncACKNOWLEDGE);
			AxisX1.ErrorReset := TRUE;
			IF (action_statusX1 = ncOK) THEN
				AxisX1Acknowlege := FALSE;
				AxisX1.ErrorReset := FALSE;
			END_IF;
		END_IF;
		IF (AxisY1Acknowlege = TRUE) THEN
			action_statusY1 := ncaction(AxisY1.Axis,ncMESSAGE,ncACKNOWLEDGE);
			AxisY1.ErrorReset := TRUE;
			IF (action_statusY1 = ncOK) THEN
				AxisY1Acknowlege := FALSE;
				AxisY1.ErrorReset := FALSE;
			END_IF;
		END_IF;
		IF (AxisY2Acknowlege = TRUE) THEN
			action_statusY2 := ncaction(AxisY2.Axis,ncMESSAGE,ncACKNOWLEDGE);
			AxisY2.ErrorReset := TRUE;
			IF (action_statusY2 = ncOK) THEN
				AxisY2Acknowlege := FALSE;
				AxisY2.ErrorReset := FALSE;
			END_IF;
		END_IF;
		IF (AxisZ1Acknowlege = TRUE) THEN
			action_statusZ1 := ncaction(AxisZ1.Axis,ncMESSAGE,ncACKNOWLEDGE);
			AxisZ1.ErrorReset := TRUE;
			IF (action_statusZ1 = ncOK) THEN
				AxisZ1Acknowlege := FALSE;
				AxisZ1.ErrorReset := FALSE;
			END_IF;
		END_IF;
		IF (AxisZ2Acknowlege = TRUE) THEN
			action_statusZ2 := ncaction(AxisZ2.Axis,ncMESSAGE,ncACKNOWLEDGE);
			AxisZ2.ErrorReset := TRUE;
			IF (action_statusZ2 = ncOK) THEN
				AxisZ2Acknowlege := FALSE;
				AxisZ2.ErrorReset := FALSE;
			END_IF;
		END_IF;
		IF (AxisRAcknowlege = TRUE) THEN
			RotaryReset_act;
			AxisRAcknowlege := FALSE;
		END_IF;
		// gLROutput.gstrError := '';
	END_IF;
			
//	IF (AxisX1.Error = FALSE) THEN
//		AxisX1.ErrorReset := FALSE;
//	END_IF;
//	IF (AxisY1.Error = FALSE) THEN
//		AxisY1.ErrorReset := FALSE;
//	END_IF;
//	IF (AxisY2.Error = FALSE) THEN
//		AxisY2.ErrorReset := FALSE;
//	END_IF;
//	IF (AxisZ1.Error = FALSE) THEN
//		AxisZ1.ErrorReset := FALSE;
//	END_IF;
//	IF (AxisZ2.Error = FALSE) THEN
//		AxisZ2.ErrorReset := FALSE;
//	END_IF;				
END_ACTION

ACTION ErrorCheck_act :
	// Error check
	IF (gLROutput.gstrError <> '') THEN // if there is an error message
		// Reset input
		bErrorCheckError := TRUE;
		RETURN;
	END_IF;
	bErrorCheckError := FALSE;
END_ACTION