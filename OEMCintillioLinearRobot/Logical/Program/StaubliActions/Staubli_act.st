
ACTION Staubli_act: 
	IF (bStaubliRobotReady = TRUE) THEN
		tLR.usiSRReadyInd := usiGreenIndColor;
	ELSE
		tLR.usiSRReadyInd := usiRedIndColor;
	END_IF;
	
	IF (bStaubliRobotSafePos = TRUE) THEN
		tLR.usiSRSafePosInd := usiGreenIndColor;
	ELSE
		tLR.usiSRSafePosInd := usiRedIndColor;
	END_IF;
	
	CASE uiSRState OF
		// Idle State - Waiting for Linear Robot Configuration Selection
		// Enter "siFOUPPortNumberB1, siFOUPPortNumberB2" before selecting load/Unload type
		// Enter "usiSRMoveLoad or usiSRMoveUnload" before selecting load/Unload type for Staubli defined motion
		// Robot defined movement locations:
		// 0 = Robot Safe position, 
		// 1,2,3 = Carrier Staging area, 
		// 4 = LEFT CPC, 5=SRD, 
		// 6 = RIGHT CPC, 
		// 7 = Carrier Load/Unload Area, 
		// 8 = Finger Wash Station
		eIdleState :
			IF ((uiCurrentSuperState = eWait) AND
				(gLRInput.bRUN = FALSE) AND
				(bStaubliRobotSafePos = TRUE) AND
				(bStaubliRobotReady = TRUE)) THEN
				IF (gLRInput.bLoad200Batch1 = TRUE) THEN		// Load 200mm Wafer 1 Batch
					gLRInput.bWaferSize300mm := FALSE;
					gLRInput.bWaferSize200mm := TRUE;
					gLRInput.bUnloadGC := FALSE;
					gLRInput.bLoadGC := TRUE;
					gLRInput.bBatchCount2 := FALSE;
					gLRInput.bBatchCount1 := TRUE;
					gLRInput.bGC1 := TRUE;
					gLRInput.bBatch1GCBottomLevel := TRUE;
					gLRInput.bBatch1GCTopLevel := FALSE;
					gLRInput.siBatch1FPort := gLRInput.siFOUPPortNumberB1;
					gLRInput.bLoadWaferType := TRUE;
					gLRInput.bUnloadWaferType := FALSE;
					gLRInput.bLoad200Batch1 := FALSE;
					uiSRState := eCheckError;
				ELSIF (gLRInput.bUnload200Batch1 = TRUE) THEN	// Unload 200mm Wafer 1 Batch
					gLRInput.bWaferSize300mm := FALSE;
					gLRInput.bWaferSize200mm := TRUE;
					gLRInput.bLoadGC := FALSE;
					gLRInput.bUnloadGC := TRUE;
					gLRInput.bBatchCount2 := FALSE;
					gLRInput.bBatchCount1 := TRUE;
					gLRInput.bGC1 := TRUE;
					gLRInput.bBatch1GCBottomLevel := TRUE;
					gLRInput.bBatch1GCTopLevel := FALSE;
					gLRInput.siBatch1FPort := gLRInput.siFOUPPortNumberB1;
					gLRInput.bUnloadWaferType := TRUE;
					gLRInput.bLoadWaferType := FALSE;
					gLRInput.bUnload200Batch1 := FALSE;
					uiSRState := eCheckError;
				ELSIF (gLRInput.bLoad200Batch2BT = TRUE) THEN		// Load 200mm Wafer 2 Batches, Batch1 Bottom & Batch2 Top Carrier Location
					IF (gLRInput.siFOUPPortNumberB1 <> gLRInput.siFOUPPortNumberB2) THEN
						gLRInput.bWaferSize300mm := FALSE;
						gLRInput.bWaferSize200mm := TRUE;
						gLRInput.bUnloadGC := FALSE;
						gLRInput.bLoadGC := TRUE;
						gLRInput.bBatchCount1 := FALSE;
						gLRInput.bBatchCount2 := TRUE;
						gLRInput.bGC1 := TRUE;						
						gLRInput.bBatch1GCTopLevel := FALSE;
						gLRInput.bBatch1GCBottomLevel := TRUE;
						gLRInput.bBatch2GCTopLevel := TRUE;
						gLRInput.bBatch2GCBottomLevel := FALSE;
						gLRInput.siBatch1FPort := gLRInput.siFOUPPortNumberB1;
						gLRInput.siBatch2FPort := gLRInput.siFOUPPortNumberB2;
						gLRInput.bLoadWaferType := TRUE;
						gLRInput.bUnloadWaferType := FALSE;
						gLRInput.bLoad200Batch2BT := FALSE;
						uiSRState := eCheckError;
					END_IF
				ELSIF (gLRInput.bLoad200Batch2TB = TRUE) THEN		// Load 200mm Wafer 2 Batches, Batch1 Top & Batch2 Bottom Carrier Location
					IF (gLRInput.siFOUPPortNumberB1 <> gLRInput.siFOUPPortNumberB2) THEN
						gLRInput.bWaferSize300mm := FALSE;
						gLRInput.bWaferSize200mm := TRUE;
						gLRInput.bUnloadGC := FALSE;
						gLRInput.bLoadGC := TRUE;
						gLRInput.bBatchCount1 := FALSE;
						gLRInput.bBatchCount2 := TRUE;
						gLRInput.bGC1 := TRUE;						
						gLRInput.bBatch1GCTopLevel := TRUE;
						gLRInput.bBatch1GCBottomLevel := FALSE;
						gLRInput.bBatch2GCTopLevel := FALSE;
						gLRInput.bBatch2GCBottomLevel := TRUE;
						gLRInput.siBatch1FPort := gLRInput.siFOUPPortNumberB1;
						gLRInput.siBatch2FPort := gLRInput.siFOUPPortNumberB2;
						gLRInput.bLoadWaferType := TRUE;
						gLRInput.bUnloadWaferType := FALSE;
						gLRInput.bLoad200Batch2TB := FALSE;
						uiSRState := eCheckError;
					END_IF
				ELSIF (gLRInput.bUnload200Batch2TB = TRUE) THEN		// Unload 200mm Wafer 2 Batches, Batch1 Top & Batch2 Bottom Carrier Location
					IF (gLRInput.siFOUPPortNumberB1 <> gLRInput.siFOUPPortNumberB2) THEN
						gLRInput.bWaferSize300mm := FALSE;
						gLRInput.bWaferSize200mm := TRUE;
						gLRInput.bLoadGC := FALSE;
						gLRInput.bUnloadGC := TRUE;
						gLRInput.bBatchCount1 := FALSE;
						gLRInput.bBatchCount2 := TRUE;
						gLRInput.bGC1 := TRUE;
						gLRInput.bBatch1GCTopLevel := TRUE;
						gLRInput.bBatch1GCBottomLevel := FALSE;
						gLRInput.bBatch2GCTopLevel := FALSE;
						gLRInput.bBatch2GCBottomLevel := TRUE;
						gLRInput.siBatch1FPort := gLRInput.siFOUPPortNumberB1;
						gLRInput.siBatch2FPort := gLRInput.siFOUPPortNumberB2;
						gLRInput.bUnloadWaferType := TRUE;
						gLRInput.bLoadWaferType := FALSE;
						gLRInput.bUnload200Batch2TB := FALSE;
						uiSRState := eCheckError;
					END_IF
				ELSIF (gLRInput.bUnload200Batch2BT = TRUE) THEN		// Unload 200mm Wafer 2 Batches, Batch1 Bottom & Batch2 Top Carrier Location
					IF (gLRInput.siFOUPPortNumberB1 <> gLRInput.siFOUPPortNumberB2) THEN
						gLRInput.bWaferSize300mm := FALSE;
						gLRInput.bWaferSize200mm := TRUE;
						gLRInput.bLoadGC := FALSE;
						gLRInput.bUnloadGC := TRUE;
						gLRInput.bBatchCount1 := FALSE;
						gLRInput.bBatchCount2 := TRUE;
						gLRInput.bGC1 := TRUE;
						gLRInput.bBatch1GCTopLevel := FALSE;
						gLRInput.bBatch1GCBottomLevel := TRUE;
						gLRInput.bBatch2GCTopLevel := TRUE;
						gLRInput.bBatch2GCBottomLevel := FALSE;
						gLRInput.siBatch1FPort := gLRInput.siFOUPPortNumberB1;
						gLRInput.siBatch2FPort := gLRInput.siFOUPPortNumberB2;
						gLRInput.bUnloadWaferType := TRUE;
						gLRInput.bLoadWaferType := FALSE;
						gLRInput.bUnload200Batch2BT := FALSE;
						uiSRState := eCheckError;
					END_IF
				ELSIF (gLRInput.bLoad300Batch1 = TRUE) THEN		// Load 300mm Wafer 1 Batch	
					gLRInput.bBatch1GCBottomLevel := FALSE;
					gLRInput.bBatch1GCTopLevel := FALSE;
					gLRInput.bWaferSize200mm := FALSE;
					gLRInput.bBatchCount2 := FALSE;
					gLRInput.bUnloadGC := FALSE;
					gLRInput.siBatch1FPort := gLRInput.siFOUPPortNumberB1;
					gLRInput.bWaferSize300mm := TRUE;					
					gLRInput.bLoadGC := TRUE;					
					gLRInput.bBatchCount1 := TRUE;
					gLRInput.bGC1 := TRUE;
					gLRInput.bLoadWaferType := TRUE;
					gLRInput.bUnloadWaferType := FALSE;
					gLRInput.bLoad300Batch1 := FALSE;
					uiSRState := eCheckError;
				ELSIF (gLRInput.bUnload300Batch1 = TRUE) THEN	// Unload 300mm Wafer 1 Batch
					gLRInput.bBatch1GCBottomLevel := FALSE;
					gLRInput.bBatch1GCTopLevel := FALSE;
					gLRInput.bWaferSize200mm := FALSE;
					gLRInput.bLoadGC := FALSE;
					gLRInput.bBatchCount2 := FALSE;
					gLRInput.siBatch1FPort := gLRInput.siFOUPPortNumberB1;
					gLRInput.bWaferSize300mm := TRUE;
					gLRInput.bUnloadGC := TRUE;					
					gLRInput.bBatchCount1 := TRUE;
					gLRInput.bGC1 := TRUE;	
					gLRInput.bUnloadWaferType := TRUE;
					gLRInput.bLoadWaferType := FALSE;
					gLRInput.bUnload300Batch1 := FALSE;
					uiSRState := eCheckError;
				END_IF
			END_IF;
			
		// Check if Error with Configuration Inputs
		eCheckError :
			IF (bErrorCheckError = TRUE) THEN
				uiSRState := eIdleState;
			ELSE
				uiSRState := eWaferHandlingType;
			END_IF;
			
		// Check if Error with Configuration Inputs
		eWaferHandlingType :
			IF (gLRInput.bLoadWaferType = TRUE) THEN
				uiSRState := eRunLinearRobot;
			ELSIF (gLRInput.bUnloadWaferType = TRUE) THEN
				uiSRState := eRunStaubliRobot;
			ELSIF (bErrorCheckError = TRUE) THEN 
				uiSRState := eIdleState;
			END_IF;
					
		// Run Linear Robot if Staubli is in a Safe Position
		eRunLinearRobot :
			IF (bStaubliRobotSafePos = TRUE) THEN							
				gLRInput.bRUN := TRUE;
				uiSRState := eLinearRobotMoveComplete;
			ELSIF (bErrorCheckError = TRUE) THEN
				uiSRState := eIdleState;
			END_IF;
			
		// Wait until Linear Robot is finished with movement			
		eLinearRobotMoveComplete :
			IF (gLROutput.bComplete = TRUE) THEN
				gLRInput.bWaferSize300mm := FALSE;
				gLRInput.bWaferSize200mm := FALSE;
				gLRInput.bLoadGC := FALSE;
				gLRInput.bUnloadGC := FALSE;
				gLRInput.bBatchCount1 := FALSE;
				gLRInput.bBatchCount2 := FALSE;
				gLRInput.bGC1 := FALSE;
				gLRInput.bGC2 := FALSE;
				gLRInput.bBatch1GCBottomLevel := FALSE;
				gLRInput.bBatch1GCTopLevel := FALSE;
				gLRInput.bBatch2GCBottomLevel := FALSE;
				gLRInput.bBatch2GCTopLevel := FALSE;
				IF (gLRInput.bLoadWaferType = TRUE) THEN
					gLROutput.bComplete := FALSE;
					uiSRState := eRunStaubliRobot;
				ELSIF (gLRInput.bUnloadWaferType = TRUE) THEN
					gLROutput.bComplete := FALSE;
					gLRInput.bUnloadWaferType := FALSE;
					uiSRState := eIdleState;
				END_IF	
			ELSIF (bErrorCheckError = TRUE) THEN
				uiSRState := eIdleState;
			END_IF;
			
		// Run Staubli Robot after Linear Robot movement is complete
		// Robot defined movement locations:
		// 0 = Robot Safe position 
		// 1,2,3 = Carrier Staging area 
		// 4 = LEFT CPC
		// 5 = SRD
		// 6 = RIGHT CPC
		// 7 = Carrier Load/Unload Area 
		// 8 = Finger Wash Station
		eRunStaubliRobot :
			IF (bStaubliRobotReady = TRUE) THEN
				IF (gLRInput.bLoadWaferType = TRUE) THEN
					gLROutput.usiSRMoveStartPosition := usiLoadStartPosition;	// Set to Staubli Start Position
					gLROutput.usiSRMoveTargetPosition := usiLoadTargetPosition;	// Set to Staubli Target Position
					uiSRState := eResetStaubliRobotCmd;
				ELSIF (gLRInput.bUnloadWaferType = TRUE) THEN
					gLROutput.usiSRMoveStartPosition := usiUnloadStartPosition;		// Set to Staubli Start Position
					gLROutput.usiSRMoveTargetPosition := usiUnloadTargetPosition;	// Set to Staubli Target Position
					uiSRState := eResetStaubliRobotCmd;
				END_IF
			ELSIF (bErrorCheckError = TRUE) THEN
				uiSRState := eIdleState;
			END_IF;
			
		// Set Staubli Robot RUN Command
		eResetStaubliRobotCmd :
			IF ((bStaubliRobotReady = TRUE) AND
				(uiCurrentSuperState = eWait) AND
				(gLRInput.bRUN = FALSE) AND
				(bStaubliRobotRunCMD = FALSE)) THEN
				bStaubliRobotRunCMD := TRUE;
				uiSRState := eWaitStaubliRobotOnceAck;
			ELSIF (bErrorCheckError = TRUE) THEN
				uiSRState := eIdleState;
			END_IF;
			
		// Wait until Staubli Robot is starting movement and RUN Command is Reset	
		eWaitStaubliRobotOnceAck :
			IF ((bStaubliRobotSafePos = FALSE) AND
				(bStaubliRobotRunCMD = FALSE)) THEN
				uiSRState := eStaubliRobotMoveComplete;
			ELSIF (bErrorCheckError = TRUE) THEN
				uiSRState := eIdleState;
			END_IF;
			
		// Wait until Staubli Robot reaches its Safe Position
		eStaubliRobotMoveComplete :
			IF (gLRInput.obStaubliSafePosRE = TRUE) THEN	// Wait for Rising Edge of "gLRInput.bStaubliSafePos" signal
				IF (gLRInput.bLoadWaferType = TRUE) THEN
					gLRInput.bLoadWaferType := FALSE;
					uiSRState := eIdleState;
				ELSIF (gLRInput.bUnloadWaferType = TRUE) THEN
					uiSRState := eRunLinearRobot;
				END_IF
			ELSIF (bErrorCheckError = TRUE) THEN
				uiSRState := eIdleState;
			END_IF;
		
	END_CASE;
	
	// Auto Cycle Time for 1 complete cycle 
	dtCycleTimeTON(IN:= bCycleTimeStart, PT:= T#900s0ms);
	dtCycleTime := dtCycleTimeTON.ET; 
	IF (uiSRAutoCycleState = eCycleCounts) THEN
		dtPrevCycleTime := dtCycleTime;
	END_IF;
	IF (dtCycleTimeTON.Q) THEN
		bCycleTimeStart := FALSE;
	END_IF;

	
	// Stop after current runnuing Auto Cycle is complete
	IF (gLRInput.bAutoCycleMode = FALSE) THEN
		uiAutoCycleCounts := gLRInput.uiAutoCycleCnts;
	END_IF;
	
	// Auto Cycle Load and Unload for a given count
	CASE uiSRAutoCycleState OF
		// Idle State - Waiting AutoCycle Mode enable
		eIdle :
			IF (gLRInput.bAutoCycleMode = TRUE) THEN
				uiSRAutoCycleState := eAutoCycleStart;
			END_IF;
		// Start AutoCycle	
		eAutoCycleStart :
			IF (gLRInput.bAutoCycleMode = FALSE ) THEN
				uiSRAutoCycleState := eIdle;
			ELSIF (gLRInput.bStartAutoCyclePBRE = TRUE) THEN
				uiSRAutoCycleState := eResetCounts;
			END_IF;
			
		// Reset Cycle Counts
		eResetCounts :
			uiAutoCycleCounts := 0;
			uiSRAutoCycleState := eCycleCountCheck;
			
		// Check if Auto Cycle is Complete
		eCycleCountCheck :
			IF (uiAutoCycleCounts < gLRInput.uiAutoCycleCnts) THEN
				bCycleTimeStart := TRUE;
				uiSRAutoCycleState := eLoadBatchBT;
			ELSE
				uiSRAutoCycleState := eIdle;
			END_IF;
		
		// Run Load Batch 2 Bottom 1st, Top 2nd of Carrier
		eLoadBatchBT :
			gLRInput.bLoad200Batch2BT := TRUE;	
			uiSRAutoCycleState := eWaitLoadComplete;
			
		// Wait until Wafer loading is complete
		eWaitLoadComplete :
			IF (gLRInput.bLoad200Batch2BT = FALSE) THEN
				IF ((uiSRState = eIdleState) AND
					(uiCurrentSuperState = eWait) AND
					(gLRInput.bRUN = FALSE)) THEN
					uiSRAutoCycleState := eUnLoadBatchTB;
				END_IF;
			END_IF;
		
		// Run Load Batch 2 Bottom 1st, Top 2nd of Carrier
		eUnLoadBatchTB :
			gLRInput.bUnload200Batch2TB := TRUE;	
			uiSRAutoCycleState := eWaitUnLoadComplete;
		
		// Wait until Wafer unloading is complete
		eWaitUnLoadComplete :
			IF (gLRInput.bUnload200Batch2TB = FALSE) THEN
				IF ((uiSRState = eIdleState) AND
					(uiCurrentSuperState = eWait) AND
					(gLRInput.bRUN = FALSE)) THEN
					uiSRAutoCycleState := eCycleCounts;
				END_IF;
			END_IF;
			
		// Increment Cycle Counter
		eCycleCounts :
			bCycleTimeStart := FALSE;
			uiAutoCycleCounts := uiAutoCycleCounts + 1;
			uiSRAutoCycleState := eCycleCountCheck;
			
	END_CASE;

END_ACTION

// Staubli Robot Motion Control State Machine
ACTION StaubliMove_act:
	
	CASE uiSRMoveState OF
		// Idle state for Staubli Move to Position Commands
		eIdleMoveState :
			IF ((bStaubliRobotReady = TRUE) AND
				(uiCurrentSuperState = eWait) AND
				(gLRInput.bRUN = FALSE)) THEN
				IF (bStaubliRobotRunCMD = TRUE) THEN
					uiSRMoveState := eWriteMovePositions;
				END_IF;
			ELSIF (bErrorCheckError = TRUE) THEN
				RETURN;
			END_IF;
		// Set Start and Target position to Staubli Robot 				
		eWriteMovePositions :
			gLROutput.bWriteStaubliRobotOnce := FALSE;
			gLROutput.bWriteStaubliTargetOnce := FALSE;
			IF (bStaubliRobotReady = TRUE) THEN
				gLROutput.uiWriteStaubliRobot := gLROutput.usiSRMoveStartPosition;	// Write to Staubli defined 16 bit command for Start Pos. was "usiSRMoveLoad"
				gLROutput.uiWriteStaubliTargetPosition := gLROutput.usiSRMoveTargetPosition;	// Write to Staubli defined 16 bit command for Target Pos. was "usiSRMoveUnload"
				uiSRMoveState := eWriteStartPosition;
			ELSIF (bErrorCheckError = TRUE) THEN
				uiSRMoveState := eIdleMoveState;
			END_IF;
		
		// Write Start position to Staubli Robot 
		eWriteStartPosition :
			IF (gLRInput.bReadStaubliRobotOnceAck = FALSE) THEN
				gLROutput.bWriteStaubliRobotOnce := TRUE;
				uiSRMoveState := eWriteTargetPosition;
			END_IF;
		
		// Write Target position to Staubli Robot 
		eWriteTargetPosition :
			IF (gLRInput.bReadStaubliTargetPosAck = FALSE) THEN
				gLROutput.bWriteStaubliTargetOnce := TRUE;
				uiSRMoveState := eWaitStaubliOnceAck;
			END_IF;
			
		// Wait for Acknowlege signals			
		eWaitStaubliOnceAck :
			IF ((gLRInput.bReadStaubliRobotOnceAck = TRUE) AND
				(gLRInput.bReadStaubliTargetPosAck = TRUE)) THEN
				gLROutput.bWriteStaubliRobotOnce := FALSE;
				gLROutput.bWriteStaubliTargetOnce := FALSE;
				gLROutput.bStaubliRunCMDOnce := FALSE;
				uiSRMoveState := eSetRunCMDStaubliRobot;
			END_IF;
			
		// Run Staubli Robot from its Start -> Target position 
		eSetRunCMDStaubliRobot :
			gLROutput.bStaubliRunCMD := TRUE;
			IF (gLRInput.bReadStaubliRunCMDAck = FALSE) THEN
				gLROutput.bStaubliRunCMDOnce := TRUE;
				uiSRMoveState := eResetOnceSignal;
			END_IF;
			
		// Reset Staubli Once signal to be ready for next command
		eResetOnceSignal :
			IF (gLRInput.bReadStaubliRunCMDAck = TRUE) THEN
				IF (bStaubliRobotSafePos = FALSE) THEN
					gLROutput.bStaubliRunCMDOnce := FALSE;
					uiSRMoveState := eResetRunCMD;
				END_IF;
			END_IF;
		
		// Reset Staubli Run Command signal to be ready for next command
		eResetRunCMD :
			IF (gLRInput.bReadStaubliRunCMDAck = FALSE) THEN
				gLROutput.bStaubliRunCMD := FALSE;
				gLROutput.bStaubliRunCMDOnce := TRUE;
				uiSRMoveState := eResetOnceRunCMD;
			END_IF;
			
		// Reset Staubli RUN CMD Once signal to be ready for next command
		eResetOnceRunCMD :
			IF (gLRInput.bReadStaubliRunCMDAck = TRUE) THEN
				bStaubliRobotRunCMD := FALSE;					
				gLROutput.bStaubliRunCMDOnce := FALSE;
				uiSRMoveState := eIdleMoveState;
			END_IF;
		
	END_CASE;
	
END_ACTION 