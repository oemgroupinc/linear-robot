ACTION StartGCAlign_act:
	uiCurrentState := eGCAlign;
	uiCurrentSubState := eZ2SubState;
	// This must be the first substate in the State
END_ACTION

ACTION GCAlign_act: 
	CASE uiCurrentSubState OF 
		eZ2SubState : // Load GC Align Z2 
			
			tAxisZ2Move.rAccel := tLR.siSlowAccelZ2;
			tAxisZ2Move.rSpeed := tLR.siSlowVelZ2;
			IF (gLRInput.bWaferSize300mm = TRUE) THEN // bypass for 300mm wafers
				tAxisZ2Move.siTargetPos := tTargetGC.siZ2300Pos;
			ELSE
				CASE tLR.Process.uiBatchTracker OF 
					1: 
						IF (tGCLevelBatch1.bTop = TRUE) THEN // batch 1 top
							tAxisZ2Move.siTargetPos := tTargetGC.siZ2TopPos;
						ELSIF (tGCLevelBatch1.bBottom = TRUE) THEN // batch 1 bottom
							tAxisZ2Move.siTargetPos := tTargetGC.siZ2BottomPos;
						END_IF;
					2: 
						IF (tGCLevelBatch2.bTop = TRUE) THEN // batch 2 top
							tAxisZ2Move.siTargetPos := tTargetGC.siZ2TopPos;
						ELSIF (tGCLevelBatch2.bBottom = TRUE) THEN // batch 2 bottom
							tAxisZ2Move.siTargetPos := tTargetGC.siZ2BottomPos;
						END_IF;
				END_CASE;
			END_IF;
			
			Z2Move_act;
			IF (tAxisZ2Move.state = eInMotion) THEN
				uiCurrentSubState := eX1SubState;
			END_IF;
			
		eX1SubState : // Load GC Align X
			tAxisX1Move.siTargetPos := tTargetGC.siX1Pos;	
			X1Move_act; 
			Z2Move_act;
			IF (tAxisX1Move.state = eArrived) THEN
				// Set next state
				uiCurrentSubState := eZ1SubState;
			END_IF;
					
		eZ1SubState : // Load GC Align Z1
			
			IF (tLR.bWaferPossession = TRUE) THEN
				tAxisZ1Move.siTargetPos := tTargetGC.siZ1LRPossession; 
				tAxisZ1Move.rAccel := tLR.siSlowAccelZ1;
				tAxisZ1Move.rSpeed := tLR.siSlowVelZ1;
			ELSE
				tAxisZ1Move.siTargetPos := tTargetGC.siZ1GCPossession; 
				tAxisZ1Move.rAccel := tLR.siFastAccelZ1;
				tAxisZ1Move.rSpeed := tLR.siFastVelZ1;
			END_IF;
			
			Z1Move_act; 
			Z2Move_act;
			IF (tAxisZ1Move.state = eArrived) THEN
				bAxisRArrivedFlag := FALSE;
				uiCurrentSubState := eRSubState;
			END_IF;
						
		eRSubState : 
			tAxisRMove.siTargetPos := -270;
			bAxisRStartFlag := TRUE;
			RMove_act;
			Z2Move_act;
			IF (bAxisRArrivedFlag = TRUE AND tAxisZ2Move.state = eArrived) THEN
				bAxisRStartFlag := FALSE;
				// Set next substate
				uiCurrentSubState := eY1SubState;
			END_IF;
			
		eY1SubState : // Load GC Align Y1			
			tAxisY1Move.siTargetPos := tTargetGC.siY1AccessPos;
			bAxisY1ArrivedFlag := TRUE;
			Y1Move_act; 			
			IF (tAxisY1Move.state = eArrived) THEN
				// Set next state
				uiCurrentSubState := eY2SubState;
			END_IF;
		
		eY2SubState : // Load GC Align Y2
			tAxisY2Move.siTargetPos := tTargetGC.siY2AccessPos;
			bAxisY2ArrivedFlag := TRUE;
			Y2Move_act; 			
			IF (tAxisY2Move.state = eArrived) THEN
				// Set next state
				IF (tLR.bWaferPossession = TRUE) THEN
					StartLoadGC_act; 
				ELSE
					StartUnloadGC_act;
				END_IF;
			END_IF;
	END_CASE;
				
END_ACTION
