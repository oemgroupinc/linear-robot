ACTION StartUnloadGC_act:
	uiCurrentState := eUnloadGC;
	uiCurrentSubState := eZ1SubState;
	// This must be the first substate in the State
END_ACTION

ACTION UnloadGC_act : 
	CASE uiCurrentSubState OF
		eZ1SubState : // Unload GC Z1
			
			tAxisZ1Move.rAccel := tLR.siSlowAccelZ1; // set parameters for next movement
			tAxisZ1Move.rSpeed := tLR.siSlowVelZ1;
			tAxisZ1Move.siTargetPos := tTargetGC.siZ1LRPossession;
			Z1Move_act;
			
			IF (tAxisZ1Move.state = eArrived) THEN
				// Set next state
				tLR.bWaferPossession := TRUE;
				SetAcceleration_act;
				uiCurrentSubState := eY1SubState;
			END_IF;
			
		eY1SubState : // Unload GC Y1			
			tAxisY1Move.siTargetPos := tLR.siY1SafePos;
			bAxisY1ArrivedFlag := TRUE;
			Y1Move_act; 			
			IF (tAxisY1Move.state = eArrived) THEN
				// Set next state
				uiCurrentSubState := eY2SubState;
			END_IF;
		
		eY2SubState : // Unload GC Y2
			tAxisY2Move.siTargetPos := tLR.siY2SafePos;
			bAxisY2ArrivedFlag := TRUE; 
			Y2Move_act;			
			IF (tAxisY2Move.state = eArrived) THEN
				bAxisRArrivedFlag := FALSE;
				// Set next state
				uiCurrentSubState := eRSubState; 
			END_IF;
		
		eRSubState :
			tAxisRMove.siTargetPos := -180;
			bAxisRStartFlag := TRUE;
			RMove_act;
			IF (bAxisRArrivedFlag = TRUE) THEN
				bAxisRStartFlag := FALSE;
				// Set next substate
				StartFPAlign_act;
			END_IF;
	END_CASE;
END_ACTION
