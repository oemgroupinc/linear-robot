ACTION StartFPAlign_act:
	uiCurrentState := eFPAlign;
	uiCurrentSubState := eX1SubState;
	// This must be the first substate in the State
END_ACTION

ACTION FPAlign_act: 
	
	CASE uiCurrentSubState OF
		eX1SubState : // Unload FP Align X
			tAxisX1Move.siTargetPos := tCurrentTargetFP.siX1Pos;
			X1Move_act;
			IF (tAxisX1Move.state = eArrived) THEN
				// Set next substate
				uiCurrentSubState := eZ1SubState;
			END_IF;

		eZ1SubState : // Unload FP Align Z1    
			IF (tLR.bWaferPossession = TRUE) THEN
				tAxisZ1Move.siTargetPos := tCurrentTargetFP.siZ1LRPossession;
				tAxisZ1Move.rAccel := tLR.siSlowAccelZ1;
				tAxisZ1Move.rSpeed := tLR.siSlowVelZ1;
			ELSE
				tAxisZ1Move.siTargetPos := tCurrentTargetFP.siZ1FPPossession;
				tAxisZ1Move.rAccel := tLR.siFastAccelZ1;
				tAxisZ1Move.rSpeed := tLR.siFastVelZ1;
			END_IF;
			// Start movement
			Z1Move_act;
			IF (tAxisZ1Move.state = eArrived) THEN
				bAxisRArrivedFlag := FALSE;
				// Set next state
				uiCurrentSubState := eRSubState;
			END_IF;
					
		eRSubState :
			tAxisRMove.siTargetPos := -90;
			bAxisRStartFlag := TRUE;
			RMove_act;
			IF (bAxisRArrivedFlag = TRUE) THEN
				bAxisRStartFlag := FALSE;
				// Set next substate
				uiCurrentSubState := eY1SubState;
			END_IF;
			
		eY1SubState : // Unload FP Align Y1
			tAxisY1Move.siTargetPos := tCurrentTargetFP.siY1AccessPos;
			// Start movement
			bAxisY1ArrivedFlag := TRUE;
			Y1Move_act;
			IF (tAxisY1Move.state = eArrived) THEN
				// Set next state
				uiCurrentSubState := eY2SubState;
			END_IF;
						
		eY2SubState : // Unload FP Align Y2
			tAxisY2Move.siTargetPos := tCurrentTargetFP.siY2AccessPos;
			// Start movement
			bAxisY2ArrivedFlag := TRUE;
			Y2Move_act; 
			IF (tAxisY2Move.state = eArrived) THEN
				IF (tLR.bWaferPossession = TRUE) THEN
					StartLoadFP_act;
				ELSE
					StartUnloadFP_act;
				END_IF;
			END_IF;
	END_CASE;
				
END_ACTION
