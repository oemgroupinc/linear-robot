ACTION StartUnloadFP_act:
	uiCurrentState := eUnloadFP;
	uiCurrentSubState := eZ1SubState;
	// This must be the first substate in the State
END_ACTION

ACTION UnloadFP_act : 
	CASE uiCurrentSubState OF
		eZ1SubState : // Unload FP Z1 
			// set axis parameters
			tAxisZ1Move.rAccel := tLR.siSlowAccelZ1;
			tAxisZ1Move.rSpeed := tLR.siSlowVelZ1;
			tAxisZ1Move.siTargetPos := tCurrentTargetFP.siZ1LRPossession; //Destination is the LR
			// Start movement
			Z1Move_act; 
		
			IF (tAxisZ1Move.state = eArrived) THEN
				// Set next state
				tLR.bWaferPossession := TRUE;
				SetAcceleration_act;
				uiCurrentSubState := eY1SubState;
			END_IF;
		
		eY1SubState : // Unload FP Y1
			// set axis parameters
			tAxisY1Move.siTargetPos := tLR.siY1SafePos;            
			// Start movement
			bAxisY1ArrivedFlag := TRUE;
			Y1Move_act; 		
			IF (tAxisY1Move.state = eArrived) THEN
				// Set next state
				uiCurrentSubState := eY2SubState;
			END_IF;
		                         
		eY2SubState : // Unload FP Y2
			// set axis parameters		
			tAxisY2Move.siTargetPos := tLR.siY2SafePos;
			// Start movement			
			bAxisY2ArrivedFlag := TRUE;			
			Y2Move_act; 		
			IF (tAxisY2Move.state = eArrived) THEN
				// Set next UPPER state and next SubState:
				bAxisRArrivedFlag := FALSE;
				uiCurrentSubState := eRSubState;
			END_IF;
		
		eRSubState : 
			tAxisRMove.siTargetPos := -180;
			bAxisRStartFlag := TRUE;
			RMove_act;
			IF (bAxisRArrivedFlag = TRUE) THEN
				bAxisRStartFlag := FALSE;
				// Set next substate
				StartGCAlign_act;
			END_IF;
			
	END_CASE;
END_ACTION
