ACTION StartSafeAlign_act:
	uiCurrentState := eSafeAlign;
	uiCurrentSubState := eRSubState;
	// This must be the first substate in the State
END_ACTION

ACTION SafeAlign_act : 
	CASE uiCurrentSubState OF 
					    
		eRSubState : 
			tAxisRMove.siTargetPos := -180;
			bAxisRStartFlag := TRUE;
			RMove_act;
			IF (bAxisRArrivedFlag = TRUE) THEN
				bAxisRStartFlag := FALSE;
				// Set next substate
				uiCurrentSubState := eZ2SubState;
			END_IF;
			
		eZ2SubState : // Safe Align Z2
			IF (tLR.Process.bDUALLOAD = TRUE OR tLR.Process.bLOAD = TRUE) THEN
				tAxisZ2Move.rAccel := tLR.siSlowAccelZ2; // set parameters for next movement
				tAxisZ2Move.rSpeed := tLR.siSlowVelZ2;
			ELSE
				tAxisZ2Move.rAccel := tLR.siFastAccelZ2; // set parameters for next movement
				tAxisZ2Move.rSpeed := tLR.siFastVelZ2;
			END_IF
			tAxisZ2Move.siTargetPos := tLR.siZ2SafePos;
			Z2Move_act; 
			
			IF ((tAxisZ2Move.state = eInMotion) OR
				(tAxisZ2Move.state = eArrived)) THEN
				// Set next state
				uiCurrentSubState := eX1SubState;
			END_IF;
		
		eX1SubState : // Safe Align X			
			tAxisX1Move.siTargetPos := tLR.siX1SafePos;
			X1Move_act; 			
			IF (tAxisX1Move.state = eArrived) THEN
				// Set next state
				uiCurrentSubState := eY1SubState;
			END_IF;
		
		eY1SubState : // Safe Align Y1			
			tAxisY1Move.siTargetPos := tLR.siY1SafePos;
			bAxisY1ArrivedFlag := TRUE;
			Y1Move_act; 			
			IF (tAxisY1Move.state = eArrived) THEN
				// Set next state
				uiCurrentSubState := eY2SubState;
			END_IF;
						
		eY2SubState : // Safe Align Y2
			tAxisY2Move.siTargetPos := tLR.siY2SafePos;
			bAxisY2ArrivedFlag := TRUE;
			Y2Move_act; 			
			IF (tAxisY2Move.state = eArrived) THEN
				// Set next state
				uiCurrentSubState := eZ1SubState;
			END_IF;
		
		eZ1SubState : // Safe Align Z1			
			tAxisZ1Move.rAccel := tLR.siFastAccelZ1; // set parameters for next movement
			tAxisZ1Move.rSpeed := tLR.siFastVelZ1;
			tAxisZ1Move.siTargetPos := tLR.siZ1SafePos;    
			Z1Move_act; 
			
			IF (tAxisZ1Move.state = eArrived) THEN
				// Set next state
				uiCurrentSubState := eSubStateFinished;
			END_IF;
		
		eSubStateFinished : // Finished and next State
			// Next step for all Processes: Wait 
			uiCurrentSuperState := eWait;
			gLROutput.bComplete := TRUE;
					
	END_CASE;
END_ACTION
