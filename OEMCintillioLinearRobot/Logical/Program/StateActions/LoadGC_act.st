
ACTION StartLoadGC_act:
	uiCurrentState := eLoadGC;
	uiCurrentSubState := eZ1SubState;
	// This must be the first substate in the State
END_ACTION

ACTION LoadGC_act: 
	CASE uiCurrentSubState OF 
		eZ1SubState : // Load GC Z1 *)
			
			tAxisZ1Move.rAccel := tLR.siSlowAccelZ1;
			tAxisZ1Move.rSpeed := tLR.siSlowVelZ1;
			tAxisZ1Move.siTargetPos := tTargetGC.siZ1GCPossession;
			Z1Move_act;
			
			IF (tAxisZ1Move.state = eArrived) THEN
				// Set next state
				tLR.bWaferPossession := FALSE;
				SetAcceleration_act;
				uiCurrentSubState := eY1SubState;
			END_IF;

		eY1SubState : // Unload FP Y1
			tAxisY1Move.siTargetPos := tLR.siY1SafePos; 
			bAxisY1ArrivedFlag := TRUE;
			Y1Move_act; 			
			IF (tAxisY1Move.state = eArrived) THEN
				bAxisRArrivedFlag := FALSE;
				// Set next state
				uiCurrentSubState := eY2SubState;
			END_IF;
			                         
		eY2SubState : // Unload FP Y2
			tAxisY2Move.siTargetPos := tLR.siY2SafePos;
			bAxisY2ArrivedFlag := TRUE;
			Y2Move_act; 			
			IF (tAxisY2Move.state = eArrived) THEN
				// Set next UPPER state and next SubState:
				IF (tLR.Process.bLOAD = TRUE OR tLR.Process.uiBatchTracker = 2) THEN 
					// LR has completed LOAD Process:
					StartSafeAlign_act; 
				ELSIF (tLR.Process.bDUALLOAD = TRUE AND tLR.Process.uiBatchTracker = 1) THEN 
					tAxisRMove.siTargetPos := -180;
					bAxisRStartFlag := TRUE;
					RMove_act;
					IF (bAxisRArrivedFlag = TRUE) THEN
						bAxisRStartFlag := FALSE;
						// LR needs to return to get another batch:
						StartFPAlign_act;
						// Adjust batch tracker
						tLR.Process.uiBatchTracker := 2;
						tCurrentTargetFP := tTargetFP2;
					END_IF;
				END_IF;
			END_IF;
	END_CASE;
END_ACTION
