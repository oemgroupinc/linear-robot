ACTION SetAcceleration_act : 
	IF (tLR.bWaferPossession = TRUE) THEN
		tAxisX1Move.rAccel := tLR.siSlowAccelX1;
		tAxisX1Move.rSpeed := tLR.siSlowVelX1;
		tAxisY1Move.rAccel := tLR.siSlowAccelY1;
		tAxisY1Move.rSpeed := tLR.siSlowVelY1;
		tAxisY2Move.rAccel := tLR.siSlowAccelY2;
		tAxisY2Move.rSpeed := tLR.siSlowVelY2;
		tAxisRMove.rAccel := tLR.siSlowAccelR;
		tAxisRMove.rSpeed := tLR.siSlowSpeedR;
	ELSE
		tAxisX1Move.rAccel := tLR.siFastAccelX1;
		tAxisX1Move.rSpeed := tLR.siFastVelX1;
		tAxisY1Move.rAccel := tLR.siFastAccelY1;
		tAxisY1Move.rSpeed := tLR.siFastVelY1;
		tAxisY2Move.rAccel := tLR.siFastAccelY2;
		tAxisY2Move.rSpeed := tLR.siFastVelY2;
		tAxisRMove.rAccel := tLR.siFastAccelR;
		tAxisRMove.rSpeed := tLR.siFastSpeedR;
	END_IF
END_ACTION
