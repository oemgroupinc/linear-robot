PROGRAM _CYCLIC
	
	// Staubli Interface
	Staubli_act;
	StaubliMove_act;
	bStaubliRobotReady := gLRInput.bStaubliReady;
	bStaubliRobotSafePos := gLRInput.bStaubliSafePos;
	fbSRSafePosTrigRE(CLK := gLRInput.bStaubliSafePos);
	gLRInput.obStaubliSafePosRE := fbSRSafePosTrigRE.Q;
	
	// X1 Axis Jog Test Modes - 0 = Off, 1 = Coarse, 2 = Fine		
	fbJogNegAxisX1PBTrigRE(CLK := gLRInput.bJogNegAxisX1PB);
	gLRInput.bJogNegAxisX1PBRE := fbJogNegAxisX1PBTrigRE.Q;	
	
	fbJogPosAxisX1PBTrigRE(CLK := gLRInput.bJogPosAxisX1PB);
	gLRInput.bJogPosAxisX1PBRE := fbJogPosAxisX1PBTrigRE.Q;	
	
	fbHomeAllAxisPBTrigRE(CLK := gLRInput.bHomeAllAxisPB);
	gLRInput.bHomeAllAxisPBRE := fbHomeAllAxisPBTrigRE.Q;	
	
	fbRotateAxisRPBTrigRE(CLK := gLRInput.bRotateAxisRPB);
	gLRInput.bRotateAxisRPBRE := fbRotateAxisRPBTrigRE.Q;
	
	fbSetTargetPosPBTrigRE(CLK := gLRInput.bSetTargetPosPB);
	gLRInput.bSetTargetPosPBRE := fbSetTargetPosPBTrigRE.Q;
	
	fbStartAutoCycleTrigRE(CLK := gLRInput.bStartAutoCyclePB);
	gLRInput.bStartAutoCyclePBRE := fbStartAutoCycleTrigRE.Q;
	
	fbAxisErrorResetTrigRE(CLK := gLRInput.bAxisErrorResetPB);			
	gLRInput.bAxisErrorResetPBRE := fbAxisErrorResetTrigRE.Q;
	
	IF ((gLRInput.siSelectJogAxis <> eAxisJogOff) AND (uiCurrentSuperState = eWait)) THEN
		uiAxisJogState := gLRInput.siSelectJogAxis;	// Axis Selection States - 0=Off, 10=X1, 20=Y1, 30=Y2, 40=Z1, 50=Z2, 60=R, 70=Home
		uiAxisTargetPosState := gLRInput.siSelectAxisX1Loc;	// Axis Location States - 1=FOUP1, 2=FOUP2, 3=FOUP3, 4=FOUP4, 10=Carrier1
		uiAxisTargetLevel := gLRInput.siSelectAxisTargetLevel; // Z Axis Target Level to set - 1=Upper(Top), 2=Lower(Bottom)
		AxisJog_act;
		IF (gLRInput.bHomeAllAxisPBRE AND (uiAxisJogState = eAxisReturnHome)) THEN	// Axis Homing Order States -> Y1 -> Y2 -> R -> X1 -> Z1 -> Z2
			uiAxisHomeState := eY1HomeState;
			tLR.bHomeAllAxis := TRUE;
		END_IF;
	ELSE
		uiAxisX1JogState := eWaitJogCmdState;
		uiAxisHomeState := eY1HomeState;
		uiAxisRRotateState := eWaitRotateCmdState;
		AxisX1Jog.Enable := FALSE;
		AxisY1Jog.Enable := FALSE;
		gLRInput.bJogNegAxisX1PB := FALSE;
		gLRInput.bJogPosAxisX1PB := FALSE;
		gLRInput.bSetTargetPosPB := FALSE;
		tLR.bHomeAllAxis := FALSE;
	END_IF;
	
	EmergencyCheck_act;
	IF (bEmergencyCheckError = TRUE) THEN
		RETURN;
	END_IF
	
	ErrorCheck_act;
	IF (bErrorCheckError = TRUE) THEN
		RETURN;
	END_IF
	
	// Magic Code ~
	AxisX1();
	AxisZ1();
	AxisZ2();
	AxisY1();
	AxisY2();
	RotaryParse_act;
	gLROutput.bGuardianCarrierPresent := NOT tGuardianCarrier1.bAbsentInd;
	
	// TODO respond to Y1 and Y2 encoder error
	// MC_BR_WriteParID_0(Axis := ADR(Y1), Execute := TRUE, ParID := 719, DataAddress := ADR(cudiMICRO_ENCODER_COMMAND), DataType := ncPAR_TYP_UDINT);
	
	AxisX1HardwareInputs(Axis := ADR(X1), Enable := TRUE );
	AxisZ1HardwareInputs(Axis := ADR(Z1), Enable := TRUE );
	AxisZ2HardwareInputs(Axis := ADR(Z2), Enable := TRUE );
	AxisY1HardwareInputs(Axis := ADR(Y1), Enable := TRUE );
	AxisY2HardwareInputs(Axis := ADR(Y2), Enable := TRUE );

	CASE uiCurrentSuperState OF
				
		eHardwareInitialization :
			HardwareInit_act;	
			IF (bHardwareInitError = TRUE) THEN
				tLR.usiSWStateIndTxt := 0; // HMI Text Indication = Not Ready
				tLR.usiSWStateIndColor := usiRedIndColor; // HMI Indicator Color
				RETURN;
			END_IF;
		eWait :
			//------------------------- Input Handling ----------------------------//
			tLR.usiSWStateIndTxt := 1; // HMI Text Indication = eWait
			tLR.usiSWStateIndColor := usiBlueIndColor; // HMI Indicator Color
			
			IF (gLRInput.bRUN = FALSE) THEN
				// reset all processes to FALSE when RUN is turned off
				tLR.Process.bDUALLOAD := FALSE;
				tLR.Process.bDUALUNLOAD := FALSE;
				tLR.Process.bLOAD := FALSE;
				tLR.Process.bUNLOAD := FALSE;
				//gLROutput.gstrError:= ''; // clear error code
			ELSIF (gLRInput.bRUN = TRUE) THEN
				gLRInput.bRUN := FALSE;
				gLROutput.bComplete := FALSE;
				gLROutput.gstrError := '';
				// When RUN is triggered, parse input and set up state machine.
				// When state machine is complete, SuperState is set to Wait until RUN is called again
				// The following actions are located in the folder "InputActions"
				
				InvalidInputCheck_act;
				
				IF(gLROutput.gstrError = '') THEN
					
					ProcessSelection_act;
					
					GenerateLocalVar_act;
		
					StateMachineInit_act;
					
				END_IF;
			END_IF;
	    
		eRunning :	
			//------------------------- Stage Machine ----------------------------//
			tLR.usiSWStateIndTxt := 2; // HMI Text Indication = eRunning
			tLR.usiSWStateIndColor := usiGreenIndColor; // HMI Indicator Color
			
			CASE uiCurrentState OF
				//////////////////////////
				//////LOAD, DUALLOAD//////
				//////////////////////////
				eFPAlign:
					FPAlign_act; // X --> Z1 --> Y1 --> Y2
					// Next: UnloadFP
				eUnloadFP : 
					UnloadFP_act; // Z1 --> Y1 --> Y2
					// Next: LoadGCAlign
				eGCAlign :
					GCAlign_act; // Z2 --> X --> Z1 --> Y1 --> Y2
					// Next: LoadGC
				eLoadGC :
					LoadGC_act; // Z1 --> Y1 --> Y2
					// Next:
					// // Get another batch: UnloadFPAlign
					// // Done: SafeAlign
					
					//////////////////////////
					////UNLOAD, DUALUNLOAD////
					//////////////////////////			
					//eGCAlign : 
					//GCAlign_act; // Z2 --> X --> Z1 --> Y1 --> Y2
					// Next: UnloadGC
				eUnloadGC : 
					UnloadGC_act; // Z1 --> Y1 --> Y2
					// Next: LoadFPAlign			
					//eFPAlign :
					//FPAlign_act; // X --> Z1 --> Y1 --> Y2
					// Next: LoadFP
				eLoadFP : 
					LoadFP_act; // Z1 --> Y1 --> Y2
					// Next:
					// // Get another batch: UnloadGCAlign
					// // Done: SafeAlign
					
					//////////////////////////
					////////SAFE ALIGN////////
					//////////////////////////
				eSafeAlign : 
					// X --> Y1 --> Y2 --> Z1 --> Z2
					SafeAlign_act;
			END_CASE;
	END_CASE;
END_PROGRAM