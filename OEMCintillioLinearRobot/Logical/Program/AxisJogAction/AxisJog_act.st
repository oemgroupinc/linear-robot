
ACTION AxisJog_act :
	// Axis Selection States - 0=Off, 10=X1, 20=Y1, 30=Y2, 40=Z1, 50=Z2, 60=R, 70=Home
	CASE uiAxisJogState OF
		// No Axis Selected to Jog
		eAxisJogOff :
			RETURN;
			
		// Axis X1 Selected to Jog
		eAxisX1Jog :				
			AxisX1.Enable := TRUE;
			AxisX1Parameters.Jog.Velocity := tLR.rCoarseJogVelX1;
			AxisX1Parameters.Jog.Acceleration := tLR.rCoarseJogAccelX1;
			AxisX1Parameters.Jog.Deceleration := tLR.rCoarseJogDelX1;
			AxisX1Parameters.Jog.LowerLimit := tLR.rX1JogLowerTravelLimit;
			AxisX1Parameters.Jog.UpperLimit := tLR.rX1JogUpperTravelLimit;
			
			// Set Target Position and Store in Perm Mem
			// Axis Location States - 1=FOUP1, 2=FOUP2, 3=FOUP3, 4=FOUP4, 10=Carrier1
			CASE uiAxisTargetPosState OF
				eFOUP1Position :	
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tFOUPPort1.rPrevAxisX1TargetPos <> AxisX1.Position) THEN
							rFP1AxisX1TargetPos := AxisX1.Position;
							tFOUPPort1.siX1Pos := rFP1AxisX1TargetPos; 
							tFOUPPort1.rPrevAxisX1TargetPos := rFP1AxisX1TargetPos;
						END_IF;
					END_IF;
					
				eFOUP2Position :	
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tFOUPPort2.rPrevAxisX1TargetPos <> AxisX1.Position) THEN
							rFP2AxisX1TargetPos := AxisX1.Position;
							tFOUPPort2.siX1Pos := rFP2AxisX1TargetPos; 
							tFOUPPort2.rPrevAxisX1TargetPos := rFP2AxisX1TargetPos;
						END_IF;
					END_IF;
					
				eFOUP3Position :	
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tFOUPPort3.rPrevAxisX1TargetPos <> AxisX1.Position) THEN
							rFP3AxisX1TargetPos := AxisX1.Position;
							tFOUPPort3.siX1Pos := rFP3AxisX1TargetPos;
							tFOUPPort3.rPrevAxisX1TargetPos := rFP3AxisX1TargetPos;
						END_IF;
					END_IF;
					
				eFOUP4Position :
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tFOUPPort4.rPrevAxisX1TargetPos <> AxisX1.Position) THEN
							rFP4AxisX1TargetPos := AxisX1.Position;
							tFOUPPort4.siX1Pos := rFP4AxisX1TargetPos; 
							tFOUPPort4.rPrevAxisX1TargetPos := rFP4AxisX1TargetPos;
						END_IF;
					END_IF;
					
				eCarrier1Position :
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tGuardianCarrier1.rPrevAxisX1TargetPos <> AxisX1.Position) THEN
							rGC1AxisX1TargetPos := AxisX1.Position;
							tGuardianCarrier1.siX1Pos := rGC1AxisX1TargetPos;
							tGuardianCarrier1.rPrevAxisX1TargetPos := rGC1AxisX1TargetPos;
						END_IF;
					END_IF;
			END_CASE;
			
			// Coarse Jogging X1 Axis
			IF (gLRInput.siSetAxisX1JogType = siCoarseJogType) THEN
				IF (gLRInput.bJogNegAxisX1PB = TRUE) THEN
					AxisX1.JogNegative := TRUE;
				ELSE
					AxisX1.JogNegative := FALSE;
				END_IF;
	
				IF (gLRInput.bJogPosAxisX1PB = TRUE) THEN
					AxisX1.JogPositive := TRUE;
				ELSE
					AxisX1.JogPositive := FALSE;
				END_IF;	
		
			// Fine Movement steps X1 Axis
			ELSIF (gLRInput.siSetAxisX1JogType = siFineJogType) THEN		
				CASE uiAxisX1JogState OF			
					// Waiting for Jog Neg or Jog Pos Command Signal
					eWaitJogCmdState :
						IF (gLRInput.bJogNegAxisX1PBRE = TRUE) THEN
							AxisX1Parameters.Position := (AxisX1.Position - tLR.rFineJogIncX1);
							uiAxisX1JogState := eSetJogDirection;	
						ELSIF (gLRInput.bJogPosAxisX1PBRE = TRUE) THEN
							AxisX1Parameters.Position := (AxisX1.Position + tLR.rFineJogIncX1);
							uiAxisX1JogState := eSetJogDirection;
						END_IF;
				
					// Set Direction for Jog movement	
					eSetJogDirection :
						IF (AxisX1.MoveActive = FALSE) THEN
							AxisX1.MoveAbsolute := TRUE; 
							uiAxisX1JogState := eWaitTargetReached;	
						END_IF;
				
					// Waiting for Jog Motion to be complete			
					eWaitTargetReached :
						IF (AxisX1.InPosition = TRUE OR AxisX1.Position = AxisX1Parameters.Position) THEN
							AxisX1.MoveAbsolute := FALSE;
							uiAxisX1JogState := eWaitJogCmdState;
						END_IF;						
				END_CASE;		
			END_IF;
			
		// Axis Y1 Selected to Jog
		eAxisY1Jog :				
			AxisY1.Enable := TRUE;
			AxisY1Parameters.Jog.Velocity := tLR.rCoarseJogVelX1;
			AxisY1Parameters.Jog.Acceleration := tLR.rCoarseJogAccelX1;
			AxisY1Parameters.Jog.Deceleration := tLR.rCoarseJogDelX1;
			AxisY1Parameters.Jog.LowerLimit := tLR.rYJogLowerTravelLimit;
			AxisY1Parameters.Jog.UpperLimit := tLR.rYJogUpperTravelLimit;
			
			// Set Target Position and Store in Perm Mem
			// Axis Location States - 1=FOUP1, 2=FOUP2, 3=FOUP3, 4=FOUP4, 10=Carrier1
			CASE uiAxisTargetPosState OF
				eFOUP1Position :	
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tFOUPPort1.rPrevAxisY1TargetPos <> AxisY1.Position) THEN
							rFP1AxisY1TargetPos := AxisY1.Position;
							tFOUPPort1.siY1AccessPos := rFP1AxisY1TargetPos;
							tFOUPPort1.rPrevAxisY1TargetPos := rFP1AxisY1TargetPos;
						END_IF;
					END_IF;
					
				eFOUP2Position :	
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tFOUPPort2.rPrevAxisY1TargetPos <> AxisY1.Position) THEN
							rFP2AxisY1TargetPos := AxisY1.Position;
							tFOUPPort2.siY1AccessPos := rFP2AxisY1TargetPos;
							tFOUPPort2.rPrevAxisY1TargetPos := rFP2AxisY1TargetPos;
						END_IF;
					END_IF;
					
				eFOUP3Position :	
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tFOUPPort3.rPrevAxisY1TargetPos <> AxisY1.Position) THEN
							rFP3AxisY1TargetPos := AxisY1.Position;
							tFOUPPort3.siY1AccessPos := rFP3AxisY1TargetPos;
							tFOUPPort3.rPrevAxisY1TargetPos := rFP3AxisY1TargetPos;
						END_IF;
					END_IF;
					
				eFOUP4Position :
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tFOUPPort4.rPrevAxisY1TargetPos <> AxisY1.Position) THEN
							rFP4AxisY1TargetPos := AxisY1.Position;
							tFOUPPort4.siY1AccessPos := rFP4AxisY1TargetPos;
							tFOUPPort4.rPrevAxisY1TargetPos := rFP4AxisY1TargetPos;
						END_IF;
					END_IF;
					
				eCarrier1Position :
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tGuardianCarrier1.rPrevAxisY1TargetPos <> AxisY1.Position) THEN
							rGC1AxisY1TargetPos := AxisY1.Position;
							tGuardianCarrier1.siY1AccessPos := rGC1AxisY1TargetPos;
							tGuardianCarrier1.rPrevAxisY1TargetPos := rGC1AxisY1TargetPos;
						END_IF;
					END_IF;
			END_CASE;
			
			// Coarse Jogging Y1 Axis
			IF (gLRInput.siSetAxisX1JogType = siCoarseJogType) THEN
				IF (gLRInput.bJogNegAxisX1PB = TRUE) THEN
					AxisY1.JogNegative := TRUE;
				ELSE
					AxisY1.JogNegative := FALSE;
				END_IF;
	
				IF (gLRInput.bJogPosAxisX1PB = TRUE) THEN
					AxisY1.JogPositive := TRUE;
				ELSE
					AxisY1.JogPositive := FALSE;
				END_IF;	
		
				// Fine Movement steps Y1 Axis
			ELSIF (gLRInput.siSetAxisX1JogType = siFineJogType) THEN		
				CASE uiAxisX1JogState OF			
					// Waiting for Jog Neg or Jog Pos Command Signal
					eWaitJogCmdState :
						IF (gLRInput.bJogNegAxisX1PBRE = TRUE) THEN
							AxisY1Parameters.Position := (AxisY1.Position - tLR.rFineJogIncX1);
							uiAxisX1JogState := eSetJogDirection;	
						ELSIF (gLRInput.bJogPosAxisX1PBRE = TRUE) THEN
							AxisY1Parameters.Position := (AxisY1.Position + tLR.rFineJogIncX1);
							uiAxisX1JogState := eSetJogDirection;
						END_IF;
				
						// Set Direction for Jog movement	
					eSetJogDirection :
						IF (AxisY1.MoveActive = FALSE) THEN
							AxisY1.MoveAbsolute := TRUE; 
							uiAxisX1JogState := eWaitTargetReached;	
						END_IF;
				
						// Waiting for Jog Motion to be complete			
					eWaitTargetReached :
						IF (AxisY1.InPosition = TRUE OR AxisY1.Position = AxisY1Parameters.Position) THEN
							AxisY1.MoveAbsolute := FALSE;
							uiAxisX1JogState := eWaitJogCmdState;
						END_IF;						
				END_CASE;		
			END_IF;
			
		// Axis Y2 Selected to Jog
		eAxisY2Jog :				
			AxisY2.Enable := TRUE;
			AxisY2Parameters.Jog.Velocity := tLR.rCoarseJogVelX1;
			AxisY2Parameters.Jog.Acceleration := tLR.rCoarseJogAccelX1;
			AxisY2Parameters.Jog.Deceleration := tLR.rCoarseJogDelX1;
			AxisY2Parameters.Jog.LowerLimit := tLR.rY2JogLowerTravelLimit;
			AxisY2Parameters.Jog.UpperLimit := tLR.rY2JogUpperTravelLimit;
			
			// Set Target Position and Store in Perm Mem
			// Axis Location States - 1=FOUP1, 2=FOUP2, 3=FOUP3, 4=FOUP4, 10=Carrier1
			CASE uiAxisTargetPosState OF
				eFOUP1Position :	
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tFOUPPort1.rPrevAxisY2TargetPos <> AxisY2.Position) THEN
							rFP1AxisY2TargetPos := AxisY2.Position;
							tFOUPPort1.siY2AccessPos := rFP1AxisY2TargetPos;
							tFOUPPort1.rPrevAxisY2TargetPos := rFP1AxisY2TargetPos;
						END_IF;
					END_IF;
					
				eFOUP2Position :	
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tFOUPPort2.rPrevAxisY2TargetPos <> AxisY2.Position) THEN
							rFP2AxisY2TargetPos := AxisY2.Position;
							tFOUPPort2.siY2AccessPos := rFP2AxisY2TargetPos; 
							tFOUPPort2.rPrevAxisY2TargetPos := rFP2AxisY2TargetPos;
						END_IF;
					END_IF;
					
				eFOUP3Position :	
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tFOUPPort3.rPrevAxisY2TargetPos <> AxisY2.Position) THEN
							rFP3AxisY2TargetPos := AxisY2.Position;
							tFOUPPort3.siY2AccessPos := rFP3AxisY2TargetPos;
							tFOUPPort3.rPrevAxisY2TargetPos := rFP3AxisY2TargetPos;
						END_IF;
					END_IF;
					
				eFOUP4Position :
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tFOUPPort4.rPrevAxisY2TargetPos <> AxisY2.Position) THEN
							rFP4AxisY2TargetPos := AxisY2.Position;
							tFOUPPort4.siY2AccessPos := rFP4AxisY2TargetPos;
							tFOUPPort4.rPrevAxisY2TargetPos := rFP4AxisY2TargetPos;
						END_IF;
					END_IF;
					
				eCarrier1Position :
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (tGuardianCarrier1.rPrevAxisY2TargetPos <> AxisY2.Position) THEN
							rGC1AxisY2TargetPos := AxisY2.Position;
							tGuardianCarrier1.siY2AccessPos := rGC1AxisY2TargetPos;
							tGuardianCarrier1.rPrevAxisY2TargetPos := rGC1AxisY2TargetPos;
						END_IF;
					END_IF;
			END_CASE;
			
			// Coarse Jogging Y2 Axis
			IF (gLRInput.siSetAxisX1JogType = siCoarseJogType) THEN
				IF (gLRInput.bJogNegAxisX1PB = TRUE) THEN
					AxisY2.JogNegative := TRUE;
				ELSE
					AxisY2.JogNegative := FALSE;
				END_IF;
	
				IF (gLRInput.bJogPosAxisX1PB = TRUE) THEN
					AxisY2.JogPositive := TRUE;
				ELSE
					AxisY2.JogPositive := FALSE;
				END_IF;	
		
				// Fine Movement steps Y2 Axis
			ELSIF (gLRInput.siSetAxisX1JogType = siFineJogType) THEN		
				CASE uiAxisX1JogState OF			
					// Waiting for Jog Neg or Jog Pos Command Signal
					eWaitJogCmdState :
						IF (gLRInput.bJogNegAxisX1PBRE = TRUE) THEN
							AxisY2Parameters.Position := (AxisY2.Position - tLR.rFineJogIncX1);
							uiAxisX1JogState := eSetJogDirection;	
						ELSIF (gLRInput.bJogPosAxisX1PBRE = TRUE) THEN
							AxisY2Parameters.Position := (AxisY2.Position + tLR.rFineJogIncX1);
							uiAxisX1JogState := eSetJogDirection;
						END_IF;
				
					// Set Direction for Jog movement	
					eSetJogDirection :
						IF (AxisY2.MoveActive = FALSE) THEN
							AxisY2.MoveAbsolute := TRUE; 
							uiAxisX1JogState := eWaitTargetReached;	
						END_IF;
				
					// Waiting for Jog Motion to be complete			
					eWaitTargetReached :
						IF (AxisY2.InPosition = TRUE OR AxisY2.Position = AxisY2Parameters.Position) THEN
							AxisY2.MoveAbsolute := FALSE;
							uiAxisX1JogState := eWaitJogCmdState;
						END_IF;						
				END_CASE;		
			END_IF;
			
		// Axis Z1 Selected to Jog
		eAxisZ1Jog :				
			AxisZ1.Enable := TRUE;
			AxisZ1Parameters.Jog.Velocity := tLR.rCoarseJogVelX1;
			AxisZ1Parameters.Jog.Acceleration := tLR.rCoarseJogAccelX1;
			AxisZ1Parameters.Jog.Deceleration := tLR.rCoarseJogDelX1;
			AxisZ1Parameters.Jog.LowerLimit := tLR.rZ1JogLowerTravelLimit;
			AxisZ1Parameters.Jog.UpperLimit := tLR.rZ1JogUpperTravelLimit;
			
			// Set Target Position and Store in Perm Mem
			// Axis Location States - 1=FOUP1, 2=FOUP2, 3=FOUP3, 4=FOUP4, 10=Carrier1
			CASE uiAxisTargetPosState OF
				eFOUP1Position :	
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (uiAxisTargetLevel = eUpperPosition) THEN
							IF (tFOUPPort1.rPrevAxisZ1UpTargetPos <> AxisZ1.Position) THEN
								rFP1AxisZ1UpTargetPos := AxisZ1.Position;
								tFOUPPort1.siZ1LRPossession := rFP1AxisZ1UpTargetPos;
								tFOUPPort1.rPrevAxisZ1UpTargetPos := rFP1AxisZ1UpTargetPos;
							END_IF;
						ELSIF (uiAxisTargetLevel = eLowerPosition) THEN
							IF (tFOUPPort1.rPrevAxisZ1LowTargetPos <> AxisZ1.Position) THEN
								rFP1AxisZ1LowTargetPos := AxisZ1.Position;
								tFOUPPort1.siZ1FPPossession := rFP1AxisZ1LowTargetPos;
								tFOUPPort1.rPrevAxisZ1LowTargetPos := rFP1AxisZ1LowTargetPos;
							END_IF;
						END_IF;
					END_IF;
					
				eFOUP2Position :	
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (uiAxisTargetLevel = eUpperPosition) THEN
							IF (tFOUPPort2.rPrevAxisZ1UpTargetPos <> AxisZ1.Position) THEN
								rFP2AxisZ1UpTargetPos := AxisZ1.Position;
								tFOUPPort2.siZ1LRPossession := rFP2AxisZ1UpTargetPos;
								tFOUPPort2.rPrevAxisZ1UpTargetPos := rFP2AxisZ1UpTargetPos;
							END_IF;
						ELSIF (uiAxisTargetLevel = eLowerPosition) THEN
							IF (tFOUPPort2.rPrevAxisZ1LowTargetPos <> AxisZ1.Position) THEN
								rFP2AxisZ1LowTargetPos := AxisZ1.Position;
								tFOUPPort2.siZ1FPPossession := rFP2AxisZ1LowTargetPos;
								tFOUPPort2.rPrevAxisZ1LowTargetPos := rFP2AxisZ1LowTargetPos;
							END_IF;
						END_IF;
					END_IF;
					
				eFOUP3Position :	
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (uiAxisTargetLevel = eUpperPosition) THEN
							IF (tFOUPPort3.rPrevAxisZ1UpTargetPos <> AxisZ1.Position) THEN
								rFP3AxisZ1UpTargetPos := AxisZ1.Position;
								tFOUPPort3.siZ1LRPossession := rFP3AxisZ1UpTargetPos;
								tFOUPPort3.rPrevAxisZ1UpTargetPos := rFP3AxisZ1UpTargetPos;
							END_IF;
						ELSIF (uiAxisTargetLevel = eLowerPosition) THEN
							IF (tFOUPPort3.rPrevAxisZ1LowTargetPos <> AxisZ1.Position) THEN
								rFP3AxisZ1LowTargetPos := AxisZ1.Position;
								tFOUPPort3.siZ1FPPossession := rFP3AxisZ1LowTargetPos;
								tFOUPPort3.rPrevAxisZ1LowTargetPos := rFP3AxisZ1LowTargetPos;
							END_IF;
						END_IF;
					END_IF;
					
				eFOUP4Position :
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (uiAxisTargetLevel = eUpperPosition) THEN
							IF (tFOUPPort4.rPrevAxisZ1UpTargetPos <> AxisZ1.Position) THEN
								rFP4AxisZ1UpTargetPos := AxisZ1.Position;
								tFOUPPort4.siZ1LRPossession := rFP4AxisZ1UpTargetPos;
								tFOUPPort4.rPrevAxisZ1UpTargetPos := rFP4AxisZ1UpTargetPos;
							END_IF;
						ELSIF (uiAxisTargetLevel = eLowerPosition) THEN
							IF (tFOUPPort4.rPrevAxisZ1LowTargetPos <> AxisZ1.Position) THEN
								rFP4AxisZ1LowTargetPos := AxisZ1.Position;
								tFOUPPort4.siZ1FPPossession := rFP4AxisZ1LowTargetPos;
								tFOUPPort4.rPrevAxisZ1LowTargetPos := rFP4AxisZ1LowTargetPos;
							END_IF;
						END_IF;
					END_IF;
					
				eCarrier1Position :
					IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
						IF (uiAxisTargetLevel = eUpperPosition) THEN
							IF (tGuardianCarrier1.rPrevAxisZ1UpTargetPos <> AxisZ1.Position) THEN
								rGC1AxisZ1UpTargetPos := AxisZ1.Position;
								tGuardianCarrier1.siZ1LRPossession := rGC1AxisZ1UpTargetPos;
								tGuardianCarrier1.rPrevAxisZ1UpTargetPos := rGC1AxisZ1UpTargetPos;
							END_IF;
						ELSIF (uiAxisTargetLevel = eLowerPosition) THEN
							IF (tGuardianCarrier1.rPrevAxisZ1LowTargetPos <> AxisZ1.Position) THEN
								rGC1AxisZ1LowTargetPos := AxisZ1.Position;
								tGuardianCarrier1.siZ1GCPossession := rGC1AxisZ1LowTargetPos;
								tGuardianCarrier1.rPrevAxisZ1LowTargetPos := rGC1AxisZ1LowTargetPos;
							END_IF;
						END_IF;
					END_IF;
			END_CASE;
			// Coarse Jogging Z1 Axis
			IF (gLRInput.siSetAxisX1JogType = siCoarseJogType) THEN
				IF (gLRInput.bJogNegAxisX1PB = TRUE) THEN
					AxisZ1.JogNegative := TRUE;
				ELSE
					AxisZ1.JogNegative := FALSE;
				END_IF;
	
				IF (gLRInput.bJogPosAxisX1PB = TRUE) THEN
					AxisZ1.JogPositive := TRUE;
				ELSE
					AxisZ1.JogPositive := FALSE;
				END_IF;	
		
			// Fine Movement steps Z1 Axis
			ELSIF (gLRInput.siSetAxisX1JogType = siFineJogType) THEN		
				CASE uiAxisX1JogState OF			
					// Waiting for Jog Neg or Jog Pos Command Signal
					eWaitJogCmdState :
						IF (gLRInput.bJogNegAxisX1PBRE = TRUE) THEN
							AxisZ1Parameters.Position := (AxisZ1.Position - tLR.rFineJogIncX1);
							uiAxisX1JogState := eSetJogDirection;	
						ELSIF (gLRInput.bJogPosAxisX1PBRE = TRUE) THEN
							AxisZ1Parameters.Position := (AxisZ1.Position + tLR.rFineJogIncX1);
							uiAxisX1JogState := eSetJogDirection;
						END_IF;
				
					// Set Direction for Jog movement	
					eSetJogDirection :
						IF (AxisZ1.MoveActive = FALSE) THEN
							AxisZ1.MoveAbsolute := TRUE; 
							uiAxisX1JogState := eWaitTargetReached;	
						END_IF;
				
					// Waiting for Jog Motion to be complete			
					eWaitTargetReached :
						IF (AxisZ1.InPosition = TRUE OR AxisZ1.Position = AxisZ1Parameters.Position) THEN
							AxisZ1.MoveAbsolute := FALSE;
							uiAxisX1JogState := eWaitJogCmdState;
						END_IF;						
				END_CASE;		
			END_IF;
			
		// Axis Z2 Selected to Jog
		eAxisZ2Jog :				
			AxisZ2.Enable := TRUE;
			AxisZ2Parameters.Jog.Velocity := tLR.rCoarseJogVelX1;
			AxisZ2Parameters.Jog.Acceleration := tLR.rCoarseJogAccelX1;
			AxisZ2Parameters.Jog.Deceleration := tLR.rCoarseJogDelX1;
			AxisZ2Parameters.Jog.LowerLimit := tLR.rZ2JogLowerTravelLimit;
			AxisZ2Parameters.Jog.UpperLimit := tLR.rZ2JogUpperTravelLimit;
			
			// Z Axis Target Level to set - 1=Upper(Top), 2=Lower(Bottom)	
			IF (gLRInput.bSetTargetPosPBRE = TRUE) THEN
				IF (uiAxisTargetLevel = eUpperPosition) THEN // Top Carrier Position
					IF (tGuardianCarrier1.rPrevAxisZ2UpCarrierPos <> AxisZ2.Position) THEN
						rGC1AxisZ2UpCarrierPos := AxisZ2.Position;
						tGuardianCarrier1.siZ2TopPos := rGC1AxisZ2UpCarrierPos;
						tGuardianCarrier1.rPrevAxisZ2UpCarrierPos := rGC1AxisZ2UpCarrierPos;
					END_IF;
				ELSIF (uiAxisTargetLevel = eLowerPosition) THEN	// Bottom Carrier Position
					IF (tGuardianCarrier1.rPrevAxisZ2LowCarrierPos <> AxisZ2.Position) THEN
						rGC1AxisZ2LowCarrierPos := AxisZ2.Position;
						tGuardianCarrier1.siZ2BottomPos := rGC1AxisZ2LowCarrierPos;
						tGuardianCarrier1.rPrevAxisZ2LowCarrierPos := rGC1AxisZ2LowCarrierPos;
					END_IF;
				END_IF;
			END_IF;
			
			// Coarse Jogging Z2 Axis
			IF (gLRInput.siSetAxisX1JogType = siCoarseJogType) THEN
				IF (gLRInput.bJogNegAxisX1PB = TRUE) THEN
					AxisZ2.JogNegative := TRUE;
				ELSE
					AxisZ2.JogNegative := FALSE;
				END_IF;
	
				IF (gLRInput.bJogPosAxisX1PB = TRUE) THEN
					AxisZ2.JogPositive := TRUE;
				ELSE
					AxisZ2.JogPositive := FALSE;
				END_IF;	
		
			// Fine Movement steps Z2 Axis
			ELSIF (gLRInput.siSetAxisX1JogType = siFineJogType) THEN		
				CASE uiAxisX1JogState OF			
					// Waiting for Jog Neg or Jog Pos Command Signal
					eWaitJogCmdState :
						IF (gLRInput.bJogNegAxisX1PBRE = TRUE) THEN
							AxisZ2Parameters.Position := (AxisZ2.Position - tLR.rFineJogIncX1);
							uiAxisX1JogState := eSetJogDirection;	
						ELSIF (gLRInput.bJogPosAxisX1PBRE = TRUE) THEN
							AxisZ2Parameters.Position := (AxisZ2.Position + tLR.rFineJogIncX1);
							uiAxisX1JogState := eSetJogDirection;
						END_IF;
				
					// Set Direction for Jog movement	
					eSetJogDirection :
						IF (AxisZ2.MoveActive = FALSE) THEN
							AxisZ2.MoveAbsolute := TRUE; 
							uiAxisX1JogState := eWaitTargetReached;	
						END_IF;
				
					// Waiting for Jog Motion to be complete			
					eWaitTargetReached :
						IF (AxisZ2.InPosition = TRUE OR AxisZ2.Position = AxisZ2Parameters.Position) THEN
							AxisZ2.MoveAbsolute := FALSE;
							uiAxisX1JogState := eWaitJogCmdState;
						END_IF;						
				END_CASE;		
			END_IF;
			
		// Axis R Selected to Rotate (-90), (-180), (-270) degree
		eAxisRRotate :
			CASE uiAxisRRotateState OF
				eWaitRotateCmdState :
					IF (gLRInput.bRotateAxisRPBRE) THEN
						bAxisRStartFlag := TRUE;
						bAxisRArrivedFlag := FALSE;
						IF (gLRInput.uiRotateAxisPos = 180) THEN
							uiAxisRRotateState := eAxisRRotate180;
						ELSIF (gLRInput.uiRotateAxisPos = 90) THEN
							uiAxisRRotateState := eAxisRRotate90;
						ELSIF (gLRInput.uiRotateAxisPos = 270) THEN
							uiAxisRRotateState := eAxisRRotate270;
						END_IF;
					END_IF;
					
				// Rotate R Axis -180 degree 
				eAxisRRotate180 :		
					tAxisRMove.siTargetPos := -180;
					bAxisRStartFlag := TRUE;
					RMove_act;
					IF (bAxisRArrivedFlag = TRUE) THEN
						bAxisRStartFlag := FALSE;
						// Set next substate
						uiAxisRRotateState := eWaitRotateCmdState;
					END_IF;	
					
				// Rotate R Axis -90 degree
				eAxisRRotate90 :
					tAxisRMove.siTargetPos := -90;
					bAxisRStartFlag := TRUE;
					RMove_act;
					IF (bAxisRArrivedFlag = TRUE) THEN
						bAxisRStartFlag := FALSE;
						// Set next substate
						uiAxisRRotateState := eWaitRotateCmdState;
					END_IF;
					
				// Rotate R Axis -90 degree
				eAxisRRotate270 :
					tAxisRMove.siTargetPos := -270;
					bAxisRStartFlag := TRUE;
					RMove_act;
					IF (bAxisRArrivedFlag = TRUE) THEN
						bAxisRStartFlag := FALSE;
						// Set next substate
						uiAxisRRotateState := eWaitRotateCmdState;
					END_IF;	
			END_CASE;
			
		// Return All Axis to Home Position	
		// Axis Homing Order States -> Y1 -> Y2 -> R -> X1 -> Z1 -> Z2
		eAxisReturnHome :
			IF (tLR.bHomeAllAxis = TRUE) THEN
				bAxisRArrivedFlag := FALSE;
				tLR.bWaferPossession := FALSE;
				SetAcceleration_act;
				CASE uiAxisHomeState OF 
				
					eY1HomeState : // Safe Align Y1			
						tAxisY1Move.siTargetPos := tLR.siY1SafePos;
						bAxisY1ArrivedFlag := TRUE;
						Y1Move_act; 			
						IF (tAxisY1Move.state = eArrived) THEN
							// Set next state
							uiAxisHomeState := eY2HomeState;
						END_IF;
						
					eY2HomeState : // Safe Align Y2			
						tAxisY2Move.siTargetPos := tLR.siY2SafePos;
						bAxisY2ArrivedFlag := TRUE;
						Y2Move_act; 			
						IF (tAxisY2Move.state = eArrived) THEN
							// Set next state
							uiAxisHomeState := eRHomeState;
						END_IF;	
			
					eRHomeState : 
						tAxisRMove.siTargetPos := -180;
						bAxisRStartFlag := TRUE;
						RMove_act;
						IF (bAxisRArrivedFlag = TRUE) THEN
							bAxisRStartFlag := FALSE;
							// Set next substate
							uiAxisHomeState := eX1HomeState;
						END_IF;
			
					eX1HomeState : // Safe Align X			
						tAxisX1Move.siTargetPos := tLR.siX1SafePos;
						X1Move_act; 			
						IF (tAxisX1Move.state = eArrived) THEN
							// Set next state
							uiAxisHomeState := eZ1HomeState;
						END_IF;
			
					eZ1HomeState : // Safe Align Z1
						tAxisZ1Move.rAccel := tLR.siFastAccelZ1; // set parameters for next movement
						tAxisZ1Move.rSpeed := tLR.siFastVelZ1;
						tAxisZ1Move.siTargetPos := tLR.siZ1SafePos;    
						Z1Move_act; 			
						IF (tAxisZ1Move.state = eArrived) THEN
							// Set next state
							uiAxisHomeState := eZ2HomeState;
						END_IF;
		                
					eZ2HomeState : // Safe Align Z2
						tAxisZ2Move.rAccel := tLR.siFastAccelZ2; // set parameters for next movement
						tAxisZ2Move.rSpeed := tLR.siFastVelZ2;
						tAxisZ2Move.siTargetPos := tLR.siZ2SafePos;
						Z2Move_act; 			
						IF (tAxisZ2Move.state = eArrived) THEN
							// Set next state
							uiAxisHomeState := eHomeStateFinished;
						END_IF;
			
					eHomeStateFinished : // Finished and next State
						tLR.bHomeAllAxis := FALSE;
						uiAxisHomeState := eY1HomeState;
		
				END_CASE;
			END_IF;	
	END_CASE;
	
END_ACTION
