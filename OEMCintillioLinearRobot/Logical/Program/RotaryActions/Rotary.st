
ACTION RotaryPower :
	RSOutput00 := 0;
	RSOutput01 := 0;
	RSOutput02 := 0;
	RSOutput03 := 0;
	RSOutput04 := 4;
	RSOutput05 := 0;
	RSOutput06 := 0;
	RSOutput07 := 0;
	RSOutput08 := 20;
	RSOutput09 := 0;
	RSOutput10 := 30;
	RSOutput11 := 0;
	RSOutput12 := 255;
	RSOutput13 := 0;
	RSOutput14 := 48;
	RSOutput15 := 0;
END_ACTION

ACTION RotaryHome : 
	
	RSOutput00 := 224;
	RSOutput01 := 64;
	RSOutput02 := 253;
	RSOutput03 := 255;
	RSOutput04 := 4;
	RSOutput05 := 0;
	RSOutput06 := 0;
	RSOutput07 := 0;
	RSOutput08 := LREAL_TO_USINT(tAxisRMove.rSpeed);
	RSOutput09 := 0;
	RSOutput10 := LREAL_TO_USINT(tAxisRMove.rAccel);
	RSOutput11 := 0;
	RSOutput12 := 255;
	RSOutput13 := 0;
	RSOutput14 := 49;
	RSOutput15 := 0;
(*
	RSOutput00 := 0;
	RSOutput01 := 0;
	RSOutput02 := 0;
	RSOutput03 := 0;
	RSOutput04 := 4;
	RSOutput05 := 0;
	RSOutput06 := 0;
	RSOutput07 := 0;
	RSOutput08 := 20;
	RSOutput09 := 0;
	RSOutput10 := 30;
	RSOutput11 := 0;
	RSOutput12 := 255;
	RSOutput13 := 0;
	RSOutput14 := 50;
	RSOutput15 := 0;
*)
END_ACTION

ACTION RotaryPause : 
	RSOutput00 := 0;
	RSOutput01 := 0;
	RSOutput02 := 0;
	RSOutput03 := 0;
	RSOutput04 := 4;
	RSOutput05 := 0;
	RSOutput06 := 0;
	RSOutput07 := 0;
	RSOutput08 := 20;
	RSOutput09 := 0;
	RSOutput10 := 30;
	RSOutput11 := 0;
	RSOutput12 := 255;
	RSOutput13 := 0;
	RSOutput14 := 52;
	RSOutput15 := 0;
END_ACTION

ACTION RotaryOff : 
	RSOutput00 := 0;
	RSOutput01 := 0;
	RSOutput02 := 0;
	RSOutput03 := 0;
	RSOutput04 := 4;
	RSOutput05 := 0;
	RSOutput06 := 0;
	RSOutput07 := 0;
	RSOutput08 := 20;
	RSOutput09 := 0;
	RSOutput10 := 30;
	RSOutput11 := 0;
	RSOutput12 := 255;
	RSOutput13 := 0;
	RSOutput14 := 4;
	RSOutput15 := 0;
END_ACTION

ACTION RotaryMove_act :
	CASE LREAL_TO_INT(tAxisRMove.siTargetPos) OF 
		180: 
			RSOutput00 := 32;
			RSOutput01 := 191;
			RSOutput02 := 2;
			RSOutput03 := 0;
		-90: 
			RSOutput00 := 112;
			RSOutput01 := 160;
			RSOutput02 := 254;
			RSOutput03 := 255;
		-180:
			RSOutput00 := 224;
			RSOutput01 := 64;
			RSOutput02 := 253;
			RSOutput03 := 255;
		-270:
			RSOutput00 := 80;
			RSOutput01 := 225;
			RSOutput02 := 251;
			RSOutput03 := 255;
	END_CASE
	
	RSOutput04 := 4;
	RSOutput05 := 0;
	RSOutput06 := 0;
	RSOutput07 := 0;
	RSOutput08 := LREAL_TO_USINT(tAxisRMove.rSpeed);
	RSOutput09 := 0;
	RSOutput10 := LREAL_TO_USINT(tAxisRMove.rAccel);
	RSOutput11 := 0;
	RSOutput12 := 255;
	RSOutput13 := 0;
	RSOutput14 := 49;
	RSOutput15 := 0;
END_ACTION

ACTION RotaryMoveDone_act :
	CASE LREAL_TO_INT(tAxisRMove.siTargetPos) OF 
		180: 
			RSOutput00 := 32;
			RSOutput01 := 191;
			RSOutput02 := 2;
			RSOutput03 := 0;
		-90: 
			RSOutput00 := 112;
			RSOutput01 := 160;
			RSOutput02 := 254;
			RSOutput03 := 255;
		-180:
			RSOutput00 := 224;
			RSOutput01 := 64;
			RSOutput02 := 253;
			RSOutput03 := 255;
		-270:
			RSOutput00 := 80;
			RSOutput01 := 225;
			RSOutput02 := 251;
			RSOutput03 := 255;
	END_CASE
	
	RSOutput04 := 4;
	RSOutput05 := 0;
	RSOutput06 := 0;
	RSOutput07 := 0;
	RSOutput08 := 20;
	RSOutput09 := 0;
	RSOutput10 := 30;
	RSOutput11 := 0;
	RSOutput12 := 255;
	RSOutput13 := 0;
	RSOutput14 := 48;
	RSOutput15 := 0;
END_ACTION

ACTION RotaryParse_act :
	// Unused Rotary Inputs
	RSInput04;
	RSInput05;
	RSInput06;
	RSInput07;
	
	// Parse parameters from the rotary stage
	
	RotaryPosition := USINT_TO_LREAL( RSInput00 OR SHL(RSInput01, 8) OR SHL(RSInput02, 16) OR SHL(RSInput03, 24) )/1000; // units: mm

	RotaryComCurrent := USINT_TO_REAL(RSInput04 OR SHL(RSInput05, 8) OR SHL(RSInput06,16) OR SHL(RSInput07, 24)); // units: mA
	
	RotarySpeed := USINT_TO_LREAL(RSInput08 OR SHL(RSInput09, 8) OR SHL(RSInput10,16) OR SHL(RSInput11, 24))/100; // units: mm/sec
	
	RotaryAlarmCode := RSInput12 + RSInput13*256;

	Input14Byte := USINT_TO_BYTE(RSInput14);
	RotaryPEND 	:= Input14Byte.0; 	// Positioning completion signal (TRUE = complete)
	RotaryHEND 	:= Input14Byte.1; 	// Home Return Completion
	RotaryMOVE 	:= Input14Byte.2; 	// TRUE while actuator is moving
	RotaryALM 	:= Input14Byte.3; 	// Alarm
	RotarySV 	:= Input14Byte.4;	// Servo ON signal
	RotaryPSEL 	:= Input14Byte.5;	// "Pressing and a miss"
	RotaryBALM 	:= Input14Byte.7;	// Absolute battery voltage low warning
	
	Input15Byte := USINT_TO_BYTE(RSInput15);
	RotaryRMDS 	:= Input15Byte.0;
	RotaryZONE1 := Input15Byte.4;
	RotaryZONE2 := Input15Byte.5;
	RotaryPWR 	:= Input15Byte.6;
	RotaryEMGS 	:= Input15Byte.7;
END_ACTION

ACTION RotaryReset_act :	
	RSOutput00 := 0;
	RSOutput01 := 0;
	RSOutput02 := 0;
	RSOutput03 := 0;
	RSOutput04 := 4;
	RSOutput05 := 0;
	RSOutput06 := 0;
	RSOutput07 := 0;
	RSOutput08 := 20;
	RSOutput09 := 0;
	RSOutput10 := 30;
	RSOutput11 := 0;
	RSOutput12 := 255;
	RSOutput13 := 0;
	RSOutput14 := 8;
	RSOutput15 := 0;
END_ACTION