
ACTION HardwareInit_act: 
	//--------------------------------- Check for axis errors -------------------------------//
	//HWInitErrorCode := "";
	IF (AxisX1.Error = TRUE) THEN
		(*
		CASE AxisX1.Info.Diag.Internal.ID OF
			5019:
				HWInitErrorCode := "5019: Homing parameter outside the valid range";
			5020:
				HWInitErrorCode := "5020: Homing procedure not possible: Both limit switches are closed";
			5021:
				HWInitErrorCode := "5021: Limit switch closed: No direction change for this homing mode";
			5022: 
				HWInitErrorCode := "5022: Second limit switch signal received: Reference switch not found";
			5023: 
				HWInitErrorCode := "5023: Incorrect limit switch signal received for current movement direction";
			
		END_CASE
		*)
		gLROutput.gstrError := 'Hardware Init, error on Axis X1. ';
		bHardwareInitError := TRUE;
		RETURN;
	END_IF
	IF (AxisY1.Error = TRUE) THEN
		gLROutput.gstrError := 'Hardware Init, error on Axis Y1. ';
		bHardwareInitError := TRUE;
		RETURN;
	END_IF
	IF (AxisY2.Error = TRUE) THEN
		gLROutput.gstrError := 'Hardware Init, error on Axis Y2. ';
		bHardwareInitError := TRUE;
		RETURN;
	END_IF
	IF (AxisZ1.Error = TRUE) THEN
		gLROutput.gstrError := 'Hardware Init, error on Axis Z1. ';
		bHardwareInitError := TRUE;
		RETURN;
	END_IF
	IF (AxisZ2.Error = TRUE) THEN
		gLROutput.gstrError := 'Hardware Init, error on Axis Z2. ';
		bHardwareInitError := TRUE;
		RETURN;
	END_IF
	IF (RotaryALM = TRUE) THEN
		gLROutput.gstrError := 'Hardware Init, error on Axis R. ';
		bHardwareInitError := TRUE;
		RETURN;
	END_IF
	
	//------------------------------------ Enable all axes ----------------------------------//
	
	IF (AxisX1.Active = FALSE AND AxisX1.Enable = FALSE) THEN
		AxisX1.Enable := TRUE;
	END_IF
	IF (AxisY1.Active = FALSE AND AxisY1.Enable = FALSE) THEN
		AxisY1.Enable := TRUE;
	END_IF
	IF (AxisY2.Active = FALSE AND AxisY2.Enable = FALSE) THEN
		AxisY2.Enable := TRUE;
	END_IF
	IF (AxisZ1.Active = FALSE AND AxisZ1.Enable = FALSE) THEN
		AxisZ1.Enable := TRUE;
	END_IF
	IF (AxisZ2.Active = FALSE AND AxisZ2.Enable = FALSE) THEN
		AxisZ2.Enable := TRUE;
	END_IF
	
	// R
	
	//---------------------------------- Power on all axes ---------------------------------//

	IF (AxisX1.Active = TRUE AND 
		AxisY1.Active = TRUE AND 
		AxisY2.Active = TRUE AND 
		AxisZ1.Active = TRUE AND 
		AxisZ2.Active = TRUE) THEN
		// TODO add R axis condition
		// X
		IF (RotarySV = FALSE) THEN
			RotaryPower;
		END_IF
		
		IF (AxisX1.Power = FALSE AND AxisX1.PowerOn = FALSE) THEN
			AxisX1.Power := TRUE;
		END_IF            
		// Y1
		IF (AxisY1.Power = FALSE AND AxisY1.PowerOn = FALSE) THEN
			AxisY1.Power := TRUE;
		END_IF;
		// Y2 
		IF (AxisY2.Power = FALSE AND AxisY2.PowerOn = FALSE) THEN
			AxisY2.Power := TRUE;
		END_IF;
		// Z1
		IF (AxisZ1.Power = FALSE AND AxisZ1.PowerOn = FALSE) THEN
			AxisZ1.Power := TRUE;
		END_IF;
		// Z2
		IF (AxisZ2.Power = FALSE AND AxisZ2.PowerOn = FALSE) THEN
			AxisZ2.Power := TRUE;
		END_IF;
		// R

	END_IF;

	//-------------------- Home all axes + Move to Save Position ------------------//
	
	IF (AxisX1.PowerOn = TRUE AND 
		AxisY1.PowerOn = TRUE AND 
		AxisY2.PowerOn = TRUE AND 
		AxisZ1.PowerOn = TRUE AND 
		AxisZ2.PowerOn = TRUE AND
		RotarySV = TRUE) THEN
		// TODO add R axis condition
		
		
	
		// Y1
		IF (AxisY1.Home = FALSE) THEN
			AxisY1.Home := TRUE;
		ELSIF (AxisY1.IsHomed = TRUE AND AxisY2.Home = FALSE) THEN
			// once Y1 is homed, start to home Y2
			// Y2
			AxisY2.Home := TRUE;
		ELSIF (AxisY1.IsHomed = TRUE AND AxisY2.IsHomed = TRUE) THEN
			// When Y1 and Y2 are both homed, move both to the safe position
			tAxisY1Move.rAccel := tLR.siFastAccelY1;
			tAxisY1Move.rSpeed := tLR.siFastVelY1;
			tAxisY1Move.siTargetPos := tLR.siY1SafePos;
			bAxisY1ArrivedFlag := TRUE;
			Y1Move_act;
			tAxisY2Move.rAccel := tLR.siFastAccelY2;
			tAxisY2Move.rSpeed := tLR.siFastVelY2;
			tAxisY2Move.siTargetPos := tLR.siY2SafePos;
			bAxisY2ArrivedFlag := TRUE;
			Y2Move_act;
		END_IF
		
		IF (AxisY1.IsHomed = TRUE AND AxisY2.IsHomed = TRUE) THEN
			
			// Z2 - can move independently 
			IF (AxisZ2.Home = FALSE) THEN
				AxisZ2.Home := TRUE;
			ELSIF (AxisZ2.IsHomed = TRUE) THEN 
				tAxisZ2Move.rAccel := tLR.siFastAccelZ2;
				tAxisZ2Move.rSpeed := tLR.siFastVelZ2;
				tAxisZ2Move.siTargetPos := tLR.siZ2SafePos;
				Z2Move_act;
			END_IF;
			
			// R home
			RotaryHome;
			
			// Z1
			IF (AxisZ1.Home = FALSE AND RotaryHEND = TRUE) THEN
				AxisZ1.Home := TRUE;
			ELSIF (AxisZ1.IsHomed = TRUE) THEN
				tAxisZ1Move.rAccel := tLR.siFastAccelZ1;
				tAxisZ1Move.rSpeed := tLR.siFastVelZ1;
				tAxisZ1Move.siTargetPos := tLR.siZ1SafePos;
				Z1Move_act;	
			END_IF
			
			IF (AxisZ1.IsHomed = TRUE) THEN
				
				// X
				IF (AxisX1.Home = FALSE) THEN
					AxisX1.Home := TRUE;
				ELSIF (AxisX1.IsHomed = TRUE) THEN
					tAxisX1Move.rAccel := tLR.siFastAccelX1;
					tAxisX1Move.rSpeed := tLR.siFastVelX1;
					tAxisX1Move.siTargetPos := tLR.siX1SafePos;
					X1Move_act;
				END_IF;
			END_IF
		END_IF

		//------------------------- End of Hardware initialization ----------------------------//
        
		IF (AxisX1.IsHomed = TRUE AND 
			AxisY1.IsHomed = TRUE AND 
			AxisY2.IsHomed = TRUE AND 
			AxisZ1.IsHomed = TRUE AND 
			AxisZ2.IsHomed = TRUE AND 
			RotaryHEND = TRUE) THEN
			
			uiCurrentSuperState := eWait;
			//SafeAlign_act; // will set superstate to WAIT when complete
			
		END_IF;
		bHardwareInitError := FALSE;

	END_IF
	
	tAxisX1Move.state := eArrived;
	tAxisY1Move.state := eArrived;
	tAxisY2Move.state := eArrived;
	tAxisZ1Move.state := eArrived;
	tAxisZ2Move.state := eArrived;
	tAxisRMove.state := eArrived;
	SetAcceleration_act;
	
END_ACTION
