﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.4.7.144 SP?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Objects>
    <Object Type="File" Description="Cyclic code">Cyclic.st</Object>
    <Object Type="File" Description="Init code">Init.st</Object>
    <Object Type="File" Description="Exit code">Exit.st</Object>
    <Object Type="File" Description="Local data types" Private="true">Types.typ</Object>
    <Object Type="File" Description="Local variables" Private="true">Axis.var</Object>
    <Object Type="File" Private="true">Rotary.var</Object>
    <Object Type="File" Private="true">States.var</Object>
    <Object Type="File" Private="true">FOUP_GC.var</Object>
    <Object Type="File" Private="true">Variables.var</Object>
    <Object Type="File" Private="true">AxisJog.var</Object>
    <Object Type="File">AxisMove.st</Object>
    <Object Type="File">HWInit.st</Object>
    <Object Type="File">ErrorCheck.st</Object>
    <Object Type="Package">StaubliActions</Object>
    <Object Type="Package">InputActions</Object>
    <Object Type="Package">RotaryActions</Object>
    <Object Type="Package">StateActions</Object>
    <Object Type="Package">AxisJogAction</Object>
  </Objects>
</Program>