/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1568739698_1_
#define _BUR_1568739698_1_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef struct gtLinearRobotInput
{	plcbit bWaferSize200mm;
	plcbit bWaferSize300mm;
	plcbit bBatchCount1;
	plcbit bBatchCount2;
	signed char siBatch1FPort;	 /* IEC bounds for this type: 0..8*/
	signed char siBatch2FPort;	 /* IEC bounds for this type: 0..8*/
	plcbit bGC1;
	plcbit bGC2;
	plcbit bLoadGC;
	plcbit bUnloadGC;
	plcbit bBatch1GCTopLevel;
	plcbit bBatch1GCBottomLevel;
	plcbit bBatch2GCTopLevel;
	plcbit bBatch2GCBottomLevel;
	plcbit bEmergencyShutoff;
	plcbit bRUN;
	unsigned char siInputCount;
	plcbit bStaubliSafePos;
	plcbit bStaubliReady;
	plcbit obStaubliSafePosRE;
	unsigned char siFOUPPortNumberB1;
	unsigned char siFOUPPortNumberB2;
	plcbit bUnload300Batch1;
	plcbit bLoad300Batch1;
	plcbit bUnload200Batch2TB;
	plcbit bUnload200Batch2BT;
	plcbit bLoad200Batch2TB;
	plcbit bLoad200Batch2BT;
	plcbit bUnload200Batch1;
	plcbit bUnloadWaferType;
	plcbit bLoadWaferType;
	plcbit bLoad200Batch1;
	unsigned char siSelectJogAxis;
	unsigned char siSetAxisX1JogType;
	unsigned char siSelectAxisX1Loc;
	unsigned char siSelectAxisTargetLevel;
	plcbit bJogNegAxisX1PB;
	plcbit bJogNegAxisX1PBRE;
	plcbit bJogPosAxisX1PB;
	plcbit bJogPosAxisX1PBRE;
	plcbit bHomeAllAxisPB;
	plcbit bHomeAllAxisPBRE;
	plcbit bRotateAxisRPB;
	plcbit bRotateAxisRPBRE;
	unsigned short uiRotateAxisPos;
	plcbit bSetTargetPosPB;
	plcbit bSetTargetPosPBRE;
	plcbit bAutoCycleMode;
	plcbit bStartAutoCyclePB;
	plcbit bStartAutoCyclePBRE;
	plcbit bAxisErrorResetPB;
	plcbit bAxisErrorResetPBRE;
	unsigned short uiAutoCycleCnts;
	plcbit bReadStaubliRunCMDAck;
	plcbit bReadStaubliTargetPosAck;
	plcbit bReadStaubliRobotOnceAck;
	plcbit bWriteStaubliHeartBeatAck;
	plcbit bReadStaubliHeartbeatReturn;
	plcbit bStaubliArmPowerON;
	plcbit bStaubliPrgRunning;
	unsigned short uiStaubliErrorNumber;
	plcbit bStaubliErrorResetCMDAck;
	plcbit bStaubliArmPowerOnCMDAck;
	plcbit bStaubliArmPowerOffCMDAck;
} gtLinearRobotInput;

typedef struct gtLinearRobotOutpt
{	plcbit bComplete;
	plcbit bGuardianCarrierPresent;
	plcstring gstrError[81];
	unsigned short usiSRMoveStartPosition;
	unsigned short usiSRMoveTargetPosition;
	unsigned short uiWriteStaubliRobot;
	plcbit bWriteStaubliRobotOnce;
	unsigned short uiWriteStaubliTargetPosition;
	plcbit bWriteStaubliTargetOnce;
	plcbit bStaubliRunCMD;
	plcbit bStaubliRunCMDOnce;
	plcbit bWriteStaubliHeartBeat;
	plcbit bWriteStaubliHeartBeatOnce;
	plcbit bStaubliErrorResetCMD;
	plcbit bStaubliErrorResetCMDOnce;
	plcbit bStaubliArmPowerOnCMD;
	plcbit bStaubliArmPowerOnCMDOnce;
	plcbit bStaubliArmPowerOffCMD;
	plcbit bStaubliArmPowerOffCMDOnce;
} gtLinearRobotOutpt;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1568739698_1_ */

