/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1568739698_3_
#define _BUR_1568739698_3_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_GLOBAL struct MpComIdentType gZ2MpAxis;
_GLOBAL struct MpComIdentType gZ1MpAxis;
_GLOBAL struct MpComIdentType gY2MpAxis;
_GLOBAL struct MpComIdentType gY1MpAxis;
_GLOBAL struct MpComIdentType gX1MpAxis;
_GLOBAL struct MpComIdentType gAxisBasic;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/LinearRobotConfiguration/GlobalComponents/MpComponents.var\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1568739698_3_ */

