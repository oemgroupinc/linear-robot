/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1568739698_2_
#define _BUR_1568739698_2_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_GLOBAL plctime dtPrevCycleTime;
_GLOBAL plctime dtCycleTime;
_GLOBAL plcbit bCycleTimeStart;
_GLOBAL struct TON dtCycleTimeTON;
_GLOBAL plcwstring SW_Revision[11];
_GLOBAL unsigned char usiUnloadTargetPosition;
_GLOBAL unsigned char usiLoadTargetPosition;
_GLOBAL unsigned char usiUnloadStartPosition;
_GLOBAL unsigned char usiLoadStartPosition;
_GLOBAL unsigned short uiAutoCycleCounts;
_GLOBAL unsigned char usiRedIndColor;
_GLOBAL unsigned char usiBlueIndColor;
_GLOBAL unsigned char usiGreenIndColor;
_GLOBAL_RETAIN double rGC1AxisZ2LowCarrierPos;
_GLOBAL_RETAIN double rGC1AxisZ2UpCarrierPos;
_GLOBAL_RETAIN double rGC1AxisZ1LowTargetPos;
_GLOBAL_RETAIN double rFP4AxisZ1LowTargetPos;
_GLOBAL_RETAIN double rFP3AxisZ1LowTargetPos;
_GLOBAL_RETAIN double rFP2AxisZ1LowTargetPos;
_GLOBAL_RETAIN double rFP1AxisZ1LowTargetPos;
_GLOBAL_RETAIN double rGC1AxisZ1UpTargetPos;
_GLOBAL_RETAIN double rFP4AxisZ1UpTargetPos;
_GLOBAL_RETAIN double rFP3AxisZ1UpTargetPos;
_GLOBAL_RETAIN double rFP2AxisZ1UpTargetPos;
_GLOBAL_RETAIN double rFP1AxisZ1UpTargetPos;
_GLOBAL_RETAIN double rGC1AxisY2TargetPos;
_GLOBAL_RETAIN double rFP4AxisY2TargetPos;
_GLOBAL_RETAIN double rFP3AxisY2TargetPos;
_GLOBAL_RETAIN double rFP2AxisY2TargetPos;
_GLOBAL_RETAIN double rFP1AxisY2TargetPos;
_GLOBAL_RETAIN double rGC1AxisY1TargetPos;
_GLOBAL_RETAIN double rFP4AxisY1TargetPos;
_GLOBAL_RETAIN double rFP3AxisY1TargetPos;
_GLOBAL_RETAIN double rFP2AxisY1TargetPos;
_GLOBAL_RETAIN double rFP1AxisY1TargetPos;
_GLOBAL_RETAIN double rGC1AxisX1TargetPos;
_GLOBAL_RETAIN double rFP4AxisX1TargetPos;
_GLOBAL_RETAIN double rFP3AxisX1TargetPos;
_GLOBAL_RETAIN double rFP2AxisX1TargetPos;
_GLOBAL_RETAIN double rFP1AxisX1TargetPos;
_GLOBAL struct R_TRIG fbAxisErrorResetTrigRE;
_GLOBAL struct R_TRIG fbStartAutoCycleTrigRE;
_GLOBAL struct R_TRIG fbSetTargetPosPBTrigRE;
_GLOBAL struct R_TRIG fbRotateAxisRPBTrigRE;
_GLOBAL struct R_TRIG fbJogNegAxisX1PBTrigRE;
_GLOBAL struct R_TRIG fbHomeAllAxisPBTrigRE;
_GLOBAL struct R_TRIG fbJogPosAxisX1PBTrigRE;
_GLOBAL struct R_TRIG fbSRSafePosTrigRE;
_GLOBAL struct gtLinearRobotOutpt gLROutput;
_GLOBAL struct gtLinearRobotInput gLRInput;
_GLOBAL struct ACP10AXIS_typ Z2;
_GLOBAL struct ACP10AXIS_typ Z1;
_GLOBAL struct ACP10AXIS_typ X1;
_GLOBAL struct ACP10AXIS_typ Y2;
_GLOBAL struct ACP10AXIS_typ Y1;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1568739698_2_ */

