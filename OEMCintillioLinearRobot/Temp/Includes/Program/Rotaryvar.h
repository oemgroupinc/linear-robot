/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1568739698_6_
#define _BUR_1568739698_6_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL plcbit RotaryEMGS;
_BUR_LOCAL plcbit RotaryPWR;
_BUR_LOCAL plcbit RotaryZONE2;
_BUR_LOCAL plcbit RotaryZONE1;
_BUR_LOCAL plcbit RotaryRMDS;
_BUR_LOCAL plcbit RotaryBALM;
_BUR_LOCAL plcbit RotaryPSEL;
_BUR_LOCAL plcbit RotarySV;
_BUR_LOCAL plcbit RotaryALM;
_BUR_LOCAL plcbit RotaryMOVE;
_BUR_LOCAL plcbit RotaryHEND;
_BUR_LOCAL plcbit RotaryPEND;
_BUR_LOCAL unsigned short RotaryAlarmCode;
_BUR_LOCAL double RotarySpeed;
_BUR_LOCAL double RotaryComCurrent;
_BUR_LOCAL double RotaryPosition;
_BUR_LOCAL plcbyte Input15Byte;
_BUR_LOCAL plcbyte Input14Byte;
_BUR_LOCAL unsigned char RSOutput15;
_BUR_LOCAL unsigned char RSOutput14;
_BUR_LOCAL unsigned char RSOutput13;
_BUR_LOCAL unsigned char RSOutput12;
_BUR_LOCAL unsigned char RSOutput11;
_BUR_LOCAL unsigned char RSOutput10;
_BUR_LOCAL unsigned char RSOutput09;
_BUR_LOCAL unsigned char RSOutput08;
_BUR_LOCAL unsigned char RSOutput07;
_BUR_LOCAL unsigned char RSOutput06;
_BUR_LOCAL unsigned char RSOutput05;
_BUR_LOCAL unsigned char RSOutput04;
_BUR_LOCAL unsigned char RSOutput03;
_BUR_LOCAL unsigned char RSOutput02;
_BUR_LOCAL unsigned char RSOutput01;
_BUR_LOCAL unsigned char RSOutput00;
_BUR_LOCAL unsigned char RSInput15;
_BUR_LOCAL unsigned char RSInput14;
_BUR_LOCAL unsigned char RSInput13;
_BUR_LOCAL unsigned char RSInput12;
_BUR_LOCAL unsigned char RSInput11;
_BUR_LOCAL unsigned char RSInput10;
_BUR_LOCAL unsigned char RSInput09;
_BUR_LOCAL unsigned char RSInput08;
_BUR_LOCAL unsigned char RSInput07;
_BUR_LOCAL unsigned char RSInput06;
_BUR_LOCAL unsigned char RSInput05;
_BUR_LOCAL unsigned char RSInput04;
_BUR_LOCAL unsigned char RSInput03;
_BUR_LOCAL unsigned char RSInput02;
_BUR_LOCAL unsigned char RSInput01;
_BUR_LOCAL unsigned char RSInput00;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Program/Rotary.var\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1568739698_6_ */

