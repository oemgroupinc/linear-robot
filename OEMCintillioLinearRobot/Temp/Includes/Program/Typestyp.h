/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1568739698_4_
#define _BUR_1568739698_4_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef enum AxisState_enum
{	eInitiate = 10,
	ePending = 11,
	eInMotion = 15,
	eArrived = 20
} AxisState_enum;

typedef enum LRSuperState_enum
{	eWait = 0,
	eHardwareInitialization = 100,
	eRunning = 200
} LRSuperState_enum;

typedef enum LRState_enum
{	eSafeAlign = 0,
	eFPAlign = 100,
	eUnloadFP = 200,
	eGCAlign = 300,
	eLoadGC = 400,
	eUnloadGC = 600,
	eLoadFP = 800
} LRState_enum;

typedef enum LRSubState_enum
{	eX1SubState = 10,
	eX1SubStateWait = 11,
	eY1SubState = 20,
	eY1SubStateWait = 21,
	eY2SubState = 30,
	eY2SubStateWait = 31,
	eZ1SubState = 40,
	eZ1SubStateWait = 41,
	eZ2SubState = 50,
	eZ2SubStateWait = 51,
	eRSubState = 60,
	eRSubStateWait = 61,
	eSubStateFinished = 100
} LRSubState_enum;

typedef enum AxisX1Jog_enum
{	eWaitTargetReached = 30,
	eSetJogDirection = 20,
	eWaitJogCmdState = 10
} AxisX1Jog_enum;

typedef enum SRState_enum
{	eIdleState = 10,
	eCheckError = 15,
	eWaferHandlingType = 16,
	eRunLinearRobot = 20,
	eLinearRobotMoveComplete = 30,
	eRunStaubliRobot = 40,
	eResetStaubliRobotCmd = 45,
	eWaitStaubliRobotOnceAck = 47,
	eStaubliRobotMoveComplete = 50
} SRState_enum;

typedef enum LRAxisHome_enum
{	eY1HomeState = 10,
	eY2HomeState = 20,
	eRHomeState = 30,
	eX1HomeState = 40,
	eZ1HomeState = 50,
	eZ2HomeState = 60,
	eHomeStateFinished = 100
} LRAxisHome_enum;

typedef enum LRAxisJog_enum
{	eAxisJogOff = 0,
	eAxisX1Jog = 10,
	eAxisY1Jog = 20,
	eAxisY2Jog = 30,
	eAxisZ1Jog = 40,
	eAxisZ2Jog = 50,
	eAxisRRotate = 60,
	eAxisReturnHome = 70
} LRAxisJog_enum;

typedef enum LRAxisRRotate_enum
{	eWaitRotateCmdState = 10,
	eAxisRRotate180 = 20,
	eAxisRRotate90 = 30,
	eAxisRRotate270 = 40
} LRAxisRRotate_enum;

typedef enum LRAxisX1TargetPos_enum
{	eFOUP1Position = 1,
	eFOUP2Position = 2,
	eFOUP3Position = 3,
	eFOUP4Position = 4,
	eCarrier1Position = 10
} LRAxisX1TargetPos_enum;

typedef enum LRAxisTargetLevel_enum
{	eUpperPosition = 1,
	eLowerPosition = 2
} LRAxisTargetLevel_enum;

typedef enum SRAutoCycleState_enum
{	eIdle = 0,
	eAutoCycleStart = 10,
	eResetCounts = 20,
	eCycleCountCheck = 30,
	eLoadBatchBT = 40,
	eWaitLoadComplete = 50,
	eUnLoadBatchTB = 60,
	eWaitUnLoadComplete = 70,
	eCycleCounts = 80
} SRAutoCycleState_enum;

typedef enum SRMoveState_enum
{	eIdleMoveState = 0,
	eWriteMovePositions = 10,
	eWriteStartPosition = 20,
	eWriteTargetPosition = 30,
	eWaitStaubliOnceAck = 40,
	eSetRunCMDStaubliRobot = 50,
	eResetOnceSignal = 60,
	eResetRunCMD = 70,
	eResetOnceRunCMD = 80
} SRMoveState_enum;

typedef struct Process_typ
{	plcbit bLOAD;
	plcbit bUNLOAD;
	plcbit bDUALLOAD;
	plcbit bDUALUNLOAD;
	unsigned char uiBatchTracker;
} Process_typ;

typedef struct LinearRobot_typ
{	float siX1SafePos;
	float siY1SafePos;
	float siY2SafePos;
	float siZ1SafePos;
	float siZ2SafePos;
	float siRSafePos;
	float siXHomePos;
	float siY1HomePos;
	float siY2HomePos;
	float siZ1HomePos;
	float siZ2HomePos;
	float siRHomePos;
	float siFastAccelX1;
	float siSlowAccelX1;
	float siFastVelX1;
	float siSlowVelX1;
	float siFastAccelY1;
	float siSlowAccelY1;
	float siFastVelY1;
	float siSlowVelY1;
	float siFastAccelY2;
	float siSlowAccelY2;
	float siFastVelY2;
	float siSlowVelY2;
	float siFastAccelZ1;
	float siSlowAccelZ1;
	float siFastVelZ1;
	float siSlowVelZ1;
	float siFastAccelZ2;
	float siSlowAccelZ2;
	float siFastVelZ2;
	float siSlowVelZ2;
	float siFastAccelR;
	float siSlowAccelR;
	float siFastSpeedR;
	float siSlowSpeedR;
	struct Process_typ Process;
	plcbit bWaferPossession;
	float rCoarseJogDelX1;
	float rCoarseJogAccelX1;
	float rFineJogIncX1;
	float rCoarseJogVelX1;
	double rX1JogUpperTravelLimit;
	double rX1JogLowerTravelLimit;
	double rYJogUpperTravelLimit;
	double rYJogLowerTravelLimit;
	double rY2JogUpperTravelLimit;
	double rY2JogLowerTravelLimit;
	double rZ1JogUpperTravelLimit;
	double rZ1JogLowerTravelLimit;
	double rZ2JogUpperTravelLimit;
	double rZ2JogLowerTravelLimit;
	plcbit bHomeAllAxis;
	unsigned char usiSWStateIndTxt;
	unsigned char usiSWStateIndColor;
	unsigned char usiSRSafePosInd;
	unsigned char usiSRReadyInd;
} LinearRobot_typ;

typedef struct GuardianCarrierLevel_typ
{	plcbit bTop;
	plcbit bBottom;
	plcbit bNA;
} GuardianCarrierLevel_typ;

typedef struct GuardianCarrierOperation_typ
{	plcbit bLoad;
	plcbit bUnload;
} GuardianCarrierOperation_typ;

typedef struct FOUPPort_typ
{	double siX1Pos;
	double siY1AccessPos;
	double siY2AccessPos;
	double siZ1LRPossession;
	double siZ1FPPossession;
	double rNewAxisX1TargetPos;
	double rPrevAxisX1TargetPos;
	double rNewAxisY1TargetPos;
	double rPrevAxisY1TargetPos;
	double rNewAxisY2TargetPos;
	double rPrevAxisY2TargetPos;
	double rNewAxisZ1UpTargetPos;
	double rPrevAxisZ1UpTargetPos;
	double rNewAxisZ1LowTargetPos;
	double rPrevAxisZ1LowTargetPos;
} FOUPPort_typ;

typedef struct GuardianCarrier_typ
{	signed short eLocationID;
	plcbit bAbsentInd;
	double siX1Pos;
	double siY1AccessPos;
	double siY2AccessPos;
	double siZ1LRPossession;
	double siZ1GCPossession;
	double siZ2TopPos;
	double siZ2BottomPos;
	double siZ2300Pos;
	double rNewAxisX1TargetPos;
	double rPrevAxisX1TargetPos;
	double rNewAxisY1TargetPos;
	double rPrevAxisY1TargetPos;
	double rNewAxisY2TargetPos;
	double rPrevAxisY2TargetPos;
	double rNewAxisZ1LowTargetPos;
	double rPrevAxisZ1LowTargetPos;
	double rNewAxisZ1UpTargetPos;
	double rPrevAxisZ1UpTargetPos;
	double rPrevAxisZ2UpCarrierPos;
	double rPrevAxisZ2LowCarrierPos;
} GuardianCarrier_typ;

typedef struct AxisMove_typ
{	struct MpAxisBasic* AXIS;
	enum AxisState_enum state;
	plcbit bMove;
	double siTargetPos;
	float rAccel;
	float rSpeed;
} AxisMove_typ;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Program/Types.typ\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1568739698_4_ */

