/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1568739698_8_
#define _BUR_1568739698_8_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL struct GuardianCarrier_typ tTargetGC;
_BUR_LOCAL struct GuardianCarrierLevel_typ tGCLevelBatch2;
_BUR_LOCAL struct GuardianCarrierLevel_typ tGCLevelBatch1;
_BUR_LOCAL struct GuardianCarrier_typ tGuardianCarrier2;
_BUR_LOCAL struct GuardianCarrier_typ tGuardianCarrier1;
_BUR_LOCAL struct FOUPPort_typ tFOUPPort8;
_BUR_LOCAL struct FOUPPort_typ tFOUPPort7;
_BUR_LOCAL struct FOUPPort_typ tFOUPPort6;
_BUR_LOCAL struct FOUPPort_typ tFOUPPort5;
_BUR_LOCAL struct FOUPPort_typ tFOUPPort4;
_BUR_LOCAL struct FOUPPort_typ tFOUPPort3;
_BUR_LOCAL struct FOUPPort_typ tFOUPPort2;
_BUR_LOCAL struct FOUPPort_typ tFOUPPort1;
_BUR_LOCAL struct FOUPPort_typ tCurrentTargetFP;
_BUR_LOCAL struct FOUPPort_typ tTargetFP1;
_BUR_LOCAL struct FOUPPort_typ tTargetFP2;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Program/FOUP_GC.var\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1568739698_8_ */

