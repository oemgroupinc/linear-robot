#ifndef _PROGRAM_DEFAULT_2141241086
#define _PROGRAM_DEFAULT_2141241086
#include "../AsDefault.h"
#include <bur/plctypes.h>
#include <bur/plc.h>
#include <typesTYP.h>
#include <axisVAR.h>
#include <rotaryVAR.h>
#include <statesVAR.h>
#include <foup_gcVAR.h>
#include <variablesVAR.h>
#include <axisjogVAR.h>
#endif
