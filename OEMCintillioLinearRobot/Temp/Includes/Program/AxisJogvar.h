/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1568739698_10_
#define _BUR_1568739698_10_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define siCoarseJogType 1U
 #define siFineJogType 2U
 #define siJogModeOff 0U
#else
 _LOCAL_CONST unsigned char siCoarseJogType;
 _LOCAL_CONST unsigned char siFineJogType;
 _LOCAL_CONST unsigned char siJogModeOff;
#endif







__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Program/AxisJog.var\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1568739698_10_ */

