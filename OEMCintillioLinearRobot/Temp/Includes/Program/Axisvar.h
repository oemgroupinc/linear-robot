/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1568739698_5_
#define _BUR_1568739698_5_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL plcbit AxisRAcknowlege;
_BUR_LOCAL struct AxisMove_typ tAxisRMove;
_BUR_LOCAL plcbit AxisY2Acknowlege;
_BUR_LOCAL unsigned short action_statusY2;
_BUR_LOCAL struct AxisMove_typ tAxisY2Move;
_BUR_LOCAL struct MC_BR_JogTargetPosition AxisY2Jog;
_BUR_LOCAL struct MC_BR_SetHardwareInputs AxisY2HardwareInputs;
_BUR_LOCAL struct MpAxisBasicParType AxisY2Parameters;
_BUR_LOCAL struct MpAxisBasic AxisY2;
_BUR_LOCAL plcbit AxisY1Acknowlege;
_BUR_LOCAL unsigned short action_statusY1;
_BUR_LOCAL struct AxisMove_typ tAxisY1Move;
_BUR_LOCAL struct MC_BR_JogTargetPosition AxisY1Jog;
_BUR_LOCAL struct MC_BR_SetHardwareInputs AxisY1HardwareInputs;
_BUR_LOCAL struct MpAxisBasicParType AxisY1Parameters;
_BUR_LOCAL struct MpAxisBasic AxisY1;
_BUR_LOCAL plcbit AxisZ2Acknowlege;
_BUR_LOCAL unsigned short action_statusZ2;
_BUR_LOCAL struct AxisMove_typ tAxisZ2Move;
_BUR_LOCAL struct MC_BR_JogTargetPosition AxisZ2Jog;
_BUR_LOCAL struct MC_BR_SetHardwareInputs AxisZ2HardwareInputs;
_BUR_LOCAL struct MpAxisBasicParType AxisZ2Parameters;
_BUR_LOCAL struct MpAxisBasic AxisZ2;
_BUR_LOCAL plcbit AxisZ1Acknowlege;
_BUR_LOCAL unsigned short action_statusZ1;
_BUR_LOCAL struct AxisMove_typ tAxisZ1Move;
_BUR_LOCAL struct MC_BR_JogTargetPosition AxisZ1Jog;
_BUR_LOCAL struct MC_BR_SetHardwareInputs AxisZ1HardwareInputs;
_BUR_LOCAL struct MpAxisBasicParType AxisZ1Parameters;
_BUR_LOCAL struct MpAxisBasic AxisZ1;
_BUR_LOCAL plcbit AxisX1Acknowlege;
_BUR_LOCAL unsigned short action_statusX1;
_BUR_LOCAL struct AxisMove_typ tAxisX1Move;
_BUR_LOCAL struct MC_BR_JogTargetPosition AxisX1Jog;
_BUR_LOCAL struct MC_BR_SetHardwareInputs AxisX1HardwareInputs;
_BUR_LOCAL struct MpAxisBasicParType AxisX1Parameters;
_BUR_LOCAL struct MpAxisBasic AxisX1;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Program/Axis.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/Acp10_MC/acp10_mc.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.fun\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1568739698_5_ */

