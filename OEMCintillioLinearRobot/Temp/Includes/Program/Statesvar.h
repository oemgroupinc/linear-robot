/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1568739698_7_
#define _BUR_1568739698_7_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL enum SRMoveState_enum uiSRMoveState;
_BUR_LOCAL enum SRAutoCycleState_enum uiSRAutoCycleState;
_BUR_LOCAL enum LRAxisTargetLevel_enum uiAxisTargetLevel;
_BUR_LOCAL enum LRAxisX1TargetPos_enum uiAxisTargetPosState;
_BUR_LOCAL enum LRAxisRRotate_enum uiAxisRRotateState;
_BUR_LOCAL enum LRAxisHome_enum uiAxisHomeState;
_BUR_LOCAL enum LRAxisJog_enum uiAxisJogState;
_BUR_LOCAL enum AxisX1Jog_enum uiAxisX1JogState;
_BUR_LOCAL enum SRState_enum uiSRState;
_BUR_LOCAL enum LRSubState_enum uiCurrentSubState;
_BUR_LOCAL enum LRState_enum uiCurrentState;
_BUR_LOCAL enum LRSuperState_enum uiCurrentSuperState;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Program/States.var\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1568739698_7_ */

