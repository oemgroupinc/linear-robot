/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1568739698_9_
#define _BUR_1568739698_9_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL plcbit bAxisY2ArrivedFlag;
_BUR_LOCAL plcbit bAxisY1ArrivedFlag;
_BUR_LOCAL plcbit bAxisRStartFlag;
_BUR_LOCAL plcbit bAxisRArrivedFlag;
_BUR_LOCAL plcbit bStaubliRobotRunCMD;
_BUR_LOCAL plcbit bStaubliRobotSafePos;
_BUR_LOCAL plcbit bStaubliRobotReady;
_BUR_LOCAL unsigned long cudiMICRO_ENCODER_COMMAND;
_BUR_LOCAL plcbit bHardwareInitError;
_BUR_LOCAL plcbit bErrorCheckError;
_BUR_LOCAL plcbit bEmergencyCheckError;
_BUR_LOCAL struct LinearRobot_typ tLR;
_BUR_LOCAL plcwstring sHWInitErrorCode[81];





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Program/Variables.var\\\" scope \\\"local\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1568739698_9_ */

