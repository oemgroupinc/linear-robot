#ifndef _DEFAULT_757602046
#define _DEFAULT_757602046
#include <bur/plctypes.h>
#include <bur/plc.h>

#ifdef __cplusplus 
extern "C" 
{
#endif
	#include <operator.h>
	#include <runtime.h>
	#include <astime.h>
	#include <AsIecCon.h>
	#include <brsystem.h>
	#include <sys_lib.h>
	#include <standard.h>
	#include <AsZip.h>
	#include <FileIO.h>
	#include <MpBase.h>
	#include <NcGlobal.h>
	#include <Acp10par.h>
	#include <CoTrace.h>
	#include <MpCom.h>
	#include <Acp10man.h>
	#include <Acp10_MC.h>
	#include <MpAxis.h>
#ifdef __cplusplus
};
#endif

#include <mpcomponentsVAR.h>
#include <globalTYP.h>
#include <globalVAR.h>
#endif
