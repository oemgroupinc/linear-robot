######################################################
#                                                    #
# Automatic generated Makefile for Visual Components #
#                                                    #
#                  Do NOT edit!                      #
#                                                    #
######################################################

VCC:=@'$(AS_BIN_PATH)/br.vc.pc.exe'
LINK:=@'$(AS_BIN_PATH)/BR.VC.Link.exe'
MODGEN:=@'$(AS_BIN_PATH)/BR.VC.ModGen.exe'
VCPL:=@'$(AS_BIN_PATH)/BR.VC.PL.exe'
VCHWPP:=@'$(AS_BIN_PATH)/BR.VC.HWPP.exe'
VCDEP:=@'$(AS_BIN_PATH)/BR.VC.Depend.exe'
VCFLGEN:=@'$(AS_BIN_PATH)/BR.VC.lfgen.exe'
VCREFHANDLER:=@'$(AS_BIN_PATH)/BR.VC.CrossReferenceHandler.exe'
VCXREFEXTENDER:=@'$(AS_BIN_PATH)/BR.AS.CrossRefVCExtender.exe'
RM=CMD /C DEL
PALFILE_HMI=$(AS_PROJECT_PATH)/Logical/HMI/Palette.vcr
VCCFLAGS_HMI=-server -proj HMI -vc '$(AS_PROJECT_PATH)/Logical/HMI/VCObject.vc' -prj_path '$(AS_PROJECT_PATH)' -temp_path '$(AS_TEMP_PATH)' -cfg $(AS_CONFIGURATION) -plc $(AS_PLC) -plctemp $(AS_TEMP_PLC) -cpu_path '$(AS_CPU_PATH)'
VCFIRMWARE=4.44.0
VCFIRMWAREPATH=$(AS_VC_PATH)/Firmware/V4.44.0/SG4
VCOBJECT_HMI=$(AS_PROJECT_PATH)/Logical/HMI/VCObject.vc
VCSTARTUP='vcstart.br'
VCLOD='vclod.br'
VCSTPOST='vcstpost.br'
TARGET_FILE_HMI=$(AS_CPU_PATH)/HMI.br
OBJ_SCOPE_HMI=
PRJ_PATH_HMI=$(AS_PROJECT_PATH)
SRC_PATH_HMI=$(AS_PROJECT_PATH)/Logical/$(OBJ_SCOPE_HMI)/HMI
TEMP_PATH_HMI=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/HMI
TEMP_PATH_Shared=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared
TEMP_PATH_ROOT_HMI=$(AS_TEMP_PATH)
VC_LIBRARY_LIST_HMI=$(TEMP_PATH_HMI)/libraries.vci
VC_XREF_BUILDFILE_HMI=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcxref.build
VC_XREF_CLEANFILE=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcxref.clean
VC_LANGUAGES_HMI=$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr
CPUHWC='$(TEMP_PATH_HMI)/cpuhwc.vci'
VC_STATIC_OPTIONS_HMI='$(TEMP_PATH_HMI)/vcStaticOptions.xml'
VC_STATIC_OPTIONS_Shared='$(TEMP_PATH_Shared)/vcStaticOptions.xml'
# include Shared and Font Makefile (only once)
	include $(AS_TEMP_PATH)/objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCFntDat/Font_HMI.mak
ifneq ($(VCINC),1)
	VCINC=1
	include $(AS_TEMP_PATH)/objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/VCShared.mak
endif

DEPENDENCIES_HMI=-d vcgclass -profile 'False'
DEFAULT_STYLE_SHEET_HMI='Source[local].StyleSheet[Color]'
SHARED_MODULE=$(TEMP_PATH_ROOT_HMI)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcshared.br
LFNTFLAGS_HMI=-P '$(AS_PROJECT_PATH)' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)'
BDRFLAGS_HMI=-P '$(AS_PROJECT_PATH)' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)'

# Local Libs
LIB_LOCAL_OBJ_HMI=$(TEMP_PATH_HMI)/localobj.vca

# Hardware sources
PANEL_HW_OBJECT_HMI=$(TEMP_PATH_ROOT_HMI)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/HMI/dis.Hardware.vco
PANEL_HW_VCI_HMI=$(TEMP_PATH_ROOT_HMI)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/HMI/dis.Hardware.vci
PANEL_HW_SOURCE_HMI=C:/Projects/Linear\ Robot/OEMCintillioLinearRobot/Physical/LinearRobotConfiguration/Hardware.hw 
DIS_OBJECTS_HMI=$(PANEL_HW_OBJECT_HMI) $(KEYMAP_OBJECTS_HMI)

# KeyMapping flags
$(TEMP_PATH_HMI)/dis.6PPT30.101G-20W.vco: $(AS_PROJECT_PATH)/Physical/LinearRobotConfiguration/X20CP0484/VC/6PPT30.101G-20W.dis $(PANEL_HW_SOURCE_HMI)
	$(VCHWPP) -f '$(PANEL_HW_SOURCE_HMI)' -o '$(subst .vco,.vci,$(TEMP_PATH_HMI)/dis.6PPT30.101G-20W.vco)' -n HMI -d HMI -pal '$(PALFILE_HMI)' -c '$(AS_CONFIGURATION)' -p '$(AS_PLC)' -ptemp '$(AS_TEMP_PLC)' -B 'F4.44' -L '' -hw '$(CPUHWC)' -warninglevel 2 -so $(VC_STATIC_OPTIONS_HMI) -sos $(VC_STATIC_OPTIONS_Shared) -keyboard '$(AS_PROJECT_PATH)/Physical/LinearRobotConfiguration/X20CP0484/VC/6PPT30.101G-20W.dis' -fp '$(AS_VC_PATH)/Firmware/V4.44.0/SG4' -prj '$(AS_PROJECT_PATH)' -apj 'OEMCintillioLinearRobot' -sfas -vcob '$(VCOBJECT_HMI)'
	$(VCC) -f '$(subst .vco,.vci,$@)' -o '$@' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -k '$(VCVK_SOURCES_HMI)' $(VCCFLAGS_HMI) -p HMI -sfas

KEYMAP_SOURCES_HMI=$(AS_PROJECT_PATH)/Physical/LinearRobotConfiguration/X20CP0484/VC/6PPT30.101G-20W.dis 
KEYMAP_OBJECTS_HMI=$(TEMP_PATH_HMI)/dis.6PPT30.101G-20W.vco 

# All Source Objects
FNINFO_SOURCES_HMI=$(AS_PROJECT_PATH)/Logical/HMI/Fonts/DefaultFont.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Fonts/Arial9px.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Fonts/Arial9pxBold.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Fonts/Arial10pxBold.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Fonts/Arial12px.fninfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Fonts/Arial9pxValue.fninfo 

BMINFO_SOURCES_HMI=$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA3.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadVGA_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA2_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA3_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA1_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadVGA.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/NumPad_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/NumPad.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AcknowledgeReset.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmActive.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmBypassOFF.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmBypassON.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmInactive.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmLatched.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmNotQuit.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmQuit.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/Reset.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ResetAcknowledge.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/Triggered.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ListPadHor.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ListPadHor_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ListPadVer.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ListPadVer_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/backward_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/backward_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/global_area_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/global_area_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/forward_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/forward_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/control_button_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/control_button_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/right_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/right_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/left_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/left_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/up_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/up_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/down_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/down_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/multi_up_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/multi_up_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/multi_down_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/multi_down_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ProgressBorder.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/down_active_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/down_pressed_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/up_active_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/global_area_gradient_upside.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/global_area_gradient_downside.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/frame_header.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/up_pressed_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/EditPadVGA.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/EditPadVGA_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneNumPad_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneListPadVer_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPad_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPad_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPadQVGA1_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPadQVGA1_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPadQVGA2_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPadQVGA3_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneEditpadQVGA2_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneEditPadQVGA3_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneEditPadVga_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneEditPadVga_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneListPadHor_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneListPadHor_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneListPadVer_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneNumPad_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/Bitmap_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/Bitmap_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/OEM_Logo1.bminfo 

BMGRP_SOURCES_HMI=$(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/AlphaPadQVGA.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/NumPad.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/AlphaPad.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/AlarmEvent.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/AlarmState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/BypassState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/AcknowledgeState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/NavigationPad.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/Borders.bmgrp \
	$(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/OEMGroup.bmgrp 

PAGE_SOURCES_HMI=$(AS_PROJECT_PATH)/Logical/HMI/Pages/Init_Page.page \
	$(AS_PROJECT_PATH)/Logical/HMI/Pages/Jog_Page.page \
	$(AS_PROJECT_PATH)/Logical/HMI/Pages/Axis_Alarms.page 

VCS_SOURCES_HMI=$(AS_PROJECT_PATH)/Logical/HMI/StyleSheets/Gray.vcs \
	$(AS_PROJECT_PATH)/Logical/HMI/StyleSheets/Color.vcs 

BDR_SOURCES_HMI=$(AS_PROJECT_PATH)/Logical/HMI/Borders/Raised.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/Sunken.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/Etched.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/Bump.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/SunkenOuter.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/RaisedInner.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/Flat_black.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/Flat_grey.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/BackwardActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/BackwardPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ControlActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ControlPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/DownActiveControl.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/DownPressedControl.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ForwardActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ForwardPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/GlobalAreaActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/GlobalAreaPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/MultiScrollDownActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/MultiScrollDownPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/MultiScrollUpActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/MultiScrollUpPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ProgressBarBorder.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollDownActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollDownPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollUpActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollUpPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollLeftActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollLeftPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollRightActive.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollRightPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/UpActiveControl.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/UpPressedControl.bdr \
	$(AS_PROJECT_PATH)/Logical/HMI/Borders/FrameHeader.bdr 

TPR_SOURCES_HMI=$(AS_PROJECT_PATH)/Logical/HMI/TouchPads/NumPad.tpr \
	$(AS_PROJECT_PATH)/Logical/HMI/TouchPads/AlphaPadQVGA.tpr \
	$(AS_PROJECT_PATH)/Logical/HMI/TouchPads/AlphaPad.tpr \
	$(AS_PROJECT_PATH)/Logical/HMI/TouchPads/NavigationPad_ver.tpr \
	$(AS_PROJECT_PATH)/Logical/HMI/TouchPads/NavigationPad_hor.tpr \
	$(AS_PROJECT_PATH)/Logical/HMI/TouchPads/EditPad.tpr 

TDC_SOURCES_HMI=$(AS_PROJECT_PATH)/Logical/HMI/Trends/TrendData.tdc 

VCVK_SOURCES_HMI=$(AS_PROJECT_PATH)/Logical/HMI/VirtualKeys.vcvk 

VCR_SOURCES_HMI=$(AS_PROJECT_PATH)/Logical/HMI/Palette.vcr 

# Runtime Object sources
VCR_OBJECT_HMI=$(TEMP_PATH_HMI)/vcrt.vco
VCR_SOURCE_HMI=$(SRC_PATH_HMI)/package.vcp
# All Source Objects END

#Panel Hardware
$(PANEL_HW_VCI_HMI): $(PANEL_HW_SOURCE_HMI) $(VC_LIBRARY_LIST_HMI) $(KEYMAP_SOURCES_HMI)
	$(VCHWPP) -f '$<' -o '$@' -n HMI -d HMI -pal '$(PALFILE_HMI)' -c '$(AS_CONFIGURATION)' -p '$(AS_PLC)' -ptemp '$(AS_TEMP_PLC)' -B 'F4.44' -L '' -verbose 'False' -profile 'False' -hw '$(CPUHWC)' -warninglevel 2 -so $(VC_STATIC_OPTIONS_HMI) -sos $(VC_STATIC_OPTIONS_Shared) -fp '$(AS_VC_PATH)/Firmware/V4.44.0/SG4' -sfas -prj '$(AS_PROJECT_PATH)' -apj 'OEMCintillioLinearRobot' -vcob '$(VCOBJECT_HMI)'

$(PANEL_HW_OBJECT_HMI): $(PANEL_HW_VCI_HMI) $(PALFILE_HMI) $(VC_LIBRARY_LIST_HMI)
	$(VCC) -f '$(subst .vco,.vci,$@)' -o '$@' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -k '$(VCVK_SOURCES_HMI)' $(VCCFLAGS_HMI) -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


# Pages
PAGE_OBJECTS_HMI = $(addprefix $(TEMP_PATH_HMI)/page., $(notdir $(PAGE_SOURCES_HMI:.page=.vco)))

$(TEMP_PATH_HMI)/page.Init_Page.vco: $(AS_PROJECT_PATH)/Logical/HMI/Pages/Init_Page.page $(VC_LANGUAGES_HMI)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI) -P '$(AS_PROJECT_PATH)' -ds '$(SRC_PATH_HMI)/StyleSheets/Color.vcs' -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/page.Jog_Page.vco: $(AS_PROJECT_PATH)/Logical/HMI/Pages/Jog_Page.page $(VC_LANGUAGES_HMI)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI) -P '$(AS_PROJECT_PATH)' -ds '$(SRC_PATH_HMI)/StyleSheets/Color.vcs' -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/page.Axis_Alarms.vco: $(AS_PROJECT_PATH)/Logical/HMI/Pages/Axis_Alarms.page $(VC_LANGUAGES_HMI)
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI) -P '$(AS_PROJECT_PATH)' -ds '$(SRC_PATH_HMI)/StyleSheets/Color.vcs' -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


#Pages END




# Stylesheets
VCS_OBJECTS_HMI = $(addprefix $(TEMP_PATH_HMI)/vcs., $(notdir $(VCS_SOURCES_HMI:.vcs=.vco)))

$(TEMP_PATH_HMI)/vcs.Gray.vco: $(AS_PROJECT_PATH)/Logical/HMI/StyleSheets/Gray.vcs
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI) -P '$(AS_PROJECT_PATH)' -ds $(DEFAULT_STYLE_SHEET_HMI) -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/vcs.Color.vco: $(AS_PROJECT_PATH)/Logical/HMI/StyleSheets/Color.vcs
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI) -P '$(AS_PROJECT_PATH)' -ds $(DEFAULT_STYLE_SHEET_HMI) -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


#Stylesheets END




# Virtual Keys
VCVK_OBJECTS_HMI = $(addprefix $(TEMP_PATH_HMI)/vcvk., $(notdir $(VCVK_SOURCES_HMI:.vcvk=.vco)))

$(TEMP_PATH_HMI)/vcvk.VirtualKeys.vco: $(AS_PROJECT_PATH)/Logical/HMI/VirtualKeys.vcvk
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas

$(VCVK_OBJECTS_HMI): $(VC_LANGUAGES_HMI)

#Virtual Keys END




# Touch Pads
TPR_OBJECTS_HMI = $(addprefix $(TEMP_PATH_HMI)/tpr., $(notdir $(TPR_SOURCES_HMI:.tpr=.vco)))

$(TEMP_PATH_HMI)/tpr.NumPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/TouchPads/NumPad.tpr
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI) -prj 'C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/HMI' -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/tpr.AlphaPadQVGA.vco: $(AS_PROJECT_PATH)/Logical/HMI/TouchPads/AlphaPadQVGA.tpr
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI) -prj 'C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/HMI' -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/tpr.AlphaPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/TouchPads/AlphaPad.tpr
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI) -prj 'C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/HMI' -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/tpr.NavigationPad_ver.vco: $(AS_PROJECT_PATH)/Logical/HMI/TouchPads/NavigationPad_ver.tpr
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI) -prj 'C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/HMI' -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/tpr.NavigationPad_hor.vco: $(AS_PROJECT_PATH)/Logical/HMI/TouchPads/NavigationPad_hor.tpr
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI) -prj 'C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/HMI' -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/tpr.EditPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/TouchPads/EditPad.tpr
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI) -prj 'C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/HMI' -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


#Touch Pads END




# BitmapGroups
BMGRP_OBJECTS_HMI = $(addprefix $(TEMP_PATH_HMI)/bmgrp., $(notdir $(BMGRP_SOURCES_HMI:.bmgrp=.vco)))

$(TEMP_PATH_HMI)/bmgrp.AlphaPadQVGA.vco: $(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/AlphaPadQVGA.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bmgrp.NumPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/NumPad.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bmgrp.AlphaPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/AlphaPad.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bmgrp.AlarmEvent.vco: $(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/AlarmEvent.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bmgrp.AlarmState.vco: $(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/AlarmState.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bmgrp.BypassState.vco: $(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/BypassState.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bmgrp.AcknowledgeState.vco: $(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/AcknowledgeState.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bmgrp.NavigationPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/NavigationPad.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bmgrp.Borders.vco: $(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/Borders.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bmgrp.OEMGroup.vco: $(AS_PROJECT_PATH)/Logical/HMI/BitmapGroups/OEMGroup.bmgrp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


#BitmapGroups END




# Bitmaps
BMINFO_OBJECTS_HMI = $(addprefix $(TEMP_PATH_HMI)/bminfo., $(notdir $(BMINFO_SOURCES_HMI:.bminfo=.vco)))

$(TEMP_PATH_HMI)/bminfo.AlphaPadQVGA1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlphaPadQVGA2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA2.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlphaPadQVGA3.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA3.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA3.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlphaPadVGA_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadVGA_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadVGA_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlphaPadQVGA2_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA2_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA2_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlphaPadQVGA3_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA3_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA3_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlphaPadQVGA1_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA1_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadQVGA1_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlphaPadVGA.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadVGA.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlphaPadVGA.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.NumPad_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/NumPad_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/NumPad_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.NumPad.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/NumPad.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/NumPad.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AcknowledgeReset.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AcknowledgeReset.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AcknowledgeReset.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlarmActive.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmActive.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmActive.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlarmBypassOFF.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmBypassOFF.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmBypassOFF.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlarmBypassON.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmBypassON.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmBypassON.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlarmInactive.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmInactive.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmInactive.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlarmLatched.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmLatched.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmLatched.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlarmNotQuit.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmNotQuit.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmNotQuit.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.AlarmQuit.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmQuit.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/AlarmQuit.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.Reset.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/Reset.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/Reset.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.ResetAcknowledge.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ResetAcknowledge.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ResetAcknowledge.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.Triggered.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/Triggered.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/Triggered.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.ListPadHor.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ListPadHor.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ListPadHor.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.ListPadHor_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ListPadHor_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ListPadHor_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.ListPadVer.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ListPadVer.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ListPadVer.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.ListPadVer_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ListPadVer_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ListPadVer_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.backward_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/backward_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/backward_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.backward_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/backward_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/backward_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.global_area_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/global_area_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/global_area_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.global_area_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/global_area_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/global_area_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.forward_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/forward_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/forward_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.forward_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/forward_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/forward_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.control_button_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/control_button_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/control_button_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.control_button_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/control_button_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/control_button_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.right_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/right_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/right_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.right_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/right_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/right_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.left_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/left_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/left_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.left_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/left_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/left_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.up_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/up_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/up_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.up_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/up_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/up_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.down_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/down_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/down_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.down_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/down_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/down_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.multi_up_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/multi_up_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/multi_up_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.multi_up_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/multi_up_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/multi_up_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.multi_down_active.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/multi_down_active.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/multi_down_active.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.multi_down_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/multi_down_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/multi_down_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.ProgressBorder.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ProgressBorder.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/ProgressBorder.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.down_active_control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/down_active_control.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/down_active_control.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.down_pressed_control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/down_pressed_control.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/down_pressed_control.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.up_active_control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/up_active_control.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/up_active_control.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.global_area_gradient_upside.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/global_area_gradient_upside.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/global_area_gradient_upside.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.global_area_gradient_downside.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/global_area_gradient_downside.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/global_area_gradient_downside.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.frame_header.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/frame_header.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/frame_header.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.up_pressed_control.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/up_pressed_control.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/up_pressed_control.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.EditPadVGA.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/EditPadVGA.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/EditPadVGA.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.EditPadVGA_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/EditPadVGA_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/EditPadVGA_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneNumPad_released.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneNumPad_released.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneNumPad_released.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneListPadVer_released.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneListPadVer_released.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneListPadVer_released.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneAlphaPad_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPad_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPad_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneAlphaPad_released.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPad_released.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPad_released.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneAlphaPadQVGA1_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPadQVGA1_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPadQVGA1_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneAlphaPadQVGA1_released.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPadQVGA1_released.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPadQVGA1_released.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneAlphaPadQVGA2_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPadQVGA2_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPadQVGA2_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneAlphaPadQVGA3_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPadQVGA3_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneAlphaPadQVGA3_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneEditpadQVGA2_released.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneEditpadQVGA2_released.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneEditpadQVGA2_released.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneEditPadQVGA3_released.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneEditPadQVGA3_released.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneEditPadQVGA3_released.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneEditPadVga_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneEditPadVga_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneEditPadVga_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneEditPadVga_released.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneEditPadVga_released.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneEditPadVga_released.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneListPadHor_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneListPadHor_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneListPadHor_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneListPadHor_released.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneListPadHor_released.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneListPadHor_released.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneListPadVer_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneListPadVer_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneListPadVer_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.zuneNumPad_pressed.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneNumPad_pressed.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/zuneNumPad_pressed.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.Bitmap_1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/Bitmap_1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/Bitmap_1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.Bitmap_2.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/Bitmap_2.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/Bitmap_2.gif
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


$(TEMP_PATH_HMI)/bminfo.OEM_Logo1.vco: $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/OEM_Logo1.bminfo $(AS_PROJECT_PATH)/Logical/HMI/Bitmaps/OEM_Logo1.bmp
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


#Bitmaps END




# Trend Data Configuration
TDC_OBJECTS_HMI = $(addprefix $(TEMP_PATH_HMI)/tdc., $(notdir $(TDC_SOURCES_HMI:.tdc=.vco)))

$(TEMP_PATH_HMI)/tdc.TrendData.vco: $(AS_PROJECT_PATH)/Logical/HMI/Trends/TrendData.tdc
	 $(VCC) -f '$<' -o '$@' -l '$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -pal '$(PALFILE_HMI)' $(VCCFLAGS_HMI)  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas


#Trend Data Configuration END


#
# Borders
#
BDR_SOURCES_HMI=$(AS_PROJECT_PATH)/Logical/HMI/Borders/Raised.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/Sunken.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/Etched.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/Bump.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/SunkenOuter.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/RaisedInner.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/Flat_black.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/Flat_grey.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/BackwardActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/BackwardPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ControlActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ControlPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/DownActiveControl.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/DownPressedControl.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ForwardActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ForwardPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/GlobalAreaActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/GlobalAreaPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/MultiScrollDownActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/MultiScrollDownPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/MultiScrollUpActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/MultiScrollUpPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ProgressBarBorder.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollDownActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollDownPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollUpActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollUpPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollLeftActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollLeftPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollRightActive.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/ScrollRightPressed.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/UpActiveControl.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/UpPressedControl.bdr $(AS_PROJECT_PATH)/Logical/HMI/Borders/FrameHeader.bdr 
BDR_OBJECTS_HMI=$(TEMP_PATH_HMI)/bdr.Bordermanager.vco
$(TEMP_PATH_HMI)/bdr.Bordermanager.vco: $(BDR_SOURCES_HMI)
	$(VCC) -f '$<' -o '$@' -pkg '$(SRC_PATH_HMI)' $(BDRFLAGS_HMI) $(VCCFLAGS_HMI) -p HMI$(SRC_PATH_HMI)
#
# Logical fonts
#
$(TEMP_PATH_HMI)/lfnt.en.vco: $(TEMP_PATH_HMI)/en.lfnt $(VC_LANGUAGES_HMI)
	 $(VCC) -f '$<' -o '$@' $(LFNTFLAGS_HMI) $(VCCFLAGS_HMI) -p HMI -sfas
$(TEMP_PATH_HMI)/lfnt.de.vco: $(TEMP_PATH_HMI)/de.lfnt $(VC_LANGUAGES_HMI)
	 $(VCC) -f '$<' -o '$@' $(LFNTFLAGS_HMI) $(VCCFLAGS_HMI) -p HMI -sfas
LFNT_OBJECTS_HMI=$(TEMP_PATH_HMI)/lfnt.en.vco $(TEMP_PATH_HMI)/lfnt.de.vco 

#Runtime Object
$(VCR_OBJECT_HMI) : $(VCR_SOURCE_HMI)
	$(VCC) -f '$<' -o '$@' -cv '$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo' -sl en $(VCCFLAGS_HMI) -rt  -p HMI -so $(VC_STATIC_OPTIONS_HMI) -vcr 4440 -sfas
# Local resources Library rules
LIB_LOCAL_RES_HMI=$(TEMP_PATH_HMI)/localres.vca
$(LIB_LOCAL_RES_HMI) : $(TEMP_PATH_HMI)/HMI02.ccf

# Bitmap Library rules
LIB_BMP_RES_HMI=$(TEMP_PATH_HMI)/bmpres.vca
$(LIB_BMP_RES_HMI) : $(TEMP_PATH_HMI)/HMI03.ccf
$(BMGRP_OBJECTS_HMI) : $(PALFILE_HMI) $(VC_LANGUAGES_HMI)
$(BMINFO_OBJECTS_HMI) : $(PALFILE_HMI)

BUILD_FILE_HMI=$(TEMP_PATH_HMI)/BuildFiles.arg
$(BUILD_FILE_HMI) : BUILD_FILE_CLEAN_HMI $(BUILD_SOURCES_HMI)
BUILD_FILE_CLEAN_HMI:
	$(RM) /F /Q '$(BUILD_FILE_HMI)' 2>nul
#All Modules depending to this project
PROJECT_MODULES_HMI=$(AS_CPU_PATH)/HMI01.br $(AS_CPU_PATH)/HMI02.br $(AS_CPU_PATH)/HMI03.br $(FONT_MODULES_HMI) $(SHARED_MODULE)

# General Build rules

$(TARGET_FILE_HMI): $(PROJECT_MODULES_HMI) $(TEMP_PATH_HMI)/HMI.prj
	$(MODGEN) -so $(VC_STATIC_OPTIONS_HMI) -fw '$(VCFIRMWAREPATH)' -m $(VCSTPOST) -v V1.00.0 -f '$(TEMP_PATH_HMI)/HMI.prj' -o '$@' -vc '$(VCOBJECT_HMI)' $(DEPENDENCIES_HMI) $(addprefix -d ,$(notdir $(PROJECT_MODULES_HMI:.br=)))

$(AS_CPU_PATH)/HMI01.br: $(TEMP_PATH_HMI)/HMI01.ccf
	$(MODGEN) -so $(VC_STATIC_OPTIONS_HMI) -fw '$(VCFIRMWAREPATH)' -m $(VCLOD) -v V1.00.0 -b -vc '$(VCOBJECT_HMI)' -f '$<' -o '$@' $(DEPENDENCIES_HMI)

$(AS_CPU_PATH)/HMI02.br: $(TEMP_PATH_HMI)/HMI02.ccf
	$(MODGEN) -so $(VC_STATIC_OPTIONS_HMI) -fw '$(VCFIRMWAREPATH)' -m $(VCLOD) -v V1.00.0 -b -vc '$(VCOBJECT_HMI)' -f '$<' -o '$@' $(DEPENDENCIES_HMI)

$(AS_CPU_PATH)/HMI03.br: $(TEMP_PATH_HMI)/HMI03.ccf
	$(MODGEN) -so $(VC_STATIC_OPTIONS_HMI) -fw '$(VCFIRMWAREPATH)' -m $(VCLOD) -v V1.00.0 -b -vc '$(VCOBJECT_HMI)' -f '$<' -o '$@' $(DEPENDENCIES_HMI)

# General Build rules END
$(LIB_LOCAL_OBJ_HMI) : $(TEMP_PATH_HMI)/HMI01.ccf

# Main Module
$(TEMP_PATH_ROOT_HMI)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/HMI.vcm:
$(TEMP_PATH_HMI)/HMI.prj: $(TEMP_PATH_ROOT_HMI)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/HMI.vcm
	$(VCDEP) -m '$(TEMP_PATH_ROOT_HMI)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/HMI.vcm' -s '$(AS_CPU_PATH)/VCShared/Shared.vcm' -p '$(AS_PATH)/AS/VC/Firmware' -c '$(AS_CPU_PATH)' -fw '$(VCFIRMWAREPATH)' -hw '$(CPUHWC)' -so $(VC_STATIC_OPTIONS_HMI) -o HMI -proj HMI
	$(VCPL) $(notdir $(PROJECT_MODULES_HMI:.br=,4)) HMI,2 -o '$@' -p HMI -vc 'HMI' -verbose 'False' -fl '$(TEMP_PATH_ROOT_HMI)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/HMI.vcm' -vcr '$(VCR_SOURCE_HMI)' -prj '$(AS_PROJECT_PATH)' -warningLevel2 -sfas

# 01 Module

DEL_TARGET01_LFL_HMI=$(TEMP_PATH_HMI)\HMI01.ccf.lfl
$(TEMP_PATH_HMI)/HMI01.ccf: $(LIB_SHARED) $(SHARED_CCF) $(LIB_BMP_RES_HMI) $(TEMP_PATH_HMI)/HMI03.ccf $(LIB_LOCAL_RES_HMI) $(TEMP_PATH_HMI)/HMI02.ccf $(DIS_OBJECTS_HMI) $(PAGE_OBJECTS_HMI) $(VCS_OBJECTS_HMI) $(VCVK_OBJECTS_HMI) $(VCRT_OBJECTS_HMI) $(TPR_OBJECTS_HMI) $(TXTGRP_OBJECTS_HMI) $(LAYER_OBJECTS_HMI) $(VCR_OBJECT_HMI) $(TDC_OBJECTS_HMI) $(TRD_OBJECTS_HMI) $(TRE_OBJECTS_HMI) $(PRC_OBJECTS_HMI) $(SCR_OBJECTS_HMI)
	-@CMD /Q /C if exist "$(DEL_TARGET01_LFL_HMI)" DEL /F /Q "$(DEL_TARGET01_LFL_HMI)" 2>nul
	@$(VCFLGEN) '$@.lfl' '$(LIB_SHARED)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(LIB_BMP_RES_HMI)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(LIB_LOCAL_RES_HMI)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(DIS_OBJECTS_HMI:.vco=.vco|)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .page -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Package.vcp' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(VCS_OBJECTS_HMI:.vco=.vco|)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .vcvk -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Package.vcp' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(VCRT_OBJECTS_HMI:.vco=.vco|)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(TPR_OBJECTS_HMI:.vco=.vco|)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .txtgrp -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Package.vcp' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .layer -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Package.vcp' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(VCR_OBJECT_HMI:.vco=.vco|)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .tdc -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Package.vcp' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .trd -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Package.vcp' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(SCR_OBJECTS_HMI:.vco=.vco|)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	$(LINK) '$@.lfl' -o '$@' -p HMI -lib '$(LIB_LOCAL_OBJ_HMI)' -P '$(AS_PROJECT_PATH)' -m 'local objects' -profile 'False' -warningLevel2 -vcr 4440 -sfas
# 01 Module END

# 02 Module

DEL_TARGET02_LFL_HMI=$(TEMP_PATH_HMI)\HMI02.ccf.lfl
$(TEMP_PATH_HMI)/HMI02.ccf: $(LIB_SHARED) $(SHARED_CCF) $(LIB_BMP_RES_HMI) $(TEMP_PATH_HMI)/HMI03.ccf $(BDR_OBJECTS_HMI) $(LFNT_OBJECTS_HMI) $(CLM_OBJECTS_HMI)
	-@CMD /Q /C if exist "$(DEL_TARGET02_LFL_HMI)" DEL /F /Q "$(DEL_TARGET02_LFL_HMI)" 2>nul
	@$(VCFLGEN) '$@.lfl' '$(LIB_SHARED)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(LIB_BMP_RES_HMI)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(BDR_OBJECTS_HMI:.vco=.vco|)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(LFNT_OBJECTS_HMI:.vco=.vco|)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' '$(CLM_OBJECTS_HMI:.vco=.vco|)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	$(LINK) '$@.lfl' -o '$@' -p HMI -lib '$(LIB_LOCAL_RES_HMI)' -P '$(AS_PROJECT_PATH)' -m 'local resources' -profile 'False' -warningLevel2 -vcr 4440 -sfas
# 02 Module END

# 03 Module

DEL_TARGET03_LFL_HMI=$(TEMP_PATH_HMI)\HMI03.ccf.lfl
$(TEMP_PATH_HMI)/HMI03.ccf: $(LIB_SHARED) $(SHARED_CCF) $(BMGRP_OBJECTS_HMI) $(BMINFO_OBJECTS_HMI) $(PALFILE_HMI)
	-@CMD /Q /C if exist "$(DEL_TARGET03_LFL_HMI)" DEL /F /Q "$(DEL_TARGET03_LFL_HMI)" 2>nul
	@$(VCFLGEN) '$@.lfl' '$(LIB_SHARED)' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .bmgrp -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Package.vcp' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	@$(VCFLGEN) '$@.lfl' -mask .bminfo -vcp '$(AS_PROJECT_PATH)/Logical/HMI/Package.vcp' -temp '$(TEMP_PATH_HMI)' -prj '$(PRJ_PATH_HMI)' -sfas
	$(LINK) '$@.lfl' -o '$@' -p HMI -lib '$(LIB_BMP_RES_HMI)' -P '$(AS_PROJECT_PATH)' -m 'bitmap resources' -profile 'False' -warningLevel2 -vcr 4440 -sfas
# 03 Module END

