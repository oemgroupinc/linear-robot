export AS_SYSTEM_PATH := C:/BrAutomation/AS/System
export AS_BIN_PATH := C:/BrAutomation/AS44/bin-en
export AS_INSTALL_PATH := C:/BrAutomation/AS44
export AS_PATH := C:/BrAutomation/AS44
export AS_VC_PATH := C:/BrAutomation/AS44/AS/VC
export AS_GNU_INST_PATH := C:/BrAutomation/AS44/AS/GnuInst/V4.1.2
export AS_STATIC_ARCHIVES_PATH := C:/Projects/Linear\ Robot/OEMCintillioLinearRobot/Temp/Archives/LinearRobotConfiguration/X20CP0484
export AS_CPU_PATH := C:/Projects/Linear\ Robot/OEMCintillioLinearRobot/Temp/Objects/LinearRobotConfiguration/X20CP0484
export AS_CPU_PATH_2 := C:/Projects/Linear Robot/OEMCintillioLinearRobot/Temp/Objects/LinearRobotConfiguration/X20CP0484
export AS_TEMP_PATH := C:/Projects/Linear\ Robot/OEMCintillioLinearRobot/Temp
export AS_BINARIES_PATH := C:/Projects/Linear\ Robot/OEMCintillioLinearRobot/Binaries
export AS_PROJECT_CPU_PATH := C:/Projects/Linear\ Robot/OEMCintillioLinearRobot/Physical/LinearRobotConfiguration/X20CP0484
export AS_PROJECT_CONFIG_PATH := C:/Projects/Linear\ Robot/OEMCintillioLinearRobot/Physical/LinearRobotConfiguration
export AS_PROJECT_PATH := C:/Projects/Linear\ Robot/OEMCintillioLinearRobot
export AS_PROJECT_NAME := OEMCintillioLinearRobot
export AS_PLC := X20CP0484
export AS_TEMP_PLC := X20CP0484
export AS_USER_NAME := robert.ruth
export AS_CONFIGURATION := LinearRobotConfiguration
export AS_COMPANY_NAME := \ 
export AS_VERSION := 4.4.7.144\ SP
export AS_BUILD_MODE := Rebuild


default: \
	$(AS_CPU_PATH)/HMI.br \



include $(AS_CPU_PATH)/HMI/HMI.mak
