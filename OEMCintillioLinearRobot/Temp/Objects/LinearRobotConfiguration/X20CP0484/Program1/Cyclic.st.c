#include <bur\plctypes.h>
#include "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Temp/Objects/LinearRobotConfiguration/X20CP0484/Program1/Cyclicst.h"
#line 1 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
void _CYCLIC __BUR__ENTRY_CYCLIC_FUNCT__(void){{


__AS__Action__Staubli_act();
__AS__Action__StaubliMove_act();
(bStaubliRobotReady=gLRInput.bStaubliReady);
(bStaubliRobotSafePos=gLRInput.bStaubliSafePos);
(fbSRSafePosTrigRE.CLK=gLRInput.bStaubliSafePos);;R_TRIG(&fbSRSafePosTrigRE);
(gLRInput.obStaubliSafePosRE=fbSRSafePosTrigRE.Q);


(fbJogNegAxisX1PBTrigRE.CLK=gLRInput.bJogNegAxisX1PB);;R_TRIG(&fbJogNegAxisX1PBTrigRE);
(gLRInput.bJogNegAxisX1PBRE=fbJogNegAxisX1PBTrigRE.Q);

(fbJogPosAxisX1PBTrigRE.CLK=gLRInput.bJogPosAxisX1PB);;R_TRIG(&fbJogPosAxisX1PBTrigRE);
(gLRInput.bJogPosAxisX1PBRE=fbJogPosAxisX1PBTrigRE.Q);

(fbHomeAllAxisPBTrigRE.CLK=gLRInput.bHomeAllAxisPB);;R_TRIG(&fbHomeAllAxisPBTrigRE);
(gLRInput.bHomeAllAxisPBRE=fbHomeAllAxisPBTrigRE.Q);

(fbRotateAxisRPBTrigRE.CLK=gLRInput.bRotateAxisRPB);;R_TRIG(&fbRotateAxisRPBTrigRE);
(gLRInput.bRotateAxisRPBRE=fbRotateAxisRPBTrigRE.Q);

(fbSetTargetPosPBTrigRE.CLK=gLRInput.bSetTargetPosPB);;R_TRIG(&fbSetTargetPosPBTrigRE);
(gLRInput.bSetTargetPosPBRE=fbSetTargetPosPBTrigRE.Q);

(fbStartAutoCycleTrigRE.CLK=gLRInput.bStartAutoCyclePB);;R_TRIG(&fbStartAutoCycleTrigRE);
(gLRInput.bStartAutoCyclePBRE=fbStartAutoCycleTrigRE.Q);

(fbAxisErrorResetTrigRE.CLK=gLRInput.bAxisErrorResetPB);;R_TRIG(&fbAxisErrorResetTrigRE);
(gLRInput.bAxisErrorResetPBRE=fbAxisErrorResetTrigRE.Q);

if(((((unsigned long)(unsigned char)gLRInput.siSelectJogAxis!=(unsigned long)0))&(((signed long)uiCurrentSuperState==(signed long)0)))){
(uiAxisJogState=gLRInput.siSelectJogAxis);
(uiAxisTargetPosState=gLRInput.siSelectAxisX1Loc);
(uiAxisTargetLevel=gLRInput.siSelectAxisTargetLevel);
__AS__Action__AxisJog_act();
if((gLRInput.bHomeAllAxisPBRE&(((signed long)uiAxisJogState==(signed long)70)))){
(uiAxisHomeState=10);
(tLR.bHomeAllAxis=1);
}
}else{
(uiAxisX1JogState=10);
(uiAxisHomeState=10);
(uiAxisRRotateState=10);
(AxisX1Jog.Enable=0);
(AxisY1Jog.Enable=0);
(gLRInput.bJogNegAxisX1PB=0);
(gLRInput.bJogPosAxisX1PB=0);
(gLRInput.bSetTargetPosPB=0);
(tLR.bHomeAllAxis=0);
}

__AS__Action__EmergencyCheck_act();
if((((unsigned long)(unsigned char)bEmergencyCheckError==(unsigned long)(unsigned char)1))){
return;
}

__AS__Action__ErrorCheck_act();
if((((unsigned long)(unsigned char)bErrorCheckError==(unsigned long)(unsigned char)1))){
return;
}


MpAxisBasic(&AxisX1);
MpAxisBasic(&AxisZ1);
MpAxisBasic(&AxisZ2);
MpAxisBasic(&AxisY1);
MpAxisBasic(&AxisY2);
__AS__Action__RotaryParse_act();
(gLROutput.bGuardianCarrierPresent=(tGuardianCarrier1.bAbsentInd^1));




(AxisX1HardwareInputs.Axis=((unsigned long)(&X1)));;(AxisX1HardwareInputs.Enable=1);;MC_BR_SetHardwareInputs(&AxisX1HardwareInputs);
(AxisZ1HardwareInputs.Axis=((unsigned long)(&Z1)));;(AxisZ1HardwareInputs.Enable=1);;MC_BR_SetHardwareInputs(&AxisZ1HardwareInputs);
(AxisZ2HardwareInputs.Axis=((unsigned long)(&Z2)));;(AxisZ2HardwareInputs.Enable=1);;MC_BR_SetHardwareInputs(&AxisZ2HardwareInputs);
(AxisY1HardwareInputs.Axis=((unsigned long)(&Y1)));;(AxisY1HardwareInputs.Enable=1);;MC_BR_SetHardwareInputs(&AxisY1HardwareInputs);
(AxisY2HardwareInputs.Axis=((unsigned long)(&Y2)));;(AxisY2HardwareInputs.Enable=1);;MC_BR_SetHardwareInputs(&AxisY2HardwareInputs);

switch(uiCurrentSuperState){

case 100:{
__AS__Action__HardwareInit_act();
if((((unsigned long)(unsigned char)bHardwareInitError==(unsigned long)(unsigned char)1))){
(tLR.usiSWStateIndTxt=0);
(tLR.usiSWStateIndColor=usiRedIndColor);
return;
}
}break;case 0:{

(tLR.usiSWStateIndTxt=1);
(tLR.usiSWStateIndColor=usiBlueIndColor);

if((((unsigned long)(unsigned char)gLRInput.bRUN==(unsigned long)(unsigned char)0))){

(tLR.Process.bDUALLOAD=0);
(tLR.Process.bDUALUNLOAD=0);
(tLR.Process.bLOAD=0);
(tLR.Process.bUNLOAD=0);

}else if((((unsigned long)(unsigned char)gLRInput.bRUN==(unsigned long)(unsigned char)1))){
(gLRInput.bRUN=0);
(gLROutput.bComplete=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};




__AS__Action__InvalidInputCheck_act();

if(((__AS__STRING_CMP(gLROutput.gstrError,"")==0))){

__AS__Action__ProcessSelection_act();

__AS__Action__GenerateLocalVar_act();

__AS__Action__StateMachineInit_act();

}
}

}break;case 200:{

(tLR.usiSWStateIndTxt=2);
(tLR.usiSWStateIndColor=usiGreenIndColor);

switch(uiCurrentState){



case 100:{
__AS__Action__FPAlign_act();

}break;case 200:{
__AS__Action__UnloadFP_act();

}break;case 300:{
__AS__Action__GCAlign_act();

}break;case 400:{
__AS__Action__LoadGC_act();










}break;case 600:{
__AS__Action__UnloadGC_act();




}break;case 800:{
__AS__Action__LoadFP_act();







}break;case 0:{

__AS__Action__SafeAlign_act();
}break;}
}break;}
}imp40_case4_2:imp40_endcase4_0:;}
#line 2 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/AxisJogAction/AxisJog_act.st"
static void __AS__Action__AxisJog_act(void){
{
switch(uiAxisJogState){

case 0:{
return;


}break;case 10:{
(AxisX1.Enable=1);
(AxisX1Parameters.Jog.Velocity=tLR.rCoarseJogVelX1);
(AxisX1Parameters.Jog.Acceleration=tLR.rCoarseJogAccelX1);
(AxisX1Parameters.Jog.Deceleration=tLR.rCoarseJogDelX1);
(AxisX1Parameters.Jog.LowerLimit=tLR.rX1JogLowerTravelLimit);
(AxisX1Parameters.Jog.UpperLimit=tLR.rX1JogUpperTravelLimit);



switch(uiAxisTargetPosState){
case 1:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tFOUPPort1.rPrevAxisX1TargetPos!=AxisX1.Position))){
(rFP1AxisX1TargetPos=AxisX1.Position);
(tFOUPPort1.siX1Pos=rFP1AxisX1TargetPos);
(tFOUPPort1.rPrevAxisX1TargetPos=rFP1AxisX1TargetPos);
}
}

}break;case 2:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tFOUPPort2.rPrevAxisX1TargetPos!=AxisX1.Position))){
(rFP2AxisX1TargetPos=AxisX1.Position);
(tFOUPPort2.siX1Pos=rFP2AxisX1TargetPos);
(tFOUPPort2.rPrevAxisX1TargetPos=rFP2AxisX1TargetPos);
}
}

}break;case 3:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tFOUPPort3.rPrevAxisX1TargetPos!=AxisX1.Position))){
(rFP3AxisX1TargetPos=AxisX1.Position);
(tFOUPPort3.siX1Pos=rFP3AxisX1TargetPos);
(tFOUPPort3.rPrevAxisX1TargetPos=rFP3AxisX1TargetPos);
}
}

}break;case 4:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tFOUPPort4.rPrevAxisX1TargetPos!=AxisX1.Position))){
(rFP4AxisX1TargetPos=AxisX1.Position);
(tFOUPPort4.siX1Pos=rFP4AxisX1TargetPos);
(tFOUPPort4.rPrevAxisX1TargetPos=rFP4AxisX1TargetPos);
}
}

}break;case 10:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tGuardianCarrier1.rPrevAxisX1TargetPos!=AxisX1.Position))){
(rGC1AxisX1TargetPos=AxisX1.Position);
(tGuardianCarrier1.siX1Pos=rGC1AxisX1TargetPos);
(tGuardianCarrier1.rPrevAxisX1TargetPos=rGC1AxisX1TargetPos);
}
}
}break;}


if((((unsigned long)(unsigned char)gLRInput.siSetAxisX1JogType==(unsigned long)(unsigned char)siCoarseJogType))){
if((((unsigned long)(unsigned char)gLRInput.bJogNegAxisX1PB==(unsigned long)(unsigned char)1))){
(AxisX1.JogNegative=1);
}else{
(AxisX1.JogNegative=0);
}

if((((unsigned long)(unsigned char)gLRInput.bJogPosAxisX1PB==(unsigned long)(unsigned char)1))){
(AxisX1.JogPositive=1);
}else{
(AxisX1.JogPositive=0);
}


}else if((((unsigned long)(unsigned char)gLRInput.siSetAxisX1JogType==(unsigned long)(unsigned char)siFineJogType))){
switch(uiAxisX1JogState){

case 10:{
if((((unsigned long)(unsigned char)gLRInput.bJogNegAxisX1PBRE==(unsigned long)(unsigned char)1))){
(AxisX1Parameters.Position=(AxisX1.Position-tLR.rFineJogIncX1));
(uiAxisX1JogState=20);
}else if((((unsigned long)(unsigned char)gLRInput.bJogPosAxisX1PBRE==(unsigned long)(unsigned char)1))){
(AxisX1Parameters.Position=(AxisX1.Position+tLR.rFineJogIncX1));
(uiAxisX1JogState=20);
}


}break;case 20:{
if((((unsigned long)(unsigned char)AxisX1.MoveActive==(unsigned long)(unsigned char)0))){
(AxisX1.MoveAbsolute=1);
(uiAxisX1JogState=30);
}


}break;case 30:{
if(((((unsigned long)(unsigned char)AxisX1.InPosition==(unsigned long)(unsigned char)1))|((AxisX1.Position==AxisX1Parameters.Position)))){
(AxisX1.MoveAbsolute=0);
(uiAxisX1JogState=10);
}
}break;}
}


}break;case 20:{
(AxisY1.Enable=1);
(AxisY1Parameters.Jog.Velocity=tLR.rCoarseJogVelX1);
(AxisY1Parameters.Jog.Acceleration=tLR.rCoarseJogAccelX1);
(AxisY1Parameters.Jog.Deceleration=tLR.rCoarseJogDelX1);
(AxisY1Parameters.Jog.LowerLimit=tLR.rYJogLowerTravelLimit);
(AxisY1Parameters.Jog.UpperLimit=tLR.rYJogUpperTravelLimit);



switch(uiAxisTargetPosState){
case 1:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tFOUPPort1.rPrevAxisY1TargetPos!=AxisY1.Position))){
(rFP1AxisY1TargetPos=AxisY1.Position);
(tFOUPPort1.siY1AccessPos=rFP1AxisY1TargetPos);
(tFOUPPort1.rPrevAxisY1TargetPos=rFP1AxisY1TargetPos);
}
}

}break;case 2:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tFOUPPort2.rPrevAxisY1TargetPos!=AxisY1.Position))){
(rFP2AxisY1TargetPos=AxisY1.Position);
(tFOUPPort2.siY1AccessPos=rFP2AxisY1TargetPos);
(tFOUPPort2.rPrevAxisY1TargetPos=rFP2AxisY1TargetPos);
}
}

}break;case 3:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tFOUPPort3.rPrevAxisY1TargetPos!=AxisY1.Position))){
(rFP3AxisY1TargetPos=AxisY1.Position);
(tFOUPPort3.siY1AccessPos=rFP3AxisY1TargetPos);
(tFOUPPort3.rPrevAxisY1TargetPos=rFP3AxisY1TargetPos);
}
}

}break;case 4:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tFOUPPort4.rPrevAxisY1TargetPos!=AxisY1.Position))){
(rFP4AxisY1TargetPos=AxisY1.Position);
(tFOUPPort4.siY1AccessPos=rFP4AxisY1TargetPos);
(tFOUPPort4.rPrevAxisY1TargetPos=rFP4AxisY1TargetPos);
}
}

}break;case 10:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tGuardianCarrier1.rPrevAxisY1TargetPos!=AxisY1.Position))){
(rGC1AxisY1TargetPos=AxisY1.Position);
(tGuardianCarrier1.siY1AccessPos=rGC1AxisY1TargetPos);
(tGuardianCarrier1.rPrevAxisY1TargetPos=rGC1AxisY1TargetPos);
}
}
}break;}


if((((unsigned long)(unsigned char)gLRInput.siSetAxisX1JogType==(unsigned long)(unsigned char)siCoarseJogType))){
if((((unsigned long)(unsigned char)gLRInput.bJogNegAxisX1PB==(unsigned long)(unsigned char)1))){
(AxisY1.JogNegative=1);
}else{
(AxisY1.JogNegative=0);
}

if((((unsigned long)(unsigned char)gLRInput.bJogPosAxisX1PB==(unsigned long)(unsigned char)1))){
(AxisY1.JogPositive=1);
}else{
(AxisY1.JogPositive=0);
}


}else if((((unsigned long)(unsigned char)gLRInput.siSetAxisX1JogType==(unsigned long)(unsigned char)siFineJogType))){
switch(uiAxisX1JogState){

case 10:{
if((((unsigned long)(unsigned char)gLRInput.bJogNegAxisX1PBRE==(unsigned long)(unsigned char)1))){
(AxisY1Parameters.Position=(AxisY1.Position-tLR.rFineJogIncX1));
(uiAxisX1JogState=20);
}else if((((unsigned long)(unsigned char)gLRInput.bJogPosAxisX1PBRE==(unsigned long)(unsigned char)1))){
(AxisY1Parameters.Position=(AxisY1.Position+tLR.rFineJogIncX1));
(uiAxisX1JogState=20);
}


}break;case 20:{
if((((unsigned long)(unsigned char)AxisY1.MoveActive==(unsigned long)(unsigned char)0))){
(AxisY1.MoveAbsolute=1);
(uiAxisX1JogState=30);
}


}break;case 30:{
if(((((unsigned long)(unsigned char)AxisY1.InPosition==(unsigned long)(unsigned char)1))|((AxisY1.Position==AxisY1Parameters.Position)))){
(AxisY1.MoveAbsolute=0);
(uiAxisX1JogState=10);
}
}break;}
}


}break;case 30:{
(AxisY2.Enable=1);
(AxisY2Parameters.Jog.Velocity=tLR.rCoarseJogVelX1);
(AxisY2Parameters.Jog.Acceleration=tLR.rCoarseJogAccelX1);
(AxisY2Parameters.Jog.Deceleration=tLR.rCoarseJogDelX1);
(AxisY2Parameters.Jog.LowerLimit=tLR.rY2JogLowerTravelLimit);
(AxisY2Parameters.Jog.UpperLimit=tLR.rY2JogUpperTravelLimit);



switch(uiAxisTargetPosState){
case 1:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tFOUPPort1.rPrevAxisY2TargetPos!=AxisY2.Position))){
(rFP1AxisY2TargetPos=AxisY2.Position);
(tFOUPPort1.siY2AccessPos=rFP1AxisY2TargetPos);
(tFOUPPort1.rPrevAxisY2TargetPos=rFP1AxisY2TargetPos);
}
}

}break;case 2:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tFOUPPort2.rPrevAxisY2TargetPos!=AxisY2.Position))){
(rFP2AxisY2TargetPos=AxisY2.Position);
(tFOUPPort2.siY2AccessPos=rFP2AxisY2TargetPos);
(tFOUPPort2.rPrevAxisY2TargetPos=rFP2AxisY2TargetPos);
}
}

}break;case 3:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tFOUPPort3.rPrevAxisY2TargetPos!=AxisY2.Position))){
(rFP3AxisY2TargetPos=AxisY2.Position);
(tFOUPPort3.siY2AccessPos=rFP3AxisY2TargetPos);
(tFOUPPort3.rPrevAxisY2TargetPos=rFP3AxisY2TargetPos);
}
}

}break;case 4:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tFOUPPort4.rPrevAxisY2TargetPos!=AxisY2.Position))){
(rFP4AxisY2TargetPos=AxisY2.Position);
(tFOUPPort4.siY2AccessPos=rFP4AxisY2TargetPos);
(tFOUPPort4.rPrevAxisY2TargetPos=rFP4AxisY2TargetPos);
}
}

}break;case 10:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if(((tGuardianCarrier1.rPrevAxisY2TargetPos!=AxisY2.Position))){
(rGC1AxisY2TargetPos=AxisY2.Position);
(tGuardianCarrier1.siY2AccessPos=rGC1AxisY2TargetPos);
(tGuardianCarrier1.rPrevAxisY2TargetPos=rGC1AxisY2TargetPos);
}
}
}break;}


if((((unsigned long)(unsigned char)gLRInput.siSetAxisX1JogType==(unsigned long)(unsigned char)siCoarseJogType))){
if((((unsigned long)(unsigned char)gLRInput.bJogNegAxisX1PB==(unsigned long)(unsigned char)1))){
(AxisY2.JogNegative=1);
}else{
(AxisY2.JogNegative=0);
}

if((((unsigned long)(unsigned char)gLRInput.bJogPosAxisX1PB==(unsigned long)(unsigned char)1))){
(AxisY2.JogPositive=1);
}else{
(AxisY2.JogPositive=0);
}


}else if((((unsigned long)(unsigned char)gLRInput.siSetAxisX1JogType==(unsigned long)(unsigned char)siFineJogType))){
switch(uiAxisX1JogState){

case 10:{
if((((unsigned long)(unsigned char)gLRInput.bJogNegAxisX1PBRE==(unsigned long)(unsigned char)1))){
(AxisY2Parameters.Position=(AxisY2.Position-tLR.rFineJogIncX1));
(uiAxisX1JogState=20);
}else if((((unsigned long)(unsigned char)gLRInput.bJogPosAxisX1PBRE==(unsigned long)(unsigned char)1))){
(AxisY2Parameters.Position=(AxisY2.Position+tLR.rFineJogIncX1));
(uiAxisX1JogState=20);
}


}break;case 20:{
if((((unsigned long)(unsigned char)AxisY2.MoveActive==(unsigned long)(unsigned char)0))){
(AxisY2.MoveAbsolute=1);
(uiAxisX1JogState=30);
}


}break;case 30:{
if(((((unsigned long)(unsigned char)AxisY2.InPosition==(unsigned long)(unsigned char)1))|((AxisY2.Position==AxisY2Parameters.Position)))){
(AxisY2.MoveAbsolute=0);
(uiAxisX1JogState=10);
}
}break;}
}


}break;case 40:{
(AxisZ1.Enable=1);
(AxisZ1Parameters.Jog.Velocity=tLR.rCoarseJogVelX1);
(AxisZ1Parameters.Jog.Acceleration=tLR.rCoarseJogAccelX1);
(AxisZ1Parameters.Jog.Deceleration=tLR.rCoarseJogDelX1);
(AxisZ1Parameters.Jog.LowerLimit=tLR.rZ1JogLowerTravelLimit);
(AxisZ1Parameters.Jog.UpperLimit=tLR.rZ1JogUpperTravelLimit);



switch(uiAxisTargetPosState){
case 1:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if((((signed long)uiAxisTargetLevel==(signed long)1))){
if(((tFOUPPort1.rPrevAxisZ1UpTargetPos!=AxisZ1.Position))){
(rFP1AxisZ1UpTargetPos=AxisZ1.Position);
(tFOUPPort1.siZ1LRPossession=rFP1AxisZ1UpTargetPos);
(tFOUPPort1.rPrevAxisZ1UpTargetPos=rFP1AxisZ1UpTargetPos);
}
}else if((((signed long)uiAxisTargetLevel==(signed long)2))){
if(((tFOUPPort1.rPrevAxisZ1LowTargetPos!=AxisZ1.Position))){
(rFP1AxisZ1LowTargetPos=AxisZ1.Position);
(tFOUPPort1.siZ1FPPossession=rFP1AxisZ1LowTargetPos);
(tFOUPPort1.rPrevAxisZ1LowTargetPos=rFP1AxisZ1LowTargetPos);
}
}
}

}break;case 2:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if((((signed long)uiAxisTargetLevel==(signed long)1))){
if(((tFOUPPort2.rPrevAxisZ1UpTargetPos!=AxisZ1.Position))){
(rFP2AxisZ1UpTargetPos=AxisZ1.Position);
(tFOUPPort2.siZ1LRPossession=rFP2AxisZ1UpTargetPos);
(tFOUPPort2.rPrevAxisZ1UpTargetPos=rFP2AxisZ1UpTargetPos);
}
}else if((((signed long)uiAxisTargetLevel==(signed long)2))){
if(((tFOUPPort2.rPrevAxisZ1LowTargetPos!=AxisZ1.Position))){
(rFP2AxisZ1LowTargetPos=AxisZ1.Position);
(tFOUPPort2.siZ1FPPossession=rFP2AxisZ1LowTargetPos);
(tFOUPPort2.rPrevAxisZ1LowTargetPos=rFP2AxisZ1LowTargetPos);
}
}
}

}break;case 3:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if((((signed long)uiAxisTargetLevel==(signed long)1))){
if(((tFOUPPort3.rPrevAxisZ1UpTargetPos!=AxisZ1.Position))){
(rFP3AxisZ1UpTargetPos=AxisZ1.Position);
(tFOUPPort3.siZ1LRPossession=rFP3AxisZ1UpTargetPos);
(tFOUPPort3.rPrevAxisZ1UpTargetPos=rFP3AxisZ1UpTargetPos);
}
}else if((((signed long)uiAxisTargetLevel==(signed long)2))){
if(((tFOUPPort3.rPrevAxisZ1LowTargetPos!=AxisZ1.Position))){
(rFP3AxisZ1LowTargetPos=AxisZ1.Position);
(tFOUPPort3.siZ1FPPossession=rFP3AxisZ1LowTargetPos);
(tFOUPPort3.rPrevAxisZ1LowTargetPos=rFP3AxisZ1LowTargetPos);
}
}
}

}break;case 4:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if((((signed long)uiAxisTargetLevel==(signed long)1))){
if(((tFOUPPort4.rPrevAxisZ1UpTargetPos!=AxisZ1.Position))){
(rFP4AxisZ1UpTargetPos=AxisZ1.Position);
(tFOUPPort4.siZ1LRPossession=rFP4AxisZ1UpTargetPos);
(tFOUPPort4.rPrevAxisZ1UpTargetPos=rFP4AxisZ1UpTargetPos);
}
}else if((((signed long)uiAxisTargetLevel==(signed long)2))){
if(((tFOUPPort4.rPrevAxisZ1LowTargetPos!=AxisZ1.Position))){
(rFP4AxisZ1LowTargetPos=AxisZ1.Position);
(tFOUPPort4.siZ1FPPossession=rFP4AxisZ1LowTargetPos);
(tFOUPPort4.rPrevAxisZ1LowTargetPos=rFP4AxisZ1LowTargetPos);
}
}
}

}break;case 10:{
if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if((((signed long)uiAxisTargetLevel==(signed long)1))){
if(((tGuardianCarrier1.rPrevAxisZ1UpTargetPos!=AxisZ1.Position))){
(rGC1AxisZ1UpTargetPos=AxisZ1.Position);
(tGuardianCarrier1.siZ1LRPossession=rGC1AxisZ1UpTargetPos);
(tGuardianCarrier1.rPrevAxisZ1UpTargetPos=rGC1AxisZ1UpTargetPos);
}
}else if((((signed long)uiAxisTargetLevel==(signed long)2))){
if(((tGuardianCarrier1.rPrevAxisZ1LowTargetPos!=AxisZ1.Position))){
(rGC1AxisZ1LowTargetPos=AxisZ1.Position);
(tGuardianCarrier1.siZ1GCPossession=rGC1AxisZ1LowTargetPos);
(tGuardianCarrier1.rPrevAxisZ1LowTargetPos=rGC1AxisZ1LowTargetPos);
}
}
}
}break;}

if((((unsigned long)(unsigned char)gLRInput.siSetAxisX1JogType==(unsigned long)(unsigned char)siCoarseJogType))){
if((((unsigned long)(unsigned char)gLRInput.bJogNegAxisX1PB==(unsigned long)(unsigned char)1))){
(AxisZ1.JogNegative=1);
}else{
(AxisZ1.JogNegative=0);
}

if((((unsigned long)(unsigned char)gLRInput.bJogPosAxisX1PB==(unsigned long)(unsigned char)1))){
(AxisZ1.JogPositive=1);
}else{
(AxisZ1.JogPositive=0);
}


}else if((((unsigned long)(unsigned char)gLRInput.siSetAxisX1JogType==(unsigned long)(unsigned char)siFineJogType))){
switch(uiAxisX1JogState){

case 10:{
if((((unsigned long)(unsigned char)gLRInput.bJogNegAxisX1PBRE==(unsigned long)(unsigned char)1))){
(AxisZ1Parameters.Position=(AxisZ1.Position-tLR.rFineJogIncX1));
(uiAxisX1JogState=20);
}else if((((unsigned long)(unsigned char)gLRInput.bJogPosAxisX1PBRE==(unsigned long)(unsigned char)1))){
(AxisZ1Parameters.Position=(AxisZ1.Position+tLR.rFineJogIncX1));
(uiAxisX1JogState=20);
}


}break;case 20:{
if((((unsigned long)(unsigned char)AxisZ1.MoveActive==(unsigned long)(unsigned char)0))){
(AxisZ1.MoveAbsolute=1);
(uiAxisX1JogState=30);
}


}break;case 30:{
if(((((unsigned long)(unsigned char)AxisZ1.InPosition==(unsigned long)(unsigned char)1))|((AxisZ1.Position==AxisZ1Parameters.Position)))){
(AxisZ1.MoveAbsolute=0);
(uiAxisX1JogState=10);
}
}break;}
}


}break;case 50:{
(AxisZ2.Enable=1);
(AxisZ2Parameters.Jog.Velocity=tLR.rCoarseJogVelX1);
(AxisZ2Parameters.Jog.Acceleration=tLR.rCoarseJogAccelX1);
(AxisZ2Parameters.Jog.Deceleration=tLR.rCoarseJogDelX1);
(AxisZ2Parameters.Jog.LowerLimit=tLR.rZ2JogLowerTravelLimit);
(AxisZ2Parameters.Jog.UpperLimit=tLR.rZ2JogUpperTravelLimit);


if((((unsigned long)(unsigned char)gLRInput.bSetTargetPosPBRE==(unsigned long)(unsigned char)1))){
if((((signed long)uiAxisTargetLevel==(signed long)1))){
if(((tGuardianCarrier1.rPrevAxisZ2UpCarrierPos!=AxisZ2.Position))){
(rGC1AxisZ2UpCarrierPos=AxisZ2.Position);
(tGuardianCarrier1.siZ2TopPos=rGC1AxisZ2UpCarrierPos);
(tGuardianCarrier1.rPrevAxisZ2UpCarrierPos=rGC1AxisZ2UpCarrierPos);
}
}else if((((signed long)uiAxisTargetLevel==(signed long)2))){
if(((tGuardianCarrier1.rPrevAxisZ2LowCarrierPos!=AxisZ2.Position))){
(rGC1AxisZ2LowCarrierPos=AxisZ2.Position);
(tGuardianCarrier1.siZ2BottomPos=rGC1AxisZ2LowCarrierPos);
(tGuardianCarrier1.rPrevAxisZ2LowCarrierPos=rGC1AxisZ2LowCarrierPos);
}
}
}


if((((unsigned long)(unsigned char)gLRInput.siSetAxisX1JogType==(unsigned long)(unsigned char)siCoarseJogType))){
if((((unsigned long)(unsigned char)gLRInput.bJogNegAxisX1PB==(unsigned long)(unsigned char)1))){
(AxisZ2.JogNegative=1);
}else{
(AxisZ2.JogNegative=0);
}

if((((unsigned long)(unsigned char)gLRInput.bJogPosAxisX1PB==(unsigned long)(unsigned char)1))){
(AxisZ2.JogPositive=1);
}else{
(AxisZ2.JogPositive=0);
}


}else if((((unsigned long)(unsigned char)gLRInput.siSetAxisX1JogType==(unsigned long)(unsigned char)siFineJogType))){
switch(uiAxisX1JogState){

case 10:{
if((((unsigned long)(unsigned char)gLRInput.bJogNegAxisX1PBRE==(unsigned long)(unsigned char)1))){
(AxisZ2Parameters.Position=(AxisZ2.Position-tLR.rFineJogIncX1));
(uiAxisX1JogState=20);
}else if((((unsigned long)(unsigned char)gLRInput.bJogPosAxisX1PBRE==(unsigned long)(unsigned char)1))){
(AxisZ2Parameters.Position=(AxisZ2.Position+tLR.rFineJogIncX1));
(uiAxisX1JogState=20);
}


}break;case 20:{
if((((unsigned long)(unsigned char)AxisZ2.MoveActive==(unsigned long)(unsigned char)0))){
(AxisZ2.MoveAbsolute=1);
(uiAxisX1JogState=30);
}


}break;case 30:{
if(((((unsigned long)(unsigned char)AxisZ2.InPosition==(unsigned long)(unsigned char)1))|((AxisZ2.Position==AxisZ2Parameters.Position)))){
(AxisZ2.MoveAbsolute=0);
(uiAxisX1JogState=10);
}
}break;}
}


}break;case 60:{
switch(uiAxisRRotateState){
case 10:{
if(gLRInput.bRotateAxisRPBRE){
(bAxisRStartFlag=1);
(bAxisRArrivedFlag=0);
if((((unsigned long)(unsigned short)gLRInput.uiRotateAxisPos==(unsigned long)(unsigned short)180))){
(uiAxisRRotateState=20);
}else if((((unsigned long)(unsigned short)gLRInput.uiRotateAxisPos==(unsigned long)(unsigned short)90))){
(uiAxisRRotateState=30);
}else if((((unsigned long)(unsigned short)gLRInput.uiRotateAxisPos==(unsigned long)(unsigned short)270))){
(uiAxisRRotateState=40);
}
}


}break;case 20:{
(tAxisRMove.siTargetPos=-180);
(bAxisRStartFlag=1);
__AS__Action__RMove_act();
if((((unsigned long)(unsigned char)bAxisRArrivedFlag==(unsigned long)(unsigned char)1))){
(bAxisRStartFlag=0);

(uiAxisRRotateState=10);
}


}break;case 30:{
(tAxisRMove.siTargetPos=-90);
(bAxisRStartFlag=1);
__AS__Action__RMove_act();
if((((unsigned long)(unsigned char)bAxisRArrivedFlag==(unsigned long)(unsigned char)1))){
(bAxisRStartFlag=0);

(uiAxisRRotateState=10);
}


}break;case 40:{
(tAxisRMove.siTargetPos=-270);
(bAxisRStartFlag=1);
__AS__Action__RMove_act();
if((((unsigned long)(unsigned char)bAxisRArrivedFlag==(unsigned long)(unsigned char)1))){
(bAxisRStartFlag=0);

(uiAxisRRotateState=10);
}
}break;}



}break;case 70:{
if((((unsigned long)(unsigned char)tLR.bHomeAllAxis==(unsigned long)(unsigned char)1))){
(bAxisRArrivedFlag=0);
(tLR.bWaferPossession=0);
__AS__Action__SetAcceleration_act();
switch(uiAxisHomeState){

case 10:{
(tAxisY1Move.siTargetPos=tLR.siY1SafePos);
(bAxisY1ArrivedFlag=1);
__AS__Action__Y1Move_act();
if((((signed long)tAxisY1Move.state==(signed long)20))){

(uiAxisHomeState=20);
}

}break;case 20:{
(tAxisY2Move.siTargetPos=tLR.siY2SafePos);
(bAxisY2ArrivedFlag=1);
__AS__Action__Y2Move_act();
if((((signed long)tAxisY2Move.state==(signed long)20))){

(uiAxisHomeState=30);
}

}break;case 30:{
(tAxisRMove.siTargetPos=-180);
(bAxisRStartFlag=1);
__AS__Action__RMove_act();
if((((unsigned long)(unsigned char)bAxisRArrivedFlag==(unsigned long)(unsigned char)1))){
(bAxisRStartFlag=0);

(uiAxisHomeState=40);
}

}break;case 40:{
(tAxisX1Move.siTargetPos=tLR.siX1SafePos);
__AS__Action__X1Move_act();
if((((signed long)tAxisX1Move.state==(signed long)20))){

(uiAxisHomeState=50);
}

}break;case 50:{
(tAxisZ1Move.rAccel=tLR.siFastAccelZ1);
(tAxisZ1Move.rSpeed=tLR.siFastVelZ1);
(tAxisZ1Move.siTargetPos=tLR.siZ1SafePos);
__AS__Action__Z1Move_act();
if((((signed long)tAxisZ1Move.state==(signed long)20))){

(uiAxisHomeState=60);
}

}break;case 60:{
(tAxisZ2Move.rAccel=tLR.siFastAccelZ2);
(tAxisZ2Move.rSpeed=tLR.siFastVelZ2);
(tAxisZ2Move.siTargetPos=tLR.siZ2SafePos);
__AS__Action__Z2Move_act();
if((((signed long)tAxisZ2Move.state==(signed long)20))){

(uiAxisHomeState=100);
}

}break;case 100:{
(tLR.bHomeAllAxis=0);
(uiAxisHomeState=10);

}break;}
}
}break;}

}imp1_case0_7:imp1_endcase0_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 1 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/AxisMove.st"
static void __AS__Action__RMove_act(void){
{
if((((unsigned long)(unsigned char)RotaryPWR==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"R Axis not powered. "; for(zzIndex=0; zzIndex<20l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}else if((((unsigned long)(unsigned char)RotarySV==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"R Axis Servo not powered. "; for(zzIndex=0; zzIndex<26l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}else if((((unsigned long)(unsigned char)RotaryHEND==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"R Axis not Homed. "; for(zzIndex=0; zzIndex<18l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}


switch(tAxisRMove.state){
case 10:{

__AS__Action__RotaryMove_act();
if((((unsigned long)(unsigned char)RotaryMOVE==(unsigned long)(unsigned char)1))){
(tAxisRMove.state=11);
}
}break;case 11:{
if(((((unsigned long)(unsigned char)RotaryMOVE==(unsigned long)(unsigned char)1))|((RotaryPosition==tAxisRMove.siTargetPos)))){
(tAxisRMove.state=15);
}
}break;case 15:{
if((((unsigned long)(unsigned char)RotaryMOVE==(unsigned long)(unsigned char)0))){
(bAxisRArrivedFlag=1);
(tAxisRMove.state=20);
}
}break;case 20:{
__AS__Action__RotaryMoveDone_act();
if(((((unsigned long)(unsigned char)bAxisRStartFlag==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)RotaryPEND==(unsigned long)(unsigned char)1)))){
(bAxisRArrivedFlag=0);
(tAxisRMove.state=10);
}
}break;}
}imp12_case1_3:imp12_endcase1_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 43 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/AxisMove.st"
static void __AS__Action__X1Move_act(void){
{
if((((unsigned long)(unsigned char)AxisX1.Active==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"X1 Axis disabled. "; for(zzIndex=0; zzIndex<18l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}else if((((unsigned long)(unsigned char)AxisX1.PowerOn==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"X1 Axis not powered. "; for(zzIndex=0; zzIndex<21l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}else if((((unsigned long)(unsigned char)AxisX1.IsHomed==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"X1 Axis not Homed. "; for(zzIndex=0; zzIndex<19l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}


switch(tAxisX1Move.state){
case 10:{
(AxisX1Parameters.Acceleration=tAxisX1Move.rAccel);
(AxisX1Parameters.Deceleration=tAxisX1Move.rAccel);
(AxisX1Parameters.Velocity=tAxisX1Move.rSpeed);
(AxisX1Parameters.Position=tAxisX1Move.siTargetPos);

(AxisX1.MoveAbsolute=1);
(tAxisX1Move.state=15);
}break;case 15:{
if(((((unsigned long)(unsigned char)AxisX1.InPosition==(unsigned long)(unsigned char)1))|((AxisX1.Position==tAxisX1Move.siTargetPos)))){
(AxisX1.MoveAbsolute=0);
(tAxisX1Move.state=20);
}
}break;case 20:{
(tAxisX1Move.state=10);
}break;}
}imp35_case1_2:imp35_endcase1_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 80 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/AxisMove.st"
static void __AS__Action__Y1Move_act(void){
{
if((((unsigned long)(unsigned char)AxisY1.Active==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Y1 Axis disabled. "; for(zzIndex=0; zzIndex<18l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}else if((((unsigned long)(unsigned char)AxisY1.PowerOn==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Y1 Axis not powered. "; for(zzIndex=0; zzIndex<21l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}else if((((unsigned long)(unsigned char)AxisY1.IsHomed==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Y1 Axis not Homed. "; for(zzIndex=0; zzIndex<19l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}


switch(tAxisY1Move.state){
case 10:{
(AxisY1Parameters.Acceleration=tAxisY1Move.rAccel);
(AxisY1Parameters.Deceleration=tAxisY1Move.rAccel);
(AxisY1Parameters.Velocity=tAxisY1Move.rSpeed);
(AxisY1Parameters.Position=tAxisY1Move.siTargetPos);

(AxisY1.MoveAbsolute=1);
(tAxisY1Move.state=15);
}break;case 15:{
if(((((unsigned long)(unsigned char)AxisY1.InPosition==(unsigned long)(unsigned char)1))|((AxisY1.Position==tAxisY1Move.siTargetPos)))){
(AxisY1.MoveAbsolute=0);
(tAxisY1Move.state=20);
}
}break;case 20:{
if((((unsigned long)(unsigned char)bAxisY1ArrivedFlag==(unsigned long)(unsigned char)1))){
(bAxisY1ArrivedFlag=0);
(tAxisY1Move.state=10);
}
}break;}
}imp36_case1_2:imp36_endcase1_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 119 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/AxisMove.st"
static void __AS__Action__Y2Move_act(void){
{
if((((unsigned long)(unsigned char)AxisY2.Active==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Y2 Axis disabled. "; for(zzIndex=0; zzIndex<18l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}else if((((unsigned long)(unsigned char)AxisY2.PowerOn==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Y2 Axis not powered. "; for(zzIndex=0; zzIndex<21l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}else if((((unsigned long)(unsigned char)AxisY2.IsHomed==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Y2 Axis not Homed. "; for(zzIndex=0; zzIndex<19l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}


switch(tAxisY2Move.state){
case 10:{
(AxisY2Parameters.Acceleration=tAxisY2Move.rAccel);
(AxisY2Parameters.Deceleration=tAxisY2Move.rAccel);
(AxisY2Parameters.Velocity=tAxisY2Move.rSpeed);
(AxisY2Parameters.Position=tAxisY2Move.siTargetPos);

(AxisY2.MoveAbsolute=1);
(tAxisY2Move.state=15);
}break;case 15:{
if(((((unsigned long)(unsigned char)AxisY2.InPosition==(unsigned long)(unsigned char)1))|((AxisY2.Position==tAxisY2Move.siTargetPos)))){
(AxisY2.MoveAbsolute=0);
(tAxisY2Move.state=20);
}
}break;case 20:{
if((((unsigned long)(unsigned char)bAxisY2ArrivedFlag==(unsigned long)(unsigned char)1))){
(bAxisY2ArrivedFlag=0);
(tAxisY2Move.state=10);
}
}break;}
}imp37_case1_2:imp37_endcase1_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 158 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/AxisMove.st"
static void __AS__Action__Z1Move_act(void){
{
if((((unsigned long)(unsigned char)AxisZ1.Active==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Z1 Axis disabled. "; for(zzIndex=0; zzIndex<18l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}else if((((unsigned long)(unsigned char)AxisZ1.PowerOn==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Z1 Axis not powered. "; for(zzIndex=0; zzIndex<21l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}else if((((unsigned long)(unsigned char)AxisZ1.IsHomed==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Z1 Axis not Homed. "; for(zzIndex=0; zzIndex<19l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}


switch(tAxisZ1Move.state){
case 10:{
(AxisZ1Parameters.Acceleration=tAxisZ1Move.rAccel);
(AxisZ1Parameters.Deceleration=tAxisZ1Move.rAccel);
(AxisZ1Parameters.Velocity=tAxisZ1Move.rSpeed);
(AxisZ1Parameters.Position=tAxisZ1Move.siTargetPos);

(AxisZ1.MoveAbsolute=1);
(tAxisZ1Move.state=15);
}break;case 15:{
if(((((unsigned long)(unsigned char)AxisZ1.InPosition==(unsigned long)(unsigned char)1))|((AxisZ1.Position==tAxisZ1Move.siTargetPos)))){
(AxisZ1.MoveAbsolute=0);
(tAxisZ1Move.state=20);
}
}break;case 20:{
(tAxisZ1Move.state=10);
}break;}
}imp38_case1_2:imp38_endcase1_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 194 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/AxisMove.st"
static void __AS__Action__Z2Move_act(void){
{
if((((unsigned long)(unsigned char)AxisZ2.Active==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Z2 Axis disabled. "; for(zzIndex=0; zzIndex<18l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}else if((((unsigned long)(unsigned char)AxisZ2.PowerOn==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Z2 Axis not powered. "; for(zzIndex=0; zzIndex<21l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}else if((((unsigned long)(unsigned char)AxisZ2.IsHomed==(unsigned long)(unsigned char)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Z2 Axis not Homed. "; for(zzIndex=0; zzIndex<19l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}


switch(tAxisZ2Move.state){
case 10:{
(AxisZ2Parameters.Acceleration=tAxisZ2Move.rAccel);
(AxisZ2Parameters.Deceleration=tAxisZ2Move.rAccel);
(AxisZ2Parameters.Velocity=tAxisZ2Move.rSpeed);
(AxisZ2Parameters.Position=tAxisZ2Move.siTargetPos);

(AxisZ2.MoveAbsolute=1);
(tAxisZ2Move.state=15);
}break;case 15:{
if(((((unsigned long)(unsigned char)AxisZ2.InPosition==(unsigned long)(unsigned char)1))|((AxisZ2.Position==tAxisZ2Move.siTargetPos)))){
(AxisZ2.MoveAbsolute=0);
(tAxisZ2Move.state=20);
}
}break;case 20:{
(tAxisZ2Move.state=10);
}break;}
}imp39_case1_2:imp39_endcase1_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 2 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/ErrorCheck.st"
static void __AS__Action__EmergencyCheck_act(void){
{










if(((((unsigned long)(unsigned char)gLRInput.bEmergencyShutoff==(unsigned long)(unsigned char)1))|(((unsigned long)(unsigned char)X1.message.count.error!=(unsigned long)(unsigned char)0))|(((unsigned long)(unsigned char)X1.message.count.warning!=(unsigned long)(unsigned char)0))|(((unsigned long)(unsigned char)Y1.message.count.error!=(unsigned long)(unsigned char)0))|(((unsigned long)(unsigned char)Y1.message.count.warning!=(unsigned long)(unsigned char)0))|(((unsigned long)(unsigned char)Y2.message.count.error!=(unsigned long)(unsigned char)0))|(((unsigned long)(unsigned char)Y2.message.count.warning!=(unsigned long)(unsigned char)0))|(((unsigned long)(unsigned char)Z1.message.count.error!=(unsigned long)(unsigned char)0))|(((unsigned long)(unsigned char)Z1.message.count.warning!=(unsigned long)(unsigned char)0))|(((unsigned long)(unsigned char)Z2.message.count.error!=(unsigned long)(unsigned char)0))|(((unsigned long)(unsigned char)Z2.message.count.warning!=(unsigned long)(unsigned char)0))|(((unsigned long)(unsigned char)RotaryEMGS==(unsigned long)(unsigned char)1))|(((unsigned long)(unsigned char)RotaryALM==(unsigned long)(unsigned char)1)))){
(AxisX1.Stop=1);
(AxisY1.Stop=1);
(AxisY2.Stop=1);
(AxisZ1.Stop=1);
(AxisZ2.Stop=1);
__AS__Action__RotaryPause();
(bErrorCheckError=1);
(bAxisY1ArrivedFlag=0);
(bAxisY2ArrivedFlag=0);
(bAxisRArrivedFlag=0);
(bAxisRStartFlag=0);

(bEmergencyCheckError=1);

}else{
(bEmergencyCheckError=0);
(AxisX1.ErrorReset=0);
(AxisY1.ErrorReset=0);
(AxisY2.ErrorReset=0);
(AxisZ1.ErrorReset=0);
(AxisZ2.ErrorReset=0);
(AxisX1.Stop=0);
(AxisY1.Stop=0);
(AxisY2.Stop=0);
(AxisZ1.Stop=0);
(AxisZ2.Stop=0);
}

if((((unsigned long)(unsigned char)gLRInput.bAxisErrorResetPBRE==(unsigned long)(unsigned char)1))){
(AxisX1Acknowlege=1);
(AxisY1Acknowlege=1);
(AxisY2Acknowlege=1);
(AxisZ1Acknowlege=1);
(AxisZ2Acknowlege=1);
(AxisRAcknowlege=1);
}

if((((unsigned long)(unsigned char)bEmergencyCheckError==(unsigned long)(unsigned char)1))){
if((((unsigned long)(unsigned char)AxisX1Acknowlege==(unsigned long)(unsigned char)1))){
(action_statusX1=ncaction(AxisX1.Axis,ncMESSAGE,ncACKNOWLEDGE));
(AxisX1.ErrorReset=1);
if((((unsigned long)(unsigned short)action_statusX1==(unsigned long)(unsigned short)ncOK))){
(AxisX1Acknowlege=0);
(AxisX1.ErrorReset=0);
}
}
if((((unsigned long)(unsigned char)AxisY1Acknowlege==(unsigned long)(unsigned char)1))){
(action_statusY1=ncaction(AxisY1.Axis,ncMESSAGE,ncACKNOWLEDGE));
(AxisY1.ErrorReset=1);
if((((unsigned long)(unsigned short)action_statusY1==(unsigned long)(unsigned short)ncOK))){
(AxisY1Acknowlege=0);
(AxisY1.ErrorReset=0);
}
}
if((((unsigned long)(unsigned char)AxisY2Acknowlege==(unsigned long)(unsigned char)1))){
(action_statusY2=ncaction(AxisY2.Axis,ncMESSAGE,ncACKNOWLEDGE));
(AxisY2.ErrorReset=1);
if((((unsigned long)(unsigned short)action_statusY2==(unsigned long)(unsigned short)ncOK))){
(AxisY2Acknowlege=0);
(AxisY2.ErrorReset=0);
}
}
if((((unsigned long)(unsigned char)AxisZ1Acknowlege==(unsigned long)(unsigned char)1))){
(action_statusZ1=ncaction(AxisZ1.Axis,ncMESSAGE,ncACKNOWLEDGE));
(AxisZ1.ErrorReset=1);
if((((unsigned long)(unsigned short)action_statusZ1==(unsigned long)(unsigned short)ncOK))){
(AxisZ1Acknowlege=0);
(AxisZ1.ErrorReset=0);
}
}
if((((unsigned long)(unsigned char)AxisZ2Acknowlege==(unsigned long)(unsigned char)1))){
(action_statusZ2=ncaction(AxisZ2.Axis,ncMESSAGE,ncACKNOWLEDGE));
(AxisZ2.ErrorReset=1);
if((((unsigned long)(unsigned short)action_statusZ2==(unsigned long)(unsigned short)ncOK))){
(AxisZ2Acknowlege=0);
(AxisZ2.ErrorReset=0);
}
}
if((((unsigned long)(unsigned char)AxisRAcknowlege==(unsigned long)(unsigned char)1))){
__AS__Action__RotaryReset_act();
(AxisRAcknowlege=0);
}

}
















}imp2_else13_0:imp2_end13_0:imp2_else2_0:imp2_end2_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 117 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/ErrorCheck.st"
static void __AS__Action__ErrorCheck_act(void){
{
if(((__AS__STRING_CMP(gLROutput.gstrError,"")!=0))){

(bErrorCheckError=1);
return;
}
(bErrorCheckError=0);
}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 2 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/HWInit.st"
static void __AS__Action__HardwareInit_act(void){
{

if((((unsigned long)(unsigned char)AxisX1.Error==(unsigned long)(unsigned char)1))){















{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Hardware Init, error on Axis X1. "; for(zzIndex=0; zzIndex<33l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}
if((((unsigned long)(unsigned char)AxisY1.Error==(unsigned long)(unsigned char)1))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Hardware Init, error on Axis Y1. "; for(zzIndex=0; zzIndex<33l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}
if((((unsigned long)(unsigned char)AxisY2.Error==(unsigned long)(unsigned char)1))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Hardware Init, error on Axis Y2. "; for(zzIndex=0; zzIndex<33l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}
if((((unsigned long)(unsigned char)AxisZ1.Error==(unsigned long)(unsigned char)1))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Hardware Init, error on Axis Z1. "; for(zzIndex=0; zzIndex<33l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}
if((((unsigned long)(unsigned char)AxisZ2.Error==(unsigned long)(unsigned char)1))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Hardware Init, error on Axis Z2. "; for(zzIndex=0; zzIndex<33l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}
if((((unsigned long)(unsigned char)RotaryALM==(unsigned long)(unsigned char)1))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Hardware Init, error on Axis R. "; for(zzIndex=0; zzIndex<32l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(bHardwareInitError=1);
return;
}



if(((((unsigned long)(unsigned char)AxisX1.Active==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)AxisX1.Enable==(unsigned long)(unsigned char)0)))){
(AxisX1.Enable=1);
}
if(((((unsigned long)(unsigned char)AxisY1.Active==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)AxisY1.Enable==(unsigned long)(unsigned char)0)))){
(AxisY1.Enable=1);
}
if(((((unsigned long)(unsigned char)AxisY2.Active==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)AxisY2.Enable==(unsigned long)(unsigned char)0)))){
(AxisY2.Enable=1);
}
if(((((unsigned long)(unsigned char)AxisZ1.Active==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)AxisZ1.Enable==(unsigned long)(unsigned char)0)))){
(AxisZ1.Enable=1);
}
if(((((unsigned long)(unsigned char)AxisZ2.Active==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)AxisZ2.Enable==(unsigned long)(unsigned char)0)))){
(AxisZ2.Enable=1);
}









if(((((unsigned long)(unsigned char)AxisX1.Active==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisY1.Active==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisY2.Active==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisZ1.Active==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisZ2.Active==(unsigned long)(unsigned char)1)))){


if((((unsigned long)(unsigned char)RotarySV==(unsigned long)(unsigned char)0))){
__AS__Action__RotaryPower();
}

if(((((unsigned long)(unsigned char)AxisX1.Power==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)AxisX1.PowerOn==(unsigned long)(unsigned char)0)))){
(AxisX1.Power=1);
}

if(((((unsigned long)(unsigned char)AxisY1.Power==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)AxisY1.PowerOn==(unsigned long)(unsigned char)0)))){
(AxisY1.Power=1);
}

if(((((unsigned long)(unsigned char)AxisY2.Power==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)AxisY2.PowerOn==(unsigned long)(unsigned char)0)))){
(AxisY2.Power=1);
}

if(((((unsigned long)(unsigned char)AxisZ1.Power==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)AxisZ1.PowerOn==(unsigned long)(unsigned char)0)))){
(AxisZ1.Power=1);
}

if(((((unsigned long)(unsigned char)AxisZ2.Power==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)AxisZ2.PowerOn==(unsigned long)(unsigned char)0)))){
(AxisZ2.Power=1);
}


}








if(((((unsigned long)(unsigned char)AxisX1.PowerOn==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisY1.PowerOn==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisY2.PowerOn==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisZ1.PowerOn==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisZ2.PowerOn==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)RotarySV==(unsigned long)(unsigned char)1)))){





if((((unsigned long)(unsigned char)AxisY1.Home==(unsigned long)(unsigned char)0))){
(AxisY1.Home=1);
}else if(((((unsigned long)(unsigned char)AxisY1.IsHomed==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisY2.Home==(unsigned long)(unsigned char)0)))){


(AxisY2.Home=1);
}else if(((((unsigned long)(unsigned char)AxisY1.IsHomed==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisY2.IsHomed==(unsigned long)(unsigned char)1)))){

(tAxisY1Move.rAccel=tLR.siFastAccelY1);
(tAxisY1Move.rSpeed=tLR.siFastVelY1);
(tAxisY1Move.siTargetPos=tLR.siY1SafePos);
(bAxisY1ArrivedFlag=1);
__AS__Action__Y1Move_act();
(tAxisY2Move.rAccel=tLR.siFastAccelY2);
(tAxisY2Move.rSpeed=tLR.siFastVelY2);
(tAxisY2Move.siTargetPos=tLR.siY2SafePos);
(bAxisY2ArrivedFlag=1);
__AS__Action__Y2Move_act();
}

if(((((unsigned long)(unsigned char)AxisY1.IsHomed==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisY2.IsHomed==(unsigned long)(unsigned char)1)))){


if((((unsigned long)(unsigned char)AxisZ2.Home==(unsigned long)(unsigned char)0))){
(AxisZ2.Home=1);
}else if((((unsigned long)(unsigned char)AxisZ2.IsHomed==(unsigned long)(unsigned char)1))){
(tAxisZ2Move.rAccel=tLR.siFastAccelZ2);
(tAxisZ2Move.rSpeed=tLR.siFastVelZ2);
(tAxisZ2Move.siTargetPos=tLR.siZ2SafePos);
__AS__Action__Z2Move_act();
}


__AS__Action__RotaryHome();


if(((((unsigned long)(unsigned char)AxisZ1.Home==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)RotaryHEND==(unsigned long)(unsigned char)1)))){
(AxisZ1.Home=1);
}else if((((unsigned long)(unsigned char)AxisZ1.IsHomed==(unsigned long)(unsigned char)1))){
(tAxisZ1Move.rAccel=tLR.siFastAccelZ1);
(tAxisZ1Move.rSpeed=tLR.siFastVelZ1);
(tAxisZ1Move.siTargetPos=tLR.siZ1SafePos);
__AS__Action__Z1Move_act();
}

if((((unsigned long)(unsigned char)AxisZ1.IsHomed==(unsigned long)(unsigned char)1))){


if((((unsigned long)(unsigned char)AxisX1.Home==(unsigned long)(unsigned char)0))){
(AxisX1.Home=1);
}else if((((unsigned long)(unsigned char)AxisX1.IsHomed==(unsigned long)(unsigned char)1))){
(tAxisX1Move.rAccel=tLR.siFastAccelX1);
(tAxisX1Move.rSpeed=tLR.siFastVelX1);
(tAxisX1Move.siTargetPos=tLR.siX1SafePos);
__AS__Action__X1Move_act();
}
}
}








if(((((unsigned long)(unsigned char)AxisX1.IsHomed==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisY1.IsHomed==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisY2.IsHomed==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisZ1.IsHomed==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)AxisZ2.IsHomed==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)RotaryHEND==(unsigned long)(unsigned char)1)))){

(uiCurrentSuperState=0);


}
(bHardwareInitError=0);

}

(tAxisX1Move.state=20);
(tAxisY1Move.state=20);
(tAxisY2Move.state=20);
(tAxisZ1Move.state=20);
(tAxisZ2Move.state=20);
(tAxisRMove.state=20);
__AS__Action__SetAcceleration_act();

}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 2 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/InputActions/GenerateLocalVar.st"
static void __AS__Action__GenerateLocalVar_act(void){
{


switch(gLRInput.siBatch1FPort){
case 1:{(tTargetFP1=*(struct FOUPPort_typ*)&tFOUPPort1);
}break;case 2:{(tTargetFP1=*(struct FOUPPort_typ*)&tFOUPPort2);
}break;case 3:{(tTargetFP1=*(struct FOUPPort_typ*)&tFOUPPort3);
}break;case 4:{(tTargetFP1=*(struct FOUPPort_typ*)&tFOUPPort4);
}break;case 5:{(tTargetFP1=*(struct FOUPPort_typ*)&tFOUPPort5);
}break;case 6:{(tTargetFP1=*(struct FOUPPort_typ*)&tFOUPPort6);
}break;case 7:{(tTargetFP1=*(struct FOUPPort_typ*)&tFOUPPort7);
}break;case 8:{(tTargetFP1=*(struct FOUPPort_typ*)&tFOUPPort8);
}break;}

if((((unsigned long)(unsigned char)gLRInput.bBatchCount2==(unsigned long)(unsigned char)1))){
switch(gLRInput.siBatch2FPort){
case 1:{(tTargetFP2=*(struct FOUPPort_typ*)&tFOUPPort1);
}break;case 2:{(tTargetFP2=*(struct FOUPPort_typ*)&tFOUPPort2);
}break;case 3:{(tTargetFP2=*(struct FOUPPort_typ*)&tFOUPPort3);
}break;case 4:{(tTargetFP2=*(struct FOUPPort_typ*)&tFOUPPort4);
}break;case 5:{(tTargetFP2=*(struct FOUPPort_typ*)&tFOUPPort5);
}break;case 6:{(tTargetFP2=*(struct FOUPPort_typ*)&tFOUPPort6);
}break;case 7:{(tTargetFP2=*(struct FOUPPort_typ*)&tFOUPPort7);
}break;case 8:{(tTargetFP2=*(struct FOUPPort_typ*)&tFOUPPort8);
}break;}
}


if((((unsigned long)(unsigned char)gLRInput.bGC1==(unsigned long)(unsigned char)1))){
(tTargetGC=*(struct GuardianCarrier_typ*)&tGuardianCarrier1);
}else if((((unsigned long)(unsigned char)gLRInput.bGC2==(unsigned long)(unsigned char)1))){
(tTargetGC=*(struct GuardianCarrier_typ*)&tGuardianCarrier2);
}



if((((unsigned long)(unsigned char)gLRInput.bWaferSize300mm==(unsigned long)(unsigned char)1))){
(tGCLevelBatch1.bNA=1);
(tGCLevelBatch1.bTop=0);
(tGCLevelBatch1.bBottom=1);

(tGCLevelBatch2.bNA=0);
(tGCLevelBatch2.bTop=0);
(tGCLevelBatch2.bBottom=1);
}else if((((unsigned long)(unsigned char)gLRInput.bWaferSize200mm==(unsigned long)(unsigned char)1))){
if((((unsigned long)(unsigned char)gLRInput.bBatch1GCBottomLevel==(unsigned long)(unsigned char)1))){
(tGCLevelBatch1.bNA=0);
(tGCLevelBatch1.bTop=0);
(tGCLevelBatch1.bBottom=1);
}else if(gLRInput.bBatch1GCTopLevel){
(tGCLevelBatch1.bNA=0);
(tGCLevelBatch1.bTop=1);
(tGCLevelBatch1.bBottom=0);
}

if((((unsigned long)(unsigned char)gLRInput.bBatchCount2==(unsigned long)(unsigned char)1))){
if((((unsigned long)(unsigned char)gLRInput.bBatch2GCTopLevel==(unsigned long)(unsigned char)1))){
(tGCLevelBatch2.bNA=0);
(tGCLevelBatch2.bTop=1);
(tGCLevelBatch2.bBottom=0);
}else if((((unsigned long)(unsigned char)gLRInput.bBatch2GCBottomLevel==(unsigned long)(unsigned char)1))){
(tGCLevelBatch2.bNA=0);
(tGCLevelBatch2.bTop=0);
(tGCLevelBatch2.bBottom=1);
}
}
}
}imp6_else7_1:imp6_end7_0:imp6_else6_0:imp6_end6_0:imp6_else4_1:imp6_end4_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 2 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/InputActions/InvalidInputCheck.st"
static void __AS__Action__InvalidInputCheck_act(void){
{
if(((((unsigned long)(unsigned char)gLRInput.bWaferSize200mm==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)gLRInput.bWaferSize300mm==(unsigned long)(unsigned char)0)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Incomplete input. Please select a Wafer Size."; for(zzIndex=0; zzIndex<45l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bRUN=0);
return;
}

if(((((unsigned long)(unsigned char)gLRInput.bLoadGC==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)gLRInput.bUnloadGC==(unsigned long)(unsigned char)0)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Incomplete input. Please select a Linear Robot operation."; for(zzIndex=0; zzIndex<57l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bRUN=0);
return;
}

if(((((unsigned long)(unsigned char)gLRInput.bWaferSize200mm==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)gLRInput.bBatchCount1==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)gLRInput.bBatchCount2==(unsigned long)(unsigned char)0)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Incomplete input. Please select a Batch Count."; for(zzIndex=0; zzIndex<46l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bRUN=0);
return;
}

if(((((signed long)(signed long)(char)gLRInput.siBatch1FPort<=(signed long)(signed long)(char)0))|(((signed long)(signed long)(char)gLRInput.siBatch1FPort>(signed long)(signed long)(char)8)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Incomplete input. Please select a valid Target FOUP Port for batch 1."; for(zzIndex=0; zzIndex<69l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bRUN=0);
return;
}

if(((((unsigned long)(unsigned char)gLRInput.bBatchCount2==(unsigned long)(unsigned char)1))&((((signed long)(signed long)(char)gLRInput.siBatch2FPort<=(signed long)(signed long)(char)0))|(((signed long)(signed long)(char)gLRInput.siBatch2FPort>(signed long)(signed long)(char)8))))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Incomplete input. Please select a valid Target FOUP Port for batch 2."; for(zzIndex=0; zzIndex<69l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bRUN=0);
return;
}

if(((((unsigned long)(unsigned char)gLRInput.bBatchCount2==(unsigned long)(unsigned char)1))&(((signed long)(signed long)(char)gLRInput.siBatch1FPort==(signed long)(signed long)(char)gLRInput.siBatch2FPort)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. FOUP port for batch 1 and batch 2 must be unique."; for(zzIndex=0; zzIndex<64l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bRUN=0);
return;
}



if(((((unsigned long)(unsigned char)gLRInput.bGC1==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)gLRInput.bGC2==(unsigned long)(unsigned char)1)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Guardian Carrier may be selected."; for(zzIndex=0; zzIndex<57l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bGC1=0);
(gLRInput.bGC2=0);
(gLRInput.bRUN=0);
return;
}else if(((((unsigned long)(unsigned char)gLRInput.bGC1==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)gLRInput.bGC2==(unsigned long)(unsigned char)0)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Incomplete input. Please select a Guardian Carrier."; for(zzIndex=0; zzIndex<51l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bRUN=0);
return;
}


if(((((unsigned long)(unsigned char)gLRInput.bBatch1GCTopLevel==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)gLRInput.bBatch1GCBottomLevel==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)gLRInput.bWaferSize200mm==(unsigned long)(unsigned char)1)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Guardian Carrier level may be selected for batch 1."; for(zzIndex=0; zzIndex<75l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bBatch1GCTopLevel=0);
(gLRInput.bBatch1GCBottomLevel=0);
(gLRInput.bRUN=0);
return;
}else if(((((unsigned long)(unsigned char)gLRInput.bBatch1GCTopLevel==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)gLRInput.bBatch1GCBottomLevel==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)gLRInput.bWaferSize200mm==(unsigned long)(unsigned char)1)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Incomplete input. Please select a Guardian Carrier level for batch 1."; for(zzIndex=0; zzIndex<69l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bRUN=0);
return;
}else if(((((unsigned long)(unsigned char)gLRInput.bBatch1GCTopLevel==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)gLRInput.bBatch2GCTopLevel==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)gLRInput.bWaferSize200mm==(unsigned long)(unsigned char)1)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Guardian Carrier level must be different for each batch."; for(zzIndex=0; zzIndex<71l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bBatch1GCTopLevel=0);
(gLRInput.bBatch2GCTopLevel=0);
(gLRInput.bRUN=0);
return;
}else if(((((unsigned long)(unsigned char)gLRInput.bBatch1GCBottomLevel==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)gLRInput.bBatch2GCBottomLevel==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)gLRInput.bWaferSize200mm==(unsigned long)(unsigned char)1)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Guardian Carrier level must be different for each batch."; for(zzIndex=0; zzIndex<71l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bBatch1GCBottomLevel=0);
(gLRInput.bBatch2GCBottomLevel=0);
(gLRInput.bRUN=0);
return;
}

if(((((unsigned long)(unsigned char)gLRInput.bBatch2GCTopLevel==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)gLRInput.bBatch2GCBottomLevel==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)gLRInput.bWaferSize200mm==(unsigned long)(unsigned char)1)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Guardian Carrier level may be selected for batch 2."; for(zzIndex=0; zzIndex<75l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bBatch2GCTopLevel=0);
(gLRInput.bBatch2GCBottomLevel=0);
(gLRInput.bRUN=0);
return;
}else if(((((unsigned long)(unsigned char)gLRInput.bBatch2GCTopLevel==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)gLRInput.bBatch2GCBottomLevel==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)gLRInput.bWaferSize200mm==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)gLRInput.bBatchCount2==(unsigned long)(unsigned char)1)))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Incomplete input. Please select a Guardian Carrier level for batch 2."; for(zzIndex=0; zzIndex<69l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bRUN=0);
return;
}

if((((unsigned long)(unsigned char)gLRInput.bWaferSize300mm==(unsigned long)(unsigned char)1))){
if((((unsigned long)(unsigned char)gLRInput.bBatch2GCBottomLevel==(unsigned long)(unsigned char)1))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. 300mm wafers do not support 2 batch processes."; for(zzIndex=0; zzIndex<61l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bBatch2GCBottomLevel=0);
(gLRInput.bRUN=0);
return;
}else if((((unsigned long)(unsigned char)gLRInput.bBatch2GCTopLevel==(unsigned long)(unsigned char)1))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. 300mm wafers do not support 2 batch processes."; for(zzIndex=0; zzIndex<61l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bBatch2GCTopLevel=0);
(gLRInput.bRUN=0);
return;
}
}







}imp8_else10_1:imp8_end10_0:imp8_else9_0:imp8_end9_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 2 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/InputActions/ProcessSelection.st"
static void __AS__Action__ProcessSelection_act(void){
{if((((unsigned long)(unsigned char)gLRInput.bWaferSize200mm==(unsigned long)(unsigned char)1))){

if((((unsigned long)(unsigned char)gLRInput.bWaferSize300mm==(unsigned long)(unsigned char)1))){
(gLRInput.bWaferSize200mm=0);
(gLRInput.bWaferSize300mm=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Wafer Size may be selected at a time."; for(zzIndex=0; zzIndex<61l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bRUN=0);
return;
}else if((((unsigned long)(unsigned char)gLRInput.bBatchCount1==(unsigned long)(unsigned char)1))){

if((((unsigned long)(unsigned char)gLRInput.bBatchCount2==(unsigned long)(unsigned char)1))){
(gLRInput.bBatchCount1=0);
(gLRInput.bBatchCount2=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Batch Count may be selected at a time."; for(zzIndex=0; zzIndex<62l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bRUN=0);
return;
}else if((((unsigned long)(unsigned char)gLRInput.bLoadGC==(unsigned long)(unsigned char)1))){

if((((unsigned long)(unsigned char)gLRInput.bUnloadGC==(unsigned long)(unsigned char)1))){
(gLRInput.bLoadGC=0);
(gLRInput.bUnloadGC=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Linear Robot Operation may be selected at a time."; for(zzIndex=0; zzIndex<73l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gLRInput.bRUN=0);
return;
}else{


(tLR.Process.bLOAD=1);
(uiCurrentSuperState=200);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

}else if((((unsigned long)(unsigned char)gLRInput.bUnloadGC==(unsigned long)(unsigned char)1))){

if((((unsigned long)(unsigned char)gLRInput.bLoadGC==(unsigned long)(unsigned char)1))){
(gLRInput.bUnloadGC=0);
(gLRInput.bLoadGC=0);
(gLRInput.bRUN=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Linear Robot Operation may be selected at a time."; for(zzIndex=0; zzIndex<73l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
return;
}else{

(tLR.Process.bUNLOAD=1);
(uiCurrentSuperState=200);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}
}

}else if((((unsigned long)(unsigned char)gLRInput.bBatchCount2==(unsigned long)(unsigned char)1))){

if((((unsigned long)(unsigned char)gLRInput.bBatchCount1==(unsigned long)(unsigned char)1))){
(gLRInput.bBatchCount1=0);
(gLRInput.bBatchCount2=0);
(gLRInput.bRUN=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Batch Count may be selected at a time."; for(zzIndex=0; zzIndex<62l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
return;
}else if((((unsigned long)(unsigned char)gLRInput.bLoadGC==(unsigned long)(unsigned char)1))){

if((((unsigned long)(unsigned char)gLRInput.bUnloadGC==(unsigned long)(unsigned char)1))){
(gLRInput.bUnloadGC=0);
(gLRInput.bLoadGC=0);
(gLRInput.bRUN=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Linear Robot Operation may be selected at a time."; for(zzIndex=0; zzIndex<73l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
return;
}else{

(tLR.Process.bDUALLOAD=1);
(uiCurrentSuperState=200);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

}else if((((unsigned long)(unsigned char)gLRInput.bUnloadGC==(unsigned long)(unsigned char)1))){

if((((unsigned long)(unsigned char)gLRInput.bLoadGC==(unsigned long)(unsigned char)1))){
(gLRInput.bUnloadGC=0);
(gLRInput.bLoadGC=0);
(gLRInput.bRUN=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Linear Robot Operation may be selected at a time."; for(zzIndex=0; zzIndex<73l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
return;
}else{

(tLR.Process.bDUALUNLOAD=1);
(uiCurrentSuperState=200);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

}
}


}else if((((unsigned long)(unsigned char)gLRInput.bWaferSize300mm==(unsigned long)(unsigned char)1))){

if((((unsigned long)(unsigned char)gLRInput.bWaferSize200mm==(unsigned long)(unsigned char)1))){
(gLRInput.bWaferSize200mm=0);
(gLRInput.bWaferSize300mm=0);
(gLRInput.bRUN=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Wafer Size may be selected at a time."; for(zzIndex=0; zzIndex<61l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
return;
}else if((((unsigned long)(unsigned char)gLRInput.bBatchCount2==(unsigned long)(unsigned char)1))){

(gLRInput.bBatchCount2=0);
(gLRInput.bWaferSize300mm=0);
(gLRInput.bRUN=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. 300mm wafers do not support more than 1 Batch Count."; for(zzIndex=0; zzIndex<67l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
return;
}else if((((unsigned long)(unsigned char)gLRInput.bLoadGC==(unsigned long)(unsigned char)1))){

if((((unsigned long)(unsigned char)gLRInput.bUnloadGC==(unsigned long)(unsigned char)1))){
(gLRInput.bLoadGC=0);
(gLRInput.bUnloadGC=0);
(gLRInput.bRUN=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Linear Robot Operation may be selected at a time."; for(zzIndex=0; zzIndex<73l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
return;
}else{


(tLR.Process.bLOAD=1);
(uiCurrentSuperState=200);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

}else if((((unsigned long)(unsigned char)gLRInput.bUnloadGC==(unsigned long)(unsigned char)1))){

if((((unsigned long)(unsigned char)gLRInput.bLoadGC==(unsigned long)(unsigned char)1))){
(gLRInput.bLoadGC=0);
(gLRInput.bUnloadGC=0);
(gLRInput.bRUN=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)"Invalid input. Only one Linear Robot Operation may be selected at a time"; for(zzIndex=0; zzIndex<72l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
return;
}else{


(tLR.Process.bUNLOAD=1);
(uiCurrentSuperState=200);
{int zzIndex; plcstring* zzLValue=(plcstring*)gLROutput.gstrError; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

}
}
}imp11_end10_0:imp11_else8_3:imp11_end8_0:imp11_else0_1:imp11_end0_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 2 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/InputActions/StateMachineInit.st"
static void __AS__Action__StateMachineInit_act(void){
{
if(((((unsigned long)(unsigned char)tLR.Process.bLOAD==(unsigned long)(unsigned char)1))|(((unsigned long)(unsigned char)tLR.Process.bDUALLOAD==(unsigned long)(unsigned char)1)))){

(uiCurrentSuperState=200);
__AS__Action__StartFPAlign_act();
(tLR.bWaferPossession=0);
(tLR.Process.uiBatchTracker=1);
(tCurrentTargetFP=*(struct FOUPPort_typ*)&tTargetFP1);

}else if(((((unsigned long)(unsigned char)tLR.Process.bUNLOAD==(unsigned long)(unsigned char)1))|(((unsigned long)(unsigned char)tLR.Process.bDUALUNLOAD==(unsigned long)(unsigned char)1)))){

(uiCurrentSuperState=200);
__AS__Action__StartGCAlign_act();
(tLR.bWaferPossession=0);
(tLR.Process.uiBatchTracker=1);
(tCurrentTargetFP=*(struct FOUPPort_typ*)&tTargetFP1);

}
}imp30_else0_1:imp30_end0_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 2 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/RotaryActions/Rotary.st"
static void __AS__Action__RotaryPower(void){
{(RSOutput00=0);
(RSOutput01=0);
(RSOutput02=0);
(RSOutput03=0);
(RSOutput04=4);
(RSOutput05=0);
(RSOutput06=0);
(RSOutput07=0);
(RSOutput08=20);
(RSOutput09=0);
(RSOutput10=30);
(RSOutput11=0);
(RSOutput12=255);
(RSOutput13=0);
(RSOutput14=48);
(RSOutput15=0);
}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 21 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/RotaryActions/Rotary.st"
static void __AS__Action__RotaryHome(void){
{
(RSOutput00=224);
(RSOutput01=64);
(RSOutput02=253);
(RSOutput03=255);
(RSOutput04=4);
(RSOutput05=0);
(RSOutput06=0);
(RSOutput07=0);
(RSOutput08=(unsigned char)(tAxisRMove.rSpeed>=0.0?tAxisRMove.rSpeed+0.5:tAxisRMove.rSpeed-0.5));
(RSOutput09=0);
(RSOutput10=(unsigned char)(tAxisRMove.rAccel>=0.0?tAxisRMove.rAccel+0.5:tAxisRMove.rAccel-0.5));
(RSOutput11=0);
(RSOutput12=255);
(RSOutput13=0);
(RSOutput14=49);
(RSOutput15=0);


















}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 59 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/RotaryActions/Rotary.st"
static void __AS__Action__RotaryPause(void){
{(RSOutput00=0);
(RSOutput01=0);
(RSOutput02=0);
(RSOutput03=0);
(RSOutput04=4);
(RSOutput05=0);
(RSOutput06=0);
(RSOutput07=0);
(RSOutput08=20);
(RSOutput09=0);
(RSOutput10=30);
(RSOutput11=0);
(RSOutput12=255);
(RSOutput13=0);
(RSOutput14=52);
(RSOutput15=0);
}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 97 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/RotaryActions/Rotary.st"
static void __AS__Action__RotaryMove_act(void){
{switch((signed short)(tAxisRMove.siTargetPos>=0.0?tAxisRMove.siTargetPos+0.5:tAxisRMove.siTargetPos-0.5)){
case 180:{
(RSOutput00=32);
(RSOutput01=191);
(RSOutput02=2);
(RSOutput03=0);
}break;case -90:{
(RSOutput00=112);
(RSOutput01=160);
(RSOutput02=254);
(RSOutput03=255);
}break;case -180:{
(RSOutput00=224);
(RSOutput01=64);
(RSOutput02=253);
(RSOutput03=255);
}break;case -270:{
(RSOutput00=80);
(RSOutput01=225);
(RSOutput02=251);
(RSOutput03=255);
}break;}

(RSOutput04=4);
(RSOutput05=0);
(RSOutput06=0);
(RSOutput07=0);
(RSOutput08=(unsigned char)(tAxisRMove.rSpeed>=0.0?tAxisRMove.rSpeed+0.5:tAxisRMove.rSpeed-0.5));
(RSOutput09=0);
(RSOutput10=(unsigned char)(tAxisRMove.rAccel>=0.0?tAxisRMove.rAccel+0.5:tAxisRMove.rAccel-0.5));
(RSOutput11=0);
(RSOutput12=255);
(RSOutput13=0);
(RSOutput14=49);
(RSOutput15=0);
}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 135 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/RotaryActions/Rotary.st"
static void __AS__Action__RotaryMoveDone_act(void){
{switch((signed short)(tAxisRMove.siTargetPos>=0.0?tAxisRMove.siTargetPos+0.5:tAxisRMove.siTargetPos-0.5)){
case 180:{
(RSOutput00=32);
(RSOutput01=191);
(RSOutput02=2);
(RSOutput03=0);
}break;case -90:{
(RSOutput00=112);
(RSOutput01=160);
(RSOutput02=254);
(RSOutput03=255);
}break;case -180:{
(RSOutput00=224);
(RSOutput01=64);
(RSOutput02=253);
(RSOutput03=255);
}break;case -270:{
(RSOutput00=80);
(RSOutput01=225);
(RSOutput02=251);
(RSOutput03=255);
}break;}

(RSOutput04=4);
(RSOutput05=0);
(RSOutput06=0);
(RSOutput07=0);
(RSOutput08=20);
(RSOutput09=0);
(RSOutput10=30);
(RSOutput11=0);
(RSOutput12=255);
(RSOutput13=0);
(RSOutput14=48);
(RSOutput15=0);
}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 173 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/RotaryActions/Rotary.st"
static void __AS__Action__RotaryParse_act(void){
{
*((char volatile*)&(RSInput04)) = *((char*)&(RSInput04));
*((char volatile*)&(RSInput05)) = *((char*)&(RSInput05));
*((char volatile*)&(RSInput06)) = *((char*)&(RSInput06));
*((char volatile*)&(RSInput07)) = *((char*)&(RSInput07));



(RotaryPosition=((double)(RSInput00|(RSInput01<<8)|(RSInput02<<16)|(RSInput03<<24))/1000));

(RotaryComCurrent=(float)(RSInput04|(RSInput05<<8)|(RSInput06<<16)|(RSInput07<<24)));

(RotarySpeed=((double)(RSInput08|(RSInput09<<8)|(RSInput10<<16)|(RSInput11<<24))/100));

(RotaryAlarmCode=(RSInput12+(RSInput13*256)));

(Input14Byte=(plcbyte)RSInput14);
(RotaryPEND=((_1byte_bit_field_*)(&Input14Byte))->bit0);
(RotaryHEND=((_1byte_bit_field_*)(&Input14Byte))->bit1);
(RotaryMOVE=((_1byte_bit_field_*)(&Input14Byte))->bit2);
(RotaryALM=((_1byte_bit_field_*)(&Input14Byte))->bit3);
(RotarySV=((_1byte_bit_field_*)(&Input14Byte))->bit4);
(RotaryPSEL=((_1byte_bit_field_*)(&Input14Byte))->bit5);
(RotaryBALM=((_1byte_bit_field_*)(&Input14Byte))->bit7);

(Input15Byte=(plcbyte)RSInput15);
(RotaryRMDS=((_1byte_bit_field_*)(&Input15Byte))->bit0);
(RotaryZONE1=((_1byte_bit_field_*)(&Input15Byte))->bit4);
(RotaryZONE2=((_1byte_bit_field_*)(&Input15Byte))->bit5);
(RotaryPWR=((_1byte_bit_field_*)(&Input15Byte))->bit6);
(RotaryEMGS=((_1byte_bit_field_*)(&Input15Byte))->bit7);
}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 207 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/RotaryActions/Rotary.st"
static void __AS__Action__RotaryReset_act(void){
{(RSOutput00=0);
(RSOutput01=0);
(RSOutput02=0);
(RSOutput03=0);
(RSOutput04=4);
(RSOutput05=0);
(RSOutput06=0);
(RSOutput07=0);
(RSOutput08=20);
(RSOutput09=0);
(RSOutput10=30);
(RSOutput11=0);
(RSOutput12=255);
(RSOutput13=0);
(RSOutput14=8);
(RSOutput15=0);
}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 1 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/FPAlign_act.st"
static void __AS__Action__StartFPAlign_act(void){
{(uiCurrentState=100);
(uiCurrentSubState=10);

}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 7 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/FPAlign_act.st"
static void __AS__Action__FPAlign_act(void){
{
switch(uiCurrentSubState){
case 10:{
(tAxisX1Move.siTargetPos=tCurrentTargetFP.siX1Pos);
__AS__Action__X1Move_act();
if((((signed long)tAxisX1Move.state==(signed long)20))){

(uiCurrentSubState=40);
}

}break;case 40:{
if((((unsigned long)(unsigned char)tLR.bWaferPossession==(unsigned long)(unsigned char)1))){
(tAxisZ1Move.siTargetPos=tCurrentTargetFP.siZ1LRPossession);
(tAxisZ1Move.rAccel=tLR.siSlowAccelZ1);
(tAxisZ1Move.rSpeed=tLR.siSlowVelZ1);
}else{
(tAxisZ1Move.siTargetPos=tCurrentTargetFP.siZ1FPPossession);
(tAxisZ1Move.rAccel=tLR.siFastAccelZ1);
(tAxisZ1Move.rSpeed=tLR.siFastVelZ1);
}

__AS__Action__Z1Move_act();
if((((signed long)tAxisZ1Move.state==(signed long)20))){
(bAxisRArrivedFlag=0);

(uiCurrentSubState=60);
}

}break;case 60:{
(tAxisRMove.siTargetPos=-90);
(bAxisRStartFlag=1);
__AS__Action__RMove_act();
if((((unsigned long)(unsigned char)bAxisRArrivedFlag==(unsigned long)(unsigned char)1))){
(bAxisRStartFlag=0);

(uiCurrentSubState=20);
}

}break;case 20:{
(tAxisY1Move.siTargetPos=tCurrentTargetFP.siY1AccessPos);

(bAxisY1ArrivedFlag=1);
__AS__Action__Y1Move_act();
if((((signed long)tAxisY1Move.state==(signed long)20))){

(uiCurrentSubState=30);
}

}break;case 30:{
(tAxisY2Move.siTargetPos=tCurrentTargetFP.siY2AccessPos);

(bAxisY2ArrivedFlag=1);
__AS__Action__Y2Move_act();
if((((signed long)tAxisY2Move.state==(signed long)20))){
if((((unsigned long)(unsigned char)tLR.bWaferPossession==(unsigned long)(unsigned char)1))){
__AS__Action__StartLoadFP_act();
}else{
__AS__Action__StartUnloadFP_act();
}
}
}break;}

}imp4_case0_4:imp4_endcase0_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 1 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/GCAlign_act.st"
static void __AS__Action__StartGCAlign_act(void){
{(uiCurrentState=300);
(uiCurrentSubState=50);

}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 7 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/GCAlign_act.st"
static void __AS__Action__GCAlign_act(void){
{switch(uiCurrentSubState){
case 50:{

(tAxisZ2Move.rAccel=tLR.siSlowAccelZ2);
(tAxisZ2Move.rSpeed=tLR.siSlowVelZ2);
if((((unsigned long)(unsigned char)gLRInput.bWaferSize300mm==(unsigned long)(unsigned char)1))){
(tAxisZ2Move.siTargetPos=tTargetGC.siZ2300Pos);
}else{
switch(tLR.Process.uiBatchTracker){
case 1:{
if((((unsigned long)(unsigned char)tGCLevelBatch1.bTop==(unsigned long)(unsigned char)1))){
(tAxisZ2Move.siTargetPos=tTargetGC.siZ2TopPos);
}else if((((unsigned long)(unsigned char)tGCLevelBatch1.bBottom==(unsigned long)(unsigned char)1))){
(tAxisZ2Move.siTargetPos=tTargetGC.siZ2BottomPos);
}
}break;case 2:{
if((((unsigned long)(unsigned char)tGCLevelBatch2.bTop==(unsigned long)(unsigned char)1))){
(tAxisZ2Move.siTargetPos=tTargetGC.siZ2TopPos);
}else if((((unsigned long)(unsigned char)tGCLevelBatch2.bBottom==(unsigned long)(unsigned char)1))){
(tAxisZ2Move.siTargetPos=tTargetGC.siZ2BottomPos);
}
}break;}
}

__AS__Action__Z2Move_act();
if((((signed long)tAxisZ2Move.state==(signed long)15))){
(uiCurrentSubState=10);
}

}break;case 10:{
(tAxisX1Move.siTargetPos=tTargetGC.siX1Pos);
__AS__Action__X1Move_act();
__AS__Action__Z2Move_act();
if((((signed long)tAxisX1Move.state==(signed long)20))){

(uiCurrentSubState=40);
}

}break;case 40:{

if((((unsigned long)(unsigned char)tLR.bWaferPossession==(unsigned long)(unsigned char)1))){
(tAxisZ1Move.siTargetPos=tTargetGC.siZ1LRPossession);
(tAxisZ1Move.rAccel=tLR.siSlowAccelZ1);
(tAxisZ1Move.rSpeed=tLR.siSlowVelZ1);
}else{
(tAxisZ1Move.siTargetPos=tTargetGC.siZ1GCPossession);
(tAxisZ1Move.rAccel=tLR.siFastAccelZ1);
(tAxisZ1Move.rSpeed=tLR.siFastVelZ1);
}

__AS__Action__Z1Move_act();
__AS__Action__Z2Move_act();
if((((signed long)tAxisZ1Move.state==(signed long)20))){
(bAxisRArrivedFlag=0);
(uiCurrentSubState=60);
}

}break;case 60:{
(tAxisRMove.siTargetPos=-270);
(bAxisRStartFlag=1);
__AS__Action__RMove_act();
__AS__Action__Z2Move_act();
if(((((unsigned long)(unsigned char)bAxisRArrivedFlag==(unsigned long)(unsigned char)1))&(((signed long)tAxisZ2Move.state==(signed long)20)))){
(bAxisRStartFlag=0);

(uiCurrentSubState=20);
}

}break;case 20:{
(tAxisY1Move.siTargetPos=tTargetGC.siY1AccessPos);
(bAxisY1ArrivedFlag=1);
__AS__Action__Y1Move_act();
if((((signed long)tAxisY1Move.state==(signed long)20))){

(uiCurrentSubState=30);
}

}break;case 30:{
(tAxisY2Move.siTargetPos=tTargetGC.siY2AccessPos);
(bAxisY2ArrivedFlag=1);
__AS__Action__Y2Move_act();
if((((signed long)tAxisY2Move.state==(signed long)20))){

if((((unsigned long)(unsigned char)tLR.bWaferPossession==(unsigned long)(unsigned char)1))){
__AS__Action__StartLoadGC_act();
}else{
__AS__Action__StartUnloadGC_act();
}
}
}break;}

}imp5_case0_5:imp5_endcase0_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 1 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/LoadFP_act.st"
static void __AS__Action__StartLoadFP_act(void){
{(uiCurrentState=800);
(uiCurrentSubState=40);

}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 7 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/LoadFP_act.st"
static void __AS__Action__LoadFP_act(void){
{switch(uiCurrentSubState){
case 40:{

(tAxisZ1Move.rAccel=tLR.siSlowAccelZ1);
(tAxisZ1Move.rSpeed=tLR.siSlowVelZ1);
(tAxisZ1Move.siTargetPos=tCurrentTargetFP.siZ1FPPossession);
__AS__Action__Z1Move_act();

if((((signed long)tAxisZ1Move.state==(signed long)20))){
(tLR.bWaferPossession=0);
__AS__Action__SetAcceleration_act();
(uiCurrentSubState=20);
}

}break;case 20:{
(tAxisY1Move.siTargetPos=tLR.siY1SafePos);
(bAxisY1ArrivedFlag=1);
__AS__Action__Y1Move_act();
if((((signed long)tAxisY1Move.state==(signed long)20))){
(bAxisRArrivedFlag=0);

(uiCurrentSubState=30);
}

}break;case 30:{
(tAxisY2Move.siTargetPos=tLR.siY2SafePos);
(bAxisY2ArrivedFlag=1);
__AS__Action__Y2Move_act();
if((((signed long)tAxisY2Move.state==(signed long)20))){

if(((((unsigned long)(unsigned char)tLR.Process.bUNLOAD==(unsigned long)(unsigned char)1))|(((unsigned long)(unsigned char)tLR.Process.uiBatchTracker==(unsigned long)(unsigned char)2)))){
__AS__Action__StartSafeAlign_act();

}else if(((((unsigned long)(unsigned char)tLR.Process.bDUALUNLOAD==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)tLR.Process.uiBatchTracker==(unsigned long)(unsigned char)1)))){
(tAxisRMove.siTargetPos=-180);
(bAxisRStartFlag=1);
__AS__Action__RMove_act();
if((((unsigned long)(unsigned char)bAxisRArrivedFlag==(unsigned long)(unsigned char)1))){
(bAxisRStartFlag=0);

__AS__Action__StartGCAlign_act();

(tLR.Process.uiBatchTracker=2);
(tCurrentTargetFP=*(struct FOUPPort_typ*)&tTargetFP2);
}
}
}
}break;}
}imp9_case0_2:imp9_endcase0_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 2 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/LoadGC_act.st"
static void __AS__Action__StartLoadGC_act(void){
{(uiCurrentState=400);
(uiCurrentSubState=40);

}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 8 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/LoadGC_act.st"
static void __AS__Action__LoadGC_act(void){
{switch(uiCurrentSubState){
case 40:{

(tAxisZ1Move.rAccel=tLR.siSlowAccelZ1);
(tAxisZ1Move.rSpeed=tLR.siSlowVelZ1);
(tAxisZ1Move.siTargetPos=tTargetGC.siZ1GCPossession);
__AS__Action__Z1Move_act();

if((((signed long)tAxisZ1Move.state==(signed long)20))){

(tLR.bWaferPossession=0);
__AS__Action__SetAcceleration_act();
(uiCurrentSubState=20);
}

}break;case 20:{
(tAxisY1Move.siTargetPos=tLR.siY1SafePos);
(bAxisY1ArrivedFlag=1);
__AS__Action__Y1Move_act();
if((((signed long)tAxisY1Move.state==(signed long)20))){
(bAxisRArrivedFlag=0);

(uiCurrentSubState=30);
}

}break;case 30:{
(tAxisY2Move.siTargetPos=tLR.siY2SafePos);
(bAxisY2ArrivedFlag=1);
__AS__Action__Y2Move_act();
if((((signed long)tAxisY2Move.state==(signed long)20))){

if(((((unsigned long)(unsigned char)tLR.Process.bLOAD==(unsigned long)(unsigned char)1))|(((unsigned long)(unsigned char)tLR.Process.uiBatchTracker==(unsigned long)(unsigned char)2)))){

__AS__Action__StartSafeAlign_act();
}else if(((((unsigned long)(unsigned char)tLR.Process.bDUALLOAD==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)tLR.Process.uiBatchTracker==(unsigned long)(unsigned char)1)))){
(tAxisRMove.siTargetPos=-180);
(bAxisRStartFlag=1);
__AS__Action__RMove_act();
if((((unsigned long)(unsigned char)bAxisRArrivedFlag==(unsigned long)(unsigned char)1))){
(bAxisRStartFlag=0);

__AS__Action__StartFPAlign_act();

(tLR.Process.uiBatchTracker=2);
(tCurrentTargetFP=*(struct FOUPPort_typ*)&tTargetFP2);
}
}
}
}break;}
}imp10_case0_2:imp10_endcase0_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 1 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/SafeAlign_act.st"
static void __AS__Action__StartSafeAlign_act(void){
{(uiCurrentState=0);
(uiCurrentSubState=60);

}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 7 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/SafeAlign_act.st"
static void __AS__Action__SafeAlign_act(void){
{switch(uiCurrentSubState){

case 60:{
(tAxisRMove.siTargetPos=-180);
(bAxisRStartFlag=1);
__AS__Action__RMove_act();
if((((unsigned long)(unsigned char)bAxisRArrivedFlag==(unsigned long)(unsigned char)1))){
(bAxisRStartFlag=0);

(uiCurrentSubState=50);
}

}break;case 50:{
if(((((unsigned long)(unsigned char)tLR.Process.bDUALLOAD==(unsigned long)(unsigned char)1))|(((unsigned long)(unsigned char)tLR.Process.bLOAD==(unsigned long)(unsigned char)1)))){
(tAxisZ2Move.rAccel=tLR.siSlowAccelZ2);
(tAxisZ2Move.rSpeed=tLR.siSlowVelZ2);
}else{
(tAxisZ2Move.rAccel=tLR.siFastAccelZ2);
(tAxisZ2Move.rSpeed=tLR.siFastVelZ2);
}
(tAxisZ2Move.siTargetPos=tLR.siZ2SafePos);
__AS__Action__Z2Move_act();


if(((((signed long)tAxisZ2Move.state==(signed long)15))|(((signed long)tAxisZ2Move.state==(signed long)20)))){

(uiCurrentSubState=10);
}

}break;case 10:{
(tAxisX1Move.siTargetPos=tLR.siX1SafePos);
__AS__Action__X1Move_act();
if((((signed long)tAxisX1Move.state==(signed long)20))){

(uiCurrentSubState=20);
}

}break;case 20:{
(tAxisY1Move.siTargetPos=tLR.siY1SafePos);
(bAxisY1ArrivedFlag=1);
__AS__Action__Y1Move_act();
if((((signed long)tAxisY1Move.state==(signed long)20))){

(uiCurrentSubState=30);
}

}break;case 30:{
(tAxisY2Move.siTargetPos=tLR.siY2SafePos);
(bAxisY2ArrivedFlag=1);
__AS__Action__Y2Move_act();
if((((signed long)tAxisY2Move.state==(signed long)20))){

(uiCurrentSubState=40);
}

}break;case 40:{
(tAxisZ1Move.rAccel=tLR.siFastAccelZ1);
(tAxisZ1Move.rSpeed=tLR.siFastVelZ1);
(tAxisZ1Move.siTargetPos=tLR.siZ1SafePos);
__AS__Action__Z1Move_act();

if((((signed long)tAxisZ1Move.state==(signed long)20))){

(uiCurrentSubState=100);
}

}break;case 100:{

(uiCurrentSuperState=0);
(gLROutput.bComplete=1);

}break;}
}imp21_case0_6:imp21_endcase0_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 1 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/SetAcceleration_act.st"
static void __AS__Action__SetAcceleration_act(void){
{if((((unsigned long)(unsigned char)tLR.bWaferPossession==(unsigned long)(unsigned char)1))){
(tAxisX1Move.rAccel=tLR.siSlowAccelX1);
(tAxisX1Move.rSpeed=tLR.siSlowVelX1);
(tAxisY1Move.rAccel=tLR.siSlowAccelY1);
(tAxisY1Move.rSpeed=tLR.siSlowVelY1);
(tAxisY2Move.rAccel=tLR.siSlowAccelY2);
(tAxisY2Move.rSpeed=tLR.siSlowVelY2);
(tAxisRMove.rAccel=tLR.siSlowAccelR);
(tAxisRMove.rSpeed=tLR.siSlowSpeedR);
}else{
(tAxisX1Move.rAccel=tLR.siFastAccelX1);
(tAxisX1Move.rSpeed=tLR.siFastVelX1);
(tAxisY1Move.rAccel=tLR.siFastAccelY1);
(tAxisY1Move.rSpeed=tLR.siFastVelY1);
(tAxisY2Move.rAccel=tLR.siFastAccelY2);
(tAxisY2Move.rSpeed=tLR.siFastVelY2);
(tAxisRMove.rAccel=tLR.siFastAccelR);
(tAxisRMove.rSpeed=tLR.siFastSpeedR);
}
}imp22_end0_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 1 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/UnloadFP_act.st"
static void __AS__Action__StartUnloadFP_act(void){
{(uiCurrentState=200);
(uiCurrentSubState=40);

}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 7 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/UnloadFP_act.st"
static void __AS__Action__UnloadFP_act(void){
{switch(uiCurrentSubState){
case 40:{

(tAxisZ1Move.rAccel=tLR.siSlowAccelZ1);
(tAxisZ1Move.rSpeed=tLR.siSlowVelZ1);
(tAxisZ1Move.siTargetPos=tCurrentTargetFP.siZ1LRPossession);

__AS__Action__Z1Move_act();

if((((signed long)tAxisZ1Move.state==(signed long)20))){

(tLR.bWaferPossession=1);
__AS__Action__SetAcceleration_act();
(uiCurrentSubState=20);
}

}break;case 20:{

(tAxisY1Move.siTargetPos=tLR.siY1SafePos);

(bAxisY1ArrivedFlag=1);
__AS__Action__Y1Move_act();
if((((signed long)tAxisY1Move.state==(signed long)20))){

(uiCurrentSubState=30);
}

}break;case 30:{

(tAxisY2Move.siTargetPos=tLR.siY2SafePos);

(bAxisY2ArrivedFlag=1);
__AS__Action__Y2Move_act();
if((((signed long)tAxisY2Move.state==(signed long)20))){

(bAxisRArrivedFlag=0);
(uiCurrentSubState=60);
}

}break;case 60:{
(tAxisRMove.siTargetPos=-180);
(bAxisRStartFlag=1);
__AS__Action__RMove_act();
if((((unsigned long)(unsigned char)bAxisRArrivedFlag==(unsigned long)(unsigned char)1))){
(bAxisRStartFlag=0);

__AS__Action__StartGCAlign_act();
}

}break;}
}imp33_case0_3:imp33_endcase0_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 1 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/UnloadGC_act.st"
static void __AS__Action__StartUnloadGC_act(void){
{(uiCurrentState=600);
(uiCurrentSubState=40);

}}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 7 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StateActions/UnloadGC_act.st"
static void __AS__Action__UnloadGC_act(void){
{switch(uiCurrentSubState){
case 40:{

(tAxisZ1Move.rAccel=tLR.siSlowAccelZ1);
(tAxisZ1Move.rSpeed=tLR.siSlowVelZ1);
(tAxisZ1Move.siTargetPos=tTargetGC.siZ1LRPossession);
__AS__Action__Z1Move_act();

if((((signed long)tAxisZ1Move.state==(signed long)20))){

(tLR.bWaferPossession=1);
__AS__Action__SetAcceleration_act();
(uiCurrentSubState=20);
}

}break;case 20:{
(tAxisY1Move.siTargetPos=tLR.siY1SafePos);
(bAxisY1ArrivedFlag=1);
__AS__Action__Y1Move_act();
if((((signed long)tAxisY1Move.state==(signed long)20))){

(uiCurrentSubState=30);
}

}break;case 30:{
(tAxisY2Move.siTargetPos=tLR.siY2SafePos);
(bAxisY2ArrivedFlag=1);
__AS__Action__Y2Move_act();
if((((signed long)tAxisY2Move.state==(signed long)20))){
(bAxisRArrivedFlag=0);

(uiCurrentSubState=60);
}

}break;case 60:{
(tAxisRMove.siTargetPos=-180);
(bAxisRStartFlag=1);
__AS__Action__RMove_act();
if((((unsigned long)(unsigned char)bAxisRArrivedFlag==(unsigned long)(unsigned char)1))){
(bAxisRStartFlag=0);

__AS__Action__StartFPAlign_act();
}
}break;}
}imp34_case0_3:imp34_endcase0_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 2 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StaubliActions/Staubli_act.st"
static void __AS__Action__Staubli_act(void){
{if((((unsigned long)(unsigned char)bStaubliRobotReady==(unsigned long)(unsigned char)1))){
(tLR.usiSRReadyInd=usiGreenIndColor);
}else{
(tLR.usiSRReadyInd=usiRedIndColor);
}

if((((unsigned long)(unsigned char)bStaubliRobotSafePos==(unsigned long)(unsigned char)1))){
(tLR.usiSRSafePosInd=usiGreenIndColor);
}else{
(tLR.usiSRSafePosInd=usiRedIndColor);
}

switch(uiSRState){










case 10:{



if(((((signed long)uiCurrentSuperState==(signed long)0))&(((unsigned long)(unsigned char)gLRInput.bRUN==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)bStaubliRobotSafePos==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)bStaubliRobotReady==(unsigned long)(unsigned char)1)))){
if((((unsigned long)(unsigned char)gLRInput.bLoad200Batch1==(unsigned long)(unsigned char)1))){
(gLRInput.bWaferSize300mm=0);
(gLRInput.bWaferSize200mm=1);
(gLRInput.bUnloadGC=0);
(gLRInput.bLoadGC=1);
(gLRInput.bBatchCount2=0);
(gLRInput.bBatchCount1=1);
(gLRInput.bGC1=1);
(gLRInput.bBatch1GCBottomLevel=1);
(gLRInput.bBatch1GCTopLevel=0);
(gLRInput.siBatch1FPort=gLRInput.siFOUPPortNumberB1);
(gLRInput.bLoadWaferType=1);
(gLRInput.bUnloadWaferType=0);
(gLRInput.bLoad200Batch1=0);
(uiSRState=15);
}else if((((unsigned long)(unsigned char)gLRInput.bUnload200Batch1==(unsigned long)(unsigned char)1))){
(gLRInput.bWaferSize300mm=0);
(gLRInput.bWaferSize200mm=1);
(gLRInput.bLoadGC=0);
(gLRInput.bUnloadGC=1);
(gLRInput.bBatchCount2=0);
(gLRInput.bBatchCount1=1);
(gLRInput.bGC1=1);
(gLRInput.bBatch1GCBottomLevel=1);
(gLRInput.bBatch1GCTopLevel=0);
(gLRInput.siBatch1FPort=gLRInput.siFOUPPortNumberB1);
(gLRInput.bUnloadWaferType=1);
(gLRInput.bLoadWaferType=0);
(gLRInput.bUnload200Batch1=0);
(uiSRState=15);
}else if((((unsigned long)(unsigned char)gLRInput.bLoad200Batch2BT==(unsigned long)(unsigned char)1))){
if((((unsigned long)(unsigned char)gLRInput.siFOUPPortNumberB1!=(unsigned long)(unsigned char)gLRInput.siFOUPPortNumberB2))){
(gLRInput.bWaferSize300mm=0);
(gLRInput.bWaferSize200mm=1);
(gLRInput.bUnloadGC=0);
(gLRInput.bLoadGC=1);
(gLRInput.bBatchCount1=0);
(gLRInput.bBatchCount2=1);
(gLRInput.bGC1=1);
(gLRInput.bBatch1GCTopLevel=0);
(gLRInput.bBatch1GCBottomLevel=1);
(gLRInput.bBatch2GCTopLevel=1);
(gLRInput.bBatch2GCBottomLevel=0);
(gLRInput.siBatch1FPort=gLRInput.siFOUPPortNumberB1);
(gLRInput.siBatch2FPort=gLRInput.siFOUPPortNumberB2);
(gLRInput.bLoadWaferType=1);
(gLRInput.bUnloadWaferType=0);
(gLRInput.bLoad200Batch2BT=0);
(uiSRState=15);
}
}else if((((unsigned long)(unsigned char)gLRInput.bLoad200Batch2TB==(unsigned long)(unsigned char)1))){
if((((unsigned long)(unsigned char)gLRInput.siFOUPPortNumberB1!=(unsigned long)(unsigned char)gLRInput.siFOUPPortNumberB2))){
(gLRInput.bWaferSize300mm=0);
(gLRInput.bWaferSize200mm=1);
(gLRInput.bUnloadGC=0);
(gLRInput.bLoadGC=1);
(gLRInput.bBatchCount1=0);
(gLRInput.bBatchCount2=1);
(gLRInput.bGC1=1);
(gLRInput.bBatch1GCTopLevel=1);
(gLRInput.bBatch1GCBottomLevel=0);
(gLRInput.bBatch2GCTopLevel=0);
(gLRInput.bBatch2GCBottomLevel=1);
(gLRInput.siBatch1FPort=gLRInput.siFOUPPortNumberB1);
(gLRInput.siBatch2FPort=gLRInput.siFOUPPortNumberB2);
(gLRInput.bLoadWaferType=1);
(gLRInput.bUnloadWaferType=0);
(gLRInput.bLoad200Batch2TB=0);
(uiSRState=15);
}
}else if((((unsigned long)(unsigned char)gLRInput.bUnload200Batch2TB==(unsigned long)(unsigned char)1))){
if((((unsigned long)(unsigned char)gLRInput.siFOUPPortNumberB1!=(unsigned long)(unsigned char)gLRInput.siFOUPPortNumberB2))){
(gLRInput.bWaferSize300mm=0);
(gLRInput.bWaferSize200mm=1);
(gLRInput.bLoadGC=0);
(gLRInput.bUnloadGC=1);
(gLRInput.bBatchCount1=0);
(gLRInput.bBatchCount2=1);
(gLRInput.bGC1=1);
(gLRInput.bBatch1GCTopLevel=1);
(gLRInput.bBatch1GCBottomLevel=0);
(gLRInput.bBatch2GCTopLevel=0);
(gLRInput.bBatch2GCBottomLevel=1);
(gLRInput.siBatch1FPort=gLRInput.siFOUPPortNumberB1);
(gLRInput.siBatch2FPort=gLRInput.siFOUPPortNumberB2);
(gLRInput.bUnloadWaferType=1);
(gLRInput.bLoadWaferType=0);
(gLRInput.bUnload200Batch2TB=0);
(uiSRState=15);
}
}else if((((unsigned long)(unsigned char)gLRInput.bUnload200Batch2BT==(unsigned long)(unsigned char)1))){
if((((unsigned long)(unsigned char)gLRInput.siFOUPPortNumberB1!=(unsigned long)(unsigned char)gLRInput.siFOUPPortNumberB2))){
(gLRInput.bWaferSize300mm=0);
(gLRInput.bWaferSize200mm=1);
(gLRInput.bLoadGC=0);
(gLRInput.bUnloadGC=1);
(gLRInput.bBatchCount1=0);
(gLRInput.bBatchCount2=1);
(gLRInput.bGC1=1);
(gLRInput.bBatch1GCTopLevel=0);
(gLRInput.bBatch1GCBottomLevel=1);
(gLRInput.bBatch2GCTopLevel=1);
(gLRInput.bBatch2GCBottomLevel=0);
(gLRInput.siBatch1FPort=gLRInput.siFOUPPortNumberB1);
(gLRInput.siBatch2FPort=gLRInput.siFOUPPortNumberB2);
(gLRInput.bUnloadWaferType=1);
(gLRInput.bLoadWaferType=0);
(gLRInput.bUnload200Batch2BT=0);
(uiSRState=15);
}
}else if((((unsigned long)(unsigned char)gLRInput.bLoad300Batch1==(unsigned long)(unsigned char)1))){
(gLRInput.bBatch1GCBottomLevel=0);
(gLRInput.bBatch1GCTopLevel=0);
(gLRInput.bWaferSize200mm=0);
(gLRInput.bBatchCount2=0);
(gLRInput.bUnloadGC=0);
(gLRInput.siBatch1FPort=gLRInput.siFOUPPortNumberB1);
(gLRInput.bWaferSize300mm=1);
(gLRInput.bLoadGC=1);
(gLRInput.bBatchCount1=1);
(gLRInput.bGC1=1);
(gLRInput.bLoadWaferType=1);
(gLRInput.bUnloadWaferType=0);
(gLRInput.bLoad300Batch1=0);
(uiSRState=15);
}else if((((unsigned long)(unsigned char)gLRInput.bUnload300Batch1==(unsigned long)(unsigned char)1))){
(gLRInput.bBatch1GCBottomLevel=0);
(gLRInput.bBatch1GCTopLevel=0);
(gLRInput.bWaferSize200mm=0);
(gLRInput.bLoadGC=0);
(gLRInput.bBatchCount2=0);
(gLRInput.siBatch1FPort=gLRInput.siFOUPPortNumberB1);
(gLRInput.bWaferSize300mm=1);
(gLRInput.bUnloadGC=1);
(gLRInput.bBatchCount1=1);
(gLRInput.bGC1=1);
(gLRInput.bUnloadWaferType=1);
(gLRInput.bLoadWaferType=0);
(gLRInput.bUnload300Batch1=0);
(uiSRState=15);
}
}


}break;case 15:{
if((((unsigned long)(unsigned char)bErrorCheckError==(unsigned long)(unsigned char)1))){
(uiSRState=10);
}else{
(uiSRState=16);
}


}break;case 16:{
if((((unsigned long)(unsigned char)gLRInput.bLoadWaferType==(unsigned long)(unsigned char)1))){
(uiSRState=20);
}else if((((unsigned long)(unsigned char)gLRInput.bUnloadWaferType==(unsigned long)(unsigned char)1))){
(uiSRState=40);
}else if((((unsigned long)(unsigned char)bErrorCheckError==(unsigned long)(unsigned char)1))){
(uiSRState=10);
}


}break;case 20:{
if((((unsigned long)(unsigned char)bStaubliRobotSafePos==(unsigned long)(unsigned char)1))){
(gLRInput.bRUN=1);
(uiSRState=30);
}else if((((unsigned long)(unsigned char)bErrorCheckError==(unsigned long)(unsigned char)1))){
(uiSRState=10);
}


}break;case 30:{
if((((unsigned long)(unsigned char)gLROutput.bComplete==(unsigned long)(unsigned char)1))){
(gLRInput.bWaferSize300mm=0);
(gLRInput.bWaferSize200mm=0);
(gLRInput.bLoadGC=0);
(gLRInput.bUnloadGC=0);
(gLRInput.bBatchCount1=0);
(gLRInput.bBatchCount2=0);
(gLRInput.bGC1=0);
(gLRInput.bGC2=0);
(gLRInput.bBatch1GCBottomLevel=0);
(gLRInput.bBatch1GCTopLevel=0);
(gLRInput.bBatch2GCBottomLevel=0);
(gLRInput.bBatch2GCTopLevel=0);
if((((unsigned long)(unsigned char)gLRInput.bLoadWaferType==(unsigned long)(unsigned char)1))){
(gLROutput.bComplete=0);
(uiSRState=40);
}else if((((unsigned long)(unsigned char)gLRInput.bUnloadWaferType==(unsigned long)(unsigned char)1))){
(gLROutput.bComplete=0);
(gLRInput.bUnloadWaferType=0);
(uiSRState=10);
}
}else if((((unsigned long)(unsigned char)bErrorCheckError==(unsigned long)(unsigned char)1))){
(uiSRState=10);
}










}break;case 40:{
if((((unsigned long)(unsigned char)bStaubliRobotReady==(unsigned long)(unsigned char)1))){
if((((unsigned long)(unsigned char)gLRInput.bLoadWaferType==(unsigned long)(unsigned char)1))){
(gLROutput.usiSRMoveStartPosition=usiLoadStartPosition);
(gLROutput.usiSRMoveTargetPosition=usiLoadTargetPosition);
(uiSRState=45);
}else if((((unsigned long)(unsigned char)gLRInput.bUnloadWaferType==(unsigned long)(unsigned char)1))){
(gLROutput.usiSRMoveStartPosition=usiUnloadStartPosition);
(gLROutput.usiSRMoveTargetPosition=usiUnloadTargetPosition);
(uiSRState=45);
}
}else if((((unsigned long)(unsigned char)bErrorCheckError==(unsigned long)(unsigned char)1))){
(uiSRState=10);
}


}break;case 45:{



if(((((unsigned long)(unsigned char)bStaubliRobotReady==(unsigned long)(unsigned char)1))&(((signed long)uiCurrentSuperState==(signed long)0))&(((unsigned long)(unsigned char)gLRInput.bRUN==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)bStaubliRobotRunCMD==(unsigned long)(unsigned char)0)))){
(bStaubliRobotRunCMD=1);
(uiSRState=47);
}else if((((unsigned long)(unsigned char)bErrorCheckError==(unsigned long)(unsigned char)1))){
(uiSRState=10);
}


}break;case 47:{

if(((((unsigned long)(unsigned char)bStaubliRobotSafePos==(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned char)bStaubliRobotRunCMD==(unsigned long)(unsigned char)0)))){
(uiSRState=50);
}else if((((unsigned long)(unsigned char)bErrorCheckError==(unsigned long)(unsigned char)1))){
(uiSRState=10);
}


}break;case 50:{
if((((unsigned long)(unsigned char)gLRInput.obStaubliSafePosRE==(unsigned long)(unsigned char)1))){
if((((unsigned long)(unsigned char)gLRInput.bLoadWaferType==(unsigned long)(unsigned char)1))){
(gLRInput.bLoadWaferType=0);
(uiSRState=10);
}else if((((unsigned long)(unsigned char)gLRInput.bUnloadWaferType==(unsigned long)(unsigned char)1))){
(uiSRState=20);
}
}else if((((unsigned long)(unsigned char)bErrorCheckError==(unsigned long)(unsigned char)1))){
(uiSRState=10);
}

}break;}


(dtCycleTimeTON.IN=bCycleTimeStart);;(dtCycleTimeTON.PT=900000);;TON(&dtCycleTimeTON);
(dtCycleTime=dtCycleTimeTON.ET);
if((((signed long)uiSRAutoCycleState==(signed long)80))){
(dtPrevCycleTime=dtCycleTime);
}
if(dtCycleTimeTON.Q){
(bCycleTimeStart=0);
}



if((((unsigned long)(unsigned char)gLRInput.bAutoCycleMode==(unsigned long)(unsigned char)0))){
(uiAutoCycleCounts=gLRInput.uiAutoCycleCnts);
}


switch(uiSRAutoCycleState){

case 0:{
if((((unsigned long)(unsigned char)gLRInput.bAutoCycleMode==(unsigned long)(unsigned char)1))){
(uiSRAutoCycleState=10);
}

}break;case 10:{
if((((unsigned long)(unsigned char)gLRInput.bAutoCycleMode==(unsigned long)(unsigned char)0))){
(uiSRAutoCycleState=0);
}else if((((unsigned long)(unsigned char)gLRInput.bStartAutoCyclePBRE==(unsigned long)(unsigned char)1))){
(uiSRAutoCycleState=20);
}


}break;case 20:{
(uiAutoCycleCounts=0);
(uiSRAutoCycleState=30);


}break;case 30:{
if((((unsigned long)(unsigned short)uiAutoCycleCounts<(unsigned long)(unsigned short)gLRInput.uiAutoCycleCnts))){
(bCycleTimeStart=1);
(uiSRAutoCycleState=40);
}else{
(uiSRAutoCycleState=0);
}


}break;case 40:{
(gLRInput.bLoad200Batch2BT=1);
(uiSRAutoCycleState=50);


}break;case 50:{
if((((unsigned long)(unsigned char)gLRInput.bLoad200Batch2BT==(unsigned long)(unsigned char)0))){


if(((((signed long)uiSRState==(signed long)10))&(((signed long)uiCurrentSuperState==(signed long)0))&(((unsigned long)(unsigned char)gLRInput.bRUN==(unsigned long)(unsigned char)0)))){
(uiSRAutoCycleState=60);
}
}


}break;case 60:{
(gLRInput.bUnload200Batch2TB=1);
(uiSRAutoCycleState=70);


}break;case 70:{
if((((unsigned long)(unsigned char)gLRInput.bUnload200Batch2TB==(unsigned long)(unsigned char)0))){


if(((((signed long)uiSRState==(signed long)10))&(((signed long)uiCurrentSuperState==(signed long)0))&(((unsigned long)(unsigned char)gLRInput.bRUN==(unsigned long)(unsigned char)0)))){
(uiSRAutoCycleState=80);
}
}


}break;case 80:{
(bCycleTimeStart=0);
(uiAutoCycleCounts=(uiAutoCycleCounts+1));
(uiSRAutoCycleState=30);

}break;}

}imp32_case23_8:imp32_endcase23_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 374 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/StaubliActions/Staubli_act.st"
static void __AS__Action__StaubliMove_act(void){
{
switch(uiSRMoveState){

case 0:{


if(((((unsigned long)(unsigned char)bStaubliRobotReady==(unsigned long)(unsigned char)1))&(((signed long)uiCurrentSuperState==(signed long)0))&(((unsigned long)(unsigned char)gLRInput.bRUN==(unsigned long)(unsigned char)0)))){
if((((unsigned long)(unsigned char)bStaubliRobotRunCMD==(unsigned long)(unsigned char)1))){
(uiSRMoveState=10);
}
}else if((((unsigned long)(unsigned char)bErrorCheckError==(unsigned long)(unsigned char)1))){
return;
}

}break;case 10:{
(gLROutput.bWriteStaubliRobotOnce=0);
(gLROutput.bWriteStaubliTargetOnce=0);
if((((unsigned long)(unsigned char)bStaubliRobotReady==(unsigned long)(unsigned char)1))){
(gLROutput.uiWriteStaubliRobot=gLROutput.usiSRMoveStartPosition);
(gLROutput.uiWriteStaubliTargetPosition=gLROutput.usiSRMoveTargetPosition);
(uiSRMoveState=20);
}else if((((unsigned long)(unsigned char)bErrorCheckError==(unsigned long)(unsigned char)1))){
(uiSRMoveState=0);
}


}break;case 20:{
if((((unsigned long)(unsigned char)gLRInput.bReadStaubliRobotOnceAck==(unsigned long)(unsigned char)0))){
(gLROutput.bWriteStaubliRobotOnce=1);
(uiSRMoveState=30);
}


}break;case 30:{
if((((unsigned long)(unsigned char)gLRInput.bReadStaubliTargetPosAck==(unsigned long)(unsigned char)0))){
(gLROutput.bWriteStaubliTargetOnce=1);
(uiSRMoveState=40);
}


}break;case 40:{

if(((((unsigned long)(unsigned char)gLRInput.bReadStaubliRobotOnceAck==(unsigned long)(unsigned char)1))&(((unsigned long)(unsigned char)gLRInput.bReadStaubliTargetPosAck==(unsigned long)(unsigned char)1)))){
(gLROutput.bWriteStaubliRobotOnce=0);
(gLROutput.bWriteStaubliTargetOnce=0);
(gLROutput.bStaubliRunCMDOnce=0);
(uiSRMoveState=50);
}


}break;case 50:{
(gLROutput.bStaubliRunCMD=1);
if((((unsigned long)(unsigned char)gLRInput.bReadStaubliRunCMDAck==(unsigned long)(unsigned char)0))){
(gLROutput.bStaubliRunCMDOnce=1);
(uiSRMoveState=60);
}


}break;case 60:{
if((((unsigned long)(unsigned char)gLRInput.bReadStaubliRunCMDAck==(unsigned long)(unsigned char)1))){
if((((unsigned long)(unsigned char)bStaubliRobotSafePos==(unsigned long)(unsigned char)0))){
(gLROutput.bStaubliRunCMDOnce=0);
(uiSRMoveState=70);
}
}


}break;case 70:{
if((((unsigned long)(unsigned char)gLRInput.bReadStaubliRunCMDAck==(unsigned long)(unsigned char)0))){
(gLROutput.bStaubliRunCMD=0);
(gLROutput.bStaubliRunCMDOnce=1);
(uiSRMoveState=80);
}


}break;case 80:{
if((((unsigned long)(unsigned char)gLRInput.bReadStaubliRunCMDAck==(unsigned long)(unsigned char)1))){
(bStaubliRobotRunCMD=0);
(gLROutput.bStaubliRunCMDOnce=0);
(uiSRMoveState=0);
}

}break;}

}imp31_case0_8:imp31_endcase0_0:;}
#line 174 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st"
#line 174 "__AS__IMPLICIT_INLINE.c"

signed long __AS__STRING_CMP(char* pstr1, char* pstr2)
{while (*pstr1 != 0 && *pstr1 == *pstr2){ pstr1++;pstr2++; } return (*pstr1 == 0 && *pstr2 != 0) ? -1 : (*pstr1 != 0 && *pstr2 == 0) ? 1 : *pstr1 - *pstr2;}

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/NcGlobal/NcGlobal.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpComError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/Acp10man/Acp10man.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/Acp10_MC/acp10_mc.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxisError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxisAlarm.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/NcGlobal/NcGlobal.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/Acp10_MC/acp10_mc.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/NcGlobal/NcGlobal.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/Acp10par/Acp10par.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/Acp10_MC/acp10_mc.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/LinearRobotConfiguration/GlobalComponents/MpComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/Axis.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/Rotary.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/States.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/FOUP_GC.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/AxisJog.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/Projects/Linear Robot/OEMCintillioLinearRobot/Temp/Objects/LinearRobotConfiguration/X20CP0484/Program1/Cyclic.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/Projects/Linear Robot/OEMCintillioLinearRobot/Temp/Objects/LinearRobotConfiguration/X20CP0484/Program1/Cyclic.st.c\\\" \\\"C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Cyclic.st\\\"\\n\"");
__asm__(".previous");

__asm__(".section \".plciec\"");
__asm__(".ascii \"plcdata_const 'RSInput04'\\n\"");
__asm__(".ascii \"plcdata_const 'RSInput05'\\n\"");
__asm__(".ascii \"plcdata_const 'RSInput06'\\n\"");
__asm__(".ascii \"plcdata_const 'RSInput07'\\n\"");
__asm__(".ascii \"plcdata_const 'siFineJogType'\\n\"");
__asm__(".ascii \"plcdata_const 'siCoarseJogType'\\n\"");
__asm__(".ascii \"plcdata_const 'ncACKNOWLEDGE'\\n\"");
__asm__(".ascii \"plcdata_const 'ncMESSAGE'\\n\"");
__asm__(".ascii \"plcdata_const 'ncOK'\\n\"");
__asm__(".previous");
