#ifndef __AS__TYPE_
#define __AS__TYPE_
void __AS__ImplInitInit(void);
void __AS__ImplInitDummyInit(void);

void __AS__ImplInitCyclic(void);
void __AS__ImplInitDummyCyclic(void);

void __AS__ImplInitExit(void);

typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_Process_typ
#define __AS__TYPE_Process_typ
typedef struct Process_typ
{	plcbit bLOAD;
	plcbit bUNLOAD;
	plcbit bDUALLOAD;
	plcbit bDUALUNLOAD;
	unsigned char uiBatchTracker;
} Process_typ;
#endif

#ifndef __AS__TYPE_LinearRobot_typ
#define __AS__TYPE_LinearRobot_typ
typedef struct LinearRobot_typ
{	float siX1SafePos;
	float siY1SafePos;
	float siY2SafePos;
	float siZ1SafePos;
	float siZ2SafePos;
	float siRSafePos;
	float siXHomePos;
	float siY1HomePos;
	float siY2HomePos;
	float siZ1HomePos;
	float siZ2HomePos;
	float siRHomePos;
	float siFastAccelX1;
	float siSlowAccelX1;
	float siFastVelX1;
	float siSlowVelX1;
	float siFastAccelY1;
	float siSlowAccelY1;
	float siFastVelY1;
	float siSlowVelY1;
	float siFastAccelY2;
	float siSlowAccelY2;
	float siFastVelY2;
	float siSlowVelY2;
	float siFastAccelZ1;
	float siSlowAccelZ1;
	float siFastVelZ1;
	float siSlowVelZ1;
	float siFastAccelZ2;
	float siSlowAccelZ2;
	float siFastVelZ2;
	float siSlowVelZ2;
	float siFastAccelR;
	float siSlowAccelR;
	float siFastSpeedR;
	float siSlowSpeedR;
	Process_typ Process;
	plcbit bWaferPossession;
	float rCoarseJogDelX1;
	float rCoarseJogAccelX1;
	float rFineJogIncX1;
	float rCoarseJogVelX1;
	double rX1JogUpperTravelLimit;
	double rX1JogLowerTravelLimit;
	double rYJogUpperTravelLimit;
	double rYJogLowerTravelLimit;
	double rY2JogUpperTravelLimit;
	double rY2JogLowerTravelLimit;
	double rZ1JogUpperTravelLimit;
	double rZ1JogLowerTravelLimit;
	double rZ2JogUpperTravelLimit;
	double rZ2JogLowerTravelLimit;
	plcbit bHomeAllAxis;
	unsigned char usiSWStateIndTxt;
	unsigned char usiSWStateIndColor;
	unsigned char usiSRSafePosInd;
	unsigned char usiSRReadyInd;
} LinearRobot_typ;
#endif

#ifndef __AS__TYPE_FOUPPort_typ
#define __AS__TYPE_FOUPPort_typ
typedef struct FOUPPort_typ
{	double siX1Pos;
	double siY1AccessPos;
	double siY2AccessPos;
	double siZ1LRPossession;
	double siZ1FPPossession;
	double rNewAxisX1TargetPos;
	double rPrevAxisX1TargetPos;
	double rNewAxisY1TargetPos;
	double rPrevAxisY1TargetPos;
	double rNewAxisY2TargetPos;
	double rPrevAxisY2TargetPos;
	double rNewAxisZ1UpTargetPos;
	double rPrevAxisZ1UpTargetPos;
	double rNewAxisZ1LowTargetPos;
	double rPrevAxisZ1LowTargetPos;
} FOUPPort_typ;
#endif

#ifndef __AS__TYPE_GuardianCarrier_typ
#define __AS__TYPE_GuardianCarrier_typ
typedef struct GuardianCarrier_typ
{	signed short eLocationID;
	plcbit bAbsentInd;
	double siX1Pos;
	double siY1AccessPos;
	double siY2AccessPos;
	double siZ1LRPossession;
	double siZ1GCPossession;
	double siZ2TopPos;
	double siZ2BottomPos;
	double siZ2300Pos;
	double rNewAxisX1TargetPos;
	double rPrevAxisX1TargetPos;
	double rNewAxisY1TargetPos;
	double rPrevAxisY1TargetPos;
	double rNewAxisY2TargetPos;
	double rPrevAxisY2TargetPos;
	double rNewAxisZ1LowTargetPos;
	double rPrevAxisZ1LowTargetPos;
	double rNewAxisZ1UpTargetPos;
	double rPrevAxisZ1UpTargetPos;
	double rPrevAxisZ2UpCarrierPos;
	double rPrevAxisZ2LowCarrierPos;
} GuardianCarrier_typ;
#endif

#ifndef __AS__TYPE_LRSuperState_enum
#define __AS__TYPE_LRSuperState_enum
typedef enum LRSuperState_enum
{	eWait = 0,
	eHardwareInitialization = 100,
	eRunning = 200,
} LRSuperState_enum;
#endif

#ifndef __AS__TYPE_AxisX1Jog_enum
#define __AS__TYPE_AxisX1Jog_enum
typedef enum AxisX1Jog_enum
{	eWaitTargetReached = 30,
	eSetJogDirection = 20,
	eWaitJogCmdState = 10,
} AxisX1Jog_enum;
#endif

#ifndef __AS__TYPE_SRState_enum
#define __AS__TYPE_SRState_enum
typedef enum SRState_enum
{	eIdleState = 10,
	eCheckError = 15,
	eWaferHandlingType = 16,
	eRunLinearRobot = 20,
	eLinearRobotMoveComplete = 30,
	eRunStaubliRobot = 40,
	eResetStaubliRobotCmd = 45,
	eWaitStaubliRobotOnceAck = 47,
	eStaubliRobotMoveComplete = 50,
} SRState_enum;
#endif

#ifndef __AS__TYPE_LRAxisHome_enum
#define __AS__TYPE_LRAxisHome_enum
typedef enum LRAxisHome_enum
{	eY1HomeState = 10,
	eY2HomeState = 20,
	eRHomeState = 30,
	eX1HomeState = 40,
	eZ1HomeState = 50,
	eZ2HomeState = 60,
	eHomeStateFinished = 100,
} LRAxisHome_enum;
#endif

#ifndef __AS__TYPE_LRAxisJog_enum
#define __AS__TYPE_LRAxisJog_enum
typedef enum LRAxisJog_enum
{	eAxisJogOff = 0,
	eAxisX1Jog = 10,
	eAxisY1Jog = 20,
	eAxisY2Jog = 30,
	eAxisZ1Jog = 40,
	eAxisZ2Jog = 50,
	eAxisRRotate = 60,
	eAxisReturnHome = 70,
} LRAxisJog_enum;
#endif

#ifndef __AS__TYPE_LRAxisRRotate_enum
#define __AS__TYPE_LRAxisRRotate_enum
typedef enum LRAxisRRotate_enum
{	eWaitRotateCmdState = 10,
	eAxisRRotate180 = 20,
	eAxisRRotate90 = 30,
	eAxisRRotate270 = 40,
} LRAxisRRotate_enum;
#endif

#ifndef __AS__TYPE_SRAutoCycleState_enum
#define __AS__TYPE_SRAutoCycleState_enum
typedef enum SRAutoCycleState_enum
{	eIdle = 0,
	eAutoCycleStart = 10,
	eResetCounts = 20,
	eCycleCountCheck = 30,
	eLoadBatchBT = 40,
	eWaitLoadComplete = 50,
	eUnLoadBatchTB = 60,
	eWaitUnLoadComplete = 70,
	eCycleCounts = 80,
} SRAutoCycleState_enum;
#endif

#ifndef __AS__TYPE_SRMoveState_enum
#define __AS__TYPE_SRMoveState_enum
typedef enum SRMoveState_enum
{	eIdleMoveState = 0,
	eWriteMovePositions = 10,
	eWriteStartPosition = 20,
	eWriteTargetPosition = 30,
	eWaitStaubliOnceAck = 40,
	eSetRunCMDStaubliRobot = 50,
	eResetOnceSignal = 60,
	eResetRunCMD = 70,
	eResetOnceRunCMD = 80,
} SRMoveState_enum;
#endif

#ifndef __AS__TYPE_MpComFacilitiesEnum
#define __AS__TYPE_MpComFacilitiesEnum
typedef enum MpComFacilitiesEnum
{	mpCOM_FAC_UNDEFINED = -1,
	mpCOM_FAC_ARCORE = 0,
	mpCOM_FAC_SAFETY1 = 1,
	mpCOM_FAC_SAFETY2 = 2,
	mpCOM_FAC_GMC1 = 96,
	mpCOM_FAC_GMC2 = 97,
	mpCOM_FAC_GMCAXIS = 98,
	mpCOM_FAC_GMCAXESGROUP = 99,
	mpCOM_FAC_GMCARNCGROUP = 103,
	mpCOM_FAC_TRF = 105,
	mpCOM_FAC_MAPP_INTERNAL = 144,
	mpCOM_FAC_MAPP_CORE = 145,
	mpCOM_FAC_MAPP_INFRASTRUCTURE = 146,
	mpCOM_FAC_MAPP_MECHATRONIC = 147,
	mpCOM_FAC_MAPP_INDUSTRY = 148,
} MpComFacilitiesEnum;
#endif

#ifndef __AS__TYPE_MpComSeveritiesEnum
#define __AS__TYPE_MpComSeveritiesEnum
typedef enum MpComSeveritiesEnum
{	mpCOM_SEV_SUCCESS = 0,
	mpCOM_SEV_INFORMATIONAL = 1,
	mpCOM_SEV_WARNING = 2,
	mpCOM_SEV_ERROR = 3,
} MpComSeveritiesEnum;
#endif

#ifndef __AS__TYPE_MpComIdentType
#define __AS__TYPE_MpComIdentType
typedef struct MpComIdentType
{	unsigned long Internal[2];
} MpComIdentType;
#endif

#ifndef __AS__TYPE_MpComInternalDataType
#define __AS__TYPE_MpComInternalDataType
typedef struct MpComInternalDataType
{	unsigned long pObject;
	unsigned long State;
} MpComInternalDataType;
#endif

#ifndef __AS__TYPE_ACP10SWVER_typ
#define __AS__TYPE_ACP10SWVER_typ
typedef struct ACP10SWVER_typ
{	unsigned short nc_manager;
	unsigned short nc_system;
	unsigned char NOT_USE_1[4];
} ACP10SWVER_typ;
#endif

#ifndef __AS__TYPE_ACP10OBIHW_typ
#define __AS__TYPE_ACP10OBIHW_typ
typedef struct ACP10OBIHW_typ
{	unsigned char init;
	unsigned char reserve1;
	unsigned char acp_id;
	unsigned char acp_id_nr;
	unsigned char acp_typ;
	unsigned char ax_num;
	unsigned short reserve2;
	unsigned long product_code;
	unsigned long serial_nr;
} ACP10OBIHW_typ;
#endif

#ifndef __AS__TYPE_ACP10OBINF_typ
#define __AS__TYPE_ACP10OBINF_typ
typedef struct ACP10OBINF_typ
{	unsigned short net_if_typ;
	unsigned short net_if_idx;
	unsigned short node_nr;
	unsigned short nc_obj_typ;
	unsigned short nc_obj_idx;
	unsigned short reserve;
	unsigned char nc_obj_name[48];
	ACP10OBIHW_typ hardware;
} ACP10OBINF_typ;
#endif

#ifndef __AS__TYPE_ACP10SIMM1_typ
#define __AS__TYPE_ACP10SIMM1_typ
typedef struct ACP10SIMM1_typ
{	float inertia;
	float static_friction;
	float viscous_friction;
} ACP10SIMM1_typ;
#endif

#ifndef __AS__TYPE_ACP10SIMM2_typ
#define __AS__TYPE_ACP10SIMM2_typ
typedef struct ACP10SIMM2_typ
{	float inertia;
	float static_friction;
	float viscous_friction;
	float stiffness;
	float damping;
} ACP10SIMM2_typ;
#endif

#ifndef __AS__TYPE_ACP10SIMGB_typ
#define __AS__TYPE_ACP10SIMGB_typ
typedef struct ACP10SIMGB_typ
{	unsigned char direction;
	unsigned char reserve1;
	unsigned short reserve2;
	unsigned long in_rev;
	unsigned long out_rev;
} ACP10SIMGB_typ;
#endif

#ifndef __AS__TYPE_ACP10SIMPA_typ
#define __AS__TYPE_ACP10SIMPA_typ
typedef struct ACP10SIMPA_typ
{	unsigned short mode;
	unsigned short add_load_par_id;
	ACP10SIMM1_typ mass1;
	ACP10SIMM2_typ mass2;
	ACP10SIMGB_typ gear;
} ACP10SIMPA_typ;
#endif

#ifndef __AS__TYPE_ACP10SIM_typ
#define __AS__TYPE_ACP10SIM_typ
typedef struct ACP10SIM_typ
{	unsigned char init;
	unsigned char NOT_USE_1;
	unsigned char status;
	unsigned char acp_sim;
	unsigned char NOT_USE_2[4];
	ACP10SIMPA_typ parameter;
} ACP10SIM_typ;
#endif

#ifndef __AS__TYPE_ACP10GLIPA_typ
#define __AS__TYPE_ACP10GLIPA_typ
typedef struct ACP10GLIPA_typ
{	unsigned char ok;
	unsigned char error;
	unsigned short reserve;
	unsigned long datobj_ident;
	unsigned char data_modul[12];
} ACP10GLIPA_typ;
#endif

#ifndef __AS__TYPE_ACP10GLINI_typ
#define __AS__TYPE_ACP10GLINI_typ
typedef struct ACP10GLINI_typ
{	unsigned char init;
	unsigned char reserve1;
	unsigned short reserve2;
	ACP10GLIPA_typ init_par;
} ACP10GLINI_typ;
#endif

#ifndef __AS__TYPE_ACP10NSVRQ_typ
#define __AS__TYPE_ACP10NSVRQ_typ
typedef struct ACP10NSVRQ_typ
{	unsigned short par_id;
	unsigned short reserve;
} ACP10NSVRQ_typ;
#endif

#ifndef __AS__TYPE_ACP10NSVRS_typ
#define __AS__TYPE_ACP10NSVRS_typ
typedef struct ACP10NSVRS_typ
{	unsigned short par_id;
	unsigned short reserve;
} ACP10NSVRS_typ;
#endif

#ifndef __AS__TYPE_ACP10NETSV_typ
#define __AS__TYPE_ACP10NETSV_typ
typedef struct ACP10NETSV_typ
{	unsigned long data_adr;
	unsigned char data_text[32];
	ACP10NSVRQ_typ request;
	ACP10NSVRS_typ response;
} ACP10NETSV_typ;
#endif

#ifndef __AS__TYPE_ACP10NET_typ
#define __AS__TYPE_ACP10NET_typ
typedef struct ACP10NET_typ
{	unsigned char init;
	unsigned char phase;
	unsigned char init_allowed;
	unsigned char nc_sys_restart;
	unsigned short reserve;
	unsigned short reserve1;
	ACP10NETSV_typ service;
} ACP10NET_typ;
#endif

#ifndef __AS__TYPE_ACP10AXDBS_typ
#define __AS__TYPE_ACP10AXDBS_typ
typedef struct ACP10AXDBS_typ
{	unsigned char ok;
	unsigned char error;
	unsigned char reserve1;
	unsigned char reserve2;
	unsigned long data_len;
	unsigned long datobj_ident;
	unsigned long datobj_datadr;
} ACP10AXDBS_typ;
#endif

#ifndef __AS__TYPE_ACP10AXDBP_typ
#define __AS__TYPE_ACP10AXDBP_typ
typedef struct ACP10AXDBP_typ
{	unsigned char file_device[32];
	unsigned char datobj_name[32];
	unsigned short datobj_type;
	unsigned short datblock_par_id;
	unsigned short idx1_par_id;
	unsigned short idx1;
	unsigned short idx2_par_id;
	unsigned short idx2;
	unsigned char NOT_USE_1[8];
} ACP10AXDBP_typ;
#endif

#ifndef __AS__TYPE_ACP10AXDBL_typ
#define __AS__TYPE_ACP10AXDBL_typ
typedef struct ACP10AXDBL_typ
{	ACP10AXDBS_typ status;
	ACP10AXDBP_typ parameter;
} ACP10AXDBL_typ;
#endif

#ifndef __AS__TYPE_ACP10DISTA_typ
#define __AS__TYPE_ACP10DISTA_typ
typedef struct ACP10DISTA_typ
{	unsigned char reference;
	unsigned char pos_hw_end;
	unsigned char neg_hw_end;
	unsigned char trigger1;
	unsigned char trigger2;
	unsigned char enable;
	unsigned short reserve2;
} ACP10DISTA_typ;
#endif

#ifndef __AS__TYPE_ACP10DILEV_typ
#define __AS__TYPE_ACP10DILEV_typ
typedef struct ACP10DILEV_typ
{	unsigned short reference;
	unsigned short pos_hw_end;
	unsigned short neg_hw_end;
	unsigned short trigger1;
	unsigned short trigger2;
	unsigned short reserve;
} ACP10DILEV_typ;
#endif

#ifndef __AS__TYPE_ACP10DIFRC_typ
#define __AS__TYPE_ACP10DIFRC_typ
typedef struct ACP10DIFRC_typ
{	unsigned char reference;
	unsigned char pos_hw_end;
	unsigned char neg_hw_end;
	unsigned char trigger1;
	unsigned char trigger2;
	unsigned char reserve1;
	unsigned short reserve2;
} ACP10DIFRC_typ;
#endif

#ifndef __AS__TYPE_ACP10DIGIN_typ
#define __AS__TYPE_ACP10DIGIN_typ
typedef struct ACP10DIGIN_typ
{	unsigned char init;
	unsigned char reserve1;
	unsigned short reserve2;
	ACP10DISTA_typ status;
	ACP10DILEV_typ level;
	ACP10DIFRC_typ force;
} ACP10DIGIN_typ;
#endif

#ifndef __AS__TYPE_ACP10ENCSL_typ
#define __AS__TYPE_ACP10ENCSL_typ
typedef struct ACP10ENCSL_typ
{	unsigned long units;
	unsigned long rev_motor;
} ACP10ENCSL_typ;
#endif

#ifndef __AS__TYPE_ACP10ENCSC_typ
#define __AS__TYPE_ACP10ENCSC_typ
typedef struct ACP10ENCSC_typ
{	ACP10ENCSL_typ load;
} ACP10ENCSC_typ;
#endif

#ifndef __AS__TYPE_ACP10ENCPA_typ
#define __AS__TYPE_ACP10ENCPA_typ
typedef struct ACP10ENCPA_typ
{	unsigned char count_dir;
	unsigned char reserve1;
	unsigned short reserve2;
	ACP10ENCSC_typ scaling;
} ACP10ENCPA_typ;
#endif

#ifndef __AS__TYPE_ACP10ENCIF_typ
#define __AS__TYPE_ACP10ENCIF_typ
typedef struct ACP10ENCIF_typ
{	unsigned char init;
	unsigned char reserve1;
	unsigned short reserve2;
	ACP10ENCPA_typ parameter;
} ACP10ENCIF_typ;
#endif

#ifndef __AS__TYPE_ACP10AXLPA_typ
#define __AS__TYPE_ACP10AXLPA_typ
typedef struct ACP10AXLPA_typ
{	float v_pos;
	float v_neg;
	float a1_pos;
	float a2_pos;
	float a1_neg;
	float a2_neg;
	float t_jolt;
	float t_in_pos;
	signed long pos_sw_end;
	signed long neg_sw_end;
	unsigned char NOT_USE_1[4];
	float ds_warning;
	float ds_stop;
	float a_stop;
	float dv_stop;
	unsigned long dv_stop_mode;
} ACP10AXLPA_typ;
#endif

#ifndef __AS__TYPE_ACP10AXLIM_typ
#define __AS__TYPE_ACP10AXLIM_typ
typedef struct ACP10AXLIM_typ
{	unsigned char init;
	unsigned char reserve1;
	unsigned short reserve2;
	ACP10AXLPA_typ parameter;
} ACP10AXLIM_typ;
#endif

#ifndef __AS__TYPE_ACP10CTRPO_typ
#define __AS__TYPE_ACP10CTRPO_typ
typedef struct ACP10CTRPO_typ
{	float kv;
	float tn;
	float t_predict;
	float t_total;
	float p_max;
	float i_max;
} ACP10CTRPO_typ;
#endif

#ifndef __AS__TYPE_ACP10ISQFI_typ
#define __AS__TYPE_ACP10ISQFI_typ
typedef struct ACP10ISQFI_typ
{	unsigned short type;
	unsigned short reserve;
	float a0;
	float a1;
	float b0;
	float b1;
	float b2;
	unsigned short c0_par_id;
	unsigned short c1_par_id;
} ACP10ISQFI_typ;
#endif

#ifndef __AS__TYPE_ACP10CTRSP_typ
#define __AS__TYPE_ACP10CTRSP_typ
typedef struct ACP10CTRSP_typ
{	float kv;
	float tn;
	float t_filter;
	ACP10ISQFI_typ isq_filter1;
	ACP10ISQFI_typ isq_filter2;
	ACP10ISQFI_typ isq_filter3;
} ACP10CTRSP_typ;
#endif

#ifndef __AS__TYPE_ACP10CTRUF_typ
#define __AS__TYPE_ACP10CTRUF_typ
typedef struct ACP10CTRUF_typ
{	unsigned char type;
	unsigned char auto_config;
	unsigned short reserve;
	float u0;
	float un;
	float fn;
	float k_f_slip;
} ACP10CTRUF_typ;
#endif

#ifndef __AS__TYPE_ACP10CTRFF_typ
#define __AS__TYPE_ACP10CTRFF_typ
typedef struct ACP10CTRFF_typ
{	unsigned short mode;
	unsigned short reserve;
	float torque_load;
	float torque_pos;
	float torque_neg;
	float kv_torque;
	float inertia;
	float t_filter_a;
} ACP10CTRFF_typ;
#endif

#ifndef __AS__TYPE_ACP10CTRFB_typ
#define __AS__TYPE_ACP10CTRFB_typ
typedef struct ACP10CTRFB_typ
{	unsigned short mode;
	unsigned short reserve;
	float speed_mix_ratio;
	float speed_kv;
} ACP10CTRFB_typ;
#endif

#ifndef __AS__TYPE_ACP10CTRMODM1_typ
#define __AS__TYPE_ACP10CTRMODM1_typ
typedef struct ACP10CTRMODM1_typ
{	float inertia;
	float viscous_friction;
} ACP10CTRMODM1_typ;
#endif

#ifndef __AS__TYPE_ACP10CTRMODM2_typ
#define __AS__TYPE_ACP10CTRMODM2_typ
typedef struct ACP10CTRMODM2_typ
{	float inertia;
	float viscous_friction;
	float stiffness;
	float damping;
} ACP10CTRMODM2_typ;
#endif

#ifndef __AS__TYPE_ACP10CTRMODEL_typ
#define __AS__TYPE_ACP10CTRMODEL_typ
typedef struct ACP10CTRMODEL_typ
{	ACP10CTRMODM1_typ mass1;
	ACP10CTRMODM2_typ mass2;
} ACP10CTRMODEL_typ;
#endif

#ifndef __AS__TYPE_ACP10CTRL_typ
#define __AS__TYPE_ACP10CTRL_typ
typedef struct ACP10CTRL_typ
{	unsigned char init;
	unsigned char ready;
	unsigned char status;
	unsigned char mode;
	ACP10CTRPO_typ position;
	ACP10CTRSP_typ speed;
	ACP10CTRUF_typ uf;
	ACP10CTRFF_typ ff;
	ACP10CTRFB_typ fb;
	ACP10CTRMODEL_typ model;
} ACP10CTRL_typ;
#endif

#ifndef __AS__TYPE_ACP10AXSTI_typ
#define __AS__TYPE_ACP10AXSTI_typ
typedef struct ACP10AXSTI_typ
{	unsigned char command;
	unsigned char reserve1;
	unsigned short reserve2;
} ACP10AXSTI_typ;
#endif

#ifndef __AS__TYPE_ACP10AXSTP_typ
#define __AS__TYPE_ACP10AXSTP_typ
typedef struct ACP10AXSTP_typ
{	unsigned char decel_ramp;
	unsigned char controller;
	unsigned short reserve;
} ACP10AXSTP_typ;
#endif

#ifndef __AS__TYPE_ACP10AXSTQ_typ
#define __AS__TYPE_ACP10AXSTQ_typ
typedef struct ACP10AXSTQ_typ
{	unsigned char decel_ramp;
	unsigned char reserve1;
	unsigned short reserve2;
	float t_jolt;
} ACP10AXSTQ_typ;
#endif

#ifndef __AS__TYPE_ACP10AXSTD_typ
#define __AS__TYPE_ACP10AXSTD_typ
typedef struct ACP10AXSTD_typ
{	unsigned char decel_ramp;
	unsigned char reserve1;
	unsigned short reserve2;
} ACP10AXSTD_typ;
#endif

#ifndef __AS__TYPE_ACP10AXSTO_typ
#define __AS__TYPE_ACP10AXSTO_typ
typedef struct ACP10AXSTO_typ
{	unsigned char init;
	unsigned char NOT_USE_1;
	unsigned short reserve2;
	ACP10AXSTI_typ index;
	struct ACP10AXSTP_typ parameter[4];
	ACP10AXSTQ_typ quickstop;
	ACP10AXSTD_typ drive_error;
} ACP10AXSTO_typ;
#endif

#ifndef __AS__TYPE_ACP10HOMST_typ
#define __AS__TYPE_ACP10HOMST_typ
typedef struct ACP10HOMST_typ
{	unsigned char ok;
	unsigned char reserve1;
	unsigned short reserve2;
	float tr_s_rel;
	signed long offset;
} ACP10HOMST_typ;
#endif

#ifndef __AS__TYPE_ACP10HOMPA_typ
#define __AS__TYPE_ACP10HOMPA_typ
typedef struct ACP10HOMPA_typ
{	signed long s;
	float v_switch;
	float v_trigger;
	float a;
	unsigned char mode;
	unsigned char edge_sw;
	unsigned char start_dir;
	unsigned char trigg_dir;
	unsigned char ref_pulse;
	unsigned char fix_dir;
	unsigned char reserve1;
	unsigned char reserve2;
	float tr_s_block;
	float torque_lim;
	float ds_block;
	float ds_stop;
} ACP10HOMPA_typ;
#endif

#ifndef __AS__TYPE_ACP10HOME_typ
#define __AS__TYPE_ACP10HOME_typ
typedef struct ACP10HOME_typ
{	unsigned char init;
	unsigned char reserve1;
	unsigned short reserve2;
	ACP10HOMST_typ status;
	ACP10HOMPA_typ parameter;
} ACP10HOME_typ;
#endif

#ifndef __AS__TYPE_ACP10BMVST_typ
#define __AS__TYPE_ACP10BMVST_typ
typedef struct ACP10BMVST_typ
{	unsigned char in_pos;
	unsigned char reserve1;
	unsigned short reserve2;
} ACP10BMVST_typ;
#endif

#ifndef __AS__TYPE_ACP10BMVOV_typ
#define __AS__TYPE_ACP10BMVOV_typ
typedef struct ACP10BMVOV_typ
{	unsigned short v;
	unsigned short a;
} ACP10BMVOV_typ;
#endif

#ifndef __AS__TYPE_ACP10BMVPA_typ
#define __AS__TYPE_ACP10BMVPA_typ
typedef struct ACP10BMVPA_typ
{	signed long s;
	float v_pos;
	float v_neg;
	float a1_pos;
	float a2_pos;
	float a1_neg;
	float a2_neg;
} ACP10BMVPA_typ;
#endif

#ifndef __AS__TYPE_ACP10TRSTP_typ
#define __AS__TYPE_ACP10TRSTP_typ
typedef struct ACP10TRSTP_typ
{	unsigned char init;
	unsigned char event;
	unsigned short reserve;
	signed long s_rest;
	unsigned char NOT_USE_1[12];
} ACP10TRSTP_typ;
#endif

#ifndef __AS__TYPE_ACP10BAMOV_typ
#define __AS__TYPE_ACP10BAMOV_typ
typedef struct ACP10BAMOV_typ
{	unsigned char init;
	unsigned char reserve1;
	unsigned short reserve2;
	ACP10BMVST_typ status;
	ACP10BMVOV_typ override;
	ACP10BMVPA_typ parameter;
	ACP10TRSTP_typ trg_stop;
} ACP10BAMOV_typ;
#endif

#ifndef __AS__TYPE_ACP10AXMOV_typ
#define __AS__TYPE_ACP10AXMOV_typ
typedef struct ACP10AXMOV_typ
{	unsigned short mode;
	unsigned short detail;
	ACP10AXSTO_typ stop;
	ACP10HOME_typ homing;
	ACP10BAMOV_typ basis;
} ACP10AXMOV_typ;
#endif

#ifndef __AS__TYPE_ACP10SUOST_typ
#define __AS__TYPE_ACP10SUOST_typ
typedef struct ACP10SUOST_typ
{	unsigned long ident;
	unsigned char ok;
	unsigned char error;
	unsigned char reserve1;
	unsigned char reserve2;
} ACP10SUOST_typ;
#endif

#ifndef __AS__TYPE_ACP10SUOPA_typ
#define __AS__TYPE_ACP10SUOPA_typ
typedef struct ACP10SUOPA_typ
{	unsigned char name[12];
} ACP10SUOPA_typ;
#endif

#ifndef __AS__TYPE_ACP10SUOBJ_typ
#define __AS__TYPE_ACP10SUOBJ_typ
typedef struct ACP10SUOBJ_typ
{	ACP10SUOST_typ status;
	ACP10SUOPA_typ parameter;
} ACP10SUOBJ_typ;
#endif

#ifndef __AS__TYPE_ACP10SUMAS_typ
#define __AS__TYPE_ACP10SUMAS_typ
typedef struct ACP10SUMAS_typ
{	unsigned short mode;
	unsigned char ok;
	unsigned char error;
	float quality;
} ACP10SUMAS_typ;
#endif

#ifndef __AS__TYPE_ACP10SUMAO_typ
#define __AS__TYPE_ACP10SUMAO_typ
typedef struct ACP10SUMAO_typ
{	unsigned char z_p;
	unsigned char phase;
	unsigned short reserve2;
	float u_const;
	float v_max;
	float trq_0;
	float trq_n;
	float trq_max;
	float trq_const;
	float i_0;
	float i_max;
	float i_m;
	float phase_cross_sect;
	float invcl_a1;
	float invcl_a2;
} ACP10SUMAO_typ;
#endif

#ifndef __AS__TYPE_ACP10SUMAP_typ
#define __AS__TYPE_ACP10SUMAP_typ
typedef struct ACP10SUMAP_typ
{	unsigned short mode;
	unsigned short reserve;
	float u_n;
	float i_n;
	float v_n;
	float f_n;
	float cos_phi;
	float t_tripping_therm;
	ACP10SUMAO_typ optional;
} ACP10SUMAP_typ;
#endif

#ifndef __AS__TYPE_ACP10SUMA_typ
#define __AS__TYPE_ACP10SUMA_typ
typedef struct ACP10SUMA_typ
{	ACP10SUMAS_typ status;
	ACP10SUMAP_typ parameter;
} ACP10SUMA_typ;
#endif

#ifndef __AS__TYPE_ACP10SUMSS_typ
#define __AS__TYPE_ACP10SUMSS_typ
typedef struct ACP10SUMSS_typ
{	unsigned short mode;
	unsigned char ok;
	unsigned char error;
	float quality;
} ACP10SUMSS_typ;
#endif

#ifndef __AS__TYPE_ACP10SUMSO_typ
#define __AS__TYPE_ACP10SUMSO_typ
typedef struct ACP10SUMSO_typ
{	unsigned char phase;
	unsigned char reserve1;
	unsigned short reserve2;
	float u_const;
	float v_max;
	float trq_0;
	float trq_const;
	float i_0;
	float phase_cross_sect;
	float invcl_a1;
	float invcl_a2;
} ACP10SUMSO_typ;
#endif

#ifndef __AS__TYPE_ACP10SUMSP_typ
#define __AS__TYPE_ACP10SUMSP_typ
typedef struct ACP10SUMSP_typ
{	unsigned short mode;
	unsigned char z_p;
	unsigned char reserve;
	float u_n;
	float i_n;
	float v_n;
	float trq_n;
	float trq_max;
	float i_max;
	float t_tripping_therm;
	ACP10SUMSO_typ optional;
} ACP10SUMSP_typ;
#endif

#ifndef __AS__TYPE_ACP10SUMS_typ
#define __AS__TYPE_ACP10SUMS_typ
typedef struct ACP10SUMS_typ
{	ACP10SUMSS_typ status;
	ACP10SUMSP_typ parameter;
} ACP10SUMS_typ;
#endif

#ifndef __AS__TYPE_ACP10SUPHS_typ
#define __AS__TYPE_ACP10SUPHS_typ
typedef struct ACP10SUPHS_typ
{	unsigned short mode;
	unsigned char ok;
	unsigned char error;
	float rho_0;
	unsigned char z_p;
	unsigned char reserve1;
	unsigned short reserve2;
} ACP10SUPHS_typ;
#endif

#ifndef __AS__TYPE_ACP10SUPHP_typ
#define __AS__TYPE_ACP10SUPHP_typ
typedef struct ACP10SUPHP_typ
{	unsigned short mode;
	unsigned short reserve;
	float i;
	float t;
} ACP10SUPHP_typ;
#endif

#ifndef __AS__TYPE_ACP10SUPH_typ
#define __AS__TYPE_ACP10SUPH_typ
typedef struct ACP10SUPH_typ
{	ACP10SUPHS_typ status;
	ACP10SUPHP_typ parameter;
} ACP10SUPH_typ;
#endif

#ifndef __AS__TYPE_ACP10SUCST_typ
#define __AS__TYPE_ACP10SUCST_typ
typedef struct ACP10SUCST_typ
{	unsigned short mode;
	unsigned char ok;
	unsigned char error;
} ACP10SUCST_typ;
#endif

#ifndef __AS__TYPE_ACP10SUCPA_typ
#define __AS__TYPE_ACP10SUCPA_typ
typedef struct ACP10SUCPA_typ
{	unsigned short mode;
	unsigned char orientation;
	unsigned char operating_point;
	float i_max_percent;
	float v_max_percent;
	signed long s_max;
	float ds_max;
	float kv_percent;
	unsigned long signal_order;
	float kv_max;
	float a;
	unsigned short signal_type;
	unsigned short reserve;
	float signal_f_start;
	float signal_f_stop;
	float signal_time;
} ACP10SUCPA_typ;
#endif

#ifndef __AS__TYPE_ACP10SUCTR_typ
#define __AS__TYPE_ACP10SUCTR_typ
typedef struct ACP10SUCTR_typ
{	ACP10SUCST_typ status;
	ACP10SUCPA_typ parameter;
} ACP10SUCTR_typ;
#endif

#ifndef __AS__TYPE_ACP10SUIRS_typ
#define __AS__TYPE_ACP10SUIRS_typ
typedef struct ACP10SUIRS_typ
{	unsigned short mode;
	unsigned char ok;
	unsigned char error;
	float quality;
} ACP10SUIRS_typ;
#endif

#ifndef __AS__TYPE_ACP10SUIRP_typ
#define __AS__TYPE_ACP10SUIRP_typ
typedef struct ACP10SUIRP_typ
{	unsigned short mode;
	unsigned char reserve;
	unsigned char ref_system;
	float pos_offset;
	float v;
} ACP10SUIRP_typ;
#endif

#ifndef __AS__TYPE_ACP10SUIR_typ
#define __AS__TYPE_ACP10SUIR_typ
typedef struct ACP10SUIR_typ
{	ACP10SUIRS_typ status;
	ACP10SUIRP_typ parameter;
} ACP10SUIR_typ;
#endif

#ifndef __AS__TYPE_ACP10SUMOVST_typ
#define __AS__TYPE_ACP10SUMOVST_typ
typedef struct ACP10SUMOVST_typ
{	unsigned short mode;
	unsigned char ok;
	unsigned char error;
} ACP10SUMOVST_typ;
#endif

#ifndef __AS__TYPE_ACP10SUMOPA_typ
#define __AS__TYPE_ACP10SUMOPA_typ
typedef struct ACP10SUMOPA_typ
{	unsigned short mode;
	unsigned char start_dir;
	unsigned char fix_dir;
	signed long s_max;
} ACP10SUMOPA_typ;
#endif

#ifndef __AS__TYPE_ACP10SUMOV_typ
#define __AS__TYPE_ACP10SUMOV_typ
typedef struct ACP10SUMOV_typ
{	ACP10SUMOVST_typ status;
	ACP10SUMOPA_typ parameter;
} ACP10SUMOV_typ;
#endif

#ifndef __AS__TYPE_ACP10SETUP_typ
#define __AS__TYPE_ACP10SETUP_typ
typedef struct ACP10SETUP_typ
{	unsigned char status;
	unsigned char active;
	unsigned short detail;
	ACP10SUOBJ_typ datobj;
	ACP10SUMA_typ motor_induction;
	ACP10SUMS_typ motor_synchron;
	ACP10SUPH_typ motor_phasing;
	ACP10SUCTR_typ controller;
	ACP10SUIR_typ isq_ripple;
	ACP10SUMOV_typ move;
} ACP10SETUP_typ;
#endif

#ifndef __AS__TYPE_ACP10AXMOS_typ
#define __AS__TYPE_ACP10AXMOS_typ
typedef struct ACP10AXMOS_typ
{	unsigned char error;
	unsigned char warning;
	unsigned char ds_warning;
	unsigned char reserve;
} ACP10AXMOS_typ;
#endif

#ifndef __AS__TYPE_ACP10AXMON_typ
#define __AS__TYPE_ACP10AXMON_typ
typedef struct ACP10AXMON_typ
{	signed long s;
	float v;
	ACP10AXMOS_typ status;
} ACP10AXMON_typ;
#endif

#ifndef __AS__TYPE_ACP10MSCNT_typ
#define __AS__TYPE_ACP10MSCNT_typ
typedef struct ACP10MSCNT_typ
{	unsigned char error;
	unsigned char warning;
	unsigned char mc_fb_error;
	unsigned char reserve;
} ACP10MSCNT_typ;
#endif

#ifndef __AS__TYPE_ACP10MSREC_typ
#define __AS__TYPE_ACP10MSREC_typ
typedef struct ACP10MSREC_typ
{	unsigned short par_id;
	unsigned short number;
	unsigned long info;
} ACP10MSREC_typ;
#endif

#ifndef __AS__TYPE_ACP10MTXST_typ
#define __AS__TYPE_ACP10MTXST_typ
typedef struct ACP10MTXST_typ
{	unsigned short lines;
	unsigned short error;
} ACP10MTXST_typ;
#endif

#ifndef __AS__TYPE_ACP10MTXPA_typ
#define __AS__TYPE_ACP10MTXPA_typ
typedef struct ACP10MTXPA_typ
{	unsigned short format;
	unsigned short columns;
	unsigned char data_modul[12];
	unsigned short data_len;
	unsigned short reserve;
	unsigned long data_adr;
	unsigned long record_adr;
} ACP10MTXPA_typ;
#endif

#ifndef __AS__TYPE_ACP10MSTXT_typ
#define __AS__TYPE_ACP10MSTXT_typ
typedef struct ACP10MSTXT_typ
{	ACP10MTXST_typ status;
	ACP10MTXPA_typ parameter;
} ACP10MSTXT_typ;
#endif

#ifndef __AS__TYPE_ACP10MSCMDERR_typ
#define __AS__TYPE_ACP10MSCMDERR_typ
typedef struct ACP10MSCMDERR_typ
{	unsigned short type;
	unsigned char ok;
	unsigned char error;
} ACP10MSCMDERR_typ;
#endif

#ifndef __AS__TYPE_ACP10MSG_typ
#define __AS__TYPE_ACP10MSG_typ
typedef struct ACP10MSG_typ
{	ACP10MSCNT_typ count;
	ACP10MSREC_typ record;
	ACP10MSTXT_typ text;
	ACP10MSCMDERR_typ cmd_error;
} ACP10MSG_typ;
#endif

#ifndef __AS__TYPE_ACP10NCTST_typ
#define __AS__TYPE_ACP10NCTST_typ
typedef struct ACP10NCTST_typ
{	unsigned char Open_UseApplNcObj;
	unsigned char Close_NoMoveAbort;
	unsigned char reserve1;
	unsigned char reserve2;
} ACP10NCTST_typ;
#endif

#ifndef __AS__TYPE_ACP10AXIS_typ
#define __AS__TYPE_ACP10AXIS_typ
typedef struct ACP10AXIS_typ
{	unsigned char NOT_USE_1[4];
	unsigned short size;
	unsigned char NOT_USE_2[2];
	ACP10SWVER_typ sw_version;
	ACP10OBINF_typ nc_obj_inf;
	ACP10SIM_typ simulation;
	ACP10GLINI_typ global;
	ACP10NET_typ network;
	ACP10AXDBL_typ datblock;
	ACP10DIGIN_typ dig_in;
	ACP10ENCIF_typ encoder_if;
	ACP10AXLIM_typ limit;
	ACP10CTRL_typ controller;
	ACP10AXMOV_typ move;
	ACP10SETUP_typ setup;
	ACP10AXMON_typ monitor;
	ACP10MSG_typ message;
	ACP10NCTST_typ nc_test;
	unsigned char NOT_USE_3[60];
} ACP10AXIS_typ;
#endif

#ifndef __AS__TYPE_MC_0108_IS_TYP
#define __AS__TYPE_MC_0108_IS_TYP
typedef struct MC_0108_IS_TYP
{	plcbit Enable;
	plcbit Active;
	plcbit Error;
	plcbit Busy;
	unsigned short ErrorID;
	plcbit InputsSet;
	unsigned char cmdDigInForce;
	unsigned char state;
	unsigned char LockIDPar;
	plcbit oldHomeSwitch;
	plcbit oldPosHWSwitch;
	plcbit oldNegHWSwitch;
	plcbit oldTrigger1;
	plcbit oldTrigger2;
	unsigned char Reserve1;
} MC_0108_IS_TYP;
#endif

#ifndef __AS__TYPE_MC_0120_IS_TYP
#define __AS__TYPE_MC_0120_IS_TYP
typedef struct MC_0120_IS_TYP
{	float Velocity;
	float Acceleration;
	float Deceleration;
	signed long TargetPosition;
	plcbit Enable;
	plcbit JogToTarget;
	plcbit JogPositive;
	plcbit JogNegative;
	plcbit Active;
	plcbit Busy;
	plcbit CommandAborted;
	plcbit Error;
	unsigned short ErrorID;
	plcbit Jogging;
	plcbit MovingToTarget;
	plcbit TargetReached;
	unsigned char state;
	unsigned char NextState;
	unsigned char Direction;
	unsigned char MoveID;
	unsigned char LockIDPar;
	plcbit moveActive;
	unsigned char Reserve;
	float C_Velocity;
	float C_Acceleration;
	float C_Deceleration;
	signed long startOffset;
	signed long CommandedOffset;
	signed long targetPosition;
	unsigned short C_ErrorID;
	unsigned short Reserve1;
} MC_0120_IS_TYP;
#endif

#ifndef __AS__TYPE_MpAxisMoveDirectionEnum
#define __AS__TYPE_MpAxisMoveDirectionEnum
typedef enum MpAxisMoveDirectionEnum
{	mpAXIS_DIR_POSITIVE = 0,
	mpAXIS_DIR_NEGATIVE = 1,
	mpAXIS_DIR_CURRENT = 2,
	mpAXIS_DIR_SHORTEST_WAY = 3,
	mpAXIS_DIR_EXCEED_PERIOD = 8,
} MpAxisMoveDirectionEnum;
#endif

#ifndef __AS__TYPE_MpAxisHomeModeEnum
#define __AS__TYPE_MpAxisHomeModeEnum
typedef enum MpAxisHomeModeEnum
{	mpAXIS_HOME_MODE_DEFAULT = 0,
	mpAXIS_HOME_MODE_ABS_SWITCH = 2,
	mpAXIS_HOME_MODE_SWITCH_GATE = 8,
	mpAXIS_HOME_MODE_LIMIT_SWITCH = 3,
	mpAXIS_HOME_MODE_REF_PULSE = 5,
	mpAXIS_HOME_MODE_DIRECT = 1,
	mpAXIS_HOME_MODE_ABSOLUTE = 4,
	mpAXIS_HOME_MODE_ABSOLUTE_CORR = 6,
	mpAXIS_HOME_MODE_DCM = 9,
	mpAXIS_HOME_MODE_DCM_CORR = 10,
	mpAXIS_HOME_MODE_RESTORE_POS = 11,
	mpAXIS_HOME_MODE_AXIS_REF = 12,
	mpAXIS_HOME_MODE_BLOCK_TORQUE = 13,
	mpAXIS_HOME_MODE_BLOCK_DS = 14,
} MpAxisHomeModeEnum;
#endif

#ifndef __AS__TYPE_MpAxisHomeMoveDirectionEnum
#define __AS__TYPE_MpAxisHomeMoveDirectionEnum
typedef enum MpAxisHomeMoveDirectionEnum
{	mpAXIS_HOME_DIR_POSITIVE = 0,
	mpAXIS_HOME_DIR_NEGATIVE = 1,
} MpAxisHomeMoveDirectionEnum;
#endif

#ifndef __AS__TYPE_MpAxisHomeOptionEnum
#define __AS__TYPE_MpAxisHomeOptionEnum
typedef enum MpAxisHomeOptionEnum
{	mpAXIS_HOME_OPTION_OFF = 0,
	mpAXIS_HOME_OPTION_ON = 1,
} MpAxisHomeOptionEnum;
#endif

#ifndef __AS__TYPE_MpAxisHomingType
#define __AS__TYPE_MpAxisHomingType
typedef struct MpAxisHomingType
{	MpAxisHomeModeEnum Mode;
	double Position;
	float StartVelocity;
	float HomingVelocity;
	double SensorOffset;
	float Acceleration;
	MpAxisHomeMoveDirectionEnum StartDirection;
	MpAxisHomeMoveDirectionEnum HomingDirection;
	MpAxisHomeOptionEnum NoDirectionChange;
	MpAxisHomeMoveDirectionEnum SwitchEdge;
	MpAxisHomeOptionEnum ReferencePulse;
	double ReferencePulseBlockingDistance;
	float TorqueLimit;
	double BlockDetectionPositionError;
	double PositionErrorStopLimit;
	unsigned long EndlessPositionDataRef;
} MpAxisHomingType;
#endif

#ifndef __AS__TYPE_MpAxisJogType
#define __AS__TYPE_MpAxisJogType
typedef struct MpAxisJogType
{	float Acceleration;
	float Deceleration;
	float Velocity;
	double LowerLimit;
	double UpperLimit;
} MpAxisJogType;
#endif

#ifndef __AS__TYPE_MpAxisStopType
#define __AS__TYPE_MpAxisStopType
typedef struct MpAxisStopType
{	float Deceleration;
	plcbit StopInPhase;
	double Phase;
} MpAxisStopType;
#endif

#ifndef __AS__TYPE_MpAxisTriggerSourceEnum
#define __AS__TYPE_MpAxisTriggerSourceEnum
typedef enum MpAxisTriggerSourceEnum
{	mpAXIS_TRIGGER1 = 20,
	mpAXIS_TRIGGER2 = 22,
} MpAxisTriggerSourceEnum;
#endif

#ifndef __AS__TYPE_MpAxisTriggerEdgeEnum
#define __AS__TYPE_MpAxisTriggerEdgeEnum
typedef enum MpAxisTriggerEdgeEnum
{	mpAXIS_TRIGGER_EDGE_POS = 0,
	mpAXIS_TRIGGER_EDGE_NEG = 1,
} MpAxisTriggerEdgeEnum;
#endif

#ifndef __AS__TYPE_MpAxisStopAfterTriggerType
#define __AS__TYPE_MpAxisStopAfterTriggerType
typedef struct MpAxisStopAfterTriggerType
{	plcbit Enable;
	double TriggerDistance;
	plcbit ForceTriggerDistance;
	MpAxisTriggerSourceEnum Source;
	MpAxisTriggerEdgeEnum Edge;
} MpAxisStopAfterTriggerType;
#endif

#ifndef __AS__TYPE_MpAxisTorqueLimitType
#define __AS__TYPE_MpAxisTorqueLimitType
typedef struct MpAxisTorqueLimitType
{	float Limit;
	float Window;
} MpAxisTorqueLimitType;
#endif

#ifndef __AS__TYPE_MpAxisReadInfoModeEnum
#define __AS__TYPE_MpAxisReadInfoModeEnum
typedef enum MpAxisReadInfoModeEnum
{	mpAXIS_READ_OFF = 0,
	mpAXIS_READ_CYCLIC = 1,
	mpAXIS_READ_MULTIPLEXED = 2,
	mpAXIS_READ_POLLING_1s = 3,
	mpAXIS_READ_POLLING_5s = 4,
} MpAxisReadInfoModeEnum;
#endif

#ifndef __AS__TYPE_MpAxisCyclicReadSetupType
#define __AS__TYPE_MpAxisCyclicReadSetupType
typedef struct MpAxisCyclicReadSetupType
{	MpAxisReadInfoModeEnum TorqueMode;
	MpAxisReadInfoModeEnum LagErrorMode;
	MpAxisReadInfoModeEnum MotorTempMode;
	MpAxisReadInfoModeEnum UserChannelMode;
} MpAxisCyclicReadSetupType;
#endif

#ifndef __AS__TYPE_MpAxisAutotuneModeEnum
#define __AS__TYPE_MpAxisAutotuneModeEnum
typedef enum MpAxisAutotuneModeEnum
{	mpAXIS_TUNE_AUTOMATIC = 0,
	mpAXIS_TUNE_SPEED = 2,
	mpAXIS_TUNE_POSITION = 1,
	mpAXIS_TUNE_TEST = 31,
	mpAXIS_TUNE_SPEED_ISQ_F1 = 130,
	mpAXIS_TUNE_SPEED_T_FLTR = 66,
	mpAXIS_TUNE_SPEED_T_FLTR_ISQ_F1 = 194,
	mpAXIS_TUNE_SPEED_FLTR = 6,
	mpAXIS_TUNE_ISQ_F1 = 128,
	mpAXIS_TUNE_ISQ_F1_F2 = 384,
	mpAXIS_TUNE_ISQ_F1_F2_F3 = 896,
	mpAXIS_TUNE_FF = 32,
	mpAXIS_TUNE_FF_ONLY_POS = 33,
	mpAXIS_TUNE_FF_ONLY_NEG = 34,
} MpAxisAutotuneModeEnum;
#endif

#ifndef __AS__TYPE_MpAxisFeedForwardModeEnum
#define __AS__TYPE_MpAxisFeedForwardModeEnum
typedef enum MpAxisFeedForwardModeEnum
{	mpAXIS_FF_DISABLED = 0,
	mpAXIS_FF_BOTH = 1,
	mpAXIS_FF_ONLY_POS = 2,
	mpAXIS_FF_ONLY_NEG = 3,
} MpAxisFeedForwardModeEnum;
#endif

#ifndef __AS__TYPE_MpAxisAutotuneType
#define __AS__TYPE_MpAxisAutotuneType
typedef struct MpAxisAutotuneType
{	MpAxisAutotuneModeEnum Mode;
	MpAxisFeedForwardModeEnum FeedForward;
	plcbit Vertical;
	float MaxCurrentPercent;
	float MaxSpeedPercent;
	double MaxDistance;
	float ProportionalAmplification;
} MpAxisAutotuneType;
#endif

#ifndef __AS__TYPE_MpAxisBasicParType
#define __AS__TYPE_MpAxisBasicParType
typedef struct MpAxisBasicParType
{	float Acceleration;
	float Deceleration;
	float Velocity;
	double Position;
	double Distance;
	MpAxisMoveDirectionEnum Direction;
	MpAxisHomingType Home;
	MpAxisJogType Jog;
	MpAxisStopType Stop;
	MpAxisStopAfterTriggerType StopAfterTrigger;
	MpAxisTorqueLimitType Torque;
	MpAxisCyclicReadSetupType CyclicRead;
	MpAxisAutotuneType Autotune;
} MpAxisBasicParType;
#endif

#ifndef __AS__TYPE_MpAxisCyclicReadValueType
#define __AS__TYPE_MpAxisCyclicReadValueType
typedef struct MpAxisCyclicReadValueType
{	plcbit Valid;
	double Value;
} MpAxisCyclicReadValueType;
#endif

#ifndef __AS__TYPE_MpAxisCyclicReadType
#define __AS__TYPE_MpAxisCyclicReadType
typedef struct MpAxisCyclicReadType
{	MpAxisCyclicReadValueType Torque;
	MpAxisCyclicReadValueType LagError;
	MpAxisCyclicReadValueType MotorTemperature;
	MpAxisCyclicReadValueType UserChannelParameterID;
} MpAxisCyclicReadType;
#endif

#ifndef __AS__TYPE_MpAxisBootPhaseEnum
#define __AS__TYPE_MpAxisBootPhaseEnum
typedef enum MpAxisBootPhaseEnum
{	mpAXIS_BLP_NETWORK_INACTIVE = 0,
	mpAXIS_BLP_NETWORK_INIT_STARTED = 1,
	mpAXIS_BLP_WAIT_INIT_HIGH_PRIO = 5,
	mpAXIS_BLP_HW_WAIT = 9,
	mpAXIS_BLP_HW_LINKED = 10,
	mpAXIS_BLP_HW_START = 20,
	mpAXIS_BLP_HW_UPDATE = 30,
	mpAXIS_BLP_HW_UPDATE_OTHER_DRV = 31,
	mpAXIS_BLP_FW_UPDATE = 40,
	mpAXIS_BLP_FW_UPDATE_OTHER_DRV = 41,
	mpAXIS_BLP_FW_START = 50,
	mpAXIS_BLP_WAIT_INIT_LOW_PRIO = 55,
	mpAXIS_BLP_DOWNLOAD_DEF_PARAMS = 60,
	mpAXIS_BLP_DOWNLOAD_INI_PARAMS = 70,
	mpAXIS_BLP_HW_INFO_FROM_DRIVE = 80,
	mpAXIS_BLP_DONE = 90,
} MpAxisBootPhaseEnum;
#endif

#ifndef __AS__TYPE_MpAxisPlcOpenStateEnum
#define __AS__TYPE_MpAxisPlcOpenStateEnum
typedef enum MpAxisPlcOpenStateEnum
{	mpAXIS_DISABLED = 0,
	mpAXIS_STANDSTILL = 1,
	mpAXIS_ERRORSTOP = 10,
	mpAXIS_STOPPING = 9,
	mpAXIS_DISCRETE_MOTION = 2,
	mpAXIS_CONTINUOUS_MOTION = 3,
	mpAXIS_SYNCHRONIZED_MOTION = 4,
	mpAXIS_HOMING = 5,
} MpAxisPlcOpenStateEnum;
#endif

#ifndef __AS__TYPE_MpAxisDigitalIOStatusType
#define __AS__TYPE_MpAxisDigitalIOStatusType
typedef struct MpAxisDigitalIOStatusType
{	plcbit DriveEnable;
	plcbit HomeSwitch;
	plcbit PositiveLimitSwitch;
	plcbit NegativeLimitSwitch;
	plcbit Trigger1;
	plcbit Trigger2;
} MpAxisDigitalIOStatusType;
#endif

#ifndef __AS__TYPE_MpAxisNetworkTypeEnum
#define __AS__TYPE_MpAxisNetworkTypeEnum
typedef enum MpAxisNetworkTypeEnum
{	mpAXIS_CAN_NETWORK = 0,
	mpAXIS_PLK_NETWORK = 1,
	mpAXIS_SDC_NETWORK = 129,
} MpAxisNetworkTypeEnum;
#endif

#ifndef __AS__TYPE_MpAxisDeviceTypeEnum
#define __AS__TYPE_MpAxisDeviceTypeEnum
typedef enum MpAxisDeviceTypeEnum
{	mpAXIS_DEVICE_UNKNOWN = 0,
	mpAXIS_ACOPOS = 1,
	mpAXIS_VIRTUAL = 3,
	mpAXIS_ACOPOSmulti65m = 4,
	mpAXIS_ACOPOSmulti = 5,
	mpAXIS_ACOPOSmulti_PPS = 6,
	mpAXIS_ACOPOSmulti_PS = 2,
	mpAXIS_ACOPOSmicro = 7,
	mpAXIS_ACOPOSmulti65 = 8,
	mpAXIS_ACOPOS_P3 = 12,
	mpAXIS_ACOPOS_SDC = 128,
	mpAXIS_ACOPOS_SIM = 129,
} MpAxisDeviceTypeEnum;
#endif

#ifndef __AS__TYPE_MpAxisAddInfoHardwareType
#define __AS__TYPE_MpAxisAddInfoHardwareType
typedef struct MpAxisAddInfoHardwareType
{	unsigned short NodeID;
	unsigned char Channel;
	MpAxisNetworkTypeEnum NetworkType;
	MpAxisDeviceTypeEnum DeviceType;
} MpAxisAddInfoHardwareType;
#endif

#ifndef __AS__TYPE_MpAxisErrorEnum
#define __AS__TYPE_MpAxisErrorEnum
typedef enum MpAxisErrorEnum
{	mpAXIS_NO_ERROR = 0,
	mpAXIS_ERR_ACTIVATION = -1064239103,
	mpAXIS_ERR_MPLINK_NULL = -1064239102,
	mpAXIS_ERR_MPLINK_INVALID = -1064239101,
	mpAXIS_ERR_MPLINK_CHANGED = -1064239100,
	mpAXIS_ERR_MPLINK_CORRUPT = -1064239099,
	mpAXIS_ERR_MPLINK_IN_USE = -1064239098,
	mpAXIS_ERR_PAR_NULL = -1064239097,
	mpAXIS_ERR_CONFIG_NULL = -1064239096,
	mpAXIS_ERR_CONFIG_NO_PV = -1064239095,
	mpAXIS_ERR_CONFIG_LOAD = -1064239094,
	mpAXIS_WRN_CONFIG_LOAD = -2137980917,
	mpAXIS_ERR_CONFIG_SAVE = -1064239092,
	mpAXIS_ERR_CONFIG_INVALID = -1064239091,
	mpAXIS_ERR_AXIS_HANDLE_NULL = -1064074752,
	mpAXIS_WRN_ERROR_TABLE_MISSING = -2137816575,
	mpAXIS_WRN_CFG_WAIT_ERROR_RESET = -2137816574,
	mpAXIS_WRN_CFG_WAIT_POWER_OFF = -2137816573,
	mpAXIS_WRN_CFG_WAIT_STANDSTILL = -2137816572,
	mpAXIS_ERR_PLC_OPEN = -1064074747,
	mpAXIS_WRN_PLC_OPEN = -2137816570,
	mpAXIS_WRN_READ_TORQUE_OFF = -2137816569,
	mpAXIS_ERR_MAX_TORQUE_REACHED = -1064074744,
	mpAXIS_ERR_SLAVE_NOT_FOUND = -1064074732,
	mpAXIS_ERR_MASTER_NOT_FOUND = -1064074731,
	mpAXIS_ERR_WRONG_DENOMINATOR = -1064074730,
	mpAXIS_ERR_WRONG_NUMERATOR = -1064074729,
	mpAXIS_ERR_NO_CAM_NAME = -1064074728,
	mpAXIS_WRN_SLAVE_NOT_READY = -2137816551,
	mpAXIS_ERR_CHECK_SLAVE_STATUS = -1064074726,
	mpAXIS_ERR_CMD_WRONG_AXISTYPE = -1064074725,
	mpAXIS_WRN_PARAMETER_LIMITED = -2137816548,
	mpAXIS_WRN_MULTIPLE_COMMAND = -2137816547,
	mpAXIS_ERR_CAM_PARAMETER = -1064074722,
	mpAXIS_ERR_RECOVERY_NOT_ALLOWED = -1064074721,
} MpAxisErrorEnum;
#endif

#ifndef __AS__TYPE_MpAxisStatusIDType
#define __AS__TYPE_MpAxisStatusIDType
typedef struct MpAxisStatusIDType
{	MpAxisErrorEnum ID;
	MpComSeveritiesEnum Severity;
	unsigned short Code;
} MpAxisStatusIDType;
#endif

#ifndef __AS__TYPE_MpAxisInternalType
#define __AS__TYPE_MpAxisInternalType
typedef struct MpAxisInternalType
{	signed long ID;
	MpComSeveritiesEnum Severity;
	MpComFacilitiesEnum Facility;
	unsigned short Code;
} MpAxisInternalType;
#endif

#ifndef __AS__TYPE_MpAxisExecutingCmdEnum
#define __AS__TYPE_MpAxisExecutingCmdEnum
typedef enum MpAxisExecutingCmdEnum
{	mpAXIS_CMD_IDLE = 0,
	mpAXIS_CMD_INIT = 1,
	mpAXIS_CMD_HOMING = 2,
	mpAXIS_CMD_STOP = 3,
	mpAXIS_CMD_HALT = 4,
	mpAXIS_CMD_MOVE_VELOCITY = 5,
	mpAXIS_CMD_MOVE_ABSOLUTE = 6,
	mpAXIS_CMD_GEAR_IN = 7,
	mpAXIS_CMD_GEAR_OUT = 8,
	mpAXIS_CMD_CAM_IN = 9,
	mpAXIS_CMD_CAM_OUT = 10,
	mpAXIS_CMD_DOWNLOAD_CAMS = 11,
	mpAXIS_CMD_MOVE_ADDITIVE = 12,
	mpAXIS_CMD_JOG_POSITIVE = 13,
	mpAXIS_CMD_JOG_NEGATIVE = 14,
	mpAXIS_CMD_STOP_PHASED = 15,
	mpAXIS_CMD_AUTOTUNE = 16,
	mpAXIS_CMD_REMOTE_CONTROL = 17,
	mpAXIS_CMD_MOVE_VEL_TRG_STOP = 18,
	mpAXIS_CMD_MOVE_ABS_TRG_STOP = 19,
	mpAXIS_CMD_MOVE_ADD_TRG_STOP = 20,
	mpAXIS_CMD_CAM_SEQUENCER = 50,
	mpAXIS_COUPLING_IDLE = 100,
	mpAXIS_CMD_PHASING = 101,
	mpAXIS_CMD_OFFSET = 102,
	mpAXIS_CMD_ABORT = 103,
	mpAXIS_CMD_UPDATE_GEAR = 104,
	mpAXIS_CMD_UPDATE_CAM = 105,
	mpAXIS_CMD_RECOVERY = 106,
	mpAXIS_CYCLIC_REF_IDLE = 200,
	mpAXIS_CMD_CYC_POSITION = 201,
	mpAXIS_CMD_CYC_VELOCITY = 202,
	mpAXIS_CMD_CYC_TORQUE = 203,
	mpAXIS_CMD_CYC_POSITION_UPDATE = 204,
	mpAXIS_CMD_CYC_VELOCITY_UPDATE = 205,
	mpAXIS_CMD_CYC_TORQUE_UPDATE = 206,
} MpAxisExecutingCmdEnum;
#endif

#ifndef __AS__TYPE_MpAxisDiagExtType
#define __AS__TYPE_MpAxisDiagExtType
typedef struct MpAxisDiagExtType
{	MpAxisStatusIDType StatusID;
	MpAxisInternalType Internal;
	MpAxisExecutingCmdEnum ExecutingCommand;
} MpAxisDiagExtType;
#endif

#ifndef __AS__TYPE_MpAxisBasicInfoType
#define __AS__TYPE_MpAxisBasicInfoType
typedef struct MpAxisBasicInfoType
{	plcbit AxisInitialized;
	plcbit ReadyToPowerOn;
	plcbit JogLimited;
	plcbit TorqueLimitActive;
	plcbit DriveRestarted;
	MpAxisCyclicReadType CyclicRead;
	MpAxisBootPhaseEnum BootState;
	MpAxisPlcOpenStateEnum PLCopenState;
	MpAxisDigitalIOStatusType DigitalInputsStatus;
	MpAxisAddInfoHardwareType HardwareInfo;
	MpAxisDiagExtType Diag;
	plcbit MoveDone;
} MpAxisBasicInfoType;
#endif

#ifndef __AS__TYPE_gtLinearRobotInput
#define __AS__TYPE_gtLinearRobotInput
typedef struct gtLinearRobotInput
{	plcbit bWaferSize200mm;
	plcbit bWaferSize300mm;
	plcbit bBatchCount1;
	plcbit bBatchCount2;
	signed char siBatch1FPort;
	signed char siBatch2FPort;
	plcbit bGC1;
	plcbit bGC2;
	plcbit bLoadGC;
	plcbit bUnloadGC;
	plcbit bBatch1GCTopLevel;
	plcbit bBatch1GCBottomLevel;
	plcbit bBatch2GCTopLevel;
	plcbit bBatch2GCBottomLevel;
	plcbit bEmergencyShutoff;
	plcbit bRUN;
	unsigned char siInputCount;
	plcbit bStaubliSafePos;
	plcbit bStaubliReady;
	plcbit obStaubliSafePosRE;
	unsigned char siFOUPPortNumberB1;
	unsigned char siFOUPPortNumberB2;
	plcbit bUnload300Batch1;
	plcbit bLoad300Batch1;
	plcbit bUnload200Batch2TB;
	plcbit bUnload200Batch2BT;
	plcbit bLoad200Batch2TB;
	plcbit bLoad200Batch2BT;
	plcbit bUnload200Batch1;
	plcbit bUnloadWaferType;
	plcbit bLoadWaferType;
	plcbit bLoad200Batch1;
	unsigned char siSelectJogAxis;
	unsigned char siSetAxisX1JogType;
	unsigned char siSelectAxisX1Loc;
	unsigned char siSelectAxisTargetLevel;
	plcbit bJogNegAxisX1PB;
	plcbit bJogNegAxisX1PBRE;
	plcbit bJogPosAxisX1PB;
	plcbit bJogPosAxisX1PBRE;
	plcbit bHomeAllAxisPB;
	plcbit bHomeAllAxisPBRE;
	plcbit bRotateAxisRPB;
	plcbit bRotateAxisRPBRE;
	unsigned short uiRotateAxisPos;
	plcbit bSetTargetPosPB;
	plcbit bSetTargetPosPBRE;
	plcbit bAutoCycleMode;
	plcbit bStartAutoCyclePB;
	plcbit bStartAutoCyclePBRE;
	plcbit bAxisErrorResetPB;
	plcbit bAxisErrorResetPBRE;
	unsigned short uiAutoCycleCnts;
	plcbit bReadStaubliRunCMDAck;
	plcbit bReadStaubliTargetPosAck;
	plcbit bReadStaubliRobotOnceAck;
	plcbit bWriteStaubliHeartBeatAck;
	plcbit bReadStaubliHeartbeatReturn;
	plcbit bStaubliArmPowerON;
	plcbit bStaubliPrgRunning;
	unsigned short uiStaubliErrorNumber;
	plcbit bStaubliErrorResetCMDAck;
	plcbit bStaubliArmPowerOnCMDAck;
	plcbit bStaubliArmPowerOffCMDAck;
} gtLinearRobotInput;
#endif

#ifndef __AS__TYPE_gtLinearRobotOutpt
#define __AS__TYPE_gtLinearRobotOutpt
typedef struct gtLinearRobotOutpt
{	plcbit bComplete;
	plcbit bGuardianCarrierPresent;
	plcstring gstrError[81];
	unsigned short usiSRMoveStartPosition;
	unsigned short usiSRMoveTargetPosition;
	unsigned short uiWriteStaubliRobot;
	plcbit bWriteStaubliRobotOnce;
	unsigned short uiWriteStaubliTargetPosition;
	plcbit bWriteStaubliTargetOnce;
	plcbit bStaubliRunCMD;
	plcbit bStaubliRunCMDOnce;
	plcbit bWriteStaubliHeartBeat;
	plcbit bWriteStaubliHeartBeatOnce;
	plcbit bStaubliErrorResetCMD;
	plcbit bStaubliErrorResetCMDOnce;
	plcbit bStaubliArmPowerOnCMD;
	plcbit bStaubliArmPowerOnCMDOnce;
	plcbit bStaubliArmPowerOffCMD;
	plcbit bStaubliArmPowerOffCMDOnce;
} gtLinearRobotOutpt;
#endif

struct MC_BR_JogTargetPosition
{	unsigned long Axis;
	float Velocity;
	float Acceleration;
	float Deceleration;
	float TargetPosition;
	unsigned short ErrorID;
	unsigned long C_Axis;
	MC_0120_IS_TYP IS;
	plcbit Enable;
	plcbit JogToTarget;
	plcbit JogPositive;
	plcbit JogNegative;
	plcbit Active;
	plcbit Busy;
	plcbit CommandAborted;
	plcbit Error;
	plcbit Jogging;
	plcbit MovingToTarget;
	plcbit TargetReached;
};
_BUR_PUBLIC void MC_BR_JogTargetPosition(struct MC_BR_JogTargetPosition* inst);
struct MC_BR_SetHardwareInputs
{	unsigned long Axis;
	unsigned short ErrorID;
	unsigned long C_Axis;
	MC_0108_IS_TYP IS;
	plcbit Enable;
	plcbit HomeSwitch;
	plcbit PosHWSwitch;
	plcbit NegHWSwitch;
	plcbit Trigger1;
	plcbit Trigger2;
	plcbit Active;
	plcbit Busy;
	plcbit Error;
	plcbit InputsSet;
};
_BUR_PUBLIC void MC_BR_SetHardwareInputs(struct MC_BR_SetHardwareInputs* inst);
struct MpAxisBasic
{	struct MpComIdentType(* MpLink);
	struct MpAxisBasicParType(* Parameters);
	unsigned long Axis;
	signed long StatusID;
	double Position;
	float Velocity;
	MpAxisBasicInfoType Info;
	MpComInternalDataType Internal;
	plcbit Enable;
	plcbit ErrorReset;
	plcbit Update;
	plcbit Power;
	plcbit Home;
	plcbit MoveVelocity;
	plcbit MoveAbsolute;
	plcbit MoveAdditive;
	plcbit Stop;
	plcbit JogPositive;
	plcbit JogNegative;
	plcbit Autotune;
	plcbit Simulate;
	plcbit TorqueLimit;
	plcbit ReleaseBrake;
	plcbit Active;
	plcbit Error;
	plcbit UpdateDone;
	plcbit CommandBusy;
	plcbit CommandAborted;
	plcbit PowerOn;
	plcbit IsHomed;
	plcbit InVelocity;
	plcbit InPosition;
	plcbit MoveActive;
	plcbit Stopped;
	plcbit TuningDone;
	plcbit Simulation;
	plcbit TorqueLimited;
	plcbit BrakeReleased;
};
_BUR_PUBLIC void MpAxisBasic(struct MpAxisBasic* inst);
_LOCAL struct MpAxisBasic AxisX1;
_LOCAL MpAxisBasicParType AxisX1Parameters;
_LOCAL struct MC_BR_SetHardwareInputs AxisX1HardwareInputs;
_LOCAL struct MC_BR_JogTargetPosition AxisX1Jog;
_LOCAL plcbit AxisX1Acknowlege;
_LOCAL struct MpAxisBasic AxisZ1;
_LOCAL MpAxisBasicParType AxisZ1Parameters;
_LOCAL struct MC_BR_SetHardwareInputs AxisZ1HardwareInputs;
_LOCAL plcbit AxisZ1Acknowlege;
_LOCAL struct MpAxisBasic AxisZ2;
_LOCAL MpAxisBasicParType AxisZ2Parameters;
_LOCAL struct MC_BR_SetHardwareInputs AxisZ2HardwareInputs;
_LOCAL plcbit AxisZ2Acknowlege;
_LOCAL struct MpAxisBasic AxisY1;
_LOCAL MpAxisBasicParType AxisY1Parameters;
_LOCAL struct MC_BR_SetHardwareInputs AxisY1HardwareInputs;
_LOCAL struct MC_BR_JogTargetPosition AxisY1Jog;
_LOCAL plcbit AxisY1Acknowlege;
_LOCAL struct MpAxisBasic AxisY2;
_LOCAL MpAxisBasicParType AxisY2Parameters;
_LOCAL struct MC_BR_SetHardwareInputs AxisY2HardwareInputs;
_LOCAL plcbit AxisY2Acknowlege;
_LOCAL plcbit AxisRAcknowlege;
_LOCAL unsigned char RSOutput00;
_LOCAL unsigned char RSOutput01;
_LOCAL unsigned char RSOutput02;
_LOCAL unsigned char RSOutput03;
_LOCAL unsigned char RSOutput04;
_LOCAL unsigned char RSOutput05;
_LOCAL unsigned char RSOutput06;
_LOCAL unsigned char RSOutput07;
_LOCAL unsigned char RSOutput08;
_LOCAL unsigned char RSOutput09;
_LOCAL unsigned char RSOutput10;
_LOCAL unsigned char RSOutput11;
_LOCAL unsigned char RSOutput12;
_LOCAL unsigned char RSOutput13;
_LOCAL unsigned char RSOutput14;
_LOCAL unsigned char RSOutput15;
_LOCAL LRSuperState_enum uiCurrentSuperState;
_LOCAL SRState_enum uiSRState;
_LOCAL AxisX1Jog_enum uiAxisX1JogState;
_LOCAL LRAxisJog_enum uiAxisJogState;
_LOCAL LRAxisHome_enum uiAxisHomeState;
_LOCAL LRAxisRRotate_enum uiAxisRRotateState;
_LOCAL SRAutoCycleState_enum uiSRAutoCycleState;
_LOCAL SRMoveState_enum uiSRMoveState;
_LOCAL FOUPPort_typ tFOUPPort1;
_LOCAL FOUPPort_typ tFOUPPort2;
_LOCAL FOUPPort_typ tFOUPPort3;
_LOCAL FOUPPort_typ tFOUPPort4;
_LOCAL GuardianCarrier_typ tGuardianCarrier1;
_LOCAL GuardianCarrier_typ tGuardianCarrier2;
_LOCAL LinearRobot_typ tLR;
_LOCAL plcbit bStaubliRobotReady;
_LOCAL plcbit bStaubliRobotSafePos;
_LOCAL plcbit bStaubliRobotRunCMD;
_LOCAL plcbit bAxisRArrivedFlag;
_LOCAL plcbit bAxisRStartFlag;
_LOCAL plcbit bAxisY1ArrivedFlag;
_LOCAL plcbit bAxisY2ArrivedFlag;
_LOCAL unsigned char siJogModeOff;
_GLOBAL MpComIdentType gX1MpAxis;
_GLOBAL MpComIdentType gY1MpAxis;
_GLOBAL MpComIdentType gY2MpAxis;
_GLOBAL MpComIdentType gZ1MpAxis;
_GLOBAL MpComIdentType gZ2MpAxis;
_GLOBAL ACP10AXIS_typ Y1;
_GLOBAL ACP10AXIS_typ Y2;
_GLOBAL ACP10AXIS_typ X1;
_GLOBAL ACP10AXIS_typ Z1;
_GLOBAL ACP10AXIS_typ Z2;
_GLOBAL gtLinearRobotInput gLRInput;
_GLOBAL gtLinearRobotOutpt gLROutput;
_GLOBAL_RETAIN double rFP1AxisX1TargetPos;
_GLOBAL_RETAIN double rFP2AxisX1TargetPos;
_GLOBAL_RETAIN double rFP3AxisX1TargetPos;
_GLOBAL_RETAIN double rFP4AxisX1TargetPos;
_GLOBAL_RETAIN double rGC1AxisX1TargetPos;
_GLOBAL_RETAIN double rFP1AxisY1TargetPos;
_GLOBAL_RETAIN double rFP2AxisY1TargetPos;
_GLOBAL_RETAIN double rFP3AxisY1TargetPos;
_GLOBAL_RETAIN double rFP4AxisY1TargetPos;
_GLOBAL_RETAIN double rGC1AxisY1TargetPos;
_GLOBAL_RETAIN double rFP1AxisY2TargetPos;
_GLOBAL_RETAIN double rFP2AxisY2TargetPos;
_GLOBAL_RETAIN double rFP3AxisY2TargetPos;
_GLOBAL_RETAIN double rFP4AxisY2TargetPos;
_GLOBAL_RETAIN double rGC1AxisY2TargetPos;
_GLOBAL_RETAIN double rFP1AxisZ1UpTargetPos;
_GLOBAL_RETAIN double rFP2AxisZ1UpTargetPos;
_GLOBAL_RETAIN double rFP3AxisZ1UpTargetPos;
_GLOBAL_RETAIN double rFP4AxisZ1UpTargetPos;
_GLOBAL_RETAIN double rGC1AxisZ1UpTargetPos;
_GLOBAL_RETAIN double rFP1AxisZ1LowTargetPos;
_GLOBAL_RETAIN double rFP2AxisZ1LowTargetPos;
_GLOBAL_RETAIN double rFP3AxisZ1LowTargetPos;
_GLOBAL_RETAIN double rFP4AxisZ1LowTargetPos;
_GLOBAL_RETAIN double rGC1AxisZ1LowTargetPos;
_GLOBAL_RETAIN double rGC1AxisZ2UpCarrierPos;
_GLOBAL_RETAIN double rGC1AxisZ2LowCarrierPos;
_GLOBAL unsigned char usiRedIndColor;
_GLOBAL unsigned short uiAutoCycleCounts;
_GLOBAL unsigned char usiLoadStartPosition;
_GLOBAL unsigned char usiUnloadStartPosition;
_GLOBAL unsigned char usiLoadTargetPosition;
_GLOBAL unsigned char usiUnloadTargetPosition;
_GLOBAL plcwstring SW_Revision[11];
_GLOBAL plcbit bCycleTimeStart;
_GLOBAL plctime dtCycleTime;
_GLOBAL plctime dtPrevCycleTime;
static void __AS__Action__RotaryPause(void);
