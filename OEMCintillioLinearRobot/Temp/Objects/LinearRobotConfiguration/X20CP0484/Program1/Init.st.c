#include <bur\plctypes.h>
#include "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Temp/Objects/LinearRobotConfiguration/X20CP0484/Program1/Initst.h"
#line 1 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Init.st"
void _INIT __BUR__ENTRY_INIT_FUNCT__(void){__AS__ImplInitInit();{


{int zzIndex; plcwstring* zzLValue=(plcwstring*)SW_Revision; plcwstring* zzRValue=(plcwstring*)((unsigned short[]){118,50,48,49,57,48,57,49,54,97,0}); for(zzIndex=0; zzIndex<10l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};



((*(unsigned long*)&(AxisX1.MpLink))=((unsigned long)(&gX1MpAxis)));
(AxisX1.Axis=((unsigned long)(&X1)));
((*(unsigned long*)&(AxisX1.Parameters))=((unsigned long)(&AxisX1Parameters)));
(AxisX1.Enable=0);
(AxisX1Parameters.Position=0.00000000000000000000E+00);
(AxisX1Parameters.Velocity=2.30000000000000000000E+03);
(AxisX1Parameters.Acceleration=2.30000000000000000000E+03);
(AxisX1Parameters.Deceleration=2.30000000000000000000E+03);
(AxisX1Parameters.Home.Mode=8);
(AxisX1Parameters.Home.StartVelocity=1.15000000000000000000E+03);
(AxisX1Parameters.Home.HomingVelocity=1.15000000000000000000E+02);
(AxisX1Parameters.Home.Acceleration=6.90000000000000000000E+03);
(AxisX1HardwareInputs.Axis=((unsigned long)(&X1)));
(AxisX1Jog.Axis=((unsigned long)(&X1)));



((*(unsigned long*)&(AxisY1.MpLink))=((unsigned long)(&gY1MpAxis)));
(AxisY1.Axis=((unsigned long)(&Y1)));
((*(unsigned long*)&(AxisY1.Parameters))=((unsigned long)(&AxisY1Parameters)));
(AxisY1.Enable=0);
(AxisY1Parameters.Position=0.00000000000000000000E+00);
(AxisY1Parameters.Velocity=5.00000000000000000000E+02);
(AxisY1Parameters.Acceleration=2.30000000000000000000E+03);
(AxisY1Parameters.Deceleration=2.30000000000000000000E+03);
(AxisY1Parameters.Home.Mode=8);
(AxisY1Parameters.Home.StartVelocity=5.75000000000000000000E+02);
(AxisY1Parameters.Home.HomingVelocity=5.80000000000000000000E+01);
(AxisY1Parameters.Home.Acceleration=3.45000000000000000000E+03);
(AxisY1HardwareInputs.Axis=((unsigned long)(&Y1)));
(bAxisY1ArrivedFlag=0);



((*(unsigned long*)&(AxisY2.MpLink))=((unsigned long)(&gY2MpAxis)));
(AxisY2.Axis=((unsigned long)(&Y2)));
((*(unsigned long*)&(AxisY2.Parameters))=((unsigned long)(&AxisY2Parameters)));
(AxisY2.Enable=0);
(AxisY2Parameters.Position=0.00000000000000000000E+00);
(AxisY2Parameters.Velocity=5.00000000000000000000E+02);
(AxisY2Parameters.Acceleration=2.30000000000000000000E+03);
(AxisY2Parameters.Deceleration=2.30000000000000000000E+03);
(AxisY2Parameters.Home.Mode=8);
(AxisY2Parameters.Home.StartVelocity=5.75000000000000000000E+02);
(AxisY2Parameters.Home.HomingVelocity=5.80000000000000000000E+01);
(AxisY2Parameters.Home.Acceleration=3.45000000000000000000E+03);
(AxisY2HardwareInputs.Axis=((unsigned long)(&Y2)));
(bAxisY2ArrivedFlag=0);



((*(unsigned long*)&(AxisZ1.MpLink))=((unsigned long)(&gZ1MpAxis)));
(AxisZ1.Axis=((unsigned long)(&Z1)));
((*(unsigned long*)&(AxisZ1.Parameters))=((unsigned long)(&AxisZ1Parameters)));
(AxisZ1.Enable=0);
(AxisZ1Parameters.Position=0.00000000000000000000E+00);
(AxisZ1Parameters.Velocity=6.50000000000000000000E+01);
(AxisZ1Parameters.Acceleration=1.00000000000000000000E+03);
(AxisZ1Parameters.Deceleration=1.00000000000000000000E+03);
(AxisZ1Parameters.Home.Mode=8);
(AxisZ1Parameters.Home.StartVelocity=2.30000000000000000000E+02);
(AxisZ1Parameters.Home.HomingVelocity=2.30000000000000000000E+01);
(AxisZ1Parameters.Home.Acceleration=1.00000000000000000000E+03);
(AxisZ1HardwareInputs.Axis=((unsigned long)(&Z1)));



((*(unsigned long*)&(AxisZ2.MpLink))=((unsigned long)(&gZ2MpAxis)));
(AxisZ2.Axis=((unsigned long)(&Z2)));
((*(unsigned long*)&(AxisZ2.Parameters))=((unsigned long)(&AxisZ2Parameters)));
(AxisZ2.Enable=0);
(AxisZ2Parameters.Position=0.00000000000000000000E+00);
(AxisZ2Parameters.Velocity=4.60000000000000000000E+02);
(AxisZ2Parameters.Acceleration=2.30000000000000000000E+02);
(AxisZ2Parameters.Deceleration=2.30000000000000000000E+02);
(AxisZ2Parameters.Home.Mode=8);
(AxisZ2Parameters.Home.StartVelocity=2.30000000000000000000E+02);
(AxisZ2Parameters.Home.HomingVelocity=2.30000000000000000000E+01);
(AxisZ2Parameters.Home.Acceleration=1.38000000000000000000E+03);
(AxisZ2HardwareInputs.Axis=((unsigned long)(&Z2)));


__AS__Action__RotaryPause();
(bAxisRArrivedFlag=0);
(bAxisRStartFlag=0);




(tFOUPPort1.siX1Pos=rFP1AxisX1TargetPos);
(tFOUPPort1.siY1AccessPos=rFP1AxisY1TargetPos);
(tFOUPPort1.siY2AccessPos=rFP1AxisY2TargetPos);
(tFOUPPort1.siZ1FPPossession=rFP1AxisZ1LowTargetPos);
(tFOUPPort1.siZ1LRPossession=rFP1AxisZ1UpTargetPos);

(tFOUPPort2.siX1Pos=rFP2AxisX1TargetPos);
(tFOUPPort2.siY1AccessPos=rFP2AxisY1TargetPos);
(tFOUPPort2.siY2AccessPos=rFP2AxisY2TargetPos);
(tFOUPPort2.siZ1FPPossession=rFP2AxisZ1LowTargetPos);
(tFOUPPort2.siZ1LRPossession=rFP2AxisZ1UpTargetPos);

(tFOUPPort3.siX1Pos=rFP3AxisX1TargetPos);
(tFOUPPort3.siY1AccessPos=rFP3AxisY1TargetPos);
(tFOUPPort3.siY2AccessPos=rFP3AxisY2TargetPos);
(tFOUPPort3.siZ1FPPossession=rFP3AxisZ1LowTargetPos);
(tFOUPPort3.siZ1LRPossession=rFP3AxisZ1UpTargetPos);

(tFOUPPort4.siX1Pos=rFP4AxisX1TargetPos);
(tFOUPPort4.siY1AccessPos=rFP4AxisY1TargetPos);
(tFOUPPort4.siY2AccessPos=rFP4AxisY2TargetPos);
(tFOUPPort4.siZ1FPPossession=rFP4AxisZ1LowTargetPos);
(tFOUPPort4.siZ1LRPossession=rFP4AxisZ1UpTargetPos);




(tGuardianCarrier1.siX1Pos=rGC1AxisX1TargetPos);
(tGuardianCarrier1.siY1AccessPos=rGC1AxisY1TargetPos);
(tGuardianCarrier1.siY2AccessPos=rGC1AxisY2TargetPos);
(tGuardianCarrier1.siZ1GCPossession=rGC1AxisZ1LowTargetPos);
(tGuardianCarrier1.siZ1LRPossession=rGC1AxisZ1UpTargetPos);
(tGuardianCarrier1.siZ2TopPos=rGC1AxisZ2UpCarrierPos);
(tGuardianCarrier1.siZ2BottomPos=rGC1AxisZ2LowCarrierPos);
(tGuardianCarrier1.siZ2300Pos=rGC1AxisZ2LowCarrierPos);

(tGuardianCarrier2.siX1Pos=-17135);
(tGuardianCarrier2.siY1AccessPos=-1495);
(tGuardianCarrier2.siY2AccessPos=-1495);
(tGuardianCarrier2.siZ1GCPossession=-46);
(tGuardianCarrier2.siZ1LRPossession=92);
(tGuardianCarrier2.siZ2TopPos=4.60000000000000000000E+01);
(tGuardianCarrier2.siZ2BottomPos=5.10000000000000000000E+01);
(tGuardianCarrier2.siZ2300Pos=5.10000000000000000000E+01);



(tLR.siX1SafePos=0.00000000000000000000E+00);
(tLR.siY1SafePos=-4.14000000000000000000E+02);
(tLR.siY2SafePos=3.45000000000000000000E+02);
(tLR.siZ1SafePos=0.00000000000000000000E+00);
(tLR.siZ2SafePos=0.00000000000000000000E+00);
(tLR.siRSafePos=-180);

(tLR.siFastAccelX1=4.60000000000000000000E+03);
(tLR.siSlowAccelX1=2.30000000000000000000E+03);
(tLR.siFastVelX1=7.50000000000000000000E+03);
(tLR.siSlowVelX1=2.00000000000000000000E+03);
(tLR.siFastAccelY1=2.30000000000000000000E+03);
(tLR.siSlowAccelY1=2.30000000000000000000E+03);
(tLR.siFastVelY1=3.00000000000000000000E+03);
(tLR.siSlowVelY1=5.00000000000000000000E+02);
(tLR.siFastAccelY2=2.30000000000000000000E+03);
(tLR.siSlowAccelY2=2.30000000000000000000E+03);
(tLR.siFastVelY2=3.00000000000000000000E+03);
(tLR.siSlowVelY2=5.00000000000000000000E+02);
(tLR.siFastAccelZ1=1000);
(tLR.siSlowAccelZ1=1000);
(tLR.siFastVelZ1=2.00000000000000000000E+03);
(tLR.siSlowVelZ1=6.50000000000000000000E+01);
(tLR.siFastAccelZ2=920);
(tLR.siSlowAccelZ2=230);
(tLR.siFastVelZ2=2.00000000000000000000E+03);
(tLR.siSlowVelZ2=4.60000000000000000000E+02);
(tLR.siFastAccelR=30);
(tLR.siSlowAccelR=1);
(tLR.siFastSpeedR=40);
(tLR.siSlowSpeedR=20);

(tLR.usiSWStateIndTxt=0);
(tLR.usiSWStateIndColor=usiRedIndColor);



(tLR.rCoarseJogVelX1=2.30000000000000000000E+02);
(tLR.rCoarseJogAccelX1=1.72500000000000000000E+03);
(tLR.rCoarseJogDelX1=5.75000000000000000000E+03);
(tLR.rFineJogIncX1=1.00000000000000000000E+01);
(tLR.rX1JogUpperTravelLimit=0);
(tLR.rX1JogLowerTravelLimit=-17227);
(tLR.rYJogUpperTravelLimit=216);
(tLR.rYJogLowerTravelLimit=-2040);
(tLR.rY2JogUpperTravelLimit=355);
(tLR.rY2JogLowerTravelLimit=-1914);
(tLR.rZ1JogUpperTravelLimit=288);
(tLR.rZ1JogLowerTravelLimit=-120);
(tLR.rZ2JogUpperTravelLimit=2415);
(tLR.rZ2JogLowerTravelLimit=-33);
(uiAxisJogState=0);
(uiAxisX1JogState=10);
(uiAxisHomeState=10);
(uiAxisRRotateState=10);
(gLRInput.siSelectJogAxis=0);
(gLRInput.siSetAxisX1JogType=siJogModeOff);
(gLRInput.siSelectAxisTargetLevel=0);
(AxisX1Jog.Enable=0);
(AxisY1Jog.Enable=0);
(gLRInput.bJogNegAxisX1PB=0);
(gLRInput.bJogPosAxisX1PB=0);
(gLRInput.bHomeAllAxisPB=0);
(tLR.bHomeAllAxis=0);
(gLRInput.bSetTargetPosPB=0);



(tFOUPPort1.rNewAxisX1TargetPos=rFP1AxisX1TargetPos);
(tFOUPPort2.rNewAxisX1TargetPos=rFP2AxisX1TargetPos);
(tFOUPPort3.rNewAxisX1TargetPos=rFP3AxisX1TargetPos);
(tFOUPPort4.rNewAxisX1TargetPos=rFP4AxisX1TargetPos);
(tGuardianCarrier1.rNewAxisX1TargetPos=rGC1AxisX1TargetPos);

(tFOUPPort1.rPrevAxisX1TargetPos=rFP1AxisX1TargetPos);
(tFOUPPort2.rPrevAxisX1TargetPos=rFP2AxisX1TargetPos);
(tFOUPPort3.rPrevAxisX1TargetPos=rFP3AxisX1TargetPos);
(tFOUPPort4.rPrevAxisX1TargetPos=rFP4AxisX1TargetPos);
(tGuardianCarrier1.rPrevAxisX1TargetPos=rGC1AxisX1TargetPos);


(tFOUPPort1.rNewAxisY1TargetPos=rFP1AxisY1TargetPos);
(tFOUPPort2.rNewAxisY1TargetPos=rFP2AxisY1TargetPos);
(tFOUPPort3.rNewAxisY1TargetPos=rFP3AxisY1TargetPos);
(tFOUPPort4.rNewAxisY1TargetPos=rFP4AxisY1TargetPos);
(tGuardianCarrier1.rNewAxisY1TargetPos=rGC1AxisY1TargetPos);

(tFOUPPort1.rPrevAxisY1TargetPos=rFP1AxisY1TargetPos);
(tFOUPPort2.rPrevAxisY1TargetPos=rFP2AxisY1TargetPos);
(tFOUPPort3.rPrevAxisY1TargetPos=rFP3AxisY1TargetPos);
(tFOUPPort4.rPrevAxisY1TargetPos=rFP4AxisY1TargetPos);
(tGuardianCarrier1.rPrevAxisY1TargetPos=rGC1AxisY1TargetPos);


(tFOUPPort1.rNewAxisY2TargetPos=rFP1AxisY2TargetPos);
(tFOUPPort2.rNewAxisY2TargetPos=rFP2AxisY2TargetPos);
(tFOUPPort3.rNewAxisY2TargetPos=rFP3AxisY2TargetPos);
(tFOUPPort4.rNewAxisY2TargetPos=rFP4AxisY2TargetPos);
(tGuardianCarrier1.rNewAxisY2TargetPos=rGC1AxisY2TargetPos);

(tFOUPPort1.rPrevAxisY2TargetPos=rFP1AxisY2TargetPos);
(tFOUPPort2.rPrevAxisY2TargetPos=rFP2AxisY2TargetPos);
(tFOUPPort3.rPrevAxisY2TargetPos=rFP3AxisY2TargetPos);
(tFOUPPort4.rPrevAxisY2TargetPos=rFP4AxisY2TargetPos);
(tGuardianCarrier1.rPrevAxisY2TargetPos=rGC1AxisY2TargetPos);



(tFOUPPort1.rNewAxisZ1UpTargetPos=rFP1AxisZ1UpTargetPos);
(tFOUPPort2.rNewAxisZ1UpTargetPos=rFP2AxisZ1UpTargetPos);
(tFOUPPort3.rNewAxisZ1UpTargetPos=rFP3AxisZ1UpTargetPos);
(tFOUPPort4.rNewAxisZ1UpTargetPos=rFP4AxisZ1UpTargetPos);
(tGuardianCarrier1.rNewAxisZ1UpTargetPos=rGC1AxisZ1UpTargetPos);

(tFOUPPort1.rPrevAxisZ1UpTargetPos=rFP1AxisZ1UpTargetPos);
(tFOUPPort2.rPrevAxisZ1UpTargetPos=rFP2AxisZ1UpTargetPos);
(tFOUPPort3.rPrevAxisZ1UpTargetPos=rFP3AxisZ1UpTargetPos);
(tFOUPPort4.rPrevAxisZ1UpTargetPos=rFP4AxisZ1UpTargetPos);
(tGuardianCarrier1.rPrevAxisZ1UpTargetPos=rGC1AxisZ1UpTargetPos);

(tFOUPPort1.rNewAxisZ1LowTargetPos=rFP1AxisZ1LowTargetPos);
(tFOUPPort2.rNewAxisZ1LowTargetPos=rFP2AxisZ1LowTargetPos);
(tFOUPPort3.rNewAxisZ1LowTargetPos=rFP3AxisZ1LowTargetPos);
(tFOUPPort4.rNewAxisZ1LowTargetPos=rFP4AxisZ1LowTargetPos);
(tGuardianCarrier1.rNewAxisZ1LowTargetPos=rGC1AxisZ1LowTargetPos);

(tFOUPPort1.rPrevAxisZ1LowTargetPos=rFP1AxisZ1LowTargetPos);
(tFOUPPort2.rPrevAxisZ1LowTargetPos=rFP2AxisZ1LowTargetPos);
(tFOUPPort3.rPrevAxisZ1LowTargetPos=rFP3AxisZ1LowTargetPos);
(tFOUPPort4.rPrevAxisZ1LowTargetPos=rFP4AxisZ1LowTargetPos);
(tGuardianCarrier1.rPrevAxisZ1LowTargetPos=rGC1AxisZ1LowTargetPos);


(tGuardianCarrier1.rPrevAxisZ2UpCarrierPos=rGC1AxisZ2UpCarrierPos);
(tGuardianCarrier1.rPrevAxisZ2LowCarrierPos=rGC1AxisZ2LowCarrierPos);


(uiCurrentSuperState=100);


(bStaubliRobotReady=0);
(bStaubliRobotSafePos=0);
(bStaubliRobotRunCMD=0);
(uiSRState=10);
(uiSRMoveState=0);
(usiLoadStartPosition=7);
(usiLoadTargetPosition=4);
(usiUnloadStartPosition=5);
(usiUnloadTargetPosition=7);
(gLRInput.siFOUPPortNumberB1=2);
(gLRInput.siFOUPPortNumberB2=3);
(gLRInput.bLoad200Batch1=0);
(gLRInput.bUnload200Batch1=0);
(gLRInput.bLoad200Batch2TB=0);
(gLRInput.bLoad200Batch2BT=0);
(gLRInput.bUnload200Batch2TB=0);
(gLRInput.bUnload200Batch2BT=0);
(gLRInput.bLoad300Batch1=0);
(gLRInput.bUnload300Batch1=0);
(gLRInput.bRUN=0);
(gLRInput.bWaferSize300mm=0);
(gLRInput.bWaferSize200mm=0);
(gLRInput.bLoadGC=0);
(gLRInput.bUnloadGC=0);
(gLRInput.bBatchCount1=0);
(gLRInput.bBatchCount2=0);
(gLRInput.bGC1=0);
(gLRInput.bGC2=0);
(gLRInput.bBatch1GCBottomLevel=0);
(gLRInput.bBatch1GCTopLevel=0);
(gLRInput.bBatch2GCBottomLevel=0);
(gLRInput.bBatch2GCTopLevel=0);
(gLRInput.obStaubliSafePosRE=0);
(gLROutput.bComplete=0);
(gLROutput.usiSRMoveStartPosition=0);
(gLROutput.usiSRMoveTargetPosition=0);
(gLRInput.bUnloadWaferType=0);
(gLRInput.bLoadWaferType=0);
(gLROutput.bWriteStaubliRobotOnce=0);
(gLROutput.bWriteStaubliTargetOnce=0);
(gLROutput.bStaubliRunCMD=0);
(gLROutput.bStaubliRunCMDOnce=0);


(uiSRAutoCycleState=0);
(gLRInput.bAutoCycleMode=0);
(gLRInput.bStartAutoCyclePB=0);
(uiAutoCycleCounts=0);
(bCycleTimeStart=0);
(dtCycleTime=0);
(dtPrevCycleTime=0);


(AxisX1Acknowlege=0);
(AxisY1Acknowlege=0);
(AxisY2Acknowlege=0);
(AxisZ1Acknowlege=0);
(AxisZ2Acknowlege=0);
(AxisRAcknowlege=0);
}}
#line 343 "__AS__ImplInitInit.c"

void __AS__ImplInitInit(void){__AS__ImplInitDummyInit();{}}
void __AS__ImplInitDummyInit(void){}
#line 343 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Init.st"
#line 59 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/RotaryActions/Rotary.st"
static void __AS__Action__RotaryPause(void){
{(RSOutput00=0);
(RSOutput01=0);
(RSOutput02=0);
(RSOutput03=0);
(RSOutput04=4);
(RSOutput05=0);
(RSOutput06=0);
(RSOutput07=0);
(RSOutput08=20);
(RSOutput09=0);
(RSOutput10=30);
(RSOutput11=0);
(RSOutput12=255);
(RSOutput13=0);
(RSOutput14=52);
(RSOutput15=0);
}}
#line 343 "C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Init.st"
#line 343 "__AS__IMPLICIT_INLINE.c"

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/NcGlobal/NcGlobal.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpComError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/Acp10man/Acp10man.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/Acp10_MC/acp10_mc.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxisError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxisAlarm.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/NcGlobal/NcGlobal.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/Acp10_MC/acp10_mc.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/NcGlobal/NcGlobal.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/Acp10par/Acp10par.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/Acp10_MC/acp10_mc.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/LinearRobotConfiguration/GlobalComponents/MpComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/Axis.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/Rotary.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/States.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/FOUP_GC.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Program/AxisJog.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/Projects/Linear Robot/OEMCintillioLinearRobot/Temp/Objects/LinearRobotConfiguration/X20CP0484/Program1/Init.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/Projects/Linear Robot/OEMCintillioLinearRobot/Temp/Objects/LinearRobotConfiguration/X20CP0484/Program1/Init.st.c\\\" \\\"C:/Projects/Linear Robot/OEMCintillioLinearRobot/Logical/Program/Init.st\\\"\\n\"");
__asm__(".previous");

__asm__(".section \".plciec\"");
__asm__(".ascii \"plcdata_const 'siJogModeOff'\\n\"");
__asm__(".previous");
