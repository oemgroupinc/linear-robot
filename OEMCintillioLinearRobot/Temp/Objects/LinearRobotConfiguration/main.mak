SHELL := cmd.exe
CYGWIN=nontsec
export PATH := C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\iCLS\;C:\Program Files\Intel\Intel(R) Management Engine Components\iCLS\;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Program Files (x86)\NVIDIA Corporation\PhysX\Common;C:\Program Files\Intel\WiFi\bin\;C:\Program Files\Common Files\Intel\WirelessCommon\;C:\Users\robert.ruth\AppData\Local\Microsoft\WindowsApps;C:\Users\robert.ruth\AppData\Local\Programs\Microsoft VS Code\bin;C:\Program Files (x86)\Common Files\Hilscher GmbH\TLRDecode;C:\Users\robert.ruth\AppData\Local\Microsoft\WindowsApps;C:\Users\robert.ruth\AppData\Local\Programs\Microsoft VS Code\bin;C:\Program Files (x86)\Common Files\Hilscher GmbH\TLRDecode
export AS_BUILD_MODE := Rebuild
export AS_VERSION := 4.4.7.144 SP
export AS_COMPANY_NAME :=  
export AS_USER_NAME := robert.ruth
export AS_PATH := C:/BrAutomation/AS44
export AS_BIN_PATH := C:/BrAutomation/AS44/bin-en
export AS_PROJECT_PATH := C:/Projects/Linear\ Robot/OEMCintillioLinearRobot
export AS_PROJECT_NAME := OEMCintillioLinearRobot
export AS_SYSTEM_PATH := C:/BrAutomation/AS/System
export AS_VC_PATH := C:/BrAutomation/AS44/AS/VC
export AS_TEMP_PATH := C:/Projects/Linear\ Robot/OEMCintillioLinearRobot/Temp
export AS_CONFIGURATION := LinearRobotConfiguration
export AS_BINARIES_PATH := C:/Projects/Linear\ Robot/OEMCintillioLinearRobot/Binaries
export AS_GNU_INST_PATH := C:/BrAutomation/AS44/AS/GnuInst/V4.1.2
export AS_GNU_BIN_PATH := $(AS_GNU_INST_PATH)/bin
export AS_GNU_INST_PATH_SUB_MAKE := C:/BrAutomation/AS44/AS/GnuInst/V4.1.2
export AS_GNU_BIN_PATH_SUB_MAKE := $(AS_GNU_INST_PATH_SUB_MAKE)/bin
export AS_INSTALL_PATH := C:/BrAutomation/AS44
export WIN32_AS_PATH := "C:\BrAutomation\AS44"
export WIN32_AS_BIN_PATH := "C:\BrAutomation\AS44\bin-en"
export WIN32_AS_PROJECT_PATH := "C:\Projects\Linear Robot\OEMCintillioLinearRobot"
export WIN32_AS_SYSTEM_PATH := "C:\BrAutomation\AS\System"
export WIN32_AS_VC_PATH := "C:\BrAutomation\AS44\AS\VC"
export WIN32_AS_TEMP_PATH := "C:\Projects\Linear Robot\OEMCintillioLinearRobot\Temp"
export WIN32_AS_BINARIES_PATH := "C:\Projects\Linear Robot\OEMCintillioLinearRobot\Binaries"
export WIN32_AS_GNU_INST_PATH := "C:\BrAutomation\AS44\AS\GnuInst\V4.1.2"
export WIN32_AS_GNU_BIN_PATH := "$(WIN32_AS_GNU_INST_PATH)\\bin" 
export WIN32_AS_INSTALL_PATH := "C:\BrAutomation\AS44"

.suffixes:

ProjectMakeFile:

	@'$(AS_BIN_PATH)/BR.AS.AnalyseProject.exe' '$(AS_PROJECT_PATH)/OEMCintillioLinearRobot.apj' -t '$(AS_TEMP_PATH)' -c '$(AS_CONFIGURATION)' -o '$(AS_BINARIES_PATH)'   -sfas -buildMode 'Rebuild'   

